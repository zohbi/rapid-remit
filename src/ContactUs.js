import React, { Component } from "react";
import "./css/bootstrap.css";
import "./css/style-footer.css";
import Flip from "react-reveal/Flip";
import Footer from "./components/footer";
import "./css/style-contactUs+specialOffer+blogs.css";
import ContactUsMap from "./components/ContactUsMap";
import ContactUsform from "./components/contactUsform";

export default class ContactUs extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    this.props.getSettings()
    if (
      window.location.pathname == "/" ||
      window.location.pathname == "/AboutUs"
    ) {
      //  console.log("ASsads")
      document.getElementById("routerNav").style.display = "none";
      //  document.getElementById('navigationIndex').style.display="block"
    } else {
      //  console.log("nhi hy")
      document.getElementById("routerNav").style.display = "block";
      //  document.getElementById('navigationIndex').style.display="none"
    }
  }
  // dataCome =(data) => {
  //     console.log(data)
  // }
  render() {
    return (
      <div className="coooom">
        <Flip left opposite cascade duration={3000} mirror>
          <ContactUsMap data={this.props.setting} />
        </Flip>
        <ContactUsform
          isLoading={this.props.isLoading ? this.props.isLoading : false}
          postData={this.props.postData}
          data={this.props.setting}
        />

        <Footer history={this.props.history} />
      </div>
    );
  }
}
