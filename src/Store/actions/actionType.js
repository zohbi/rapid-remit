export default class ActionType {
  static Signup = "Signup";
  static Signup_SUCCESS = "Signup_SUCCESS";
  static Signup_FAIL = "Signup_FAIL";
  static SELECT_COMPARISION = "SELECT_COMPARISION";
  static Login = "Login";
  static Login_SUCCESS = "Login_SUCCESS";
  static Login_FAIL = "Login_FAIL";
  static Forgot = "Forgot";
  static Forgot_SUCCESS = "Forgot_SUCCESS";
  static Forgot_FAIL = "Forgot_FAIL";
  static Recover = "Recover";
  static Recover_SUCCESS = "Recover_SUCCESS";
  static Recover_FAIL = "Recover_FAIL";

  static Convert = "Convert";
  static Convert_SUCCESS = "Convert_SUCCESS";
  static Convert_FAIL = "Convert_FAIL";

  static Liverates = "Liverates";
  static Liverates_SUCCESS = "Liverates_SUCCESS";
  static Liverates_FAIL = "Liverates_FAIL";

  static Convert = "Convert";
  static Convert_SUCCESS = "Convert_SUCCESS";
  static Convert_FAIL = "Convert_FAIL";

  static News = "News";
  static News_SUCCESS = "News_SUCCESS";
  static News_FAIL = "News_FAIL";

  static ContactUs = "ContactUs";
  static ContactUs_SUCCESS = "ContactUs_SUCCESS";
  static ContactUs_FAIL = "ContactUs_FAIL";

  static Article = "Article";
  static Article_SUCCESS = "Article_SUCCESS";
  static Article_FAIL = "Article_FAIL";

  static Blogs = "Blogs";
  static Blogs_SUCCESS = "Blogs_SUCCESS";
  static Blogs_FAIL = "Blogs_FAIL";

  static Logout = "Logout";
  static Logout_SUCCESS = "Logout_SUCCESS";
  static Logout_FAIL = "Logout_FAIL";

  static Promotions = "Promotions";
  static Promotions_SUCCESS = "Promotions_SUCCESS";
  static Promotions_FAIL = "Promotions_FAIL";

  static Offers = "Offers";
  static Offers_SUCCESS = "Offers_SUCCESS";
  static Offers_FAIL = "Offers_FAIL";

  static Comparisons = "Comparisons";
  static ComparisonsFormData = "ComparisonsFormData";
  static Comparisons_SUCCESS = "Comparisons_SUCCESS";
  static Comparisons_FAIL = "Comparisons_FAIL";

  static ADD_TABLE = "ADD_TABLE";
  static DEL_TABLE = "DEL_TABLE";
  static PARTNER = "PARTNER";
  static PARTNER_LOADING = "PARTNER_LOADING";
  static RATE = "RATE";
  static CURRENCY = "CURRENCY";

  static FAVOURITE_LOADING = "FAVOURITE_LOADING";
  static FAVOURITE_SUCCESS = "FAVOURITE_SUCCESS";
  static FAVOURITE_FAIL = "FAVOURITE_FAIL";

  static HISTORY_LOADING = "HISTORY_LOADING";
  static HISTORY = "HISTORY";
  static FEEDBACK = "FEEDBACK";

  static PROMOTION = "PROMOTION";
  static PROMOTION_LOADING = "PROMOTION_LOADING";
  static RATE = "RATE";

  static GET_CONTENT = "GET_CONTENT";
  static GET_REVIEWS = "GET_REVIEWS";

  static PARTNER_REVIEWS = "PARTNER_REVIEWS";
  static USER_REVIEWS = "USER_REVIEWS";

  static EMPTY = "EMPTY";
  static LIVE = "LIVE";
  static TESTIMONIALS = "TESTIMONIALS";
  static UPDATE_USER = "UPDATE_USER";

  static COUNT = "COUNT";
  static FOOTER = "FOOTER";
  static BLOG = "BLOG";
  static DEL_TABLE_INDEX ="DEL_TABLE_INDEX"


  static SelectedNav ="SelectedNav"

  static GET_FAQS="GET_FAQS"
  static IS_FAQS_LOADING ="IS_FAQS_LOADING"
  
  static GET_SETTINGS ="GET_SETTINGS"
  static IS_SETTINGS_LOADING="IS_SETTINGS_LOADING"  
  
  static GET_HOMEBANNER="GET_HOMEBANNER"
  static IS_HOMEBANNER_LOADING ="IS_HOMEBANNER_LOADING"

  static GET_WHYCHOOSEUS ="GET_WHYCHOOSEUS"
  static IS_WHYCHOOSEUS_LOADING="IS_WHYCHOOSEUS_LOADING"

  static GET_SERVICES="GET_SERVICES"
  static IS_SERVICES_LOADING ="IS_SERVICES_LOADING"

  static GET_NETWORKHEADING ="GET_NETWORKHEADING"
  static IS_NETWORKHEADING_LOADING="IS_NETWORKHEADING_LOADING"


  
  static GET_COMPARISONHEADING="GET_COMPARISONHEADING"
  static IS_COMPARISONHEADING_LOADING ="IS_COMPARISONHEADING_LOADING"
  
  static GET_OURNETWORKHEADING ="GET_OURNETWORKHEADING"
  static IS_OURNETWORKHEADING_LOADING="IS_OURNETWORKHEADING_LOADING"  
  
  static GET_ABOUTUSHEADING="GET_ABOUTUSHEADING"
  static IS_ABOUTUSHEADING_LOADING ="IS_ABOUTUSHEADING_LOADING"

  static GET_WHATWEDOHEADING ="GET_WHATWEDOHEADING"
  static IS_WHATWEDOHEADING_LOADING="IS_WHATWEDOHEADING_LOADING"

  static GET_MISSIONVISIONHEADING="GET_MISSIONVISIONHEADING"
  static IS_MISSIONVISIONHEADING_LOADING ="IS_MISSIONVISIONHEADING_LOADING"

  static GET_PRIVACYHEADING ="GET_PRIVACYHEADING"
  static IS_PRIVACYHEADING_LOADING="IS_PRIVACYHEADING_LOADING"

  static GET_COOKIEHEADING="GET_COOKIEHEADING"
  static IS_COOKIEHEADING_LOADING ="IS_COOKIEHEADING_LOADING"

  static GET_TERMNCONDHEADING ="GET_TERMNCONDHEADING"
  static IS_TERMNCONDHEADING_LOADING="IS_TERMNCONDHEADING_LOADING"

}



