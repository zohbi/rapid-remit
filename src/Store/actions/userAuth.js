import ActionType from './actionType';
import { Post, Get, Delete } from '../../utils/api-call';
import { actionDispatch } from '../../utils/return-obj';
import swal from 'sweetalert';
import comparison_instance from '../.././instance/comparison_instance';
import chart_instance from '../.././instance/chart_instance';
import calc_instance from '../../instance/calc_instance';
import axios from 'axios';
import Axios from 'axios';
import { DEBUGER } from "../../Config"
import { createBrowserHistory } from "history";

const history = createBrowserHistory();


const base_url = "http://localhost:4000/";
// const base_url = 'https://rapidremit1.herokuapp.com/';
const io = require('socket.io-client');
// var socket = io.connect('https://rapidremit1.herokuapp.com/');
var socket = io.connect('http://localhost:4000/');


// import { ToastError } from "../../containers/serviceType";

export class Signup {
  static Signup(obj) {
    DEBUGER && console.log(obj, 'ppppp');
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.Signup));
      Post(`users/signup/`, obj)
        .then((success) => {
          // alert("Succes");
          DEBUGER && console.log('users/signup/', success);
          socket.emit('log', success.data.data);
          localStorage.setItem('token', JSON.stringify(success.data.token));
          //window.location.reload();
          dispatch(actionDispatch(ActionType.Signup_SUCCESS));
          if (window.location.pathname == '/registration') {
            DEBUGER && console.log('individuals', success.data.data);
            if (success.data.data.role == 'individuals') {
              window.location = 'DashboardLayout';
            } else {
              window.location = 'DashboardLayout';
            }
          }
        })
        .catch((error) => {
          dispatch(actionDispatch(ActionType.Signup_FAIL));
          localStorage.removeItem('UserData');
          DEBUGER && console.log(error.response);
          if (error?.response?.status === 400) {
            swal({
              title: error?.response?.data?.status,
              icon: 'warning',
              button: 'Ok',
            })
          }
        });
    };
  }
}

export class Login {
  static Login(obj) {
    DEBUGER && console.log(obj, 'ppppp');
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.Login));
      Post(`users/login`, obj)
        .then((success) => {
          socket.emit('log', success.data.data);
          //alert("call");
          // alert("Succes")
          DEBUGER && console.log('Login_SUCCESS', success);
          //dispatch(actionDispatch(ActionType.Login_SUCCESS, success.data.data));
          localStorage.setItem('token', JSON.stringify(success.data.token));
          localStorage.setItem('UserData', JSON.stringify(success.data.data));
          dispatch(
            actionDispatch(
              ActionType.Login_SUCCESS,
              //  console.log(success.data.message)
              success.data.data
            )
          );

          if (window.location.pathname == '/registration') {
            DEBUGER && console.log('individuals', success.data.data);
            if (success.data.data.role == 'individuals') {
              window.location = 'DashboardLayout';
            } else {
              window.location = 'DashboardLayout';
            }
          }
        })
        .catch((error) => {
          DEBUGER && console.log('errbhai', error.message);
          if (error.message == 'Network Error') {
            DEBUGER && console.log('err', error.message);
          } else if (error.response.status === 403) {
            swal({
              title: 'your account is blocked by admin',
              icon: 'warning',
              button: 'Ok',
            });
          }
          dispatch(actionDispatch(ActionType.Login_FAIL, error?.response?.data?.error));
          DEBUGER && console.log(error?.response?.data?.error, 'kia error hy');
        });
    };
  }
}

export class Forgot {
  static Forgot(obj) {
    DEBUGER && console.log(obj, 'Forgot');
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.Forgot));
      Post(`users/forgot`, obj)
        .then((success) => {
          DEBUGER && console.log('forgot', success);
          swal({
            title: success.data.message,
            text: "Please check your email!",
            icon: 'success',
            button: 'Ok',
          })
          dispatch(actionDispatch(ActionType.Forgot_SUCCESS));
        })
        .catch((error) => {
          dispatch(actionDispatch(ActionType.Forgot_FAIL));
          DEBUGER && console.log(error.response);
          if (error?.response?.status === 400) {
            swal({
              title: error?.response?.data?.status,
              icon: 'warning',
              button: 'Ok',
            })
          }
        });
    };
  }
}

export class Recover {
  static Recover(obj) {
    try {
      DEBUGER && console.log(obj, 'Recover');
      return (dispatch) => {
        dispatch(actionDispatch(ActionType.Recover));
        Post(`users/recover/${obj.token}`, obj)
          .then((success) => {
            DEBUGER &&  console.log('recover/${obj.token}', success);
            dispatch(actionDispatch(ActionType.Recover_SUCCESS, success.data.user));
          })
          .catch((error) => {
            dispatch(actionDispatch(ActionType.Recover_FAIL));
            DEBUGER && console.log(error.response);
            if (error?.response?.status === 400) {
              swal({
                title: error?.response?.data?.status,
                icon: 'warning',
                button: 'Ok',
              })
            } else {
              swal({
                title: error?.response?.data?.err,
                icon: 'error',
                // button: 'Ok',
              })
            }
          });
      };
    }
    catch (e) {
      DEBUGER && console.log(e)
    }
  }
}
export const empty = () => (dispatch) => {
  DEBUGER && console.log('start');
  try {
    dispatch({ type: ActionType.EMPTY, payload: '' });
  } catch (err) { }
};

export const SetIsNavActive = (data) => (dispatch) => {
  DEBUGER && console.log('SetIsNavActive', data);
  try {
    dispatch({ type: ActionType.SelectedNav, payload: data });
  } catch (err) { }
};

export class Convert {
  static Convert(obj) {
    DEBUGER && console.log(obj, 'ppppp');
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.Convert));

      Post(`convert/`, obj)
        .then((success) => {
          DEBUGER && console.log('convert', success);
          //callhere

          dispatch(
            actionDispatch(
              ActionType.Convert_SUCCESS,
              success.data,
              DEBUGER && console.log(success.data)
            )
          );

          dispatch(Liverate.Liverates(obj.from));
        })
        .catch((error) => {
          dispatch(actionDispatch(ActionType.Convert_FAIL, error));

          DEBUGER && console.log(error);
        });
    };
  }
}

export const addReviews = (obj) => async (dispatch) => {
  try {
    DEBUGER && console.log('obj', obj);
    Post('review', obj).then((resp) => {
      DEBUGER && console.log('resp', resp);
      swal({
        title: "added successfully",
        icon: "success",
        button: false,
      });
    });
  } catch (err) {
    DEBUGER && console.log('err', err.message);
  }
};

export const updateReviews = (id, obj) => async (dispatch) => {
  try {
    DEBUGER && console.log('obj', obj);
    let resp = await axios.put(`${base_url}review/${id}`, obj);
    DEBUGER && console.log('resp updated', resp.data);
    // window.location.reload();
  } catch (err) {
    DEBUGER && console.log('resp err', err.message);
  }
};

export const getReviews = () => async (dispatch) => {
  try {
    let user_id = JSON.parse(localStorage.getItem('UserData'))._id;
    Get(`review/user_id/${user_id}`).then((resp) => {
      DEBUGER && console.log('review', resp);
      dispatch({
        type: ActionType.GET_REVIEWS,
        payload: resp.data,
      });
    });
  } catch (err) {
    DEBUGER && console.log('err', err.message);
  }
};

export const partnerReviews = () => async (dispatch) => {
  try {
    Get(`review/partner`).then((resp) => {
      DEBUGER && console.log('review partner', resp);
      dispatch({
        type: ActionType.PARTNER_REVIEWS,
        payload: resp.data,
      });
    });
  } catch (err) {
    DEBUGER && console.log('err', err.message);
  }
};

export const UpdateUser = (id, obj) => async (dispatch) => {
  try {
    DEBUGER && console.log('call', id, obj);
    let token = localStorage.getItem('token');
    DEBUGER && console.log('token', token);
    let data = await Axios.put(`${base_url}users/${id}`, obj, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
    if (data.status == 200) {
      // swal('successfully updated');
      swal({
        title: 'Updated Successfully!',
        icon: 'success',
        button: 'Ok',
      }).then(() => history.push('/DashboardLayout'))

      DEBUGER && console.log('updare data', data);
      dispatch({
        type: ActionType.UPDATE_USER,
        payload: data.data,
      });
    }
  } catch (err) {
    DEBUGER && console.log('err', err.message);
  }
};
export const UpdateUserPassword = (obj) => async (dispatch) => {
  try {
    let token = localStorage.getItem('token');
    DEBUGER && console.log('token', token);
    let data = await Axios.post(`${base_url}users/changepassword`, obj, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
    if (data.status == 200) {
      // swal('successfully updated');
      swal({
        title: 'Change Password Successfully!',
        icon: 'success',
        button: 'Ok',
      }).then(() => history.push('/DashboardLayout'))

      DEBUGER && console.log('updare data', data);
    }
  } catch (err) {
    DEBUGER && console.log('err', err.message);
  }
};
export const userReviews = () => async (dispatch) => {
  try {
    Get(`review`).then((resp) => {
      DEBUGER && console.log('review partner', resp);
      dispatch({
        type: ActionType.USER_REVIEWS,
        payload: resp.data,
      });
    });
  } catch (err) {
    DEBUGER && console.log('err', err.message);
  }
};

export class Liverate {
  static Liverates(base) {
    //  alert("ppppp");
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.Liverates));
      Get(`liveRates/?base=${base}`)
        .then((success) => {
          dispatch(
            actionDispatch(
              ActionType.Liverates_SUCCESS,
              // DEBUGER && console.log(success.data.message)
              success.data
              // DEBUGER && console.log(success.data, "this is our data")
            )
          );
          if (calc_instance) {
            // calc_instance.calculator_instance.forceUpdate();
            calc_instance.setState({
              data: success.data,
            });
          }
        })
        .catch((error) => {
          dispatch(actionDispatch(ActionType.Liverates_FAIL));

          DEBUGER && console.log(error);
        });
    };
  }

  static rates(country_name, time, partner) {
    // console.log(country_name, time, partner)
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.Liverates));
      Post(`rate/chart`, { time, partners: partner.toLowerCase().replace(/ /g, "") })
        .then((success) => {

          dispatch(
            actionDispatch(
              ActionType.RATE,
              // DEBUGER && console.log(success.data.message)
              success.data
              // DEBUGER && console.log(success.data, "this is our data")
            )
          );
        })
        .catch((error) => {
          dispatch(actionDispatch(ActionType.Liverates_FAIL));

          DEBUGER && console.log(error);
        });
    };
  }
}

export const footer = () => async (dispatch) => {
  try {
    let data = await axios.get(`${base_url}footer`);
    DEBUGER && console.log('footerdtaa', data);
    if (data.status == 200) {
      dispatch({ type: ActionType.FOOTER, payload: data.data });
    }
  } catch (err) {
    DEBUGER && console.log('err', err);
  }
};

export const blogs = () => async (dispatch) => {
  try {
    let data = await axios.get(`${base_url}blog`);
    DEBUGER && console.log('blogs', data);
    if (data.status == 200) {
      dispatch({ type: ActionType.BLOG, payload: data.data });
    }
  } catch (err) {
    DEBUGER && console.log('err', err);
  }
};

export const counts = () => async (dispatch) => {
  try {
    let data = await axios.get(`${base_url}count`);
    DEBUGER && console.log('counts', data);
    if (data.status == 200) {
      dispatch({ type: ActionType.COUNT, payload: data.data });
    }
  } catch (err) {
    DEBUGER && console.log('err', err);
  }
};

export class News {
  static News() {
    // DEBUGER && console.log(, "ppppp");
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.News));
      Get(`news/`)
        .then((success) => {
          dispatch(
            actionDispatch(
              ActionType.News_SUCCESS,
              // DEBUGER && console.log("success.data", success.data),
              success.data
              // DEBUGER && console.log(success.data, "this is our data")
            )
          );
        })
        .catch((error) => {
          dispatch(actionDispatch(ActionType.News_FAIL));

          DEBUGER && console.log(error);
        });
    };
  }
}
export class ContactUs {
  static ContactUs(obj) {
    DEBUGER && console.log(obj, 'ppppp');
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.ContactUs));
      Post(`contactUs/`, obj)
        .then((success) => {
          swal({
            title: 'Submitted Successfully!',
            icon: 'success',
            button: 'Ok',
          });
          // alert("Succes")
          dispatch(
            actionDispatch(
              ActionType.ContactUs_SUCCESS,
              window.location.pathname == '/registration'
                ? (window.location = '/')
                : ''
            )
          );

          dispatch(
            actionDispatch(
              ActionType.ContactUs_SUCCESS
              // DEBUGER && console.log(success.data.message)
              // DEBUGER && console.log(success.data.data)
            )
          );
        })
        .catch((error) => {
          dispatch(actionDispatch(ActionType.ContactUs_FAIL, error.message));

          DEBUGER && console.log(error, 'kia error hy');
        });
    };
  }
}

export class Article {
  static Article() {
    // DEBUGER && console.log(, "ppppp");
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.Article));
      Get(`article/`)
        .then((success) => {
          dispatch(
            actionDispatch(
              ActionType.Article_SUCCESS,
              // DEBUGER && console.log(success.data.message)
              success.data
              // DEBUGER && console.log(success.data, "this is our data")
            )
          );
        })
        .catch((error) => {
          dispatch(actionDispatch(ActionType.Article_FAIL));

          DEBUGER && console.log(error);
        });
    };
  }
}

export class Blogs {
  static Blogs() {
    // DEBUGER && console.log(, "ppppp");
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.Blogs));
      Get(`blog/`)
        .then((success) => {
          DEBUGER && console.log('blogs', success);
          dispatch(
            actionDispatch(
              ActionType.Blogs_SUCCESS,
              // DEBUGER && console.log(success.data.message)
              success.data
              // DEBUGER && console.log(success.data, "this is our data")
            )
          );
        })
        .catch((error) => {
          dispatch(actionDispatch(ActionType.Blogs_FAIL));

          DEBUGER && console.log(error);
        });
    };
  }
}

export class Logout {
  static Logout() {
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.Logout));
      Get(`users/logout/`)
        .then((success) => {
          DEBUGER && console.log('running');
          dispatch(
            actionDispatch(
              ActionType.Logout_SUCCESS
              // localStorage.clear(),
              // window.location.reload()
            )
          );

        })
        .catch((error) => {
          dispatch(actionDispatch(ActionType.Logout_FAIL));

          DEBUGER && console.log(error);
        });
      localStorage.clear()
      history.push("/registration")
    };

  }
}

export class Promotions {
  static Promotions() {
    // DEBUGER && console.log(, "ppppp");
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.Promotions));
      Get(`promotions/`)
        .then((success) => {
          DEBUGER && console.log('running', success);
          dispatch(actionDispatch(ActionType.Promotions_SUCCESS, success.data));
        })
        .catch((error) => {
          dispatch(actionDispatch(ActionType.Promotions_FAIL));

          DEBUGER && console.log(error);
        });
    };
  }
}

export class Offers {
  static Offers() {
    return (dispatch) => {
      dispatch(actionDispatch(ActionType.Offers));
      Get(`offers/`)
        .then((success) => {
          DEBUGER && console.log('runningSUCCESS', success);
          dispatch(
            actionDispatch(
              ActionType.Offers_SUCCESS,
              // localStorage.clear(),
              // window.location.reload()
              success.data
            )
          );
        })
        .catch((error) => {
          dispatch(actionDispatch(ActionType.Offers_FAIL));

          DEBUGER && console.log(error);
        });
    };
  }
}
export const transferWise = (id) => async (dispatch) => {
  const data = await Get(`partner/${id}`);
  DEBUGER && console.log('transer wise', data);
  if (data.status == 200) {
    dispatch({
      type: 'one_comparision',
      payload: data.data,
    });
  }
};

export const viewDetails = (id) => async (dispatch) => {
  DEBUGER && console.log('transer wise', id);
  dispatch({
    type: ActionType.SELECT_COMPARISION,
    payload: id,
  });
};

export const content = () => async (dispatch) => {
  Get(`content`)
    .then((resp) => {
      DEBUGER && console.log('const', resp);
      dispatch({
        type: ActionType.GET_CONTENT,
        payload: resp.data,
      });
    })
    .catch((err) => {
      DEBUGER && console.log('err', err);
    });
};

export const availPromotions = (data) => async (dispatch) => {
  Post(`promotions/click`, data)
    .then((resp) => {
      DEBUGER && console.log('const', resp);
      alert('Successfully added');
    })
    .catch((err) => {
      DEBUGER && console.log('err', err);
    });
};
export const ComparisionFormData = (obj) => async (dispatch) => {
  dispatch(actionDispatch(ActionType.ComparisonsFormData));
  dispatch({
    type: ActionType.ComparisonsFormData,
    payload: obj,
  });
};
export class Comparisons {
  static ComparisionFormData = (obj) => async (dispatch) => {
    // console.log("obj", obj)
    dispatch(actionDispatch(ActionType.ComparisonsFormData));
    dispatch({
      type: ActionType.ComparisonsFormData,
      payload: obj,
    });
  };
  static transferWise(id) {
    return async (dispatch) => {
      DEBUGER && console.log('transer wise12');
      dispatch(actionDispatch(ActionType.ComparisonsOverview));
      try {
        const data = await Get(`partner/${id}`);
        DEBUGER && console.log('data asd' + data);
        DEBUGER && console.log('new data startttttttt', data);
        dispatch({
          type: ActionType.Comparisons_SUCCESS,
          payload: data.data,
        });
      } catch (err) {
        DEBUGER && console.log('err', err);
      }
    };
  }
  static Comparisons(obj, history, text) {
    DEBUGER && console.log('HIT TO COMPARISON TABLE', obj, history, text);
    return async (dispatch) => {


      try {
        DEBUGER && console.log('run hogata', obj, history, text);
        let limit = 10;
        let sort = 1;
        let name = '';
        if (comparison_instance && comparison_instance.header) {
          comparison_instance.header.state.limit
            ? (limit = comparison_instance.header.state.limit)
            : (limit = 10);

          comparison_instance.header.state.name
            ? (name = comparison_instance.header.state.name)
            : (name = '');
        }
        DEBUGER && console.log('datattttt', limit, sort, name);
        DEBUGER && console.log('sad' + obj.latitude);
        const data = await Get(
          `partnerrate?base_currency=${obj.from}&country_name=${obj.to}&amount=${obj.amount}&limit=${limit}&name=${name}&latitude=${obj.latitude}&longitute=${obj.longitude}`
        );
        DEBUGER && console.log('data agaya', data);
        // const dataFilter = await data.json()
        // .then(success => {
        //   DEBUGER && console.log("new item" , success)
        if (obj.data == 'live') {
          dispatch({
            type: ActionType.LIVE,
            payload: data.data,
          });
        } else {
          dispatch(ComparisionFormData(obj))
          dispatch(actionDispatch(ActionType.Comparisons));
          dispatch({
            type: ActionType.Comparisons_SUCCESS,
            payload: data.data,
          });
        }

        if (text == 'inform') {
          history.push({
            pathname: '/Comparison',
            comparisionData: obj
          });
        }
      } catch (error) {
        DEBUGER && console.log('err', error);
        // dispatch(
        //   actionDispatch(
        //     ActionType.Comparisons_SUCCESS,
        //     // localStorage.clear(),
        //     // window.location.reload()
        //     success.data
        //     )
        //   );
        //})
        dispatch(actionDispatch(ActionType.Comparisons_FAIL));

        // DEBUGER && console.log("new data" , error);
        // });
      }
    };
  }

  static table(obj, history) {
    return async (dispatch, getState) => {
      DEBUGER && console.log('GET DATA DIUSPATCHC', obj);
      try {
        const table = getState().Comparisons.table;

        if (table) {
          var found = table.find(function (element) {
            return element._id === obj._id;
          });

          if (found) {
            swal({
              title: 'Already Exist',
              icon: 'error',
              button: 'Ok',
            });
            return;
          }
        }

        DEBUGER && console.log('data table ka', table);
        // const dataFilter = await data.json()
        // .then(success => {
        //   DEBUGER && console.log("new item" , success)

        if (table.length === 3) {
          swal({
            title: 'You cannot add more than 3 partners to compare.',
            icon: 'error',
            button: 'Ok',
          });
          return;
        }

        if (table.length < 3) {
          dispatch({
            type: ActionType.ADD_TABLE,
            payload: obj,
          });
          swal({
            title: 'Succesfully Added in the Table',
            icon: 'success',
            button: 'Ok',
          });
        }
        DEBUGER && console.log('tabvle  length', table.length);
        // if (table.length >= 0) {
        //   history.push("/companyoverview");
        // }
      } catch (error) {
        DEBUGER && console.log('err', error);
        // dispatch(
        //   actionDispatch(
        //     ActionType.Comparisons_SUCCESS,
        //     // localStorage.clear(),
        //     // window.location.reload()
        //     success.data
        //     )
        //   );
        //})
        //dispatch(actionDispatch(ActionType.Comparisons_FAIL));

        // DEBUGER && console.log("new data" , error);
        // });
      }
    };
  }
  static tableDel() {
    return async (dispatch, getState) => {
      try {
        const table = getState().Comparisons.table;
        // const table = getState().Comparisons.table;
        // DEBUGER && console.log("data table ka", table);
        // // const dataFilter = await data.json()
        // .then(success => {
        //   DEBUGER && console.log("new item" , success)
        if (table.length >= 3) {
          dispatch({
            type: ActionType.DEL_TABLE,
            payload: '',
          });
        }
      } catch (error) {
        DEBUGER && console.log('err', error);
        // dispatch(
        //   actionDispatch(
        //     ActionType.Comparisons_SUCCESS,
        //     // localStorage.clear(),
        //     // window.location.reload()
        //     success.data
        //     )
        //   );
        //})
        //dispatch(actionDispatch(ActionType.Comparisons_FAIL));

        // DEBUGER && console.log("new data" , error);
        // });
      }
    };
  }

  static delTableIndex(index) {
    return async (dispatch, getState) => {
      try {
        dispatch({
          type: ActionType.DEL_TABLE_INDEX,
          payload: { index: index },
        });
        // const table = getState().Comparisons.table;
        // // const table = getState().Comparisons.table;
        // // DEBUGER && console.log("data table ka", table);
        // // // const dataFilter = await data.json()
        // // .then(success => {
        // //   DEBUGER && console.log("new item" , success)
        // if (table.length >= 3) {
        //   dispatch({
        //     type: ActionType.DEL_TABLE,
        //     payload: "",
        //   });
        // }
      } catch (error) {
        // DEBUGER && console.log("err", error);
        // dispatch(
        //   actionDispatch(
        //     ActionType.Comparisons_SUCCESS,
        //     // localStorage.clear(),
        //     // window.location.reload()
        //     success.data
        //     )
        //   );
        //})
        //dispatch(actionDispatch(ActionType.Comparisons_FAIL));
        // DEBUGER && console.log("new data" , error);
        // });
      }
    };
  }

  static partner() {
    return async (dispatch) => {
      try {
        DEBUGER && console.log('call hogaya');
        dispatch({
          type: ActionType.PARTNER_LOADING,
          payload: true,
        });
        const data = await Get(`partner`);
        DEBUGER && console.log('new data start', data.data);

        dispatch({
          type: ActionType.PARTNER,
          payload: data.data,
        });

        dispatch({
          type: ActionType.PARTNER_LOADING,
          payload: false,
        });
      } catch (error) {
        DEBUGER && console.log('err tu batao', error);
        dispatch({
          type: ActionType.PARTNER_LOADING,
          payload: false,
        });
      }
    };
  }

  static rate() {
    return async (dispatch) => {
      try {
        dispatch({
          type: ActionType.PARTNER_LOADING,
          payload: true,
        });
        const data = await Get(`rate/all`);
        DEBUGER && console.log('rate agay', data.data);

        dispatch({
          type: ActionType.RATE,
          payload: data.data,
        });

        dispatch({
          type: ActionType.PARTNER_LOADING,
          payload: false,
        });
      } catch (error) {
        DEBUGER && console.log('err tu batao', error);
        dispatch({
          type: ActionType.PARTNER_LOADING,
          payload: false,
        });

        // });
      }
    };
  }

  // static Offers() {
  //   return async dispatch => {
  //     try {
  //       dispatch({
  //         type: ActionType.PARTNER_LOADING,
  //         payload: true
  //       });
  //       const data = await Get(`rate/all`);
  //       DEBUGER && console.log("rate agay", data.data);

  //       dispatch({
  //         type: ActionType.RATE,
  //         payload: data.data
  //       });

  //       dispatch({
  //         type: ActionType.PARTNER_LOADING,
  //         payload: false
  //       });
  //     } catch (error) {
  //       DEBUGER && console.log("err tu batao", error);
  //       dispatch({
  //         type: ActionType.PARTNER_LOADING,
  //         payload: false
  //       });

  //       // });
  //     }
  //   };
  // }

  static addFavourite(user_id, id, token) {
    return async (dispatch) => {
      try {
        const data = await Post(`favourite/${user_id}`, { fav_id: id }, token);
        DEBUGER && console.log('new data start', token);

        if (data.status == 200 && !data.data.error) {
          DEBUGER && console.log('addFavourite', data);
          // dispatch({
          //   type: ActionType.FAVOURITE_SUCCESS,
          //   payload: data.data,
          // });

          dispatch({
            type: ActionType.FAVOURITE_LOADING,
            payload: false,
          });
          dispatch(Comparisons.getFavourite(user_id))
          swal({
            title: 'Successfully Added to the Favourites',
            icon: 'success',
            button: 'Ok',
          });
        }
        if (data.status == 500) {
          swal({
            title: 'Already Exsists',
            icon: 'error',
            button: 'Ok',
          });
        }
      } catch (error) {
        swal({
          title: 'Already Exsists',
          icon: 'error',
          button: 'Ok',
        });

        DEBUGER && console.log('err tu batao', error);
        dispatch({
          type: ActionType.FAVOURITE_LOADING,
          payload: false,
        });

        // });
      }
    };
  }
  static removeFavourite(user_id, id, token) {
    DEBUGER && console.log("removeFavourite", user_id, id, token)
    return async (dispatch) => {
      try {
        const data = await Delete(`favourite/${user_id}`, { fav_id: id }, token);
        DEBUGER && console.log('new data start', token);

        if (data.status == 200 && !data.data.error) {
          DEBUGER && console.log('removeFavourite', data);
          // dispatch({
          //   type: ActionType.FAVOURITE_SUCCESS,
          //   payload: data.data,
          // });

          dispatch({
            type: ActionType.FAVOURITE_LOADING,
            payload: false,
          });
          dispatch(Comparisons.getFavourite(user_id))
          swal({
            title: 'Successfully Remove from the Favourites',
            icon: 'success',
            button: 'Ok',
          });
        }
        if (data.status == 500) {
          swal({
            title: 'Already Removed',
            icon: 'error',
            button: 'Ok',
          });
        }
      } catch (error) {
        swal({
          title: 'Already Removed',
          icon: 'error',
          button: 'Ok',
        });

        DEBUGER && console.log('err tu batao', error.response);
        dispatch({
          type: ActionType.FAVOURITE_LOADING,
          payload: false,
        });

        // });
      }
    };
  }

  static getFavourite(id, token) {
    return async (dispatch) => {
      DEBUGER && console.log('favourite new data start', id);
      try {
        dispatch({
          type: ActionType.FAVOURITE_LOADING,
          payload: true,
        });
        const data = await Get(`favourite/${id}`);
        DEBUGER && console.log('favourite new data start', data, id);

        dispatch({
          type: ActionType.FAVOURITE_SUCCESS,
          payload: data.data,
        });

        dispatch({
          type: ActionType.FAVOURITE_LOADING,
          payload: false,
        });
      } catch (error) {
        DEBUGER && console.log('err tu batao', error);
        dispatch({
          type: ActionType.FAVOURITE_LOADING,
          payload: false,
        });

        // });
      }
    };
  }

  static getPromotions() {
    return async (dispatch) => {
      try {
        dispatch({
          type: ActionType.PROMOTION_LOADING,
          payload: true,
        });
        const data = await Get(`promotions`);
        DEBUGER && console.log('new data start', data);

        dispatch({
          type: ActionType.PROMOTION,
          payload: data.data,
        });

        dispatch({
          type: ActionType.PROMOTION_LOADING,
          payload: false,
        });
      } catch (error) {
        DEBUGER && console.log('err tu batao', error);
        dispatch({
          type: ActionType.PROMOTION_LOADING,
          payload: false,
        });

        // });
      }
    };
  }

  static currency() {
    return async (dispatch) => {
      try {
        dispatch({
          type: ActionType.PARTNER_LOADING,
          payload: true,
        });
        const data = await Get(`rate/currency`);
        DEBUGER && console.log('rate/currency', data);

        dispatch({
          type: ActionType.CURRENCY,
          payload: data.data,
        });

        dispatch({
          type: ActionType.PARTNER_LOADING,
          payload: false,
        });
      } catch (error) {
        DEBUGER && console.log('err tu batao', error);
        dispatch({
          type: ActionType.PARTNER_LOADING,
          payload: false,
        });

        // });
      }
    };
  }

  static remmitanceHistory(id) {
    return async (dispatch) => {
      try {
        dispatch({
          type: ActionType.HISTORY_LOADING,
          payload: true,
        });
        const data = await Get(`remmitance/${id}`);
        DEBUGER && console.log('rammitance', data);

        dispatch({
          type: ActionType.HISTORY,
          payload: data.data,
        });

        dispatch({
          type: ActionType.HISTORY_LOADING,
          payload: false,
        });
      } catch (error) {
        DEBUGER && console.log('err tu batao', error);
        dispatch({
          type: ActionType.HISTORY_LOADING,
          payload: false,
        });

        // });
      }
    };
  }

  static feedback(id, obj) {
    return async (dispatch) => {
      try {
        // DEBUGER && console.log("feedback", id, obj);
        const data = await Post(`feedback/${id}`, obj);

        if (data.status == 200) {
          DEBUGER && console.log('feedback', data);
          swal({
            title: 'Goof Job!',
            icon: 'success',
            button: 'Ok',
          });
        }
      } catch (error) {
        DEBUGER && console.log('err tu batao', error);

        // });
      }
    };
  }
}

export const getTestimomials = () => async (dispatch) => {
  try {
    DEBUGER && console.log('call');
    //dispatch({ type: actionTypes.TESTIMONIAL, payload: true });
    let data = await axios.get(`${base_url}testimonial`);
    DEBUGER && console.log('testimonilas', data);
    if (data.status == 200) {
      DEBUGER && console.log('testimonilas', data);
      dispatch({ type: ActionType.TESTIMONIALS, payload: data.data });
    }
  } catch (err) {
    DEBUGER && console.log('err', err.message);
    // /dispatch({ type: ActionType.TESTIMONIAL_FAIL, payload: false });
  }
};


export const getSettings = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_SETTINGS_LOADING,
      payload: true,
    });
    const res = await axios.get(`${base_url}settings`, 
    );
    DEBUGER && console.log("getSettings response", res.data);
    dispatch({
      type: ActionType.GET_SETTINGS,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_SETTINGS_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_SETTINGS_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getSettings error", error?.response?.data);
  }
};

export const getFAQS = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_FAQS_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}faqs`, 
    );
    DEBUGER && console.log("getFAQS response", res.data);
    dispatch({
      type: ActionType.GET_FAQS,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_FAQS_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_FAQS_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getFAQS error", error?.response?.data);
  }
};



export const getHomebanner = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_HOMEBANNER_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}home-banners`, 
    );
    DEBUGER && console.log("getHomebanner response", res.data);
    dispatch({
      type: ActionType.GET_HOMEBANNER,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_HOMEBANNER_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_HOMEBANNER_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getHomebanner error", error?.response?.data);
  }
};

export const getWhyChooseUs = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_WHYCHOOSEUS_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}sub-headings`, 
    );
    DEBUGER && console.log("getWhyChooseUs response", res.data);
    dispatch({
      type: ActionType.GET_WHYCHOOSEUS,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_WHYCHOOSEUS_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_WHYCHOOSEUS_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getWhyChooseUs error", error?.response?.data);
  }
};

export const getServices = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_SERVICES_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}services-headings`, 
    );
    DEBUGER && console.log("getServices response", res.data);
    dispatch({
      type: ActionType.GET_SERVICES,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_SERVICES_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_SERVICES_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getServices error", error?.response?.data);
  }
};


export const getNetworkHeading = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_NETWORKHEADING_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}network-headings`, 
    );
    DEBUGER && console.log("getNetworkHeading response", res.data);
    dispatch({
      type: ActionType.GET_NETWORKHEADING,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_NETWORKHEADING_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_NETWORKHEADING_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getNetworkHeading error", error?.response?.data);
  }
};


export const getComparisonHeading = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_COMPARISONHEADING_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}camparison-headings`, 
    );
    DEBUGER && console.log("getComparisonHeading response", res.data);
    dispatch({
      type: ActionType.GET_COMPARISONHEADING,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_COMPARISONHEADING_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_COMPARISONHEADING_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getComparisonHeading error", error?.response?.data);
  }
};


export const getOurNetworkHeading = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_OURNETWORKHEADING_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}our-network-headings`, 
    );
    DEBUGER && console.log("getOurNetworkHeading response", res.data);
    dispatch({
      type: ActionType.GET_OURNETWORKHEADING,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_OURNETWORKHEADING_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_OURNETWORKHEADING_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getOurNetworkHeading error", error?.response?.data);
  }
};


export const getAboutHeading = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_ABOUTUSHEADING_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}about-us-headings`, 
    );
    DEBUGER && console.log("getAboutHeading response", res.data);
    dispatch({
      type: ActionType.GET_ABOUTUSHEADING,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_ABOUTUSHEADING_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_ABOUTUSHEADING_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getAboutHeading error", error?.response?.data);
  }
};



export const getWhatwedoHeading = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_WHATWEDOHEADING_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}what-we-do-headings`, 
    );
    DEBUGER && console.log("getWhatwedoHeading response", res.data);
    dispatch({
      type: ActionType.GET_WHATWEDOHEADING,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_WHATWEDOHEADING_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_WHATWEDOHEADING_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getWhatwedoHeading error", error?.response?.data);
  }
};



export const getMissionVisionHeading = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_MISSIONVISIONHEADING_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}mission-vision-headings`, 
    );
    DEBUGER && console.log("getMissionVisionHeading response", res.data);
    dispatch({
      type: ActionType.GET_MISSIONVISIONHEADING,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_MISSIONVISIONHEADING_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_MISSIONVISIONHEADING_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getMissionVisionHeading error", error?.response?.data);
  }
};



export const getPrivacyHeading = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_PRIVACYHEADING_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}privacy-policy-headings`, 
    );
    DEBUGER && console.log("getPrivacyHeading response", res.data);
    dispatch({
      type: ActionType.GET_PRIVACYHEADING,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_PRIVACYHEADING_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_PRIVACYHEADING_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getPrivacyHeading error", error?.response?.data);
  }
};


export const getCookieHeading = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_COOKIEHEADING_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}cookie-policy-headings`, 
    );
    DEBUGER && console.log("getCookieHeading response", res.data);
    dispatch({
      type: ActionType.GET_COOKIEHEADING,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_COOKIEHEADING_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_COOKIEHEADING_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getCookieHeading error", error?.response?.data);
  }
};


export const getTermNCondHeading = () => async (dispatch) => {
  try {
    dispatch({
      type: ActionType.IS_TERMNCONDHEADING_LOADING,
      payload: true,
    });
     const res = await axios.get(`${base_url}term-policy-headings`, 
    );
    DEBUGER && console.log("getTermNCondHeading response", res.data);
    dispatch({
      type: ActionType.GET_TERMNCONDHEADING,
      payload: res.data,
    });
    dispatch({
      type: ActionType.IS_TERMNCONDHEADING_LOADING,
      payload: false,
    });
  } catch (error) {
    dispatch({
      type: ActionType.IS_TERMNCONDHEADING_LOADING,
      payload: false,
    });
    DEBUGER && console.log("getTermNCondHeading error", error?.response?.data);
  }
};