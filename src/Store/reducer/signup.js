import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  Signup: []
};

export default (state = initialState, action) => {
  // console.log("action", action);
  switch (action.type) {
    // STEPPER

    case ActionType.Signup:
      return { ...state, isLoading: true };

    case ActionType.Signup_SUCCESS:
      return {
        ...state,
        isLoading: false,
        Signup: action.payload
      };

    case ActionType.Signup_FAIL:
      return { ...state, isLoading: false };

    default:
      return { ...state };
  }
};
