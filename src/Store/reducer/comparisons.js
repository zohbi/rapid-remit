import { ActionType } from '../actions';

const initialState = {
  isLoading: false,
  data: {},
  one_comparision: {},
  comparisons: [],
  table: [],
  part: [],
  rates: [],
  currency: [],
  content: [],
  comp: [],
  latitude: '',
  longitude: '',
};

export default (state = initialState, action) => {
  //console.log("action ====>", action);

  // console.log('asddasasd1');
  switch (action.type) {
    // STEPPER
    case ActionType.SELECT_COMPARISION:
      // console.log('asddasasd2', state.comp);

      let data;
      if (state.comp) {
        // console.log(
        //   'COMPARISOISDADADAD',
        //   state.comp,
        //   action.payload,
        //   state.data
        // );
        data = [];
        state.comp.map((m) => {
          if (m.partner_details[0]._id == action.payload) {
            data = [...data, m];
          }
          //data = [...data, m.partner_details[0]];
        });
      }

      return {
        ...state,
        one_comparision: data[0],
      };

    case 'one_comparision':
      // console.log('one_comparision', action.payload);

      return {
        ...state,
        Comp: action.payload,
        one_comparision: action.payload,
      };
    case ActionType.Comparisons:
      // console.log('comp', ActionType.Comparisons);
      return { ...state, isLoading: true };

    case ActionType.LIVE:
      // console.log('comp', ActionType.Comparisons);
      return { ...state, comp: action.payload };
    case ActionType.ComparisonsFormData:
      // console.log('comp', ActionType.Comparisons);
      return { ...state, comparisonsFormData: action.payload };
    case ActionType.Comparisons_SUCCESS:
      // console.log(ActionType.Comparisons_SUCCESS, action.payload);
      return {
        ...state,
        isLoading: false,
        comparisons: action.payload,
        comp: action.payload,
      };

    case ActionType.Comparions_FAIL:
      return { ...state, isLoading: false };

    case ActionType.ADD_TABLE:
      return { ...state, table: [...state.table, action.payload] };

    case ActionType.PARTNER:
      // console.log('partner', state.part);
      return { ...state, part: action.payload };

    case ActionType.DEL_TABLE:
      return { ...state, table: [] };

    case ActionType.PARTNER_LOADING:
      return { ...state, isLoading: action.payload };

    case ActionType.RATE:
      return { ...state, rates: action.payload };

    case ActionType.CURRENCY:
      // console.log('dddd22', action.payload);
      return { ...state, currency: action.payload };

    case ActionType.GET_CONTENT:
      // console.log('dddd22', action.payload);
      return { ...state, content: action.payload };

    case 'UserLocation':
      // console.log('UserLocation', state.part);
      return {
        ...state,
        latitude: action.latitude,
        longitude: action.longitude,
      };

    case ActionType.EMPTY:
      // console.log('dddd22', state.comparisons);
      return {
        ...state,
        comp: state.comparisons,
        comparisons: action.payload,
      };
    case ActionType.DEL_TABLE_INDEX: {
      let array = [];
      for (let i = 0; i < state.table.length; i++) {
        if (i !== action.payload.index) {
          array.push(state.table[i]);
        }
      }

      return {
        ...state,
        table: array,
      };
    }
    default:
      return state;
  }
};
