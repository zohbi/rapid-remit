import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  Recover: []
};

export default (state = initialState, action) => {
  // console.log("action", action);
  switch (action.type) {
    // STEPPER

    case ActionType.Recover:
      return { ...state, isLoading: true };

    case ActionType.Recover_SUCCESS:
      return {
        ...state,
        isLoading: false,
        Recover: action.payload
      };

    case ActionType.Recover_FAIL:
      return { ...state, isLoading: false };

    default:
      return { ...state };
  }
};
