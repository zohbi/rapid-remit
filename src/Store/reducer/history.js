import { ActionType } from "../actions";

const initialState = {
  isLoading: false,

  allData: []
};

export default (state = initialState, action) => {
  // console.log("action ====>", action);
  switch (action.type) {
    // STEPPER

    case ActionType.HISTORY_LOADING:
      return { ...state, isLoading: action.payload };

    case ActionType.HISTORY:
      // console.log("FAVOURITE_SUCCESS555555", action.payload);
      return {
        ...state,

        allData: action.payload
      };

    default:
      return { ...state };
  }
};
