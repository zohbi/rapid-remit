import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  Offers: [],
  reviews: [],
  partner_review: {},
  user_review: [],
  testimonials: [],
  counts: {},
};

export default (state = initialState, action) => {
  // console.log("action ====>", action);
  switch (action.type) {
    // STEPPER

    case ActionType.Offers:
      return { ...state, isLoading: true };

    case ActionType.Offers_SUCCESS:
      // console.log("Offers_SUCCESS", action.payload);
      return {
        ...state,
        isLoading: false,
        Offers: action.payload,
      };

    case ActionType.Offers_FAIL:
      return { ...state, isLoading: false };

    case ActionType.GET_REVIEWS:
      return { ...state, reviews: action.payload };

    case ActionType.PARTNER_REVIEWS:
      return { ...state, partner_review: action.payload };

    case ActionType.USER_REVIEWS:
      return { ...state, user_review: action.payload };

    case ActionType.TESTIMONIALS:
      return { ...state, testimonials: action.payload };
    case ActionType.COUNT:
      return { ...state, counts: action.payload };

    default:
      return { ...state };
  }
};
