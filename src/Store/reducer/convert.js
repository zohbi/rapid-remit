import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  Convert: []
};

export default (state = initialState, action) => {
  // console.log("action", action);
  switch (action.type) {
    // STEPPER

    case ActionType.Convert:
      return { ...state, isLoading: true };

    case ActionType.Convert_SUCCESS:
      return {
        ...state,
        isLoading: false,
        Convert: action.payload
      };

    case ActionType.Convert_FAIL:
      return { ...state, isLoading: false };

    default:
      return { ...state };
  }
};
