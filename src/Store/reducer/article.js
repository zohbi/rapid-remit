import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  Article: []
};

export default (state = initialState, action) => {
  // console.log("action ====>", action);
  switch (action.type) {
    // STEPPER

    case ActionType.Article:
      return { ...state, isLoading: true };

    case ActionType.Article_SUCCESS:
      return {
        ...state,
        isLoading: false,
        Article: action.payload
      };

    case ActionType.Article_FAIL:
      return { ...state, isLoading: false };

    default:
      return { ...state };
  }
};
