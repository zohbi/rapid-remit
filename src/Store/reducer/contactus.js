import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  ContactUs: [],
  footer: [],
};

export default (state = initialState, action) => {
  // console.log("action", action);
  switch (action.type) {
    // STEPPER

    case ActionType.ContactUs:
      return { ...state, isLoading: true };

    case ActionType.ContactUs_SUCCESS:
      return {
        ...state,
        isLoading: false,
        ContactUs: action.payload,
      };

    case ActionType.ContactUs_FAIL:
      return { ...state, isLoading: false, error: action.payload };

    case ActionType.FOOTER:
      return { ...state, footer: action.payload };

    default:
      return { ...state };
  }
};
