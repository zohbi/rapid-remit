import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  Promotions: [],
};

export default (state = initialState, action) => {
  // console.log("action ====>", action);
  switch (action.type) {
    // STEPPER

    case ActionType.Promotions:
      return { ...state, isLoading: true };

    case ActionType.Promotions_SUCCESS:
      return {
        ...state,
        isLoading: false,
        Promotions: action.payload,
      };

    case ActionType.Promotions_FAIL:
      return { ...state, isLoading: false };

    default:
      return { ...state };
  }
};
