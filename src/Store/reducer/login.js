import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  Login: [],
};

export default (state = initialState, action) => {
  // console.log("action", action);
  switch (action.type) {
    // STEPPER

    case ActionType.Login:
      return { ...state, isLoading: true };

    case ActionType.Login_SUCCESS:
      // console.log("login", action.payload);
      return {
        ...state,
        isLoading: false,
        Login: action.payload,
      };

    case ActionType.Login_FAIL:
      return { ...state, isLoading: false, error: action.payload };

    case ActionType.UPDATE_USER:
      return { ...state, Login: action.payload };

    default:
      return { ...state };
  }
};
