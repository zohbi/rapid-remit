import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  Blogs: [],
  blogData: [],
};

export default (state = initialState, action) => {
  // console.log("action ====>", action);
  switch (action.type) {
    // STEPPER

    case ActionType.Blogs:
      return { ...state, isLoading: true };

    case ActionType.Blogs_SUCCESS:
      return {
        ...state,
        isLoading: false,
        Blogs: action.payload,
      };

    case ActionType.Blogs_FAIL:
      return { ...state, isLoading: false };

    case ActionType.BLOG:
      return { ...state, blogData: action.payload };

    default:
      return { ...state };
  }
};
