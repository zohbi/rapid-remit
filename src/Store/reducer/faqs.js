import { ActionType } from "../actions";

const initialState = {
  isLoading: false,

};

export default (state = initialState, action) => {
  // console.log("action ====>", action);
  switch (action.type) {
    // STEPPER
    case ActionType.GET_FAQS:
      return { ...state, faqs: action.payload };
    case ActionType.GET_SETTINGS:
      return { ...state, setting: action.payload };
    case ActionType.IS_FAQS_LOADING:
      return { ...state, isFaqsLoading: action.payload };
    case ActionType.IS_SETTINGS_LOADING:
      return { ...state, isSettingLoading: action.payload };
    case ActionType.GET_HOMEBANNER:
      return { ...state, homebanner: action.payload };
    case ActionType.IS_HOMEBANNER_LOADING:
      return { ...state, isHomebannerLoading: action.payload };
    case ActionType.GET_WHYCHOOSEUS:
      return { ...state, whyChooseUs: action.payload };
    case ActionType.IS_WHYCHOOSEUS_LOADING:
      return { ...state, isWhyChooseUsLoading: action.payload };
    case ActionType.GET_SERVICES:
      return { ...state, services: action.payload };
    case ActionType.IS_SERVICES_LOADING:
      return { ...state, isServicesLoading: action.payload };
    case ActionType.GET_NETWORKHEADING:
      return { ...state, networkHeading: action.payload };
    case ActionType.IS_NETWORKHEADING_LOADING:
      return { ...state, isNetworkHeadingLoading: action.payload };
    case ActionType.GET_COMPARISONHEADING:
      return { ...state, comparisonheading: action.payload };
    case ActionType.IS_COMPARISONHEADING_LOADING:
      return { ...state, comparisonLoading: action.payload };
    case ActionType.GET_ABOUTUSHEADING:
      return { ...state, aboutusheading: action.payload };
    case ActionType.IS_ABOUTUSHEADING_LOADING:
      return { ...state, aboutusLoading: action.payload };
    case ActionType.GET_WHATWEDOHEADING:
      return { ...state, whatwedoheading: action.payload };
    case ActionType.IS_WHATWEDOHEADING_LOADING:
      return { ...state, whatwedoLoading: action.payload };
    case ActionType.GET_MISSIONVISIONHEADING:
      return { ...state, missionvisionheading: action.payload };
    case ActionType.IS_MISSIONVISIONHEADING_LOADING:
      return { ...state, missionvisionLoading: action.payload };
    case ActionType.GET_PRIVACYHEADING:
      return { ...state, privacyheading: action.payload };
    case ActionType.IS_PRIVACYHEADING_LOADING:
      return { ...state, privacyLoading: action.payload };
    case ActionType.GET_COOKIEHEADING:
      return { ...state, cookiesheading: action.payload };
    case ActionType.IS_COOKIEHEADING_LOADING:
      return { ...state, cookiesLoading: action.payload };
    case ActionType.GET_TERMNCONDHEADING:
      return { ...state, termncondheading: action.payload };
    case ActionType.IS_TERMNCONDHEADING_LOADING:
      return { ...state, termncondLoading: action.payload };
    case ActionType.GET_OURNETWORKHEADING:
      return { ...state, ournetworkheading: action.payload };
    case ActionType.IS_OURNETWORKHEADING_LOADING:
      return { ...state, ournetworkLoading: action.payload };
    default:
      return { ...state };
  }
};
