import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  Liverates: [],
  rate: [],
};

export default (state = initialState, action) => {
  // console.log("action ====>", action);
  switch (action.type) {
    // STEPPER

    case ActionType.Liverates:
      return { ...state, isLoading: true };

    case ActionType.Liverates_SUCCESS:
      return {
        ...state,
        isLoading: false,
        Liverates: action.payload,
      };

    case ActionType.Liverates_FAIL:
      return { ...state, isLoading: false };

    case ActionType.RATE:
      return { ...state, rate: action.payload, isLoading: false };

    default:
      return { ...state };
  }
};
