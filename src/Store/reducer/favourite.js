import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  allData: []
};

export default (state = initialState, action) => {
  // console.log("action ====>", action);
  switch (action.type) {
    // STEPPER

    case ActionType.FAVOURITE_LOADING:
      return { ...state, isLoading: action.payload };

    case ActionType.FAVOURITE_SUCCESS:
      // console.log("FAVOURITE_SUCCESS555555", action.payload);
      return {
        ...state,

        allData: action.payload
      };

    default:
      return { ...state };
  }
};
