import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  Logout: []
};

export default (state = initialState, action) => {
  // console.log("action ====>", action);
  switch (action.type) {
    // STEPPER

    case ActionType.Logout:
      return { ...state, isLoading: true };

    case ActionType.Logout_SUCCESS:
      return {
        ...state,
        isLoading: false,
        Logout: action.payload
      };

    case ActionType.Logout_FAIL:
      return { ...state, isLoading: false };

    default:
      return { ...state };
  }
};
