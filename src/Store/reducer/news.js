import { ActionType } from "../actions";

const initialState = {
  isLoading: false,
  data: {},
  News: []
};

export default (state = initialState, action) => {
  // console.log("action ====>", action);
  switch (action.type) {
    // STEPPER

    case ActionType.News:
      return { ...state, isLoading: true };

    case ActionType.News_SUCCESS:
      return {
        ...state,
        isLoading: false,
        News: action.payload
      };

    case ActionType.News_FAIL:
      return { ...state, isLoading: false };

    default:
      return { ...state };
  }
};
