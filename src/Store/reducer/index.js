import Signup from "./signup";
import Recover from "./recover";
import Login from "./login";
import Liverate from "./liverates";
import News from "./news";
import Convert from "./convert";
import ContactUs from "./contactus";
import Article from "./article";
import Blogs from "./blogs";
import Logout from "./logout";
import Promotions from "./promotions";
import Offers from "./offers";
import Comparisons from "./comparisons";
import Favourite from "./favourite";
import History from "./history";
import SelectedNav from "./navselected";
import FAQS from "./faqs";

export {
  Signup,
  Login,
  Liverate,
  Convert,
  News,
  ContactUs,
  Article,
  Blogs,
  Logout,
  Promotions,
  Offers,
  Comparisons,
  Favourite,
  History,
  SelectedNav,
  Recover,
  FAQS
};
