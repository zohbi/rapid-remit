import { ActionType } from "../actions";

const initialState = {
};

export default (state = initialState, action) => {
  // console.log("action ====>", action);
  switch (action.type) {
    // STEPPER
    case ActionType.SelectedNav:
      return {
        ...state,
        isNavActive: action.payload
      };
    default:
      return { ...state };
  }
};
