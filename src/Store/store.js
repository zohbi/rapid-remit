import { createStore, combineReducers, applyMiddleware } from "redux";

import thunk from "redux-thunk";
import { createLogger } from "redux-logger";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web

import { composeWithDevTools } from "redux-devtools-extension";

// reducers

import {
  Signup,
  Login,
  Liverate,
  Convert,
  News,
  ContactUs,
  Article,
  Blogs,
  Logout,
  Promotions,
  Offers,
  Comparisons,
  Favourite,
  History,
  SelectedNav,
  Recover,
  FAQS
} from "./reducer";

export const rootReducer = combineReducers({
  Signup,
  Login,
  Liverate,
  Convert,
  News,
  ContactUs,
  Article,
  Blogs,
  Logout,
  Promotions,
  Offers,
  Comparisons,
  Favourite,
  History,
  SelectedNav,
  Recover,
  FAQS
});

// export const store = createStore(rootReducer, middleware);

const middleware = [thunk]; //, logger];

const persistConfig = {
  key: "root",
  storage: storage,
  whitelist: [
    'SelectedNav',
  ],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(persistedReducer, applyMiddleware(...middleware));

let persistor = persistStore(store);

export { store, persistor };
