import React from 'react'
import DashboardLayout from '../components/dashboardLayout'
import DashboardLayout2 from '../components/dashboardLayout2'
import '../css/style-dashboardCommonFile.css'
import '../css/Dashboard.css'
import '../css/Dashboard_reviews.css'
import '../css/style-dashboardHistory.css'
import '../css/style-dashboardPromotions(UD).css'
import '../css/style-dashboardPromotions(UD)2.css'
import '../css/style-dashboardTracking.css'
import '../css/style-dashboardDemographics.css'
import $ from 'jquery'


export default class DashboardUser extends React.Component {
    componentDidMount() {
        /*///////////////////////////////////// STARTS/////////////////////////////////////*/
        $(".dashboard1 .divB .divBrow1 .nav ul #li1").click(function(){
            $('.buttonSlide').css({"margin-left":"1px"});
            $('.dashboard1 .divB .divBrow1 .nav ul #li1').css({"color":"#FFFFFF"});
            $('.dashboard1 .divB .divBrow1 .nav ul #li2').css({"color":"#555555"});
            $('.dashboard1 .divB .divBrow1 .nav ul #li3').css({"color":"#555555"});
        })
        $(".dashboard1 .divB .divBrow1 .nav ul #li2").click(function(){
            $('.buttonSlide').css({"margin-left":"76px"});
            $('.dashboard1 .divB .divBrow1 .nav ul #li2').css({"color":"#FFFFFF"});
            $('.dashboard1 .divB .divBrow1 .nav ul #li1').css({"color":"#555555"});
            $('.dashboard1 .divB .divBrow1 .nav ul #li3').css({"color":"#555555"});
        })
        $(".dashboard1 .divB .divBrow1 .nav ul #li3").click(function(){
            $('.buttonSlide').css({"margin-left":"149px"});
            $('.dashboard1 .divB .divBrow1 .nav ul #li3').css({"color":"#FFFFFF"});
            $('.dashboard1 .divB .divBrow1 .nav ul #li1').css({"color":"#555555"});
            $('.dashboard1 .divB .divBrow1 .nav ul #li2').css({"color":"#555555"});
        })
            // menu bar for small screen STARTS
        $(".divBurgerMenu img").click(function(){
            $('.secondVerticalMenu').css({"display":"block"});
        })
        $(".divBurgerMenu #secondVerticalMenu").click(function(){
            $('.secondVerticalMenu').css({"display":"none"});
        })
        /*//////////////////////////////////////////////////////////////////////////*/
        /*///////////////////////////////////// STARTS/////////////////////////////////////*/
        /*//////////////////////////////////////////////////////////////////////////*/

        /*//////////////////////////////////////////////////////////////////////////*/
        /*/////////////////////////////////////HISTORY STARTS/////////////////////////////////////*/
        /*//////////////////////////////////////////////////////////////////////////*/
        $('#question1 .plus .img2UD').hide();
        $('#question2 .plus .img2UD').hide();
        $('#question3 .plus .img2UD').hide();
        $('#question4 .plus .img2UD').hide();
        $('#question5 .plus .img2UD').hide();

        $('#question1 p').slideUp();
        $('#question2 p').slideUp();
        $('#question3 p').slideUp();
        $('#question4 p').slideUp();
        $('#question5 p').slideUp();

        $("#question1 .plus .img1UD").click(function(){
            $('#question1 .plus .img1UD').hide();
            $('#question1 .plus .img2UD').show();
        })
        $("#question1 .plus .img2UD").click(function(){
            $('#question1 .plus .img1UD').show();
            $('#question1 .plus .img2UD').hide();
        })

        $("#question2 .plus .img1UD").click(function(){
            $('#question2 .plus .img1UD').hide();
            $('#question2 .plus .img2UD').show();
        })
        $("#question2 .plus .img2UD").click(function(){
            $('#question2 .plus .img1UD').show();
            $('#question2 .plus .img2UD').hide();
        })

        $("#question3 .plus .img1UD").click(function(){
            $('#question3 .plus .img1UD').hide();
            $('#question3 .plus .img2UD').show();
        })
        $("#question3 .plus .img2UD").click(function(){
            $('#question3 .plus .img1UD').show();
            $('#question3 .plus .img2UD').hide();
        })

        $("#question4 .plus .img1UD").click(function(){
            $('#question4 .plus .img1UD').hide();
            $('#question4 .plus .img2UD').show();
        })
        $("#question4 .plus .img2UD").click(function(){
            $('#question4 .plus .img1UD').show();
            $('#question4 .plus .img2UD').hide();
        })
        $("#question5 .plus .img1UD").click(function(){
            $('#question5 .plus .img1UD').hide();
            $('#question5 .plus .img2UD').show();
        })
        $("#question5 .plus .img2UD").click(function(){
            $('#question5 .plus .img1UD').show();
            $('#question5 .plus .img2UD').hide();
        })


        $("#question1 .plus").click(function(){
            $('#question1 p').slideToggle();
        })
        $("#question2 .plus").click(function(){
            $('#question2 p').slideToggle();
        })
        $("#question3 .plus").click(function(){
            $('#question3 p').slideToggle();
        })
        $("#question4 .plus").click(function(){
            $('#question4 p').slideToggle();
        })
        $("#question5 .plus").click(function(){
            $('#question5 p').slideToggle();
        })
        /*//////////////////////////////////////////////////////////////////////////*/
        /*/////////////////////////////////////HISTORY ENDS/////////////////////////////////////*/
        /*//////////////////////////////////////////////////////////////////////////*/
        /*//////////////////////////////////////////////////////////////////////////*/
        /*/////////////////////////////////////dashboard PROMOTIONS (UD) STARTS/////////////////////////////////////*/
        /*//////////////////////////////////////////////////////////////////////////*/
        $("#bigCard1").hide();
        $("#bigCard2").hide();

        $("#smallCard1Button").click(function(){
            $("#smallCard1").hide();
            $("#smallCard2").hide();
            $("#bigCard1").show();
        })
        $("#bigCard1Cross").click(function(){
            $("#bigCard1").hide();
            $("#smallCard1").show();
            $("#smallCard2").show();
        })

        $("#smallCard2Button").click(function(){
            $("#smallCard1").hide();
            $("#smallCard2").hide();
            $("#bigCard2").show();
        })
        $("#bigCard2Cross").click(function(){
            $("#bigCard2").hide();
            $("#smallCard1").show();
            $("#smallCard2").show();
        })
        /*//////////////////////////////////////////////////////////////////////////*/
        /*/////////////////////////////////////dashboard PROMOTIONS (UD) ENDS/////////////////////////////////////*/
        /*//////////////////////////////////////////////////////////////////////////*/

        /////////////for traking
         // $(function () {
        //     $('#uae-map').JSMaps({
        //       map: 'uae'
        //     });
        //   });
        /////////////for traking
    }

    render() {
        return (
            // <DashboardLayout />
            <DashboardLayout2 />
        )
    }
}