import React from "react";
import HeadingImgLine from "../components/HeadingImgLine";
import RedBlack_Headings from "../components/RedBlack_Headings";
import HeadngPlusPara from "../components/headngPlusPara";
import Footer from "../components/footer";
import "../css/bootstrap.css";
import "../css/style-cookies.css";

export default class Support extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      HeadingImgLine: {
        heading: "Support",
        img: require("../img/termsAndConditions/termsAndConditions.png"),
      },

      RedBlack_Headings: {
        redHeading: "Support",
        blackHeading: "Lorem Ipsum is simply dummy text of the printing.",
      },

      arr: [
        {
          heading: "What is Lorem Ipsum?",
          paragraph:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!",
        },
        {
          heading: "Why Do we use It?",
          paragraph:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!",
        },
        {
          heading: "Where does It come from?",
          paragraph:
            "dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!",
          paragraph2:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere ut fugiat beatae ea eveniet sit quia quis, iste laborum harum, itaque aut odio pariatur. Quos obcaecati suscipit asperiores temporibus nam?",
        },
        {
          heading: "Where can I get some?",
          paragraph:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!. Lpsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lpsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lpsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!",
        },
      ],
    };
  }

  render() {
    return (
      <div>
        <div className="container-fluid cookies">
          <div className="container1">
            <HeadingImgLine
              data={this.state.HeadingImgLine}
              redlineClass="termsAndConditionsRedLine"
            />

            <div className="cookiesContent">
              <RedBlack_Headings data={this.state.RedBlack_Headings} />

              {this.state.arr.map((value, abc) => {
                return <HeadngPlusPara key={abc} data={value} />;
              })}
            </div>
          </div>
        </div>
        <Footer history={this.props.history} />
      </div>
    );
  }
}
