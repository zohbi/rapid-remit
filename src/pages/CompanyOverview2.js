import React from "react";
import "../css/bootstrap.css";
import "../css/style-CompanyOverview.css";
import "../css/font.css";
import TransferWise from "../components/transferWise";
import CompanyOverviewText from "../components/companyOverviewText";
import CompanyOverviewFeatures from "../components/CompanyOverviewFeatures";
import CompanyOverviewMap from "../components/CompanyOverviewMap";
import JohnSlider from "../components/johnSlider";
import BlackDiv from "../components/blackDiv";

export default class CompanyOverview2 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      CompanyOverview: [
        {
          review: "320",
        },
        {
          img: require("../img/CompanyOverview/CompanyOverviewImg1.png"),
          paragraph1:
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Mollitia, quo esse. Magnam nesciunt ab dolor neque cum, corrupti minus rerum nihil voluptas odit perferendis vel. Modi facilis vero cum minima. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus dicta ipsum veniam repellat sint optio sit atque alias impedit placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione. minus sed, aut ratione. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus dicta ipsum veniam sint optio sit atque alias impedit placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus omnis, minus sed, aut . Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus dicta ipsum veniam repellat sint optio sit atque alias impedit placeat eum qui officiis omnis, minus sed, aut ratione. Lorem ipsum dolor sit, amet consectetur adipisicing Repellendus dicta ipsum repellat minus sed, aut ratione. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus excepturi omnis, sint optio sit atque alias impedit placeat eum magnam qui officiis excepturi omnis,",
          paragraph2:
            "Ipsum dolor sit, amet elit. Repellendus dicta ipsum veniam repellat sint optio sit atque alias impedit placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus dicta ipsusm veniam repellat sint optio sit atque alias impedit placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione.",
        },
        {
          RATE: "150",
          TRANSFER_FEE: "AED 5",
          TRANSFER_TYPE: "International",
          CONTACT: "+111 111 111",
          COMPANY_TYPE: "Remmitance Provider",
          MOBILE_APP: "Mobile app",
          KEY_FEATURE: "Lorem Ipsum is simply dummy text",
          DOCUMENTS: "Lorem Ipsum is simply dummy text",
          img: require("../img/CompanyOverview/CompanyOverviewImg2.png"),
        },
        {
          img: require("../img/our-network/invertedcoma.png"),
          img2: require("../img/our-network/john.png"),
          img3: require("../img/our-network/invertedcoma2.png"),
          heading: "Mr.John Musk",
          paragraph:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        },
      ],
    };
  }

  render() {
    return (
      <div className="container-fluid companyOverview">
        <div className="container1">
          <TransferWise data={this.state.CompanyOverview} />
          <div className="companyOverviewContent">
            <CompanyOverviewText data={this.state.CompanyOverview} />
          </div>
          <CompanyOverviewFeatures data={this.state.CompanyOverview} />
          <CompanyOverviewMap />
        </div>

        <div className="container-fluid divBottom">
          <div className="row companyOverviewContentRow4">
            <div className="container1">
              <div className="divHeading">
                <h1>Reviews</h1>
              </div>
              <JohnSlider />
            </div>
          </div>
          <BlackDiv />
        </div>
      </div>
    );
  }
}
