import React, { Fragment } from "react";
import Footer from "./../components/footer";
import ArticleHead from "../components/articleHead";
import ArticleBody from "../components/articleBody";
import "../css/font.css";
import "../css/bootstrap.css";
import "../css/style-articles.css";

export default class Articles extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      articlesData: {
        img1: require("../img/article/articleImg1.png"),
        img2: require("../img/article/articleImg2.png"),
        articlesNumber: "01",
        date: "1TH JUNE,2019",
        heading: "What is Lorem Ipsum?",
        smallPara:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores quos amet saepe odit cum, aspernatur amet quos ratione amet odit ullam eius pariatur.",
        title: "Lorem Ipsum is simply dummy",
        paragraph1:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever book. It has survived not only five centuries, but also the leap into electronic typesetting, since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
        paragraph2:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever book. It has survived not only five centuries, but also the leap into electronic typesetting, since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
        paragraph3: `Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quae laboriosam vero quibusdam autem aperiam
                voluptatem unde ratione, molestias, dolor ex, quia odio fuga. Magni deserunt minima itaque id? Aspernatur,
                a!
                and Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quae laboriosam vero quibusdam autem aperiam
                voluptatem unde ratione, molestias, dolor ex, quia odio fuga. Magni deserunt minima itaque id? Aspernatur,
                a!.
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quae laboriosam vero quibusdam autem aperiam
                voluptatem unde ratione, , dolor ex, quia odio fuga. Magni minima itaque id? Aspernatur, a! Lorem ipsum
                dolor
                sit amet consectetur, adipisicing elit. Quae laboriosam vero quibusdam autem aperiam voluptatem unde
                ratione,
                molestias, dolor ex, quia odio fuga. Magni deserunt minima itaque id? Aspernatur, a! Lorem, ipsum dolor sit
                amet consectetur adipisicing elit. Voluptatibus ipsum, consequuntur veniam eligendi assumenda soluta, et qui
                labore facere optio iste? Numquam rerum provident ex sint accusantium quo blanditiis explicabo! Lorem ipsum
                dolor, sit amet consectetur adipisicing elit. Hic odit praesentium cumque nesciunt! Perspiciatis quis quod
                incidunt beatae quaerat repellendus! Eum ullam accusantium distinctio omnis voluptate et, laboriosam eveniet
                inventore.`,
        paragraph4: `Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quae laboriosam vero quibusdam autem aperiam
                voluptatem unde ratione, molestias, dolor ex, quia odio fuga. Magni deserunt minima itaque id? Aspernatur,
                a!
                and Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quae laboriosam vero quibusdam autem aperiam
                voluptatem unde ratione, molestias, dolor ex, quia odio fuga. Magni deserunt minima itaque id? Aspernatur,
                a!.
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quae laboriosam vero quibusdam autem aperiam
                voluptatem unde ratione, , dolor ex, quia odio fuga. Magni minima itaque id? Aspernatur, a! Lorem ipsum
                dolor
                sit amet consectetur, adipisicing elit. Quae laboriosam vero quibusdam autem aperiam voluptatem unde
                ratione,
                molestias, dolor ex, quia odio fuga. Magni deserunt minima itaque id? Aspernatur, a! Lorem, ipsum dolor sit
                amet consectetur adipisicing elit. Voluptatibus ipsum, consequuntur veniam eligendi assumenda soluta, et qui
                labore facere optio iste? Numquam rerum provident ex sint accusantium quo blanditiis explicabo! Lorem ipsum
                dolor, sit amet consectetur adipisicing elit. Hic odit praesentium cumque nesciunt! Perspiciatis quis quod
                incidunt beatae quaerat repellendus! Eum ullam accusantium distinctio omnis voluptate et, laboriosam eveniet
                inventore.`,
      },
    };
  }
  componentWillMount(){
    if(this.props?.location?.state){
      this.setState({
        articles: this.props?.location?.state
      })
    }
  }
  componentDidMount(){
    window.scroll(0,0)
  }
  render() {
  console.log(this.state.articles)
    return (
      <Fragment>
        <div className="container-fluid article">
          <div className="container1">
            <ArticleHead data={this.state.articles} />
            <ArticleBody data={this.state.articles} />
          </div>
        </div>
        <Footer history={this.props.history} />
      </Fragment>
    );
  }
}
