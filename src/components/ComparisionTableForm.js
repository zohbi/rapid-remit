import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Comparisons } from '../Store/actions';

class RedForm extends React.Component {
  constructor(props) {
    super(props);
    // console.log("ComparisionTableForm", props)
    this.state = {
      from: 'AED',
      to: props.home ? props.home.to : '',
      amount: props.home ? props.home.amount : '',
      from: 'AED',
    };
  }
  onChangeValue = (name, value) => {
    let stateName = name;
    this.setState({
      [stateName]: value,
    });
  };

  componentWillMount() {
    this.props.currency();
  }

  handleDropdown = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  send = () => {
    // console.log('asd ' + JSON.stringify(this.props));
    let data = {
      from: this.state.from,
      to: this.state.to,
      amount: this.state.amount,
      // loading: this.state.signing,
    };
    // console.log('data', data);
    if (data.from == '') {
      this.setState({ error1: 'please fill your from' });
    } else if (data.to == '') {
      this.setState({ error1: 'To field is empty' });
    } else if (data.amount == '') {
      this.setState({ error1: 'Amount field is empty' });
    } else if (parseInt(data.amount) < 0) {
      return alert('Amount must be greater than zero');
    } else {
      // console.log('all done');
      let comparisonData = {
        from: "AED",
        to: this.state.to,
        amount: parseInt(this.state.amount),
      };
      // console.log('check', comparisonData);
      //localStorage.setItem("UserData", JSON.stringify(loginData));
      this.props.getData(comparisonData);
      this.setState({ error1: '' });
      //localStorage.setItem("password", loginData.password);
    }
  };

  render() {
    // console.log('ct6', this.props.home);
    let cData = this.props.currencyData;
    return (
      <Fragment>
        <div className='row' style={{ background: "transparent", padding: "30px 0px" }}>
          <div className='reFormPortionDiv'>
            <div className='redFormItems' id='redFormItem1'>
              {/* <label className='mt8px float-left from'>FROM</label> */}
              <input
                type='text'
                className='input1 form-control textBlack'
                name='UAE'
                placeholder='AED'
                disabled
                value={this.state.from}
                style={{ boxShadow: "3px 5px 11px 3px #e2e2e2e3", height: 50, width: "100%" }}
                onChange={(e) => this.onChangeValue('from', e.target.value)}
              />
            </div>
            <div className='redFormItems' id='redFormItemTo'>
              <p className='align-middle to' style={{ color: "black", textAlign: "center", paddingTop: "15px" }}>TO</p>
            </div>
            <div className='redFormItems margin-Top-comparision-form' id='redFormItem2'>
              {/* <label className='mt8px float-left to' style={{color:"black"}}>TO</label> */}
              <select
                className='dropdownInput'
                style={{ boxShadow: "3px 5px 11px 3px #e2e2e2e3", height: 50 }}
                id='label col-xl-2 col-lg-2 col-md-2 col-sm-5 col-9'
                name='to'
                value={this.state.to}
                onChange={(e) => this.handleDropdown(e)}
              >
                <option value=''>SELECT</option>
                {cData
                  ? cData.map((data) => {
                    return <option value={data}>{data}</option>;
                  })
                  : null}
              </select>
            </div>

            <div className='redFormItems small-w-100 margin-Top-comparision-form' id='redFormItem3'>
              <input
                type='number'
                style={{ boxShadow: "3px 5px 11px 3px #e2e2e2e3", height: 50 }}
                className='input2 form-control textBlack'
                name='Amount'
                value={this.state.amount}
                placeholder='AMOUNT'
                onChange={(e) => this.onChangeValue('amount', e.target.value)}
              />
            </div>

            <div className="margin-Top-comparision-form" style={{ height: "50px" }} >
              <a href='#'>
                <button
                  className='comparision-form-table-btn'
                  onClick={() => this.send()}
                >
                  SEARCH
                </button>
              </a>
            </div>
          </div>
          {this.state.error1 ? (
            <div className="alert alert-danger alert-dismissible fade show">
               {this.state.error1}
            </div>
          ) : (
              " "
            )}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.Comparisons.isLoading,
    data: state.Comparisons.comparisons,
    currencyData: state.Comparisons.currency,
  };
};

const mapDispatchToProps = (dispatch) => ({
  currency: () => dispatch(Comparisons.currency()),
  getData: (obj) => dispatch(Comparisons.Comparisons(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RedForm);
