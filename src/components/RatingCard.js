import React from 'react';
import Flip from 'react-reveal/Flip';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import StarRatings from 'react-star-ratings';
import {
  Comparisons,
  addReviews,
  getReviews,
  updateReviews,
  partnerReviews,
  userReviews,
  Login,
} from '../Store/actions/index';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import "../css/Dashboard_reviews.css"

class RatingCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openModal: false,
      review:'' ,
      id: '',
      rating: 0,
    };
  }

  showRatings(data) {
    // console.log('rrrr', data);
    let array = [];
    for (let i = 1; i <= Math.ceil(data); i++) {
      // console.log('showRatings', i);

      array.push(<i className='fas fa-star red'></i>);
    }

    return array;
  }

  showGray(data) {
    let no = 5 - data;
    let gray = [];
    for (let i = 1; i <= Math.floor(no); i++) {
      // console.log('showGray', i);

      gray.push(<i className='fas fa-star gray'></i>);
    }

    return gray;
  }

  toggleModal = (e, state) => {
    this.setState({
      [state]: !this.state[state],
    });
    e.preventDefault();
  };

  changeRating(newRating, name) {
    // console.log('newwww', newRating, this.state.rating);
    this.setState({ rating: newRating });
  }

  addReviews = () => {
    // console.log('logindata', this.props.UserData);
    let img;
    if (this.props.UserData && this.props.UserData.image) {
      img = this.props.UserData.image;
    }
    let { review, id } = this.state;
    if (review.trim() == '') {
      alert('review text is missing');
    } else {
      let data = {
        img,
        user_id: JSON.parse(localStorage.getItem('UserData'))._id,
        partner_id: this.state.id,
        name: JSON.parse(localStorage.getItem('UserData')).firstName,
        text: this.state.review,
        rating: this.state.rating,
      };
      // console.log('review', data, this.props);
      // this.props.addReviews(data);
      // this.props.getFavouriteList();
    }
  };
  componentDidMount() {
    this.props.reviews();
    this.props.partners();
    this.props.userReview();
  }

  updateReview(id) {
    // console.log('iddd agse', IDBCursorWithValue);
    let { review, rating } = this.state;
    if (review.trim() == '') {
      alert('review is empty');
    } else {
      let data = {
        text: review,
        rating,
      };

      this.props.update(id, data);
      this.props.getFavouriteList();
    }
  }
  removeFavourite = (id) => {
    if (localStorage.getItem('token')) {
      let user_id = JSON.parse(localStorage.getItem('UserData'))._id;
      // console.log(
      //   'user_id',
      //   user_id,
      //   id,
      //   JSON.parse(localStorage.getItem('token')),
      // );

      this.props.removeFavourite(
        user_id,
        id,
        JSON.parse(localStorage.getItem('token'))
      );
    } else {
      window.location = '/registration';
    }
  }
  render() {
    // console.log('state', this.props.data);
    // console.log('logindata', this.props.UserData);
    let partner_review = this.props.partnerReviews;
    let { reviewsData } = this.props;
    // console.log('this.props.data', partner_review);
    const { data } = this.props;
    return (
      <>
        {/* <Flip right opposite cascade duration={3000} mirror> */}
        <div className='column_column'>
          
          {this.props.dashboard && <span style={{float:"right"}}>
            <i style={{cursor:"pointer", fontSize:14, color:"#F40019"}} onClick={() =>this.removeFavourite(this.props.data._id)} class='far fa-window-close'></i>
          </span>}
          <div className='div-img'>
            <img src={data && data.img ? data.img : 'no'} />
          </div>
          <br/>
          <br/>
          <br/>
          {/* <h1 className='Lato_Black' style={{ textTransform: 'capitalize', fontSize:16, marginTop: 10 }}>
            {data.name}
          </h1> */}
          {this.showRatings(partner_review[data.name])}
          {this.showGray(partner_review[data.name])}
          <div className='rating-card-bottom'>
            <div>
              <div className='review-div'>
                <span className='review'>REVIEWS</span>
                <span className='three-twenty'>
                  {this.props.review
                    ? this.props.review.length
                    : this.props.data && this.props.data.reviews
                    ? this.props.data.reviews.length
                    : 0}
                </span>
              </div>
            </div>

            <div className='two-buttons'>
              <div className='two-btn-div'>
                {localStorage.getItem('token') ? (
                  <button
                    className='add-or-edit-review'
                    onClick={() => {
                      this.setState({
                        openModal: true,
                        id: this.props.data._id,
                      });
                      let check = reviewsData.filter(
                        (f) => f.partner_id == this.props.data._id
                      )[0];
                      // console.log('check', check);

                      if (check && check.text) {
                        this.setState({
                          review: check.text,
                          rating: check.rating,
                        });
                      }
                    }}
                  >
                    {reviewsData.filter(
                      (f) => f.partner_id == this.props.data._id
                    )[0] ? (
                      <span>
                        <i class='far fa-edit'></i> Review
                      </span>
                    ) : (
                      <span>
                        <i class='far fa-plus-square'></i> Review
                      </span>
                    )}
                  </button>
                ) : null}
              </div>
              {/* <button
                      className="add-or-edit-review" > <span><i class="far fa-plus-square"></i> Review</span></button> */}
              {/* <a href='CompanyOverview.html' target='_blank'> */}
                <Link to={`/partner/${this.props.data._id}`}>
                  <p className='btn-see-details'>SEE DETAILS</p>
                </Link>
              {/* </a> */}
            </div>
          </div>
        </div>
        {/* </Flip> */}

        <Modal
          className='modal-dialog-centered'
          isOpen={this.state.openModal}
          toggle={(e) => this.toggleModal(e, 'openModal')}
        >
          <ModalHeader>
            <h5 className='modal-title' id='editModalLabel'>
              REVIEWS
            </h5>
            <StarRatings
              rating={this.state.rating}
              starRatedColor='red'
              changeRating={(sss) => this.changeRating(sss)}
              numberOfStars={5}
              name='rating'
            />
          </ModalHeader>
          <ModalBody>
            <form id="review-rating-card" className='edit-form'>
              <div className='form-group mx-sm-3'>
                <textarea
                  rows='5'
                  name='review'
                  style={{color:"black"}}
                  placeholder="Leave review..."
                  value={this.state.review}
                  onChange={(e) => this.setState({ review: e.target.value })}
                />
              </div>
              <div className='mx-sm-3'>
              <button
                type='submit'
                // className='edit-btn-md btn btn-danger mb-2'
                className="rating-card-btn"
                onClick={(e) => this.toggleModal(e, 'openModal')}
              >
                Cancel
              </button>{' '}
              <button
                type='submit'
                // className='edit-btn-md btn btn-info mb-2'
                className="rating-card-btn"
                value={this.state.review}
                onChange={(e) => this.setState({ review: e.target.value })}
                onClick={(e) => {
                  this.toggleModal(e, 'openModal');
                  let check = reviewsData.filter(
                    (f) => f.partner_id == this.props.data._id
                  )[0];
                  check && check._id
                    ? this.updateReview(check._id)
                    : this.addReviews();
                }}
              >
                Submit
              </button>
              </div>
            </form>
          </ModalBody>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    table: state.Comparisons.table,
    comparse: state.Comparisons.comparisons,
    reviewsData: state.Offers.reviews,
    partnerReviews: state.Offers.partner_review,
    UserData: state.Login.Login,
  };
};

const mapDispatchToProps = (dispatch) => ({
  delTable: bindActionCreators(Comparisons.tableDel, dispatch),
  addReviews: bindActionCreators(addReviews, dispatch),
  reviews: bindActionCreators(getReviews, dispatch),
  update: bindActionCreators(updateReviews, dispatch),
  partners: bindActionCreators(partnerReviews, dispatch),
  userReview: bindActionCreators(userReviews, dispatch),
  removeFavourite: bindActionCreators(Comparisons.removeFavourite, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(RatingCard);
