import React from 'react';
import '../css/bootstrap.css';
import '../css/style-about.css';
import RubberBand from "react-reveal/RubberBand"
import Roll from "react-reveal/Roll"
  
export default class Aboutrow_4 extends React.Component{

  render() {
      // console.log("render props.data" , this.props.data);
    return (
        <div>

            <div className="container-fluid about-third-container ">
                <div className="row m-0">
                    <div className="red-background col-xl-5 col-lg-5 col-md-6 col-sm-7 col-10">
                  <div className="red-background-content">
                    <Roll left opposite cascade duration={2000}><h6 className="Lato_Black">{this.props?.whatdata?.whattitle || "No Title"}</h6></Roll>
                        <h4 className="Lato_Black">{this.props?.whatdata?.whatheading || "No Heading"}</h4>
                        <p className="Lato_Regular">{this.props?.whatdata?.whattext || "No Description"}</p>
                       {/* <RubberBand> <button className="more-info Lato_Black">More Info</button></RubberBand> */}
                       </div>
                    </div>
                </div>
            </div>

        </div>
    )
  }
}