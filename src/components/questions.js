import React from "react";
import "../css/bootstrap.css";
import "../css/style-FAQs.css";
// import '../jquery/jquery.working'
// import '../jquery/code.js'

export default class Questions extends React.Component {
  render() {
    return (
      <div className="question" id="question8">
        <h6>{this.props.data.heading}</h6>
        <h5 className="plus">{this.props.data.plus}</h5>
        <p>{this.props.data.paragraph}</p>
      </div>
    );
  }
}
