import React from 'react';
import '../css/bootstrap.css';
import '../css/style-FAQs.css';
import Questions from './questions';
import Fade from 'react-reveal/Fade';
import Footer from '../components/footer';
import HeadingImgLine from './HeadingImgLine';
import RedBlack_Headings from './RedBlack_Headings';
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
  AccordionItemState,
} from 'react-accessible-accordion';
// Demo styles, see 'Styles' section below for some notes on use.
import 'react-accessible-accordion/dist/fancy-example.css';
import { connect } from "react-redux"
import { getFAQS } from "../Store/actions/userAuth"
class FAQs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      HeadingImgLine: {
        heading: 'Frequently Asked Questions',
        img: require('../img/FAQs/FAQsImg1.png'),
      },

      RedBlack_Headings: {
        redHeading: 'Frequently Asked Questions',
        blackHeading:
          'Everything you could possibly want to know about the Us.',
      },

      arr: [
        {
          heading: 'What is RapidRemit?',
          plus: '+',
          paragraph:
            'Rapid Remit isn’t a payment service provider itself, meaning we can’t physically send a payment on your behalf. Instead we can be used to compare payment service providers to find it the most favourable foreign exchange rates, cheapest transfer fees, shortest transfer speed and even if payments are open to further fee deductions even after it has been sent.',
        },
        {
          heading: 'none',
          paragraph: 'none',
        },
        {
          heading: 'none',
          paragraph: 'none',
        },
        {
          heading: 'none',
          paragraph: 'none',
        },
      ],
      questionsAndAnswers: []

    };
  }
  componentDidUpdate(prevprops, prevstate) {
    if (prevprops.faqs !== this.props.faqs) {
      this.setState({
        questionsAndAnswers: this.props.faqs
      })
    }
  }
  componentDidMount() {
    this.props.getFAQS()
  }
  render() {
    return (
      <div>
        <div className='container-fluid FAQs'>
          <div className='container1'>
            <Fade right>
              <HeadingImgLine data={this.state.HeadingImgLine} />
            </Fade>
            <div className='FAQsContent'>
              <Fade left delay={250}>
                <RedBlack_Headings data={this.state.RedBlack_Headings} />
              </Fade>
              <div class='questions'>
                {this.state.questionsAndAnswers.length
                  ? this.state.questionsAndAnswers.map((value, i) => (
                    <Accordion
                      allowZeroExpanded={true}
                      onChange={(change) => {
                        this.setState({
                          selected_index: change,
                        });
                      }}
                      key={i}
                    >
                      <div class='question'>
                        <AccordionItem
                          onChange={(a) => {
                            // console.log('CHANGE OF ACCORDION', a);
                          }}
                        >
                          <AccordionItemState>
                            {({ expanded }) => (
                              <React.Fragment>
                                <AccordionItemHeading>
                                  <AccordionItemButton>
                                    <h6>{value.title}</h6>
                                    <span>
                                      {expanded ? (
                                        <i className='fa fa-minus'></i>
                                      ) : (
                                          <i className='fa fa-plus'></i>
                                        )}
                                    </span>
                                  </AccordionItemButton>
                                </AccordionItemHeading>
                                <AccordionItemPanel>
                                  <p>{value.description}</p>
                                </AccordionItemPanel>
                              </React.Fragment>
                            )}
                          </AccordionItemState>
                        </AccordionItem>
                      </div>
                    </Accordion>
                  ))
                  : ''}
              </div>
            </div>
          </div>
        </div>
        <Footer history={this.props.history} />
      </div>
    );
  }
}


const mapStateToProps = ({
  FAQS: { faqs, isFaqsLoading },
}) => ({
  faqs,
  isFaqsLoading
});

export default connect(mapStateToProps, {
  getFAQS,
})(FAQs);

