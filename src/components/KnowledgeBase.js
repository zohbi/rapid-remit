import React, { Component } from "react";
import "../css/bootstrap.css";
import "../css/style-footer.css";
import "../css/style-news+articles.css";
import Footer from "./footer";
import Newstab from "./Newstab";
import Articletab from "./articletab";
import Blogtab from "./tabblogs";
import $ from "jquery";
import Fade from "react-reveal/Fade";
import Tada from "react-reveal/Tada";
import Flip from "react-reveal/Flip";
import { setInterval } from "timers";
import ContentLoader from "react-content-loader";

export default class KnowledgeBase extends Component {
  constructor(props) {
    super(props);

    this.state = {
      arr: [
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "2123Lorem ipsum dolor sit amet consectetur, adipisicing elit. ipsum dolor sit amet consectetur, Rem laudantium itaque totam repudiandae a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat. Eius excepturi qui nam! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem laudantium itaque totam repudiandaes a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat.",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "L333orem ipsum dolor sit amet consectetur, adipisicing elit. ipsum dolor sit amet consectetur, Rem laudantium itaque totam repudiandae a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat. Eius excepturi qui nam! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem laudantium itaque totam repudiandaes a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat.",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. ipsum dolor sit amet consectetur, Rem laudantium itaque totam repudiandae a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat. Eius excepturi qui nam! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem laudantium itaque totam repudiandaes a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat.",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. ipsum dolor sit amet consectetur, Rem laudantium itaque totam repudiandae a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat. Eius excepturi qui nam! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem laudantium itaque totam repudiandaes a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat.",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. ipsum dolor sit amet consectetur, Rem laudantium itaque totam repudiandae a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat. Eius excepturi qui nam! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem laudantium itaque totam repudiandaes a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat.",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. ipsum dolor sit amet consectetur, Rem laudantium itaque totam repudiandae a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat. Eius excepturi qui nam! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem laudantium itaque totam repudiandaes a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat.",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. ipsum dolor sit amet consectetur, Rem laudantium itaque totam repudiandae a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat. Eius excepturi qui nam! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem laudantium itaque totam repudiandaes a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat.",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. ipsum dolor sit amet consectetur, Rem laudantium itaque totam repudiandae a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat. Eius excepturi qui nam! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem laudantium itaque totam repudiandaes a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat.",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. ipsum dolor sit amet consectetur, Rem laudantium itaque totam repudiandae a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat. Eius excepturi qui nam! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem laudantium itaque totam repudiandaes a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat.",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. ipsum dolor sit amet consectetur, Rem laudantium itaque totam repudiandae a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat. Eius excepturi qui nam! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem laudantium itaque totam repudiandaes a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat.",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. ipsum dolor sit amet consectetur, Rem laudantium itaque totam repudiandae a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat. Eius excepturi qui nam! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem laudantium itaque totam repudiandaes a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat.",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Lorem ipsum dolor sit amet consectetur, adipisicing elit. ipsum dolor sit amet consectetur, Rem laudantium itaque totam repudiandae a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat. Eius excepturi qui nam! Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rem laudantium itaque totam repudiandaes a eos praesentium rerum qui aperiam harum, quae possimus assumenda enim tempore repellat.",
        },
      ],
      articlearr1: [
        {
          heading: "Article",
          title:
            "Title...............................................................",
          text:
            "This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.",
        },
        {
          heading: "Article",
          title:
            "Title...............................................................",
          text:
            "This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.",
        },
        {
          heading: "Article",
          title:
            "Title...............................................................",
          text:
            "This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.",
        },
      ],
      articlearr2: [
        {
          heading: "Article",
          title:
            "Title...............................................................",
          text:
            "This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.",
        },
        {
          heading: "Article",
          title:
            "Title...............................................................",
          text:
            "This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.",
        },
        {
          heading: "Article",
          title:
            "Title...............................................................",
          text:
            "This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.",
        },
      ],
      articlearr3: [
        {
          heading: "Article",
          title:
            "Title...............................................................",
          text:
            "This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.",
        },
        {
          heading: "Article",
          title:
            "Title...............................................................",
          text:
            "This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.",
        },
        {
          heading: "Article",
          title:
            "Title...............................................................",
          text:
            "This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.",
        },
      ],
      blog: [
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Ldorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet",
        },
        {
          span: "NEWS",
          span2: "13th June, 2019",
          para:
            "Title....................................................................",
          para2:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet",
        },
      ],
    };
  }
  componentDidMount() {
    this.props.SetIsNavActive("/KnowledgeBase")

    if (this.props.location.state && this.props.location.state.selected) {
      // console.log("this.props.location.state.selected", this.props.location.state.selected)
      if (this.props.location.state.selected === 3) {
        $("#newsa").hide();
        $("#articlesa").hide();
        $("#blogsb").hide();

        $("#newsTab").hide();
        $("#articlesTab").hide();
      } else if (this.props.location.state.selected === 2) {
        $("#newsa").hide();
        $("#articlesb").hide();
        $("#blogsa").hide();

        $("#newsTab").hide();
        $("#blogsTab").hide();
      } else {
        $("#newsb").hide();
        $("#articlesa").hide();
        $("#blogsa").hide();

        $("#articlesTab").hide();
        $("#blogsTab").hide();
      }
    } else {
      $("#newsb").hide();
      $("#articlesa").hide();
      $("#blogsa").hide();

      $("#articlesTab").hide();
      $("#blogsTab").hide();
    }
    $("#newsb").click(function () {
      $("#newsb").hide();
      $("#newsa").show();
      $("#articlesa").hide();
      $("#articlesb").show();
      $("#blogsa").hide();
      $("#blogsb").show();

      $("#articlesTab").hide();
      $("#blogsTab").hide();
      $("#newsTab").show();
    });

    $("#articlesb").click(function () {
      $("#newsa").hide();
      $("#newsb").show();
      $("#articlesb").hide();
      $("#articlesa").show();
      $("#blogsa").hide();
      $("#blogsb").show();

      $("#newsTab").hide();
      $("#blogsTab").hide();
      $("#articlesTab").show();
    });

    $("#blogsb").click(function () {
      $("#newsa").hide();
      $("#newsb").show();
      $("#articlesa").hide();
      $("#articlesb").show();
      $("#blogsb").hide();
      $("#blogsa").show();

      $("#newsTab").hide();
      $("#articlesTab").hide();
      $("#blogsTab").show();
    });

    if (
      window.location.pathname == "/"
      // window.location.pathname == "/AboutUs"
    ) {
      //  console.log("ASsads")
      document.getElementById("routerNav").style.display = "none";
      //  document.getElementById('navigationIndex').style.display="block"
    } else {
      //  console.log("nhi hy")
      document.getElementById("routerNav").style.display = "block";
      //  document.getElementById('navigationIndex').style.display="none"
    }
    this.props.getArticles();
    this.props.getNews();
  }

  componentWillReceiveProps(nextProps) {
    let { News, Article } = nextProps;
    this.setState({ newsData: News, ArticleData: Article });
    // console.log(this.state.newsData, this.state.ArticleData);
  }

  componentDidUpdate(prevProps, nextProps) {
    if (this.props?.location?.state?.selected) {
      if (prevProps.location?.state?.selected !== this.props.location?.state?.selected) {
        if (this.props.location.state.selected === 3) {
          
          $("#newsa").hide();
          $("#articlesa").hide();
          $("#blogsb").hide();
          $("#newsTab").hide();
          $("#articlesTab").hide();


          
          $("#newsb").show();
          $("#articlesb").show();
          $("#blogsa").show();
          $("#blogsTab").show();

        } else if (this.props.location.state.selected === 2) {
          
          $("#newsa").hide();
          $("#articlesb").hide();
          $("#blogsa").hide();
          $("#blogsTab").hide();
          $("#newsTab").hide();


          
          $("#newsb").show();
          $("#articlesa").show();
          $("#blogsb").show();
          $("#articlesTab").show();

      
        } else {
          
          $("#newsb").hide();
          $("#articlesa").hide();
          $("#blogsa").hide();
          $("#articlesTab").hide();
          $("#blogsTab").hide();


          
          $("#newsa").show();
          $("#articlesb").show();
          $("#blogsb").show();
          $("#newsTab").show();
        }
      }
    }
  }

  render() {
    // console.log(this.props)
    return (
      <div className="coooom">
        <div className="newsK">
          <div className="newsDivTop">
            <div className="row">
              <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <Flip right opposite cascade duration={3000} mirror>
                  <h1>KNOWLEDGE BASE</h1>
                </Flip>
              </div>
            </div>
            <div className="container1 newsPartB">
              <div className="row">
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#firstNews" id="firstNews news1">
                    <div className="divNews" id="newsa">
                      <h2>NEWS</h2>
                    </div>
                  </a>
                  <a href="#firstNews" id="firstNews news2">
                    <div className="divNews" id="newsb">
                      <div className="blackBelt">
                        <h3 className="blackBeltH3">NEWS</h3>
                      </div>
                    </div>
                  </a>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#firstArticle" id="firstArticle">
                    <div className="divNews" id="articlesa">
                      <div>
                        <h2>ARTICLES</h2>
                      </div>
                    </div>
                  </a>
                  <a href="#firstArticle" id="firstArticle">
                    <div className="divNews" id="articlesb">
                      <div className="blackBelt">
                        <h3 className="blackBeltH3">ARTICLES</h3>
                      </div>
                    </div>
                  </a>
                </div>
                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#firstBlog" id="firstBlog">
                    <div className="divNews" id="blogsa">
                      <div>
                        <h2>BLOGS</h2>
                      </div>
                    </div>
                  </a>
                  <a href="#firstBlog" id="firstBlog">
                    <div className="divNews" id="blogsb">
                      <div className="blackBelt">
                        <h3 className="blackBeltH3">BLOGS</h3>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
              {/* <!---- row ends ----> */}
            </div>
          </div>
          {/* <!---- newsPartB ends ----> */}
        </div>
        {/* <!---- news ends ----> */}

        <div className="container-fluid newsK">
          <div className="container-fluid newsPartTwo" id="newsTab">
            <div className="container1">
              <div className="row no-gutters">
                {/* {this.state.newsData ? this.state.newsData.map((value, news) => {
                  return <Newstab key={news} data={value} history={this.props.history} />;
                }) : ""} */}
                {this.state.newsData && this.state.newsData.length ? (
                  this.state.newsData.map((value, key) => {
                    // console.log("newsdata", value, key);
                    return (
                      <Newstab
                        key1={key + 1}
                        news={value.news}
                        heading={value.heading}
                        newsDetails={value.newsDetails}
                        author={value.newsAuthor}
                        createdAt={value.createdAt}
                        updatedAt={value.updatedAt}
                        history={this.props.history}
                      />
                    );
                  })
                ) : this.props.isLoading ? (
                  <ContentLoader
                    className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    height={900}
                    width={1360}
                    speed={2}
                    primaryColor="#F2F2F2"
                    secondaryColor="lightgray"
                  >
                    {/* <rect x="30" y="20" rx="0" ry="0" width="130" height="23" /> */}
                    <rect
                      x="30"
                      y="20"
                      rx="8"
                      ry="8"
                      width="450"
                      height="300"
                    />
                    <rect
                      x="500"
                      y="20"
                      rx="8"
                      ry="8"
                      width="450"
                      height="300"
                    />
                    <rect
                      x="970"
                      y="20"
                      rx="8"
                      ry="8"
                      width="450"
                      height="300"
                    />
                    <rect
                      x="30"
                      y="340"
                      rx="8"
                      ry="8"
                      width="450"
                      height="300"
                    />
                    <rect
                      x="500"
                      y="340"
                      rx="8"
                      ry="8"
                      width="450"
                      height="300"
                    />
                    <rect
                      x="970"
                      y="340"
                      rx="8"
                      ry="8"
                      width="450"
                      height="300"
                    />
                  </ContentLoader>
                ) : (
                      ""
                    )}
              </div>
            </div>
          </div>

          <div className="container-fluid newsPartThree" id="articlesTab">
            {this.state.ArticleData && this.state.ArticleData.length ? (
              <div className="container1">
                <div className="row justify-content-center">
                  {/* <div className="card-deck"> */}
                    {this.state.ArticleData && this.state.ArticleData.length
                      ? this.state.ArticleData.map((value, article) => {
                        return (
                          <Articletab
                            key={article}
                            articleNumber={article}
                            data={value}
                            // title={value.title}
                            // text={value.text}
                            history={this.props.history}
                          />
                        );
                      })
                      : ""}
                  </div>
                {/* </div> */}
              </div>
            ) : (
                <div className="container1">
                  <div className="row">
                    <div className="default-div">
                      <h4>No Articles</h4>
                    </div>
                  </div>
                </div>
              )}
          </div>
          {/* <!---- newsPartThree ends ----> */}

          <div className="container-fluid newsPartFour" id="blogsTab">
            <div className="container1">
              <Blogtab history={this.props.history} />
            </div>
          </div>
        </div>
        {/* <!---- container-fluid news ends ----> */}
        <Footer history={this.props.history} />
      </div>
    );
  }
}
