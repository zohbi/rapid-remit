import React from 'react';


export default class CompanyOverviewMap extends React.Component {
    render() {
        return (
            <div className="row Map">
                <div className="locate col-xl-1 col-lg-1 col-md-12 col-sm-12 col-12">
                    <h1>Locate Near Exchange</h1>
                </div>
                <div className="col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12">
                    <div className="mapouter">
                        <div className="gmap_canvas">
                            <iframe id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20Karachi&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div>
                        <img src={require("../img/locate-icon.png")} className="location" />
                        <img src={require("../img/locate-icon.png")} className="location2" />
                    </div>
                </div>
            </div>
        )
    }
}