import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Comparisons } from '../Store/actions';

class RedForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      from: 'AED',
      to: '',
      amount: '',
      from: 'AED',
    };
  }
  onChangeValue = (name, value) => {
    let stateName = name;
    this.setState({
      [stateName]: value,
    });
  };

  componentWillMount() {
    this.props.currency();
  }

  handleDropdown = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  send = () => {
    // console.log('asd ' + JSON.stringify(this.props));
    let data = {
      from: this.state.from,
      to: this.state.to,
      amount: this.state.amount,
      // loading: this.state.signing,
    };
    // console.log('data', data);
    if (data.from == '') {
      this.setState({ error1: 'please fill your from' });
    } else if (data.to == '') {
      this.setState({ error1: 'please fill your to' });
    } else if (data.amount == '') {
      this.setState({ error1: 'please fill your amount' });
    } else if (parseInt(data.amount) < 0) {
      return alert('Amount must be greater than zero');
    } else {
      // console.log('all done');
      let comparisonData = {
        from: this.state.from,
        to: this.state.to,
        amount: parseInt(this.state.amount),
      };
      // console.log('check', comparisonData);
      //localStorage.setItem("UserData", JSON.stringify(loginData));
      this.props.getData(comparisonData);
      //localStorage.setItem("password", loginData.password);
    }
  };

  render() {
    // console.log('ct6', this.props.currencyData, this.props.isLoading);
    let cData = this.props.currencyData;
    return (
      <Fragment>
        <div className='row comparision-form-row'>
          <div className='reFormPortionDiv'>
            <div className='redFormItems' id='redFormItem1'>
              <label className='mt8px float-left from'>FROM</label>
              <input
                type='text'
                className='input1 form-control textBlack'
                name='UAE'
                placeholder='AED'
                value={this.state.from}
                onChange={(e) => this.onChangeValue('from', e.target.value)}
              />
            </div>
            <div className='redFormItems' id='redFormItem2'>
              <label className='mt8px float-left to'>TO</label>
              <select
                className='dropdownInput'
                id='label col-xl-2 col-lg-2 col-md-2 col-sm-5 col-9'
                name='to'
                onChange={(e) => this.handleDropdown(e)}
              >
                <option value=''>Select</option>
                {cData
                  ? cData.map((data) => {
                      return <option value={data}>{data}</option>;
                    })
                  : null}
              </select>
            </div>

            <div className='redFormItems' id='redFormItem3'>
              <input
                type='number'
                className='input2 form-control textBlack'
                name='Amount'
                placeholder='Amount'
                onChange={(e) => this.onChangeValue('amount', e.target.value)}
              />
            </div>

            <div className='redFormItems' id='redFormItem4'>
              <a href='#'>
                <button
                  className='comparision-form-btn'
                  onClick={() => this.send()}
                >
                  Search
                </button>
              </a>
            </div>
          </div>

          {/* <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 label">
          <label className="mt8px float-left">FROM</label>
          <input
            type="text"
            className="input1 form-control textBlack"
            name="UAE"
            placeholder="AED"
            value={this.state.from}
            onChange={e => this.onChangeValue("from", e.target.value)}
            value={this.state.from}
          />
        </div>

        <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 label">
          <label className="mt8px float-left">TO</label>
          <select
            className="dropdownInput"
            id="label col-xl-2 col-lg-2 col-md-2 col-sm-5 col-9"
            name="to"
            onChange={e => this.handleDropdown(e)}
          >
            <option value="">select</option>
            {cData
              ? cData.map(data => {
                  return <option value={data}>{data}</option>;
                })
              : null}
          </select>
        </div>

        <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 label">
          <input
            type="text"
            className="input2 form-control textBlack"
            name="Amount"
            placeholder="Amount"
            onChange={e => this.onChangeValue("amount", e.target.value)}
          />
        </div>

        <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 label">
          <a href="#">
            <button className="comparision-form-btn " onClick={() => this.send()}>
              Search
            </button>
          </a>
        </div>
         */}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.Comparisons.isLoading,
    data: state.Comparisons.comparisons,
    currencyData: state.Comparisons.currency,
  };
};

const mapDispatchToProps = (dispatch) => ({
  currency: () => dispatch(Comparisons.currency()),
  getData: (obj) => dispatch(Comparisons.Comparisons(obj)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RedForm);
