import React from 'react';
import '../css/bootstrap.css';
import '../css/font.css';
import '../css/style.css';
import Jello from 'react-reveal/Jello';
import Tada from 'react-reveal/Tada';
import Zoom from 'react-reveal/Zoom';
import { connect } from 'react-redux';
import Footer from '../components/footer';
import { bindActionCreators } from 'redux';
import IndexNavigation from './indexNavigation';
import RubberBand from 'react-reveal/RubberBand';
import { Comparisons, getTestimomials, News, getServices, getNetworkHeading, getWhyChooseUs } from '../Store/actions';
import { Carousel } from 'react-responsive-carousel';
import { Link } from "react-router-dom";
import MoneyGram from "../img/our-network/MoneyGram g.png"
import UAE from "../img/our-network/UAEg.png"
import ExpressMoney from "../img/our-network/ExpressMoney.png"
import LuluExchange from "../img/our-network/luluExchange.png"
import IntroVideo from "../img/intro.mp4"
import HTMLPARSER from "react-html-parser"
// import {Skeleton } from "antd"

const ourNetwork = [
  { image: MoneyGram },
  { image: LuluExchange },
  { image: UAE },
  { image: ExpressMoney },
]

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      height: window.innerHeight,
      width: window.innerWidth,
      services: [
        {
          // bgHeading: "User",
          bgHeading: 'Personal',
          img: require('../img/home/service1.jpg'),
          number: 1,
          // smallHeading: "USER",
          smallHeading: 'PERSONAL',
          description:
            'Through our unique comparison service we make your remittance experience easier and convenient. Saving you time and money. Our service provides you have all the information at your fingertips ensuring you get the best deal when you send money to their loved ones, friends and family.',
          bgNumber: null,
        },
        {
          bgHeading: 'Business',
          img: require('../img/home/service2.jpg'),
          number: 2,
          smallHeading: 'BUSINESS',
          description:
            'Reach out to a highly valuable and growing remittance audience. Our service creates a marketplace for money transfer providers to connect for individuals and business users to access your service in real time through a convenient and user friendly tool. Our simple and user friendly integration tool make it easier for your company to market your services and reach new customers base.',
          bgNumber: null,
        },
      ],
    };
    this.updateDimensions = this.updateDimensions.bind(this);
  }
  componentDidMount() {
    if (
      window.location.pathname == '/' ||
      window.location.pathname == '/AboutUs'
    ) {
      document.getElementById('routerNav').style.display = 'none';
    } else {
      document.getElementById('routerNav').style.display = 'block';
    }
    // console.log(this.state.height);
    // Additionally I could have just used an arrow function for the binding `this` to the component...
    window.addEventListener('resize', this.updateDimensions);

  }

  componentDidUpdate(prevprops, prevstate) {
    if (prevprops.whyChooseUs !== this.props.whyChooseUs) {
      this.setState({
        whyChooseUsData: this.props.whyChooseUs
      })
    }
    if (prevprops.services !== this.props.services) {
      this.setState({
        servicesData: this.props.services
      })
    }
    if (prevprops.networkHeading !== this.props.networkHeading) {
      this.setState({
        networkHeadingData: this.props.networkHeading
      })
    }
  }
  updateDimensions() {
    this.setState({
      height: window.innerHeight,
      width: window.innerWidth,
    });
    // console.log(this.state);
  }
  updateWindowDimensions() {
    // this.setState({ width: window.innerWidth, height: window.innerHeight });
    this.forceUpdate();

    // console.log('updateWindowDimensions');
    // this.setState(
    //    {
    //      this.state.width= window.innerWidth
    //      this.state.height= window.innerHeight
    //     //  this.forceUpdate()
    //   }
    // )
    // this.forceUpdate()
  }
  componentWillMount() {
    this.props.getData();
    this.props.getTestimomials();
    this.props.getNews();
    this.props.getNetworkHeading()
    this.props.getServices()
    this.props.getWhyChooseUs()
  }
  render() {
    // console.log('conform news', this.state.whyChooseUsData, this.state.networkHeadingData, this.state.servicesData);
    const { whyChooseUsData, networkHeadingData } = this.state
    let news = this.props.News;
    let array = [];
    let { testimonial } = this.props;
    // console.log('test', this.props.testimonial);
    let { part } = this.props;
    if (part && part.length > 0) {
      for (let i = 0; i < 4; i++) {
        // console.log('props check kar', part[i].img);
        array = [...array, part[i].img];
      }
    }

    return (
      <div className='ReactDiv-main-div'>
        <div className='ReactDiv'>
          <IndexNavigation data={true} history={this.props.history} />
          {/* <!-- ---------WHYCHOOSEUS STARTS--------- --> */}
          <div className='container-fluid whyChooseUs' id='whyCHooseUs'>
            <div className='dropdown2 whyChooseUsFistDropdown2'>
              <h1>01</h1>
              {/* <h5>WHY CHOOSE US</h5> */}
              <Jello>
                <h2>Why Choose Us</h2>
              </Jello>
            </div>
            <Zoom Top>
              <div className='container1'>
                <div className='row'>
                  <div className='col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12'>
                    {whyChooseUsData && whyChooseUsData.para_1 && HTMLPARSER(whyChooseUsData.para_1)}
                    {/* <p>
                      RapidRemit is a transparent and comprehensive money
                      transfer comparison platform, applying real-time
                      innovative digital solutions. Through our network of
                      remittance companies, we deliver savings and putt users in
                      control of the transfer experience. Creating a happy
                      remittance experience for the people of UAE and the
                      region.
                    </p>
                    <p>
                      With a few clicks, we provide all your money transfer
                      services options, allowing you to compare which remittance
                      providers offers the best value for your money. Users can
                      compare, exchange rates, transfer fees, and other
                      information from the most well-known and popular money
                      transfer service providers, all in one place.
                    </p> */}
                    {/* <a href="#">
                      <span className="readMore">+Read More..</span>
                    </a> */}
                  </div>
                  <div className='col-xl-4 col-lg-4 col-md-4 col-sm-5 col-12'>
                    <div className='divTextBackground'>
                      <div className='picDiv1'>
                        <div className='picDiva'></div>
                        <div className='picDivb'>
                          <img src={require('../img/home/building1.png')} />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className='col-xl-4 col-lg-4 col-md-8 col-sm-7 col-12'>
                    {whyChooseUsData && whyChooseUsData.para_2 && HTMLPARSER(whyChooseUsData.para_2)}
                    {/* <p>
                      We use real-time data feeds to compare the leading money
                      transfer providers. We do the hard work for you, meaning
                      you can transfer money online with confidence. Why look
                      elsewhere save money and time with RapidRemit.
                    </p>
                    <p>
                      RapidRemit does not charge any fees to its users or money
                      transfer providers and hence there is no conflict of
                      interest and we are able to provide unbiased options to
                      our users.
                    </p>
                    <p>
                      ‘Our app provides additional unique features and special
                      promotions – so please download’
                    </p> */}
                  </div>
                </div>
              </div>
            </Zoom>
            <div className='img'>
              <img src={require('../img/home/verticalLine2.png')} />
            </div>
            <div className='dropdown2'>
              <h1>02</h1>
              <RubberBand>
                <h2>Services</h2>
              </RubberBand>
            </div>
          </div>
          {/* <!-- ---------WHYCHOOSEUS ENDS--------- --> */}
          <div className='container-fluid services'>
            {/* <!-- ---------SERVICES STARTS--------- --> */}
            <Zoom Top>
              <div className='container1'>
                <div className='row'></div>
                <div className='row rowServices'>
                  <div className='col-xl-2 col-lg-2 col-md-2 d-md-block d-sm-none col-sm-0 col-12'>
                    <Zoom Left>
                      <h1>Services</h1>
                    </Zoom>
                  </div>
                  <div
                    className='col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12'
                    style={{ padding: '0px' }}
                  >
                    <div
                      id='carouselExampleControls'
                      className='carousel slide'
                      data-ride='carousel'
                    >
                      <div className='carousel-controls'>
                        <a
                          className='carousel-control-prev'
                          href='#carouselExampleControls'
                          role='button'
                          data-slide='prev'
                        >
                          <span
                            className='carousel-control-prev-icon'
                            aria-hidden='true'
                          ></span>
                          <span className='sr-only'>Previous</span>
                        </a>
                        <a
                          className='carousel-control-next'
                          href='#carouselExampleControls'
                          role='button'
                          data-slide='next'
                        >
                          <span
                            className='carousel-control-next-icon'
                            aria-hidden='true'
                          ></span>
                          <span className='sr-only'>Next</span>
                        </a>
                      </div>
                      <div className='carousel-inner'>
                        {this.state.servicesData && this.state.servicesData.map((value, index) => {
                          return (
                            <div
                              class={
                                'carousel-item' + (index == 0 ? ' active' : '')
                              }
                            >
                              <div
                                className='row sliderRow2'
                                style={{
                                  backgroundImage: `url(${value.bgNumber})`,
                                }}
                              >
                                <div className='col-xl-8 col-lg-8 col-md-8 col-sm-7 col-12'>
                                  <h2>{value.title}</h2>
                                  <img
                                    className='img1'
                                    src={value.img}
                                    alt=''
                                  />
                                  <a
                                    href=''
                                    onClick={() =>
                                      this.props.history.push('/Services')
                                    }
                                  >
                                    <button
                                      className='button1 border-0'
                                      style={{ opacity: 0, height: '10px' }}
                                    >
                                      Read More
                                    </button>
                                  </a>
                                </div>
                                <div className='col-xl-4 col-lg-4 col-md-4 col-sm-5 col-12'>
                                  <div
                                    className='Number01'
                                    style={{
                                      // color: index !== 0 ? "#f90d26" : "#fff",
                                      color: '#f90d26',
                                    }}
                                  >
                                    {/* <h4>NUMBER</h4>
                                    <h6 className="text-right">{value.number}</h6> */}
                                  </div>
                                  <h5
                                    style={{
                                      // color: index !== 0 ? "#f90d26" : "#fff",
                                      color: '#f90d26',
                                    }}
                                  >
                                    {value.title}
                                  </h5>
                                  <p>{value.text}</p>
                                </div>
                              </div>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </div>
                <div className='img'>
                  <img src={require('../img/home/verticalLine2.png')} />
                </div>
                <div className='dropdown2'>
                  <h1>03</h1>
                  <Tada>
                    <h2>Our Networks</h2>
                  </Tada>
                </div>
              </div>
            </Zoom>
            {/* <!-- ---------PARTNERS STARTS--------- --> */}
            <Zoom Left>
              <div className='container-fluid partners'>
                <div className='container1'>
                  <div className='row rowPartners'>
                    <div
                      className='col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12 img-col'
                      style={{ padding: '0px' }}
                    >
                      <img
                        className='img1'
                        src={require('../img/home/partner-img.jpg')}
                        alt=''
                      />
                    </div>
                    <div className='col-xl-5 col-lg-6 col-md-6 col-sm-12 col-12 latest-news-paragraph-col'>
                      <p>
                        {networkHeadingData && networkHeadingData.text}
                        {/* Reach out to a highly valuable and growing remittance
                        audience. Our service creates a marketplace for money
                        transfer providers to connect for individuals and
                        business users to access your service in real time
                        through a convenient and user friendly tool. Our simple
                        and user friendly integration tool make it easier for
                        your company to market your services and reach new
                        customers base. */}
                      </p>

                      <div
                        id='carouselExampleIndicators'
                        className='carousel slide'
                        data-ride='carousel'
                      >
                        <div className='carousel-inner'>
                          <div className='carousel-item active' style={{ textAlign: "center" }}>
                            {/* <div className='slider'> */}
                            {/* {array.map((m) => (
                                <div className='diva'>
                                  <img className="" src={m} />
                                </div>
                              ))} */}
                            {ourNetwork.map((val, ind) => (
                              <div key={ind} className='diva'>
                                <img className="" src={val.image} />
                              </div>
                            ))}

                            {/* {array.map((m) => (
                                // <div className="diva">
                                <img src={m} />
                                // </div>
                              ))}

                              {array.map((m) => (
                                // <div className="diva">
                                <img src={m} />
                                // </div>
                              ))} */}

                            {/* <div className="diva">
                                <img src={require("../img/home/c3.png")} />
                              </div>
                              <div className="divb">
                                <img src={require("../img/home/c2.png")} />
                              </div>
                              <div className="divc">
                                <img src={require("../img/home/c3.png")} />
                              </div>
                              <div className="divd">
                                <img src={require("../img/home/c4.png")} />
                              </div>
                              <div className="dive">
                                <img src={require("../img/home/c5.png")} />
                              </div>
                              <div className="divf">
                                <img src={require("../img/home/c1.png")} />
                              </div>
                              <div className="divg">
                                <img src={require("../img/home/c1.png")} />
                              </div>
                              <div className="divh">
                                <img src={require("../img/home/c1.png")} />
                              </div>
                              <div className="divi">
                                <img src={require("../img/home/c1.png")} />
                              </div>
                              <div className="divj">
                                <img src={require("../img/home/c1.png")} />
                              </div>
                              <div className="divk">
                                <img src={require("../img/home/c1.png")} />
                              </div>
                              <div className="divl">
                                <img src={require("../img/home/c1.png")} />
                              </div> */}
                            {/* </div> */}
                          </div>
                          {/* <div className="carousel-item">
                            <div className="slider">
                              <div className="diva">
                                <img src={require("../img/home/c13.png")} />
                              </div>
                              <div className="divb">
                                <img src={require("../img/home/c14.png")} />
                              </div>
                              <div className="divc">
                                <img src={require("../img/home/c15.png")} />
                              </div>
                              <div className="divd">
                                <img src={require("../img/home/c15.png")} />
                              </div>
                              <div className="dive">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divf">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divg">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divh">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divi">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divj">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divk">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divl">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                            </div>
                          </div>
                          <div className="carousel-item">
                            <div className="slider">
                              <div className="diva">
                                <img src={require("../img/home/c13.png")} />
                              </div>
                              <div className="divb">
                                <img src={require("../img/home/c14.png")} />
                              </div>
                              <div className="divc">
                                <img src={require("../img/home/c15.png")} />
                              </div>
                              <div className="divd">
                                <img src={require("../img/home/c15.png")} />
                              </div>
                              <div className="dive">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divf">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divg">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divh">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divi">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divj">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divk">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                              <div className="divl">
                                <img src={require("../img/home/c17.png")} />
                              </div>
                            </div>
                          </div> */}
                          {/* <div className="carousel-controls">
                            <a
                              className="carousel-control-next position-static"
                              href="#carouselExampleIndicators"
                              role="button"
                              data-slide="next"
                            >
                              <span
                                className="carousel-control-next-icon"
                                aria-hidden="true"
                              ></span>
                              <span className="sr-only">Next</span>
                            </a>
                            <a
                              className="carousel-control-prev position-static"
                              href="#carouselExampleIndicators"
                              role="button"
                              data-slide="prev"
                            >
                              <span
                                className="carousel-control-prev-icon"
                                aria-hidden="true"
                              ></span>
                              <span className="sr-only">Previous</span>
                            </a>
                            <ol className="carousel-indicators position-static">
                              <li
                                data-target="#carouselExampleIndicators"
                                data-slide-to="0"
                                className="active"
                              ></li>
                              <li
                                data-target="#carouselExampleIndicators"
                                data-slide-to="1"
                              ></li>
                              <li
                                data-target="#carouselExampleIndicators"
                                data-slide-to="2"
                              ></li>
                            </ol>
                          </div> */}
                        </div>
                      </div>
                      <a href={`/ournetwork`}>
                        <div
                          className="btn-see-more" >SEE MORE</div>
                      </a>
                    </div>
                    <div className='col-xl-2 col-lg-1 d-lg-block d-md-none d-sn-none d-none'>
                      <h1>Partners</h1>
                    </div>
                  </div>
                </div>
              </div>
            </Zoom>
            {/* <!-- ---------LATEST NEWS & UPDATES STARTS--------- --> */}
            <div className='container-fluid latestNewsUpdates'>
              <div className='container1'>
                <div className='img'>
                  <img src={require('../img/home/verticalLine2.png')} />
                </div>
                <div className='dropdown2' style={{ marginBottom: "-200px" }}>
                  <h1>04</h1>
                  <RubberBand>
                    <h2>Latest News</h2>
                  </RubberBand>
                  <span className='blackHeading'>
                    <RubberBand>
                      <h2>& Updates</h2>
                    </RubberBand>
                  </span>
                </div>
                <div className='row rowLatestNewsUpdates'>
                  <div className='col-xl-2 col-lg-2 d-lg-block d-md-none d-sm-none d-none'>
                    <h3>News&Updates</h3>
                  </div>
                  <div style={{ marginTop: 200 }} className='col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12'>
                    {/* <div className='img'>
                      <img src={require('../img/home/verticalLine2.png')} />
                    </div>
                    <div className='dropdown2'>
                      <h1>04</h1>
                      <RubberBand>
                        <h2>Latest News</h2>
                      </RubberBand>
                      <span className='blackHeading'>
                        <RubberBand>
                          <h2>& Updates</h2>
                        </RubberBand>
                      </span>
                    </div> */}

                    {/*  <Carousel>
                          <div>
                              <p className="legend">Legend 1</p>
                          </div>
                          <div>
                              <p className="legend">Legend 2</p>
                          </div>
                          <div>
                              <p className="legend">Legend 3</p>
                          </div>
                        </Carousel> */}

                    <Zoom Top>
                      <div className='latestNewsUpdatesRedDiv'>
                        <Carousel
                          showIndicators={false}
                          showStatus={false}
                          showArrows={true}
                          axis='horizontal'
                          autoPlay={true}
                        >
                          {news && news.length > 0
                            ? news.map((n) => (
                              <div className=''>
                                <div className='news1'>
                                  <h1>{n ? n.heading : ''}</h1>
                                  <div className='news1Line-div'>
                                    <div className='news1Line'></div>
                                  </div>
                                  <p>{n ? n.news : 'abc'}</p>
                                </div>
                              </div>
                            ))
                            : ''}

                          {/* <div className="carousel-controls">
                              <a
                                className="carousel-control-prev"
                                href="#carouselExampleControls2"
                                role="button"
                                data-slide="prev"
                              >
                                <span
                                  className="carousel-control-prev-icon"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Previous</span>
                              </a>
                              <a
                                className="carousel-control-next"
                                href="#carouselExampleControls2"
                                role="button"
                                data-slide="next"
                              >
                                <span
                                  className="carousel-control-next-icon"
                                  aria-hidden="true"
                                ></span>
                                <span className="sr-only">Next</span>
                              </a>
                            </div> */}
                        </Carousel>

                        <div className='bottom-part'></div>
                      </div>
                    </Zoom>
                  </div>
                  <div className='col-xl-4 col-lg-4 d-lg-block d-md-none d-sm-none d-none'>
                    <div className='latestNewsUpdatesPicture-Div'>
                      <img
                        src={require('../img/home/latestNewsUpdatesPicture.png')}
                        alt=''
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* <div className='container-fluid testimonial'>
              <div className='container1'>
                <div className='row rowTestimonial1'>
                  <div className='col-xl-4 col-lg-4 col-md-2 col-sm-1 col-12'></div>
                  <div className='col-xl-4 col-lg-4 col-md-8 col-sm-9 col-12'>
                    <div className='img'>
                      <img src={require('../img/home/verticalLine2.png')} />
                    </div>
                    <div className='dropdown2'>
                      <h1>05</h1>
                      <Tada>
                        <h2>How It Works</h2>
                      </Tada>
                    </div>
                  </div>
                  <div className='col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12'></div>
                  
                </div>
                <Zoom>
                  <div className='row rowTestimonal2'>
                    <div className='row1' style={{ justifyContent:"center"}}>
                    <div className='diva' style={{width:"80%", height:"85%"}}>
                      <video style={{width:"100%", height:"100%"}} controls >
                        <source src={IntroVideo} type="video/mp4" />
                      </video>
                      </div> 
                    </div>
                  </div>
                </Zoom>
              </div>
            </div> */}


            {/* <!-- ---------TESTIMONAL STARTS--------- --> */}
            <div className='container-fluid testimonial'>
              <div className='container1'>
                <div className='row rowTestimonial1'>
                  <div className='col-xl-4 col-lg-4 col-md-2 col-sm-1 col-12'></div>
                  <div className='col-xl-4 col-lg-4 col-md-8 col-sm-10 col-12'>
                    <div className='img'>
                      <img src={require('../img/home/verticalLine2.png')} />
                    </div>
                    <div className='dropdown2'>
                      <h1>05</h1>
                      <Tada>
                        <h2>Testimonial</h2>
                      </Tada>
                    </div>
                  </div>
                  <div className='col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12'></div>

                </div>
                <Zoom>
                  <div className='row rowTestimonal2'>
                    <div className='row1'>
                      <div className='diva'>
                        <div
                          className='box1'
                          id='man1'
                          style={{
                            backgroundImage: `url(${testimonial.length > 0 && testimonial[0].img
                              ? testimonial[0].img
                              : require('../img/home/testimonial-default-img.jpg')
                              })`,
                          }}
                        >
                          <div className='box1a'>
                            <h3>
                              {testimonial.length > 0
                                ? testimonial[0].name
                                : 'head'}
                            </h3>
                            <p>
                              {testimonial.length > 0
                                ? testimonial[0].text1
                                : ''}
                            </p>
                          </div>
                        </div>
                      </div>

                      <div className='divb'>
                        <div className='divb1'>
                          <div className='divb1a'>
                            <div
                              className='box2'
                              id='man2'
                              style={{
                                backgroundImage: `url(${testimonial.length > 0 && testimonial[1].img
                                  ? testimonial[1].img
                                  : require('../img/home/testimonial-default-img.jpg')
                                  })`,
                              }}
                            >
                              <div className='box2a'>
                                <h3>
                                  {testimonial.length > 1
                                    ? testimonial[1].name
                                    : 'head'}
                                </h3>
                                <p>
                                  {testimonial.length > 1
                                    ? testimonial[1].text1
                                    : ''}
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className='divb1b'>
                            <div
                              className='box2'
                              id='man3'
                              style={{
                                backgroundImage: `url(${testimonial.length > 0 && testimonial[2].img
                                  ? testimonial[2].img
                                  : require('../img/home/testimonial-default-img.jpg')
                                  })`,
                              }}
                            // style={{
                            //   backgroundImage: `url(${require('../img/man3.png')})`,
                            // }}
                            >
                              <div className='box2a'>
                                <h3>
                                  {testimonial.length > 2
                                    ? testimonial[2].name
                                    : 'head'}
                                </h3>
                                <p>
                                  {testimonial.length > 2
                                    ? testimonial[2].text1
                                    : ''}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className='divb2'>
                          <div className='divb2a'>
                            <div
                              className='box2'
                              id='man4'
                              // style={{
                              //   backgroundImage: `url(${require('../img/man4.png')})`,
                              // }}
                              style={{
                                backgroundImage: `url(${testimonial.length > 0 && testimonial[3].img
                                  ? testimonial[3].img
                                  : require('../img/home/testimonial-default-img.jpg')
                                  })`,
                              }}
                            >
                              <div className='box2a'>
                                <h3>
                                  {testimonial.length > 3
                                    ? testimonial[3].name
                                    : 'head'}
                                </h3>
                                <p>
                                  {testimonial.length > 3
                                    ? testimonial[3].text1
                                    : ''}
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className='divb2b'>
                            <div
                              className='box2'
                              id='man5'
                              // style={{
                              //   backgroundImage: `url(${require('../img/man5.png')})`,
                              // }}
                              style={{
                                backgroundImage: `url(${testimonial.length > 0 && testimonial[4].img
                                  ? testimonial[4].img
                                  : require('../img/home/testimonial-default-img.jpg')
                                  })`,
                              }}
                            >
                              <div className='box2a'>
                                <h3>
                                  {testimonial.length > 4
                                    ? testimonial[4].name
                                    : 'head'}
                                </h3>
                                <p>
                                  {testimonial.length > 4
                                    ? testimonial[4].text1
                                    : ' '}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className='row rowTestimonal3'>
                    <div className='row1'>
                      <div className='diva'>
                        <div
                          className='box3'
                          id='man6'
                          style={{
                            backgroundImage: `url(${testimonial.length > 0 && testimonial[5].img
                              ? testimonial[5].img
                              : require('../img/home/testimonial-default-img.jpg')
                              })`,
                          }}
                        >
                          <div className='box3a'>
                            <h3>
                              {testimonial.length > 5
                                ? testimonial[5].name
                                : 'head'}
                            </h3>
                            <p>
                              {testimonial.length > 5
                                ? testimonial[5].text1
                                : ''}
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='divb'>
                        <div className='divb1 '>
                          <div className='divb1a '>
                            <div
                              className='box2'
                              id='man7'
                              // style={{
                              //   backgroundImage: `url(${require('../img/man7.png')})`,
                              // }}
                              style={{
                                backgroundImage: `url(${testimonial.length > 0 && testimonial[6].img
                                  ? testimonial[6].img
                                  : require('../img/home/testimonial-default-img.jpg')
                                  })`,
                              }}
                            >
                              <div className='box2a'>
                                <h3>
                                  {testimonial.length > 6
                                    ? testimonial[6].name
                                    : 'head'}
                                </h3>
                                <p>
                                  {testimonial.length > 6
                                    ? testimonial[6].text1
                                    : ''}
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className='divb1b '>
                            <div
                              className='box2'
                              id='man11'
                              // style={{
                              //   backgroundImage: `url(${require('../img/man11.png')})`,
                              // }}
                              style={{
                                backgroundImage: `url(${testimonial.length > 0 && testimonial[7].img
                                  ? testimonial[7].img
                                  : require('../img/home/testimonial-default-img.jpg')
                                  })`,
                              }}
                            >
                              <div className='box2a'>
                                <h3>
                                  {testimonial.length > 7
                                    ? testimonial[7].name
                                    : 'head'}
                                </h3>
                                <p>
                                  {testimonial.length > 7
                                    ? testimonial[7].text1
                                    : ''}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className='divb2'>
                          <div className='divb2a '>
                            <div
                              className='box2'
                              id='man8'
                              // style={{
                              //   backgroundImage: `url(${require('../img/man8.png')})`,
                              // }}
                              style={{
                                backgroundImage: `url(${testimonial.length > 0 && testimonial[8].img
                                  ? testimonial[8].img
                                  : require('../img/home/testimonial-default-img.jpg')
                                  })`,
                              }}
                            >
                              <div className='box2a'>
                                <h3>
                                  {testimonial.length > 8
                                    ? testimonial[8].name
                                    : 'head'}
                                </h3>
                                <p>
                                  {testimonial.length > 8
                                    ? testimonial[8].text1
                                    : ''}
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className='divb2b '>
                            <div
                              className='box2'
                              id='man12'
                              style={{
                                backgroundImage: `url(${testimonial.length > 0 && testimonial[9].img
                                  ? testimonial[9].img
                                  : require('../img/home/testimonial-default-img.jpg')
                                  })`,
                              }}
                            // style={{
                            //   backgroundImage: `url(${require('../img/man12.png')})`,
                            // }}
                            >
                              <div className='box2a'>
                                <h3>
                                  {testimonial.length > 9
                                    ? testimonial[9].name
                                    : 'head'}
                                </h3>
                                <p>
                                  {testimonial.length > 9
                                    ? testimonial[9].text1
                                    : ''}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className='divc'>
                        <div className='divc1 '>
                          <div
                            className='box2'
                            id='man9'
                            style={{
                              backgroundImage: `url(${testimonial.length > 0 && testimonial[10].img
                                ? testimonial[10].img
                                : require('../img/home/testimonial-default-img.jpg')
                                })`,
                            }}
                          // style={{
                          //   backgroundImage: `url(${require('../img/man9.png')})`,
                          // }}
                          >
                            <div className='box2a'>
                              <h3>
                                {testimonial.length > 10
                                  ? testimonial[10].name
                                  : 'head'}
                              </h3>
                              <p>
                                {' '}
                                {testimonial.length > 10
                                  ? testimonial[10].text1
                                  : ''}
                              </p>
                            </div>
                          </div>
                          <div
                            className='box2'
                            id='man10'
                            style={{
                              backgroundImage: `url(${testimonial.length > 0 && testimonial[11].img
                                ? testimonial[11].img
                                : require('../img/home/testimonial-default-img.jpg')
                                })`,
                            }}
                          // style={{
                          //   backgroundImage: `url(${require('../img/man10.png')})`,
                          // }}
                          >
                            <div className='box2a'>
                              <h3>
                                {testimonial.length > 11
                                  ? testimonial[11].name
                                  : 'head'}
                              </h3>
                              <p>
                                {testimonial.length >= 11
                                  ? testimonial[11].text1
                                  : ''}
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className='divc2'>
                          <div className='testimonialRedLine'></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Zoom>
              </div>
            </div>
          </div>
          {/* <!-- ---------container-fluid services ENDS--------- --> */}
        </div>
        <Footer history={this.props.history} />
      </div >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.Comparisons.isLoading,
    part: state.Comparisons.part,
    testimonial: state.Offers.testimonials,
    News: state.News.News,
    whyChooseUs: state.FAQS.whyChooseUs,
    services: state.FAQS.services,
    networkHeading: state.FAQS.networkHeading,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: () => dispatch(Comparisons.partner()),
  getTestimomials: bindActionCreators(getTestimomials, dispatch),
  getNews: bindActionCreators(News.News, dispatch),
  getServices: bindActionCreators(getServices, dispatch),
  getNetworkHeading: bindActionCreators(getNetworkHeading, dispatch),
  getWhyChooseUs: bindActionCreators(getWhyChooseUs, dispatch)

});

export default connect(mapStateToProps, mapDispatchToProps)(Index);
