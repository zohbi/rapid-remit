import React from "react";
import { Link } from "react-router-dom";
import Moment from "react-moment";
export default class Newstab extends React.Component {
  render() {
    // console.log(this.props, "here is data let check it");
    return (
      <div className="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
        <div className="cardNews">
          <span className="spanHeading">News</span>
          <span className="spanDate">
            <Moment format="Do MMM,YYYY">{this.props.createdAt}</Moment>
          </span>
          {/* <p className="cardNewsTitle">{this.props.heading}</p> */}
          <p className="cardNewsTitle" style={{height:82}}>{this.props.heading.length > 80 ? this.props.heading.slice(0,80) + "..." : this.props.heading}</p>
          <p className="cardNewsP" style={{height:92}}>{this.props.news.length > 200 ? this.props.news.slice(0,200) + "..." : this.props.news}</p>
          {/* <a href="" onClick={() => this.props.history.push("/News")}> */}
          <Link
            to={{
              pathname: "/News",
              state: {
                news: this.props.news,
                newsDetails: this.props.newsDetails,
                author: this.props.newsAuthor,
                createdAt: this.props.createdAt,
                updatedAt: this.props.updatedAt,
                key1: "0" + this.props.key1,
                heading: this.props.heading,
              },
            }}
          >
            <span className="spanHeading" style={{ color: "#f90d26" }} >+Read More..</span>
          </Link>
          {/* </a> */}
        </div>
      </div>
    );
  }
}
