import React from "react";
import "../css/bootstrap.css";
import "../css/style-registration.css";
import "../css/style-forget.css";
import "../css/font.css";
import "../css/loader.css";
import { connect } from "react-redux";
import { Forgot } from "../Store/actions";

// import login from "../Store/reducer/login";

// <-------------- Password Encode Decode --------------->

// import HTMLDecoderEncoder from "html-encoder-decoder";
import { Base64 } from "js-base64";
import Loader from "./loader";
import Toast from "./toast";
import validator from "validator";

// global variables
var error;
// ---

// -------------------------><-----------------------------

class Forget extends React.Component {
  constructor() {
    super();
    this.state = {
      nameOrEmail: ""
      // isLoadingLogin: falsse
    };
  }

  onChangeValue = (name, value) => {
    let stateName = name;
    this.setState({
      [stateName]: value
    });
  };

  register = (e) => {
    e.preventDefault()
    let data = {
      email: this.state.nameOrEmail,
    };
    if (!validator.isEmail(data.email)) {
      this.setState({ error1: `${data.email} is not a valid email` });
    } else {
      let ForgetData = {
        email: data.email,
      };
      this.props.PostForget(ForgetData);
    }
  };

  onChangeValue = (name, value) => {
    let stateName = name;
    this.setState({
      [stateName]: value
    });
  };


  componentDidMount() {
    // console.log(this.props, "ye state hy bhai check kar");

    if (localStorage.getItem("UserData")) {
      this.props.history.push("/");
    }
  }

  componentWillUpdate(props) {
    // console.log(props, "state check it componentwillupdate");
    if (props.loginFailed) {
      error = "User is not exists";
    }
  }

  render() {
    // console.log(this.props);
    return (
      <div className="">
        <div className="registration">
          <div className="container01">
            <div className="registrationDivMain">
              <div className="form">
              <div className="">
                <div 
                style={{margin:0}}
                  className="back-arrow-button"
                  onClick={() => this.props.history.push("/registration")}
                >
                  <i
                    className="fa fa-arrow-left"
                  
                  ></i>
                </div>
              </div>
                <form>
                  <div className="row">
                    <h1>Reset Password</h1>
                  </div>
                  <div className="form-group row">
                    <div className="col-sm-12">
                      <div className="fields">
                        <label for="email" className=" col-form-label">
                          Email
                        </label>
                        <input
                          type="email"
                          className="form-control"
                          id="email"
                          placeholder="info@gmail.com"
                          onChange={e =>
                            this.onChangeValue("nameOrEmail", e.target.value)
                          }
                        />
                      </div>
                    </div>
                  </div>

            

                  <div className="form-group row">
                    <div className="col-sm-12 d-flex justify-content-end">
                      <button
                        type="sumbit"
                        className="btn btn-primary"
                        onClick={(e) => this.register(e)}
                      >
                        {!this.state.isLoadingRegister ? (
                          "Reset"
                        ) : (
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-around"
                            }}
                          >
                            Reset <Loader />
                          </div>
                        )}
                      </button>
                    </div>
                    {this.state.error1 ? (
                      <div className="alert alert-danger alert-dismissible fade show">
                        1. {this.state.error1}
                      </div>
                    ) : (
                      " "
                    )}
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// const mapStateToProps = state => {
//   return {
//     // allInventory: state.AllInventory.AllInventory
//   };
// };
const mapDispatchToProps = dispatch => ({
  PostForget: obj => dispatch(Forgot.Forgot(obj))
});
export default connect(
  null,
  mapDispatchToProps
)(Forget);

