import React from "react";

export default class CompanyOverviewFeatures extends React.Component {
  render() {
    return (
      <div className="container1">
        <div className="row companyOverviewContentRow2">
          <div className="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
            <div className="divHeading">
              <h1>Features</h1>
            </div>
            <ul>
              <li>
                <div className="span1">
                  <h6>Transfer Time</h6>
                </div>
                <div className="span2">
                  <p>Rs.{this.props.data.transfer_time}</p>
                </div>
              </li>
              <li>
                <div className="span1">
                  <h6>payment_fees</h6>
                </div>
                <div className="span2">
                  <p>{this.props.data.payment_fees}</p>
                </div>
              </li>
              <li>
                <div className="span1">
                  <h6>TRANSFER TYPE:</h6>
                </div>
                <div className="span2">
                  <p>{this.props.data.transfer_method[0]}</p>
                </div>
              </li>
              <li>
                <div className="span1">
                  <h6>CONTACT</h6>
                </div>
                <div className="span2">
                  <p>+1213214324</p>
                </div>
              </li>
              <li>
                <div className="span1">
                  <h6>COMPANY TYPE</h6>
                </div>
                <div className="span2">
                  <p>{this.props.data.partner_details[0].company_type}</p>
                </div>
              </li>
              <li>
                <div className="span1">
                  <h6>MOBILE APP</h6>
                </div>
                <div className="span2">
                  <p>
                    Android: {this.props.data.partner_details[0].android_url}{" "}
                    Ios: {this.props.data.partner_details[0].ios_url}{" "}
                  </p>
                </div>
              </li>
              <li>
                <div className="span1">
                  <h6>KEY FEATURE</h6>
                </div>
                <div className="span2">
                  <p>{this.props.data.partner_details[0].key_features[0]}</p>
                </div>
              </li>
              <li>
                <div className="span1">
                  <h6>DOCUMENTS</h6>
                </div>
                <div className="span2">
                  <p>{this.props.data.partner_details[0].documents[0]}</p>
                </div>
              </li>
            </ul>
          </div>

          <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <div className="">
              {/* <img src={this.props.data[2].img} alt="" /> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
