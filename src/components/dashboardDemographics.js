import React from "react";
import CanvasJSReact from "../assets/canvasjs.react";
import { connect } from "react-redux";
import { Comparisons } from "../Store/actions/index";
import { bindActionCreators } from "redux";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

class DashboardDemographics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      PKR: 0,
      PND: 0,
      INR: 0,
      USD: 0
    };
  }

  componentWillMount() {
    let rates = this.props.rates;
    let PKR = rates.filter(f => f.country_name == "PKR")[0];
    //let PND = rates.filter(f => f.country_name == "PND")[0];
    let INR = rates.filter(f => f.country_name == "INR")[0];
    let USD = rates.filter(f => f.country_name == "USD")[0];
    // console.log("rrrrrr", PKR, rates);
    this.setState({ PKR, INR, USD });
  }
  render() {
    const options = {
      backgroundColor: "transparent",
      animationEnabled: true,
      dataPointWidth: 100,

      // backgroundColor:'blue',
      width: 420,
      height: 220,

      axisX: {
        lineColor: "transparent",
        tickColor: "transparent"
        // labelAngle: -90,
      },
      axisY: {
        gridColor: "transparent",
        lineColor: "transparent",
        tickColor: "transparent",
        interval: 100,
        maximum: 500
      },
      data: [
        {
          type: "column", //change it to line, area, bar, pie, etc
          showInLegend: true,
          dataPoints: [
            { label: "Jun 27       Jun 26", y: 70, color: "#500000" },
            { label: "Jun 25       Jun 24", y: 250, color: "#790000" },
            { label: "Jun 23       Jun 22", y: 500, color: "#FF0000" }
          ]
        }
      ]
    };
    const options2 = {
      backgroundColor: "transparent",
      animationEnabled: true,
      dataPointWidth: 100,

      // backgroundColor:'blue',
      width: 260,
      height: 260,

      axisX: {
        lineColor: "transparent",
        tickColor: "transparent"
        // labelAngle: -90,
      },
      axisY: {
        gridColor: "transparent",
        lineColor: "transparent",
        tickColor: "transparent",
        interval: 100,
        maximum: 500
      },
      data: [
        {
          type: "pie", //change it to line, area, bar, pie, etc
          showInLegend: true,
          startAngle: -40,
          dataPoints: [
            { label: "29.03%", y: 29.03, color: "#001F3F" },
            { label: "22.54%", y: 22.54, color: "#B10DC9" },
            { label: "19.25%", y: 19.25, color: "#85144B" },
            { label: "15.59%", y: 15.59, color: "#FF851B" },
            { label: "4.59%", y: 4.59, color: "#FF4136" },
            { label: "4.53%", y: 4.53, color: "#2ECC40" },
            { label: "4.46%", y: 4.46, color: "#39CCCC" }
          ]
        }
      ],
      data2: [
        {
          type: "line", //change it to line, area, bar, pie, etc
          lineDashType: "dot",
          markerType: "none",
          showInLegend: true,
          dataPoints: [
            { label: "Jun 27       Jun 26", y: 80 },
            { label: "Jun 25       Jun 24", y: 270 },
            { label: "Jun 23       Jun 22", y: 440 }
          ]
        }
      ]
    };
    const options3 = {
      backgroundColor: "transparent",
      animationEnabled: true,
      // dataPointWidth: 120,

      // backgroundColor:'blue',
      width: 700,
      height: 240,

      axisX: {
        lineColor: "transparent",
        tickColor: "transparent"
        // labelAngle: -90,
      },
      axisY: {
        gridColor: "transparent",
        lineColor: "transparent",
        tickColor: "transparent",
        interval: 100,
        maximum: 500
      },
      data: [
        {
          type: "column", //change it to line, area, bar, pie, etc
          showInLegend: true,
          dataPoints: [
            { label: "Jun 27       Jun 26", y: 230, color: "#2DA8A8" },
            { label: "Jun 25       Jun 24", y: 390, color: "#FF851B" },
            { label: "Jun 23       Jun 22", y: 80, color: "#9C2BAD" },
            { label: "Jun 23       Jun 22", y: 260, color: "#FFCA1E" },
            { label: "Jun 23       Jun 22", y: 450, color: "#FF0000" }
          ]
        }
      ]
    };
    // console.log("rates222", this.props.rates);

    let PKR = this.props.rates.filter(f => f.country_name == "PKR")[0];
    //let PND = this.props.rates.filter(f => f.country_name == "PND")[0];
    let INR = this.props.rates.filter(f => f.country_name == "INR")[0];
    let USD = this.props.rates.filter(f => f.country_name == "USD")[0];
    // console.log("rates", PKR, INR, USD);

    return (
      <div className="container" id="DashboardDemographics">
        <div className="row rowSmallCards">
          <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6 rowSmallCards1">
            <div className="smallCards" id="card1">
              <h6>AED TO PKR</h6>
              {/* <h5>{this.state.PKR}</h5> */}
              <p className="pSmall2">First Time Users Only</p>
            </div>
          </div>
          <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6 rowSmallCards2">
            <div className="smallCards" id="card2">
              <h6>AED TO PND</h6>
              {/* <h5>{PND ? PND : 0}</h5> */}
              <p className="pSmall2">First Time Users Only</p>
            </div>
          </div>
          <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6 rowSmallCards3">
            <div className="smallCards" id="card3">
              <h6>AED TO INR</h6>
              {/* <h5>{INR ? INR : 0}</h5> */}
              <p className="pSmall2">First Time Users Only</p>
            </div>
          </div>
          <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6 rowSmallCards4">
            <div className="smallCards" id="card4">
              <h6>AED TO USD</h6>
              {/* <h5>{USD ? USD : 0}</h5> */}
              <p className="pSmall2">First Time Users Only</p>
            </div>
          </div>
        </div>
        <div className="row rowCharts">
          <div className="main_con col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
            <div className="dashboard_tags">
              <p>Number of Consumers</p>
            </div>
            <div className="admin_main_column">
              <div
                id="chartContainer"
                style={{ height: "", width: "%", padding: "0% 10%" }}
              >
                <CanvasJSChart
                  options={options2}
                  style={{ height: "", width: "" }}
                />
              </div>
            </div>
          </div>
          <div className="main_con col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
            <div className="dashboard_tags">
              <p>Cost per Transactions</p>
            </div>
            <div className="admin_main_column">
              <div
                id="resizable"
                style={{ height: "", border: "1px solid white" }}
              >
                <div
                  id="chartContainer1"
                  style={{ height: "", width: "%", padding: "5% 5%" }}
                >
                  <CanvasJSChart options={options} />
                </div>
              </div>
            </div>
          </div>
          <div className="main_con col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div className="dashboard_tags">
              <p>Cost per Region</p>
            </div>
            <div className="admin_main_column">
              <div
                id="resizable0"
                style={{ height: "", border: "1px solid transparent" }}
              >
                <div
                  id="chartContainer3"
                  style={{ height: "", width: "", padding: "0% 2%" }}
                >
                  <CanvasJSChart options={options3} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    rates: state.Comparisons.rates
  };
};

const mapDispatchToProps = dispatch => ({
  getRates: () => dispatch(Comparisons.rate())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardDemographics);
