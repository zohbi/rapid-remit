import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  transferWise,
  userReviews,
  partnerReviews as partners,
} from '../Store/actions';
import CompanyOverview from './companyoverview';
import CompanyOverviewText from './companyOverviewText';
import CompanyOverviewFeatures from './CompanyOverviewFeatures';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';
import imgo from '../img/blogs/blogsImg2.png';
import '../css/style-CO.css'; //added
import Footer from './footer';

function isEmpty(obj) {
  return Object.entries(obj).length === 0 && obj.constructor === Object;
}

class TransferWise extends React.Component {
  constructor(props) {
    super(props);
  }

  showRatings(data) {
    // console.log('rrrr', data);
    let array = [];
    for (let i = 1; i <= data; i++) {
      // console.log('datarating', i);

      array.push(<i className='fas fa-star red'></i>);
    }

    if (data < 5) {
      const len = 5 - data;
      for (let i = 1; i <= len; i++) {
        // console.log('datarating', i);

        array.push(<i className='fas fa-star gray'></i>);
      }
    }

    return array;
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    //this.props.Comparisons();

    //this.props.getData(this.props.match.params.id);
    // console.log('trnasfer', this.props.match.params.id);
    this.props.transferWise(this.props.match.params.id);
    this.props.userReviews();
    this.props.partners();
    // console.log("abc");
  }

  render() {
    // console.log('TRANSFER_WISE_PROPS', this.props);
    let data = this.props.Comp;
    let partner_review = this.props.partnerReviews;

    console.log('final12312', data, this.props.data);

    return (
      <Fragment>
        <div  className='bg-white'>
        <div className='container-fluid bg-white pt-5'  id="partner-details">
          <div className='container1'>
            <div className='transfer_row-main-div'>
              <div className='row transfer_row'>
                <div className='col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12'>
                  <img
                    style={{ width: 200, height: 100, marginBottom: 20 }}
                    src={
                      data && data.img ? data.img : ""
                    }
                  />

                  <h1
                    className='Lato_Black'
                    style={{ textTransform: 'capitalize' }}
                  >
                    {data && data.name}
                  </h1>
                  {/* <div className="reviews-main-div">
                    <div className="all-stars-div">
                      {this.showRatings(partner_review[data.name])}
                    </div>
                    <div className="review">
                      <span>REVIEWS</span>
                      <span className="three-twenty">{data.reviews && data.reviews.length}</span>
                    </div>

                    <div className="write_review">
                      <a href="#">WRITE A REVIEW</a>
                    </div>
                  </div> */}
                </div>
                <div className='col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12'>
                  <div className='div10'>
                    <div className='divA'>
                      {/* <label for="">FROM&nbsp;</label>
                      <input type="text" placeholder="UAE" />
                      <label for="">&nbsp;TO</label>
                      <input type="text" placeholder="PKR" /> */}
                    </div>
                    <div className='divB'>
                      <a href={data && data.website_url} target='_blank'>
                        <button
                          className='buttonGoto'
                          style={{ textTransform: 'uppercase' }}
                        >
                          GO TO {data && data.name}
                        </button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className='container-fluid companyOverview bg-white'>
          <div className='container1'>
            <div className='companyOverviewContent'>
              <div className='row companyOverviewContentRow1'>
                <div className='col-xl-3 col-lg-4 col-md-12 col-sm-12 col-12 img-col'>
                  <div className='picDivb first-img'>
                    <img
                      src={require('../img/CompanyOverview/CompanyOverviewImg1.png')}
                      alt=''
                    />
                    <div className='CompanyOverviewRedLine'></div>
                  </div>
                </div>
                <div className='col-Text col-xl-9 col-lg-8 col-md-12 col-sm-12 col-12 contentDiv'>
                  <h2>Company Overview</h2>
                  <p style={{maxHeight: 500, overflowY: "auto"}}>{data && data.about_exchange}</p>
                  <p></p>
                </div>
              </div>
              <div className='container1'>
                <div className='row companyOverviewContentRow2'>
                  <div className='col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12'>
                    <div className='divHeading'>
                      <h1>Features</h1>
                    </div>
                    <ul>
                      {/* <li>
                        <div className="span1">
                          <h6>RATE:</h6>
                        </div>
                        <div className="span2">
                          <p>
                            Rs {data && data.rate ? data.rate.toFixed(2) : ""}
                          </p>
                        </div>
                      </li> */}
                      <li>
                        <div className='span1'>
                          <h6>TRANSFER FEE</h6>
                        </div>
                        <div className='span2'>
                          <p>
                          Applicable{' '}
                            {/* {data && data.payment_fees ? data.payment_fees : ''} */}
                          </p>
                        </div>
                      </li>
                      <li className='keyFeature'>
                        <div className='span1'>
                          <h6>PAYMENT TYPE</h6>
                        </div>
                        <div className='span2'>
                          <p>
                            {data.payment_methods &&
                              data.payment_methods.map((data1) => {
                                return <li>{data1.toUpperCase()}</li>;
                              })}
                          </p>
                        </div>
                      </li>
                      <li className='keyFeature'>
                        <div className='span1'>
                          <h6>PAYMENT TRANSFER METHOD</h6>
                        </div>
                        <div className='span2'>
                          <p>
                          {data?.payment_transfer_methods && Array.isArray(data.payment_transfer_methods) &&
                                data.payment_transfer_methods.filter((val, ind) => data.payment_transfer_methods.indexOf(val) == ind ).map((p) => {
                                  return <li>{p.toUpperCase()}</li>;
                                })}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className='span1'>
                          <h6>CONTACT</h6>
                        </div>
                        <div className='span2'>
                          <p>{data && `+${data.phone}`}</p>
                        </div>
                      </li>
                      <li>
                        <div className='span1'>
                          <h6>COMPANY TYPE</h6>
                        </div>
                        <div className='span2'>
                          <p>{data && data.company_type}</p>
                        </div>
                      </li>
                      <li>
                        <div className='span1'>
                          <h6>MOBILE APP</h6>
                        </div>
                        <div className='span2 d-flex j-center-480'>
                          {data && <>
                            {((data.ios_url !== "Not Available " || !data.ios_url) || (data.android_url !== "Not Available " || !data.android_url)) ?
                              <>
                                {data.android_url && data.android_url !== "Not Available " && <a href={data.android_url} target="_blank" ><img style={{marginRight:20, }} width="40" height="40" src={require('../img/play-playstore-icon.png')} /></a>}
                                {data.ios_url && data.ios_url !== "Not Available " && <a href={data.ios_url}  target="_blank"  ><img width="40" height="40"  src={require('../img/iOS-Apple-icon.png')} /></a>}
                              </>
                              :
                              <p>Not Available</p>

                            }
                          </>
                          }

                        </div>
                      </li>
                      <li className='keyFeature'>
                        <div className='span1'>
                          <h6>DOCUMENTS</h6>
                        </div>
                        <div className='span2'>
                          <p>
                            {data.documents &&
                              data.documents.map((data1) => {
                                return <li>{data1}</li>;
                              })}
                          </p>
                        </div>
                      </li>
                      <li className='documents'>
                        <div className='span1'>
                          <h6>KEY FEATURES</h6>
                        </div>
                        <div className='span2'>
                          <p>
                            {data.key_features &&
                              data.key_features.map((data2) => {
                                return <li>{data2}</li>;
                              })}
                          </p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div className='col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12'>
                    <div className='picDivb'>
                      <img
                        // style={{minHeight: 800}}
                        src={require('../img/CompanyOverview/CompanyOverviewImg2.png')}
                        alt=''
                      />
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className='row companyOverviewContentRow3'> */}
                {/* <div className='companyOverviewContentRow3Div1'>
                  <h1>Locate Near Exchange</h1>
                </div> */}
                {/* <div className='companyOverviewContentRow3Div2'> */}
                  {/* <img src={require('../img/CompanyOverview/CompanyOverviewImg3.png')} alt="" /> */}
                  {/* <div className='mapMainDiv'>
                    <div className='mapouter'>
                      <div className='gmap_canvas'>
                        <iframe
                          id='gmap_canvas'
                          src='https://maps.google.com/maps?q=dubai&t=&z=13&ie=UTF8&iwloc=&output=embed'
                          frameborder='0'
                          scrolling='no'
                          marginheight='0'
                          marginwidth='0'
                        ></iframe>
                      </div>
                    </div>
                  </div> */}
                {/* </div> */}
              {/* </div> */}
            </div>
          </div>
        </div>
       <div className='container-fluid divBottom bg-white bg-img-for-reviews' style={{paddingBottom:"0%"}}>
       {this.props.data.filter((val) => val.partner_id === data._id ).length > 0 && <div className='row companyOverviewContentRow4 ml-0 mr-0'>
            <div className='container1'>
              <div className='divHeading'>
                <h1>Reviews</h1>
              </div>
             
             
                <Carousel
                  showIndicators={false}
                  showStatus={false}
                  // showArrows={false}
                  axis='horizontal'
                  autoPlay={true}
                >
                  {this.props.data && this.props.data.length > 0
                    ? this.props.data.filter((val) => val.partner_id === data._id ).map((d) => (
                        <div className='row no-gutters john-row'>
                          <div className='col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0'></div>
                          <div className='mr-john col-xl-4 col-lg-4 col-md-2 col-sm-2 col-2'>
                            <img
                              src={require('../img/aboutUs/invertedcoma.png')}
                              className='inverted-coma'
                            />
                            <div className='pic1'>
                              <div className='pic-i'></div>
                              <div className='pic-j'>
                                <img src={d.user_id?.image || require("../img/dashboard1/profile.png")} className='john-img' />
                              </div>
                            </div>
                          </div>
                          <div className='mr-john john-detail col-xl-5 col-lg-5 col-md-9 col-sm-9 col-8 '>
                            <h5>{d.name ? d.name : ''}</h5>
                            <p id='para2'>{d.text ? d.text : ''}</p>
                          </div>
                          <div className='mr-john col-xl-1 col-lg-1 col-md-1 col-sm-1 col-2'>
                            <img
                              src={require('../img/aboutUs/invertedcoma2.png')}
                              className='inverted-coma2'
                            />
                          </div>
                          <div className='col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0'></div>
                        </div>
                      ))
                    : ''}
                </Carousel>

               </div>
            </div>}
          {/* </div> */}

          {localStorage.getItem('token') ? null : (
            <div className='row companyOverviewContentRow5 ml-0 mr-0'>
              <div className='container1'>
                <div className='row divBlack'>
                  <div className='col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12'>
                    <p>
                      Share your experience with us, Get register your self and
                      let us know yourself
                    </p>
                  </div>
                  <div className='col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12'>
                    <h3 style={{ cursor: "pointer" }} onClick={() => (window.location = '/registration')}>
                      Register/Login
                    </h3>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>

        {/* {this.props.one_comparision && !isEmpty(this.props.one_comparision) ? (
          <Fragment>
            <CompanyOverviewText data={this.props.one_comparision} />
            <CompanyOverviewFeatures data={this.props.one_comparision} />
          </Fragment>
        ) : null} */}
        <Footer history={this.props.history} />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    Comp: state.Comparisons.one_comparision,
    data: state.Offers.user_review,
    partnerReviews: state.Offers.partner_review,
    // partnerReviews: state.Offers.partner_review,
  };
};

export default connect(mapStateToProps, {
  transferWise,
  userReviews,
  partners,
})(withRouter(TransferWise));
//userReview: bindActionCreators(userReviews, dispatch),
