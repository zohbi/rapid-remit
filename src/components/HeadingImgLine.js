import React from 'react';
import '../css/bootstrap.css';
import '../css/style-cookies.css'

export default class HeadingImgLine extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
        }
    }

    render() {
        return (
            <div>
                <h1 className="h1Service">{this.props.data.heading}</h1>
                <img src={this.props.data.img} />
                <div className={this.props.redlineClass}></div>
            </div>
        )
    }

}