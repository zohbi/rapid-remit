import React from "react";

export default class JohnSlider extends React.Component {
  render() {
    return (
      <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
        <div className="carousel-inner">
          <div className="carousel-item active">
            <div className="row john-row">
              <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
              <div className="mr-john col-xl-4 col-lg-4 col-md-2 col-sm-2 col-2">
                <img
                  src={require("../img/aboutUs/invertedcoma.png")}
                  className="inverted-coma"
                />
                <div className="pic1">
                  <div className="pic-i"></div>
                  <div className="pic-j">
                    <img
                      src={require("../img/aboutUs/johnmusk.png")}
                      className="john-img"
                    />
                  </div>
                </div>
              </div>
              <div className="mr-john john-detail col-xl-5 col-lg-5 col-md-9 col-sm-9 col-8">
                <h5>Mr.John Musk</h5>
                <p id="para2">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat.
                </p>
              </div>
              <div className="mr-john col-xl-1 col-lg-1 col-md-1 col-sm-1 col-2">
                <img
                  src={require("../img/aboutUs/invertedcoma2.png")}
                  className="inverted-coma2"
                />
              </div>
              <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
            </div>
          </div>
          <div className="carousel-item">
            <div className="row john-row">
              <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
              <div className="mr-john col-xl-4 col-lg-4 col-md-2 col-sm-2 col-2">
                <img
                  src={require("../img/aboutUs/invertedcoma.png")}
                  className="inverted-coma"
                />
                <div className="pic1">
                  <div className="pic-i"></div>
                  <div className="pic-j">
                    <img
                      src={require("../img/aboutUs/john.png")}
                      className="john-img"
                    />
                  </div>
                </div>
              </div>
              <div className="mr-john john-detail col-xl-5 col-lg-5 col-md-9 col-sm-9 col-8">
                <h5>Mr.Musk</h5>
                <p id="para2">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat.
                </p>
              </div>
              <div className="mr-john col-xl-1 col-lg-1 col-md-1 col-sm-1 col-2">
                <img
                  src={require("../img/aboutUs/invertedcoma2.png")}
                  className="inverted-coma2"
                />
              </div>
              <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
            </div>
          </div>
        </div>
        <div className="carousels">
          <a
            className="carousel-control-prev"
            href="#carouselExampleControls"
            role="button"
            data-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="carousel-control-next"
            href="#carouselExampleControls"
            role="button"
            data-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div>
    );
  }
}
