import React from "react";
import { connect } from "react-redux";
import date from "date-and-time";

import { Promotions, availPromotions } from "../Store/actions/index";

class DashboardPromotionsUD extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      para:"Some quick example text to build on the card title and make upthe bulk of the card's content. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, exercitation emaperiam vel et ut aspernatur repellendus dolorem! Hic qui dignissimos recusandae in dolores eaque nemo nobis, esse fugiat mollitia sapiente"
    };
  }
  componentDidMount() {
    this.props.getData();
  }
  componentWillReceiveProps(props) {
    // console.log("ppppppp", props);
    let { promotions } = props;
    this.setState({ promotionsData: promotions });
    // console.log("successfully receive props", promotions);
  }
  getData = (data) => {
    let user_id = JSON.parse(localStorage.getItem("UserData"))._id;
    // console.log("usersadasda_id", localStorage.getItem("UserData"));
    let obj = { user_id, promotion_id: data._id };
    // console.log("obj", obj);
    this.props.availPromotions(obj);
  };
  render() {
    let { promotions } = this.state;
    // console.log("promotionsData", this.props.promotions[0]);
    const promodata = this.props.promotions;
    return (
      <div className="row no-gutters" id="DashboardPromotionsUD">
        {promodata.length < 0
          ? null
          : promodata.map((data) => {
              let datesecond = new Date(data.expiryDate);
              let now = new Date();
              let final = date.subtract(datesecond, now).toDays();
              return (
                <div
                  className="card"
                  style={{ width: "22rem", height: "26rem" }}
                  id="smallCard1"
                >
                  <img src={data.img} height="200"  className="card-img-top" alt="..." />
                  <div className="card-body">
                    <h5 style={{fontSize:14}} className="card-title">{data.title}</h5>
                    <h6 style={{fontSize:12}} className="card-subtitle mb-2 text-muted">
                      {data.subTitle && data.subTitle.length > 20 ? data.subTitle.slice(0,20)+"..." : data.subTitle }
                    </h6>
                    <p className="card-text" style={{fontSize:11, minHeight: 84}}>{data.text && data.text.length > 200 ? data.text.slice(0,200)+"..." : data.text } </p>
                    <a
                      href={data.url || "#"}
                      className="btn btn-primary"
                      id="smallCard1Button"
                      target={"_blank"}
                    >
                      Avail Now!
                    </a>
                    <p className="expireDate">
                      <small className="text-muted">
                        Expire in: {Math.ceil(final)} days
                      </small>
                    </p>
                  </div>
                </div>
              );
            })}
        {/* <div
          className="card"
          style={{ width: "22rem", height: "22.3rem" }}
          id="smallCard1"
        >
          <img
            src={require("../img/dashboard1/cardImg1.png")}
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">Avail amazing 50% discount</h5>
            <h6 className="card-subtitle mb-2 text-muted">
              You are just a single click away from Amazing discount
            </h6>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a
              href="#"
              className="btn btn-primary"
              id="smallCard1Button"
              onClick={() => this.getData()}
            >
              Avail Now!
            </a>
            <p className="expireDate">
              <small className="text-muted">Expire in: 4days</small>
            </p>
          </div>
        </div> */}
        {/* <div
          className="card"
          style={{ width: "22rem", height: "22.3rem" }}
          id="smallCard2"
        >
          <img
            src={require("../img/dashboard1/cardImg2.png")}
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">New Account discount upto 15%</h5>
            <h6 className="card-subtitle mb-2 text-muted">
              You are just a single click away from Amazing discount
            </h6>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary" id="smallCard2Button">
              Avail Now!
            </a>
            <p className="expireDate">
              <small className="text-muted">Expire in: 3days</small>
            </p>
          </div>
        </div> */}

        {/* <div className="cardBig" id="bigCard1">
          <div className="card" style={{ width: "30rem", height: "auto" }}>
            <div className="cross" id="bigCard1Cross">
              x
            </div>
            <img
              src={require("../img/dashboard1/cardImg1b.png")}
              className="card-img-top"
              alt="..."
            />
            <div className="card-body">
              <h5 className="card-title">New Account discount upto 15%</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                You are just a single click away from Amazing discount
              </h6>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content. Lorem ipsum dolor sit amet,
                consectetur adipisicing elit. Voluptatibus, exercitationem
                aperiam vel et ut aspernatur repellendus dolorem! Hic qui
                dignissimos recusandae in dolores eaque nemo nobis, esse fugiat
                mollitia sapiente.
              </p>
              <h6 className="promotionCode">PROMOTION CODE: SX014JEX</h6>
              <h6 className="promotionCode">PROMOTION CODE: SEJ76OPT</h6>
              <a href="#" className="btn btn-primary" id="bigCard2Button">
                Avail Now!
              </a>
            </div>
          </div>
        </div>
*/}
        {/* <div className="cardBig" id="bigCard2">
          <div className="card" style={{ width: "30rem", height: "auto" }}>
            <div className="cross" id="bigCard2Cross">
              x
            </div>
            <img
              src={require("../img/dashboard1/cardImg2b.png")}
              className="card-img-top"
              alt="..."
            />
            <div className="card-body">
              <h5 className="card-title">New Account discount upto 15%</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                You are just a single click away from Amazing discount
              </h6>
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content. Lorem ipsum dolor sit amet,
                consectetur adipisicing elit. Voluptatibus, exercitationem
                aperiam vel et ut aspernatur repellendus dolorem! Hic qui
                dignissimos recusandae in dolores eaque nemo nobis, esse fugiat
                mollitia sapiente.
              </p>
              <h6 className="promotionCode">PROMOTION CODE: SX014JEX</h6>
              <h6 className="promotionCode">PROMOTION CODE: SEJ76OPT</h6>
              <a href="#" className="btn btn-primary" id="bigCard2Button">
                Avail Now!
              </a>
            </div>
          </div>
        </div> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    promotions: state.Promotions.Promotions,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: (obj) => dispatch(Promotions.Promotions(obj)),
  availPromotions: (obj) => dispatch(availPromotions(obj)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardPromotionsUD);
