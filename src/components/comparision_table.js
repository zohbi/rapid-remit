import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import ContentLoader from 'react-content-loader';
import { connect } from 'react-redux';
import { Comparisons } from '../Store/actions/index';
import { bindActionCreators } from 'redux';

const MyLoader = () => <ContentLoader type='facebook' />;

class Comparision_table extends React.Component {
  btnClick() {
    // console.log('button', this.props.data);
    this.props.addToTable(this.props.data, this.props.history);
    //window.open("/companyoverview");
  }

  addFavourite(id) {
    if (localStorage.getItem('token')) {
      let user_id = JSON.parse(localStorage.getItem('UserData'))._id;
      // console.log(
      //   'user_id',
      //   user_id,
      //   JSON.parse(localStorage.getItem('token'))
      // );

      this.props.addFavourite(
        user_id,
        id,
        JSON.parse(localStorage.getItem('token'))
      );
    } else {
      window.location = '/registration';
    }
  }
  render() {
    let data = this.props.data;
    // console.log('datathistory', this.props.data);
    //const img = data.img;
    let currency_sign;
    if (
      this.props.data.country_name == 'USD' ||
      this.props.data.country_name == 'AUD' ||
      this.props.data.country_name == 'CAD'
    ) {
      currency_sign = '$';
    } else if (this.props.data.country_name == 'EGP') {
      currency_sign = 'E£';
    } else if (this.props.data.country_name == 'EGP') {
      currency_sign = '€';
    } else if (this.props.data.country_name == 'GBP') {
      currency_sign = '£';
    } else if (this.props.data.country_name == 'INR') {
      currency_sign = '₹';
    } else if (this.props.data.country_name == 'JOD') {
      currency_sign = 'د.ا';
    } else if (this.props.data.country_name == 'JOD') {
      currency_sign = 'د.ا';
    } else if (this.props.data.country_name == 'LKR') {
      currency_sign = 'රු';
    } else if (this.props.data.country_name == 'NPR') {
      currency_sign = 'रू';
    } else if (this.props.data.country_name == 'PHP') {
      currency_sign = '₱';
    } else if (this.props.data.country_name == 'PKR') {
      currency_sign = '₨';
    }

    // console.log('this.props.history', this.props.history);
    // console.table( data.partner_details)
    return (
      <div className='comp border border-success'>
        <div className='container2'>
          <div className='com_sec4'>
            <div className='row com_sec4_line1 col-md-12 col-lg-12 col-xl-12'>
              <div className='com_sec4_line1_col1 col-md-2 col-lg-2 col-xl-2'>
                <div className='com_sec4_line1_col1_img'>
                  {!this.props.data._id ? <h1>No record found</h1> : null}
                  <img
                    className='img_class'
                    src={
                      this.props.data &&
                      this.props.data.partner_details[0] &&
                      this.props.data.partner_details[0].img
                        ? this.props.data.partner_details[0].img
                        : 'no'
                    }
                  />
                </div>
                {/* <!----com_sec4_line1_img end----> */}
                <div className='com_sec4_line1_col1_text'>
                  <p>{data.reviews.length} REVIEWS</p>
                </div>
                {/* <!----com_sec4_line1_TEXT end----> */}
              </div>
              {/* <!------com_sec4_line1_col1 end------> */}

              <div className='com_sec4_line1_col col-md-1 col-lg-1 col-xl-1'>
                <h6>
                  {data.rate ? data.currency : null}{' '}
                  {data.rate ? data.rate.toFixed(2) : null}
                </h6>
              </div>
              {/* <!-----com_sec4_line1_col2 end------> */}

              <div className='com_sec4_line1_col col-md-2 col-lg-2 col-xl-2'>
                <h6>
                  {this.props.data.currency}{' '}
                  {this.props.data
                    ? `${currency_sign}` +
                      this.props.data.amount_recievable.toFixed(2) +
                      ' /-'
                    : null}
                </h6>
              </div>
              {/* <!-----com_sec4_line1_col3 end------> */}

              <div className='com_sec4_line1_col col-md-1 col-lg-1 col-xl-1'>
                <h6>AED {this.props.data.payment_fees}</h6>
              </div>
              {/* <!-----com_sec4_line1_col4 end------> */}

              <div className='com_sec4_line1_col col-md-2 col-lg-2 col-xl-2'>
                <h6>{this.props.data.transfer_time}</h6>
              </div>
              <div className='com_sec4_line1_col col-md-2 col-lg-2 col-xl-2'>
                <h6>{this.props.data.taxes_rule} AED</h6>
              </div>
              {/* <!-----com_sec4_line1_col5 end------> */}

              <div className='col-md-2 col-lg-2 col-xl-2'>
                {
                  <a href={this.props.data.partner_details[0].website_url}>
                    <button className='Lato_Regular'>VISIT</button>
                  </a>
                }
              </div>
              {/* <!-----com_sec4_line1_col6 end------> */}
              {/* to={`/DashboardLayout/${this.props.data._id}`} */}
              <div className='com_sec4_line1_col col-md-2 col-lg-2 col-xl-2'>
                <div className='com_sec4_line1_col7_row1'>
                  <img src={this.props.data.img1} />
                  <a
                    href=''
                    onClick={() => {
                      this.addFavourite(this.props.data.partner_id);
                    }}
                  >
                    ADD TO FAVOURITE
                  </a>
                  {/* <a href= "/DashboardLayout/"+ >ADD TO FAVOURITE</a> */}
                </div>
                {/* <!----com_sec4_line1_img end----> */}
                <div className='com_sec4_line1_col7_row2'>
                  <img src={this.props.data.img2} />
                  <a href='' onClick={() => this.btnClick()}>
                    ADD TO TABLE
                  </a>
                </div>
                {/* <!----com_sec4_line1_TEXT end----> */}
                <div className='com_sec4_line1_col7_row3'>
                  <Link to={`/partner/${this.props.data._id}`}>
                    <p>VIEW FULL DETAIL</p>
                  </Link>
                </div>
                {/* <!----com_sec4_line1_TEXT end----> */}
              </div>
              {/* <!-----com_sec4_line1_col7 end------> */}
            </div>
            {/* <!----com_sec4_line1 end----> */}
          </div>

          {/* <!--Small Scale Comparison Table--> */}
          <div className='small_table'>
            <div className='row small_scale_table'>
              <div className='com_tab col-sm-5 col-5'>
                <h6>PROVIDER</h6>
              </div>

              <div className='com_table com_sec4_line1_col1_text col-sm-7 col-7'>
                <img src={this.props.data.img} />
                <p>{this.props.data.paragraph}</p>
              </div>
            </div>
            <div className='row small_scale_table'>
              <div className='com_tab col-sm-5 col-5'>
                <h6>RATE</h6>
              </div>

              <div className='com_table col-sm-7 col-7'>
                <h6>{data.payment_fees}</h6>
              </div>
            </div>
            <div className='row small_scale_table'>
              <div className='com_tab col-sm-5 col-5'>
                <h6>AMOUNT RECEIVABLE</h6>
              </div>

              <div className='com_table col-sm-7 col-7'>
                <h6>{this.props.data.heading2}</h6>
              </div>
            </div>
            <div className='row small_scale_table'>
              <div className='com_tab col-sm-5 col-5'>
                <h6>FEE</h6>
              </div>

              <div className='com_table col-sm-7 col-7'>
                <h6>{data.payment_fees}</h6>
              </div>
            </div>
            <div className='row small_scale_table'>
              <div className='com_tab col-sm-5 col-5'>
                <h6>TRANSFER TIME</h6>
              </div>

              <div className='com_table col-sm-7 col-7'>
                <h6>{this.props.data.heading4}</h6>
              </div>
            </div>
            <div className='row small_scale_table'>
              <div className='com_tab col-sm-5 col-5'>
                <h6>WEBSITE</h6>
              </div>

              <div className='com_table col-sm-7 col-7'>
                <a href=''>
                  <button className='Lato_Regular'>VISIT</button>
                </a>
              </div>
            </div>
            <div className='row small_scale_table'>
              <div className='com_tab col-sm-5 col-5'>
                <h6>COMPARE TABLE</h6>
              </div>

              <div className='com_table col-sm-7 col-7'>
                <div className='com_sec4_line1_col7_row1'>
                  <img src={this.props.data.img1} />
                  <a href='/DashboardLayout'>ADD TO FAVOURITE</a>
                </div>
                {/* <!----com_sec4_line1_img end----> */}
                <div className='com_sec4_line1_col7_row2'>
                  <img src={this.props.data.img2} />
                  <a href='' onClick={(e) => this.btnClick(e)}>
                    ADD TO TABLE
                  </a>
                </div>
                {/* <!----com_sec4_line1_TEXT end----> */}
                <div className='com_sec4_line1_col7_row3'>
                  <Link to={`/tranferwise`}>
                    <p>VIEW FULL DETAIL</p>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    table: state.Comparisons.table,
  };
};

const mapDispatchToProps = (dispatch) => ({
  addToTable: bindActionCreators(Comparisons.table, dispatch),
  addFavourite: bindActionCreators(Comparisons.addFavourite, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Comparision_table);
