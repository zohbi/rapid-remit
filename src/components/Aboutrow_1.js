import React from 'react';
import '../css/bootstrap.css';
import '../css/style-about.css';
  
export default class Aboutrow_1 extends React.Component{
componentDidMount(){
    // document.getElementById("bgNone").style.backgroundImage="none";
}
  render() {
    return (
        <div>

            <div className="container-fluid about-us" id="bgNone">
                <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h1 className="Lato_Black">{this.props.data.heading}</h1>
                    </div>
                </div>
            </div>

            <div className="container1">
                <div className="row about-second-container">
                    <div className="col-xl-5 col-lg-5 col-md-10 col-sm-12 col-12">
                        <h2 className="Lato_Black">{this.props.data.heading}</h2>
                        <h5 className="Lato_Black">{this.props.data.heading2}</h5>
                        <div className="horizontal-red-line col-xl-5 col-lg-6 col-md-8 col-sm-11 col-11">
                        </div>
                    </div>

                    <div className="about-us-imgs col-xl-6 col-lg-7 col-md-0 col-sm-0 col-0">
                        <div className="img1"><img src={this.props.data.img1}/></div>
                        <div className="img2"><img src={this.props.data.img2}/></div>
                        <div className="img3"><img src={this.props.data.img3}/></div>
                    </div>

                    <div className="about-para col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                        <p className="Lato_Regular">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Excepteur sint occaecat
                            cupidatat nonproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <div className="about-para2 col-xl-8 col-lg-11 col-md-12 col-sm-12 col-12">
                        <p className="Lato_Regular">Lorem ipsum dolor sit amet, consectetur adipisicing elit.<br/> sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua.<br/> Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Excepteur sint occaecat
                            cupidatat nonproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h1 className="Lato_Black">About Us</h1>
                    </div>
                </div>
            </div>

        </div>
    )
  }
}