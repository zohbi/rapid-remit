import React from "react";
import "../css/bootstrap.css";
import "../css/style-cookies.css";
import HeadingImgLine from "./HeadingImgLine";
import RedBlack_Headings from "./RedBlack_Headings";
import HeadngPlusPara from "./headngPlusPara";
import Footer from "../components/footer";
import Fade from "react-reveal/Fade";
import { connect } from "react-redux"
import { getCookieHeading } from "../Store/actions/userAuth";
import ReactHtmlParser from "react-html-parser"
 class CookiesPolicy extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      HeadingImgLine: {
        heading: "Cookies Policy",
        img: require("../img/cookies/cookiesImg1.png")
      },

      RedBlack_Headings: {
        redHeading: "Cookies Policy",
        blackHeading: "Lorem Ipsum is simply dummy text of the printing."
      },

      arr: [
        {
          heading: "What is Lorem Ipsum?",
          paragraph:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!"
        },
        {
          heading: "Why Do we use It?",
          paragraph:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!"
        },
        {
          heading: "Where does It come from?",
          paragraph:
            "dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!",
          paragraph2:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere ut fugiat beatae ea eveniet sit quia quis, iste laborum harum, itaque aut odio pariatur. Quos obcaecati suscipit asperiores temporibus nam?"
        },
        {
          heading: "Where can I get some?",
          paragraph:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!. Lpsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lpsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lpsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!"
        }
      ]
    };
  }
  componentDidUpdate(prvProps) {
    if (prvProps.cookiesheading !== this.props.cookiesheading) {
      this.setState({
        cookiesheadingData: this.props.cookiesheading?.text
      })
    }
  }
  componentDidMount() {
    this.props.getCookieHeading()
  }

  render() {
    const {cookiesheadingData} = this.state
    return (
      <div className="coooom">
        <div className="container-fluid cookies">
          <div className="container1">
            <Fade left>
              <HeadingImgLine
                data={this.state.HeadingImgLine}
                redlineclassName="cookiesRedLine"
              />
            </Fade>
            <Fade right>
            <div className="cookiesContent">
              {/* <RedBlack_Headings data={this.state.RedBlack_Headings} />
              <Fade left>
                {this.state.arr.map((value, abc) => {
                  return <HeadngPlusPara key={abc} data={value} />;
                })}
              </Fade> */}
              {ReactHtmlParser(cookiesheadingData)}
            </div>
              </Fade>
          </div>
        </div>
        <Footer history={this.props.history} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cookiesheading: state.FAQS.cookiesheading,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getCookieHeading: () => dispatch(getCookieHeading())
});

export default connect(mapStateToProps, mapDispatchToProps)(CookiesPolicy);