import React from 'react';

export default class DashboardHistoryUD extends React.Component {
    render() {
            return(
                <div className="questions" id="DashboardHistoryUD">
                    <div className="question" id="question1">
                        <h6>TRANSACTION-AED 55,000</h6>
                        <h6 className="quesionH6Second">STATUS-CLOSED</h6>
                        <h5 className="plus">
                            <img className="img1UD" src={require("../img/dashboard1/dropdown1.png")} alt="" />
                            <img className="img2UD" src={require("../img/dashboard1/dropdown2.png")} alt="" />
                        </h5>
                        <p>it amet consectetur adipisicing elit. Modi nobis exercitationem quo labore, in voluptas necessitatibus
                        nihil. Voluptas cumque iure mollitia placeat molestias a beatae, consequuntur nam provident obcaecati?
                        Dolor? Lorem ipsum dolor, sit amet consectetur adipisicing elit.</p>
                    </div>
                    <div className="question" id="question2">
                        <h6>TRANSACTION-AED 45,000</h6>
                        <h6 className="quesionH6Second">STATUS-CLOSED</h6>
                        <h5 className="plus">
                            <img className="img1UD" src={require("../img/dashboard1/dropdown1.png")} alt="" />
                            <img className="img2UD" src={require("../img/dashboard1/dropdown2.png")} alt="" />
                        </h5>
                        <p>it amet consectetur adipisicing elit. Modi nobis exercitationem quo labore, in voluptas necessitatibus
                        nihil. Voluptas cumque iure mollitia placeat molestias a beatae, consequuntur nam provident obcaecati?
                        Dolor? Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi nobis exercitationem quo labore,
                        elit. Modi nobis exercitationem quo labore, in voluptas necessitatibus nihil. Voluptas cumque iure
                        mollitia placeat molestias a beatae.</p>
                    </div>
                    <div className="question" id="question3">
                        <h6>TRANSACTION-AED 32,000</h6>
                        <h6 className="quesionH6Second">STATUS-CLOSED</h6>
                        <h5 className="plus">
                            <img className="img1UD" src={require("../img/dashboard1/dropdown1.png")} alt="" />
                            <img className="img2UD" src={require("../img/dashboard1/dropdown2.png")} alt="" />
                        </h5>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi nobis exercitationem quo labore, in
                        voluptas necessitatibus nihil. Voluptas cumque iure mollitia placeat molestias a beatae, consequuntur nam
                        provident obcaecati? Dolor? Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi nobis
                        exercitationem quo labore, in voluptas necessitatibus nihil. Voluptas cumque iure mollitia placeat
                        molestias a beatae, consequuntur nam provident obcaecati? Dolor?</p>
                    </div>
                    <div className="question" id="question4">
                        <h6>TRANSACTION-AED 20,000</h6>
                        <h6 className="quesionH6Second">STATUS-CLOSED</h6>
                        <h5 className="plus">
                            <img className="img1UD" src={require("../img/dashboard1/dropdown1.png")} alt="" />
                            <img className="img2UD" src={require("../img/dashboard1/dropdown2.png")} alt="" />
                        </h5>
                        <p>dolor, sit amet elit. Modi nobis exercitationem quo labore, in voluptas necessitatibus nihil. Voluptas
                        cumque iure mollitia placeat molestias a beatae, consequuntur nam provident obcaecati? Dolor? Lorem ipsum
                        dolor, sit amet consectetur cumque iure mollitia placeat consectetur adipisicing elit. Modi nobis
                        exercitationem quo labore, in voluptas necessitatibus nihil. Voluptas cumque iure mollitia placeat
                        molestias a beatae, consequuntur nam provident obcaecati? Dolor? Lorem ipsum dolor, sit amet consectetur
                        adipisicing elit. Modi nobis exercitationem quo labore.</p>
                    </div>
                    <div className="question" id="question5">
                        <h6>TRANSACTION-AED 70,000</h6>
                        <h6 className="quesionH6Second">STATUS-CLOSED</h6>
                        <h5 className="plus">
                            <img className="img1UD" src={require("../img/dashboard1/dropdown1.png")} alt="" />
                            <img className="img2UD" src={require("../img/dashboard1/dropdown2.png")} alt="" />
                        </h5>
                        <p>Dolor, sit amet consectetur adipisicing elit. Modi nobis exercitationem quo labore, in voluptas
                        necessitatibus nihil. Voluptas cumque iure mollitia placeat molestias a beatae, consequuntur nam provident
                        obcaecati? Dolor? Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi nobis exercitationem quo
                        labore, in voluptas necessitatibus nihil. Voluptas cumque iure mollitia placeat molestias a beatae,
                        consequuntur nam provident obcaecati? Dolor?</p>
                    </div>
                </div>
            )
        }
    }