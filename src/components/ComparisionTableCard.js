import React, { Component } from 'react';
import '../css/bootstrap.css';
import '../css/style-footer.css';
import '../css/style-comprision.css';
import '../css/comparisonTableCard.css';
import { connect } from 'react-redux';
import 'react-activity/dist/react-activity.css';
import { Comparisons } from "../Store/actions/userAuth"


class ComparisionTableCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        function cs(data) {
            let currency_sign = '';
            if (data == 'USD' || data == 'AUD' || data == 'CAD') {
                currency_sign = '$';
            } else if (data == 'EGP') {
                currency_sign = 'E';
            } else if (data == 'EUR') {
                currency_sign = '€';
            } else if (data == 'GBP') {
                currency_sign = '£';
            } else if (data == 'INR') {
                currency_sign = '₹';
            } else if (data == 'JOD') {
                currency_sign = 'د.ا';
            } else if (data == 'JOD') {
                currency_sign = 'د.ا';
            } else if (data == 'LKR') {
                currency_sign = 'රු';
            } else if (data == 'NPR') {
                currency_sign = 'रू';
            } else if (data == 'PHP') {
                currency_sign = '₱';
            } else if (data == 'PKR') {
                currency_sign = '₨';
            }
            return currency_sign;
        }
        const { data, table_mark, ind } = this.props
        // console.log("data", data)
        return (
            <div key={ind} className="card comparision-card-table">
                <div className="row no-gutters justify-content-center comparision-card-table-main-row">
                    <div className="col-sm-12">
                        <div className="row no-gutters justify-content-end ">
                            {this.props.fav_mark.includes(
                                data.partner_details[0]._id
                            ) ? (
                                    <img
                                        style={{ cursor: "pointer" }}
                                        onClick={() => {
                                            this.props.removeFavourite(data.partner_id);
                                        }}
                                        className='icons'
                                        src={require('../img/heart_filled.png')}
                                    />
                                ) : (
                                    <img
                                        style={{ cursor: "pointer" }}
                                        onClick={() => {
                                            this.props.addFavourite(data.partner_id);
                                        }}
                                        className='icons'
                                        src={require('../img/Heart outline.png')}
                                    />
                                )}
                            {/* <img className="card-img" src={require("../img/comparision/com-heart-icon.png")} style={{ width: '20px' }} alt="" /> */}
                        </div>
                        <div className="row no-gutters d-flex justify-content-center align-items-center">
                            <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-xs-7">
                                <div className="row no-gutters d-flex justify-content-center comparision-card-table-row-1"   >
                                    <img className="card-img comparision-card-table-brand-img" src={data.partner_details[0].img} alt="" />
                                </div>
                                <div className="row no-gutters d-flex justify-content-center align-items-center margin-Top-20" >
                                    <a style={{ textDecoration: "none" }} target="_blank" href={`${data.partner_details[0].website_url}`}  >
                                        <p style={{ color: "#a3a3a3" }} className="comparision-card-table-website-link">Visit Website</p>
                                    </a>
                                </div>
                            </div>
                            <div className="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-xs-5">
                                <div className="row no-gutters d-flex justify-content-center comparision-card-table-row-1">
                                    <div className="col-sm-11 col-md-10">
                                        <div className="row no-gutters">
                                            <p className="comparision-card-table-small-text" >Rate {cs(data.country_name)} {data.rate.toFixed(2)}</p>
                                        </div>
                                        <div className="row no-gutters" >
                                            <p className="comparision-card-table-medium-text" >{data.amount_recievable.toFixed(2)}/-</p>
                                        </div>
                                        <div className="row no-gutters">
                                            <p className="comparision-card-table-small-text" >Amount Receivable</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="row no-gutters justify-content-center d-flex margin-Top-20">
                                    <div className="col-sm-11 col-md-10">
                                        <p className="comparision-card-table-small-text" >Service Fee: <span title="indicates..." style={{ cursor: "pointer" }}>AED {data.payment_fees.toFixed(2)}</span></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-5 col-sm-12 col-xs-12">
                                <div className="row no-gutters d-flex justify-content-center align-items-center comparision-card-table-row-1 margin-Top-10" >
                                    <div className="col-sm-3 col-xs-3">
                                        <div className="row no-gutters d-flex justify-content-center flex-column" >
                                            <div className="col-sm-12">
                                                <div className="row no-gutters d-flex justify-content-center" >
                                                    <div className="col-sm-12" style={{ marginBottom: 5 }}>
                                                        <div className="row no-gutters d-flex justify-content-center" >
                                                            <img className="card-img comparision-card-table-img-transfer-time" src={require("../img/comparision/transfer-time-icon.png")} alt="" />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-12">
                                                        <div className="row no-gutters d-flex justify-content-center" >
                                                            <p className="comparision-card-table-extra-small-text" >{data.transfer_time}</p>
                                                        </div>
                                                    </div>
                                                    {/* <div className="col-sm-12">
                                                        <div className="row no-gutters d-flex justify-content-center" >
                                                            <p style={{ color: "black", marginBottom: 0, fontSize: 12, fontWeight: 600 }}>DAYS</p>
                                                        </div>
                                                    </div> */}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-4  col-xs-4">
                                        <div className="row no-gutters d-flex justify-content-center flex-column" >
                                            <div className="col-sm-12">
                                                <div className="row no-gutters d-flex justify-content-center" >
                                                    <img onClick={() =>
                                                        this.props.toggleChooseLocation(
                                                            data.location
                                                        )
                                                    } className="card-img comparision-card-table-img-location" src={require("../img/comparision/view-locations-icon.png")} alt="" />
                                                </div>
                                            </div>
                                            <div className="col-sm-12">
                                                <div className="row no-gutters d-flex justify-content-center" >
                                                    <p className="comparision-card-table-text-location">View Locations</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-5  col-xs-5">
                                        <div className="row no-gutters d-flex justify-content-center flex-column" style={{ marginTop: 13 }} >
                                            <div className="col-sm-12">
                                                <div className="row no-gutters d-flex justify-content-center" >
                                                    <img className="card-img comparision-card-table-img-check" src={require("../img/comparision/check-outlined-icon.png")} alt="" />
                                                </div>
                                            </div>
                                            <div className="col-sm-12">
                                                <div className="row no-gutters d-flex justify-content-center" >
                                                    <div
                                                        className='add-to-table-div'
                                                        style={{ cursor: "pointer" }}
                                                        onClick={() => this.props.btnClick(data)}
                                                    >
                                                        {table_mark.includes(data._id) ? (
                                                            <img
                                                                className='icons'
                                                                src={require('../img/checkbox filled.png')}
                                                            />
                                                        ) : (
                                                                <img
                                                                    className='icons'
                                                                    src={require('../img/checkbox_empty.png')}
                                                                />
                                                            )}
                                                        <small className="comparision-card-table-small-text margin-Left-10" >Add to Comparison</small>{' '}
                                                    </div>{' '}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row no-gutters d-flex justify-content-space-between margin-Top-20 align-items-center" >
                                    <div className="col-sm-8">
                                        <p className="comparision-card-table-small-text" >Receiving Fee: {data.tax_deducted.toFixed(2) > 0 ? "Applicable" : "Not Applicable"}</p>
                                    </div>
                                    <div className="col-sm-4">
                                        <div className="row no-gutters d-flex justify-content-end" >
                                            <p style={{ color: "black" }} className="comparision-card-table-website-link" onClick={() => {
                                                this.props.history.push(
                                                    `/partner/${data.partner_details[0]._id}`
                                                );
                                            }} >View Detail</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        );
    }
}

// const mapStateToProps = (state) => {
//     return {
//         isLoading: state.Comparisons.isLoading,
//         data: state.Comparisons.comparisons,
//         currencyData: state.Comparisons.currency,
//         contentData: state.Comparisons.content,
//         favData: state.Favourite.allData,
//         tableData: state.Comparisons.table,
//     };
// };

// const mapDispatchToProps = (dispatch) => ({
    //     getData: (obj) => dispatch(Comparisons.Comparisons(obj)),
    //     currency: () => dispatch(Comparisons.currency()),
    //     content: () => dispatch(content()),
    //     addToTable: bindActionCreators(Comparisons.table, dispatch),
    //     getFavourite: bindActionCreators(Comparisons.getFavourite, dispatch),
    //     addToTable: bindActionCreators(Comparisons.table, dispatch),
    //     empty: bindActionCreators(empty, dispatch),
    //     delTableIndex: bindActionCreators(Comparisons.delTableIndex, dispatch),
// });

export default connect(null, null)(ComparisionTableCard);
