import React from "react";
import { connect } from "react-redux";
import { Comparisons } from "../Store/actions/index";
import { bindActionCreators } from "redux";
import RatingCard from "../components/RatingCard";
import { Spinner } from "react-activity";
import "react-activity/dist/react-activity.css";

class DashboardReviewsComponent extends React.Component {
  componentWillMount() {
    // console.log("dddddd");
    this.getFavouriteList()
  }
  getFavouriteList=() =>{
    if (localStorage.getItem("token")) {
      let user_id = JSON.parse(localStorage.getItem("UserData"))._id;
      // console.log("user_id", user_id);
      let token = JSON.parse(localStorage.getItem("token"));
      // console.log("8user", token);

      this.props.getFavourite(user_id, token);
    } else {
      window.location = "/registration";
    }
  }
  render() {
    //console.log("dattttttt", this.props.data);
    let { data } = this.props;
    // console.log("dddddd", data, this.props.isloading);
    return (
      <div className="row w-100" id="DashboardReviewsComponent">
        {!this.props.isloading && data && data.length > 0 ? (
          data.map((fav) => (
            <RatingCard
              getFavouriteList={this.getFavouriteList}
              key={Math.random}
              data={fav.partners[0]}
              review={fav.reviews}
              dashboard={true}
              history={this.props.history}
            />
          ))
        ) : data.length == 0 && !this.props.isloading ? (
          <div
            style={{
              height: "100%",
              width: "100%",
              display: "flex",

              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <h1>No favourites</h1>
          </div>
        ) : (
          <div
            style={{
              height: "100%",
              width: "100%",
              display: "flex",

              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Spinner size="100px" />
          </div>
        )}

        {/* <div className="reveiw-div">
          <ul className="dropdown">
            <li
              id="navbarDropdownMenuLink"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              adf
            </li>
            <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <li href="#">lidf</li>
              <li href="#">lidf</li>

              <li href="#">lidf</li>

              <li href="#">lidf</li>

              <li href="#">lidf</li>
            </div>
          </ul>
          <textarea rows="4">sdf</textarea>
          <button className="btn float=right">Submit</button>
        </div> */}

        {/* <div className="col-xl-1 col-lg-1 col-md-1 col-sm-0 col-0"></div>
        <div className="col-xl-5 col-lg-5 col-md-5 col-sm-11 col-11">
          <div className="Dashboard_reviews_column">
            <div className="Dashboard_reviews_div_img">
              <img src={require("../img/our-network/city-2.png")} />
            </div>
            <div className="arrows_img">
              <a href="company-overview.html" target="_blank">
                <img src={require("../img/our-network/arrows.png")} />
              </a>
            </div>
            <h1 className="Lato_Black">City Express</h1>
            <i className="fas fa-star red"></i>
            <i className="fas fa-star red"></i>
            <i className="fas fa-star red"></i>
            <i className="fas fa-star gray"></i>
            <i className="fas fa-star gray"></i>
            <a href="CompanyOverview.html" target="_blank">
              <button
                className="Dashboard_reviews_see_details Lato_Regular"
                name="action"
              >
                SEE DETAILS
              </button>
            </a>
            <div id="r_num">
              <span className="Dashboard_reviews_review">REVIEWS</span>
              <br />
              <span className="Dashboard_reviews_three_twenty">320</span>
            </div>
          </div>
        </div>

        <div className="col-xl-5 col-lg-5 col-md-5 col-sm-11 col-11">
          <div className="Dashboard_reviews_column">
            <div className="Dashboard_reviews_div_img">
              <img src={require("../img/our-network/Remitly.png")} />
            </div>
            <div className="arrows_img">
              <a href="company-overview.html" target="_blank">
                <img src={require("../img/our-network/arrows.png")} />
              </a>
            </div>
            <h1 className="Lato_Black">Remitly</h1>
            <i className="fas fa-star red"></i>
            <i className="fas fa-star red"></i>
            <i className="fas fa-star red"></i>
            <i className="fas fa-star gray"></i>
            <i className="fas fa-star gray"></i>
            <a href="CompanyOverview.html" target="_blank">
              <button
                className="Dashboard_reviews_see_details Lato_Regular"
                name="action"
              >
                SEE DETAILS
              </button>
            </a>
            <div id="r_num">
              <span className="Dashboard_reviews_review">REVIEWS</span>
              <br />
              <span className="Dashboard_reviews_three_twenty">320</span>
            </div>
          </div>
        </div>

        <div className="col-xl-1 col-lg-1 col-md-1 col-sm-0 col-0"></div>

        <div className="col-xl-1 col-lg-1 col-md-1 col-sm-0 col-0"></div>
        <div className="col-xl-5 col-lg-5 col-md-5 col-sm-11 col-11">
          <div className="Dashboard_reviews_column">
            <div className="Dashboard_reviews_div_img">
              <img src={require("../img/our-network/transfer-2.png")} />
            </div>
            <div className="arrows_img">
              <a href="company-overview.html" target="_blank">
                <img src={require("../img/our-network/arrows.png")} />
              </a>
            </div>
            <h1 className="Lato_Black">Transfer Wise</h1>
            <i className="fas fa-star red"></i>
            <i className="fas fa-star red"></i>
            <i className="fas fa-star red"></i>
            <i className="fas fa-star gray"></i>
            <i className="fas fa-star gray"></i>
            <a href="CompanyOverview.html" target="_blank">
              <button
                className="Dashboard_reviews_see_details Lato_Regular"
                name="action"
              >
                SEE DETAILS
              </button>
            </a>
            <div id="r_num">
              <span className="Dashboard_reviews_review">REVIEWS</span>
              <br />
              <span className="Dashboard_reviews_three_twenty">320</span>
            </div>
          </div>
        </div>
        <div className="col-xl-5 col-lg-5 col-md-5 col-sm-11 col-11">
          <div className="Dashboard_reviews_column">
            <div className="Dashboard_reviews_div_img">
              <img src={require("../img/our-network/world-2.png")} />
            </div>
            <div className="arrows_img">
              <a href="company-overview.html" target="_blank">
                <img src={require("../img/our-network/arrows.png")} />
              </a>
            </div>
            <h1 className="Lato_Black">World Remit</h1>
            <i className="fas fa-star red"></i>
            <i className="fas fa-star red"></i>
            <i className="fas fa-star gray"></i>
            <i className="fas fa-star gray"></i>
            <i className="fas fa-star gray"></i>
            <a href="CompanyOverview.html" target="_blank">
              <button
                className="Dashboard_reviews_see_details Lato_Regular"
                name="action"
              >
                SEE DETAILS
              </button>
            </a>
            <div id="r_num">
              <span className="Dashboard_reviews_review">REVIEWS</span>
              <br />
              <span className="Dashboard_reviews_three_twenty">320</span>
            </div>
          </div>
        </div>
        <div className="col-xl-1 col-lg-1 col-md-1 col-sm-0 col-0"></div> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.Favourite.allData,
    isloading: state.Favourite.isLoading,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getFavourite: bindActionCreators(Comparisons.getFavourite, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardReviewsComponent);
