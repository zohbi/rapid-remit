import React from 'react';
import '../css/bootstrap.css';
import '../css/style-about.css';
import ReactHtmlParser from "react-html-parser"
export default class Aboutrow_3 extends React.Component{

  render() {
    //   console.log("this.props.data", this.props.data);
    return (
        <div>
            <div className="container1 about-forth-row-main-div">
                <div className="row about-forth-row">
                    <div className="col-xl-5 col-lg-5 col-md-5 col-sm-4 d-sm-block d-none">
                        <img src={this.props.data.img} className="mission"/>
                    </div>
                    <div className="our-mission col-xl-5 col-lg-5 col-md-6 col-sm-7 col-12">
                        <h1 className="Lato_Black">{this.props.data.heading}</h1>
                        <div className="horizontal-red-line col-xl-5 col-lg-5 col-md-5 col-sm-5 col-5">
                        </div>
                        {ReactHtmlParser(this.props.ourMissionHTML)}
                        {/* <p className="Lato_Regular">Providing transparent and comprehensive information
                        </p>
                        <p>Partner with remittance companies to promote best use of  their services.</p>
<p>
Applying real time innovative digital solution
</p> */}
                    </div>
                    <div className="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-0"></div>
                </div>
            </div>

            <div className="container1">
                <div className="row about-fifth-row">
                    <div className="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-0"></div>
                    <div className="our-vision col-xl-5 col-lg-5 col-md-6 col-sm-7 col-12">
                        <h1 className="Lato_Black">{this.props.data.heading2}</h1>
                        <div className="horizontal-red-line col-xl-5 col-lg-5 col-md-5 col-sm-5 col-4">
                        </div>
                        {/* <p className="Lato_Regular">Our vision to create a highly transparent compariosn service, empowering MENA consumers to make the right choice by bringing them all the latest details to compare in one place all the remittance options and putting the user in control of the remittance experience.</p> */}
                        {ReactHtmlParser(this.props.ourVisionHTML)}
                    </div>
                    <div className="col-xl-5 col-lg-5 col-md-5 col-sm-4 d-sm-block d-none">
                        <img src={this.props.data.img2} className="vision"/>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}