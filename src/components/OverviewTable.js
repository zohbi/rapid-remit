import React from "react";
import "../css/bootstrap.css";
import "../css/style-our-network.css";

export default class OverviewTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      locationLink: '',
      chooseLocation: false,
    };
  }
  toggleChooseLocation = (location) => {
    // console.log('LOCATION WE PICKED2', location[0].location.coordinates[1]);
    // const locationLink = `https://maps.google.com/maps?api=1&q=${location.location.coordinates[0].$numberDecimal},${location.location.coordinates[1].$numberDecimal}&hl=es&z=14&amp;output=embed`;
    const locationLink = `https://maps.google.com/maps?q=${location[0].location.coordinates[1]},${location[0].location.coordinates[0]}&hl=es&z=14&output=embed`;
    // console.log('LOCATION WE PICKED3', locationLink);

    this.setState({
      chooseLocation: !this.state.chooseLocation,
      location: location,
      locationLink: locationLink,
    });
  };
  render() {
    // console.log("check data", this.props.table);
    let { table } = this.props;
    const { chooseLocation } = this.state
    return (
      <div>
        {chooseLocation ? (
          <div className='location-viewer-wrapper'>
            <div className='location-viewer'>
              <div className='d-flex justify-content-between'>
                <h5 className='instruct'>
                  {this.state.location && this.state.location.address}
                </h5>
                <i
                  className='fa fa-times'
                  onClick={() => this.toggleChooseLocation(this.state.location)}
                ></i>
              </div>
              <div class='mapouter'>
                <div class='gmap_canvas'>
                  {this.state.locationLink !== '' && (
                    <iframe
                      width='760'
                      height='480'
                      id='gmap_canvas'
                      src={this.state.locationLink} //"https://maps.google.com/maps?q=jedda&t=&z=13&ie=UTF8&iwloc=&output=embed"
                      frameborder='0'
                      scrolling='no'
                      marginheight='0'
                      marginwidth='0'
                    ></iframe>
                  )}
                </div>
              </div>
            </div>
          </div>
        ) : (
          ''
        )}
        <div className="row">
          <div className="overview-column1 col-xl-2 col-lg-2 col-md-2">
            <h5>RATE</h5>
          </div>
          <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3">
            <p>
              {table.length >= 1 ? (
                table[0].currency ? (
                  <span>{table[0].currency}</span>
                ) : null
              ) : (
                ""
              )}{" "}
              {table.length >= 1 ? table[0].rate.toFixed(3) : ""}{" "}
            </p>
          </div>
          <div className="overview-columns p-margin1 col-xl-3 col-lg-3 col-md-3">
            <p>
              {table.length >= 2 ? (
                table[1].currency ? (
                  <span>{table[1].currency}</span>
                ) : null
              ) : (
                ""
              )}{" "}
              {table.length >= 2 ? table[1].rate.toFixed(3) : ""}{" "}
            </p>
          </div>

          <div className="overview-columns p-margin col-xl-4 col-lg-4 col-md-4">
            <p>
              {table.length >= 3 ? (
                table[2].currency ? (
                  <span>{table[2].currency}</span>
                ) : null
              ) : (
                ""
              )}{" "}
              {table.length >= 3 ? table[2].rate.toFixed(3) : ""}{" "}
            </p>
          </div>
          {/* <!--colunms end--> */}
        </div>
        <div className="row">
          <div className="overview-column1 col-xl-2 col-lg-2 col-md-2">
            <h5>TRANSFER FEE</h5>
          </div>
          <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3">
          <p>{table.length >= 1 && "Applicable "}</p>

            {/* <p>{table.length >= 1 ? "Applicable " + table[0].payment_fees : ""}</p */}
          </div>
          <div className="overview-columns p-margin1 col-xl-3 col-lg-3 col-md-3">
            <p>{table.length >= 2 && "Applicable " }</p>
            {/* <p>{table.length >= 2 ? "Applicable " + table[1].payment_fees : ""}</p> */}
          </div>

          <div className="overview-columns p-margin col-xl-4 col-lg-4 col-md-4">
            <p>{table.length >= 3 && "Applicable "}</p>
            {/* <p>{table.length >= 3 ? "Applicable " + table[2].payment_fees : ""}</p> */}
          </div>
          {/* <!--colunms end--> */}
        </div>
        <div className="row">
          <div className="overview-column1 col-xl-2 col-lg-2 col-md-2">
            <h5>PAYMENT TYPE</h5>
          </div>
          <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3">
            <ul>
              {table.length >= 1
                ? table[0].partner_details[0].payment_methods.map((m) => (
                    <li style={{textTransform: 'capitalize'}}>{m}</li>
                  ))
                : ""}
            </ul>
          </div>
          <div className="overview-columns p-margin1 col-xl-3 col-lg-3 col-md-3">
            <ul>
              {table.length >= 2
                ? table[1].partner_details[0].payment_methods.map((m) => (
                    <li style={{textTransform: 'capitalize'}}>{m}</li>
                  ))
                : ""}
            </ul>
          </div>

          <div className="overview-columns p-margin col-xl-4 col-lg-4 col-md-4">
            <ul>
              {table.length >= 3
                ? table[2].partner_details[0].payment_methods.map((m) => (
                    <li style={{textTransform: 'capitalize'}}>{m}</li>
                  ))
                : ""}
            </ul>
          </div>
          {/* <!--colunms end--> */}
        </div>
        <div className="row">
          <div className="overview-column1 col-xl-2 col-lg-2 col-md-2">
            <h5>CONTACT</h5>
          </div>
          <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3">
            <p>{table.length >= 1 ? `+${table[0].partner_details[0].phone}` : ""}</p>
          </div>
          <div className="overview-columns p-margin1 col-xl-3 col-lg-3 col-md-3">
            <p>{table.length >= 2 ? `+${table[1].partner_details[0].phone}` : ""}</p>
          </div>

          <div className="overview-columns p-margin col-xl-4 col-lg-4 col-md-4">
            <p>{table.length >= 3 ? `+${table[2].partner_details[0].phone}` : ""}</p>
          </div>
          {/* <!--colunms end--> */}
        </div>
        <div className="row">
          <div className="overview-column1 col-xl-2 col-lg-2 col-md-2">
            <h5>COMPANY TYPE</h5>
          </div>
          <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3">
            <p>
              {table.length >= 1
                ? table[0].partner_details[0].company_type
                : ""}
            </p>
          </div>
          <div className="overview-columns p-margin1 col-xl-3 col-lg-3 col-md-3">
            <p>
              {table.length >= 2
                ? table[1].partner_details[0].company_type
                : ""}
            </p>
          </div>

          <div className="overview-columns p-margin col-xl-4 col-lg-4 col-md-4">
            <p>
              {table.length >= 3
                ? table[2].partner_details[0].company_type
                : ""}
            </p>
          </div>
          {/* <!--colunms end--> */}
        </div>
        {
                          }
        <div className="row">
          <div className="overview-column1 col-xl-2 col-lg-2 col-md-2">
            <h5>MOBILE APP</h5>
          </div>
          <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3">
            <p>
              {table.length >= 1 && table[0]?.partner_details[0] && <>
              {((table[0].partner_details[0].ios_url !== "Not Available " || !table[0].partner_details[0].ios_url) || (table[0].partner_details[0].android_url !== "Not Available " || !table[0].partner_details[0].android_url)) ?
                <>
                  {table[0].partner_details[0].android_url && table[0].partner_details[0].android_url !== "Not Available " && <a href={table[0].partner_details[0].android_url} target="_blank" ><img style={{marginRight:20, }} width="40" height="40" src={require('../img/play-playstore-icon.png')} /></a>}
                  {table[0].partner_details[0].ios_url && table[0].partner_details[0].ios_url !== "Not Available " && <a href={table[0].partner_details[0].ios_url}  target="_blank"  ><img width="40" height="40" src={require('../img/iOS-Apple-icon.png')} /></a>}
                </>
                :
                "Not Available"
              }
            </>
            }
            </p>
          </div>
          <div className="overview-columns p-margin1 col-xl-3 col-lg-3 col-md-3">
            <p>
            {table.length >= 1 && table[1]?.partner_details[0] && <>
              {((table[1].partner_details[0]?.ios_url !== "Not Available " || !table[1].partner_details[0].ios_url) || (table[1].partner_details[0].android_url !== "Not Available " || !table[1].partner_details[0].android_url)) ?
                <>
                  {table[1].partner_details[0].android_url && table[1].partner_details[0].android_url !== "Not Available " && <a href={table[1].partner_details[0].android_url} target="_blank" ><img style={{marginRight:20}} width="40" height="40" src={require('../img/play-playstore-icon.png')} /></a>}
                  {table[1].partner_details[0].ios_url && table[1].partner_details[0].ios_url !== "Not Available " && <a href={table[1].partner_details[0].ios_url}  target="_blank"  ><img width="40" height="40" src={require('../img/iOS-Apple-icon.png')} /></a>}
                </>
                :
                "Not Available"
              }
            </>
            }
            </p>
          </div>

          <div className="overview-columns p-margin col-xl-4 col-lg-4 col-md-4">
            <p>
            {table.length >= 1 && table[2]?.partner_details[0] && <>
              {((table[2].partner_details[0].ios_url !== "Not Available " || !table[2].partner_details[0].ios_url) || (table[2].partner_details[0].android_url !== "Not Available " || !table[2].partner_details[0].android_url)) ?
                <>
                  {table[2].partner_details[0].android_url && table[2].partner_details[0].android_url !== "Not Available " && <a href={table[2].partner_details[0].android_url} target="_blank" ><img style={{marginRight:20}} width="40" height="40" src={require('../img/play-playstore-icon.png')} /></a>}
                  {table[2].partner_details[0].ios_url && table[2].partner_details[0].ios_url !== "Not Available " && <a href={table[2].partner_details[0].ios_url}  target="_blank"  ><img width="40" height="40" src={require('../img/iOS-Apple-icon.png')} /></a>}
                </>
                :
                "Not Available"
              }
            </>
            }
            </p>
          </div>
          {/* <!--colunms end--> */}
        </div>
        <div className="row">
          <div className="overview-column1 col-xl-2 col-lg-2 col-md-2">
            <h5>KEY FEATURES</h5>
          </div>
          <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3">
            <ul>
              {table.length >= 1
                ? table[0].partner_details[0].key_features.map((data) => (
                    <li>{data}</li>
                  ))
                : ""}
            </ul>
          </div>
          <div className="overview-columns p-margin1 col-xl-3 col-lg-3 col-md-3">
            <ul>
              {table.length >= 2
                ? table[1].partner_details[0].key_features.map((data) => (
                    <li>{data}</li>
                  ))
                : ""}
            </ul>
          </div>

          <div className="overview-columns p-margin col-xl-4 col-lg-4 col-md-4">
            <ul>
              {table.length >= 3
                ? table[2].partner_details[0].key_features.map((data) => (
                    <li>{data}</li>
                  ))
                : ""}
            </ul>
          </div>
          {/* <!--colunms end--> */}
        </div>
        <div className="row">
          <div className="overview-column1 col-xl-2 col-lg-2 col-md-2">
            <h5>DOCUMENT</h5>
          </div>
          <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3">
            <ul>
              {table.length >= 1
                ? table[0].partner_details[0].documents.map((data) => (
                    <li>{data}</li>
                  ))
                : ""}
              {/* <li>{this.props.data.doc1[0]}</li>
              <li>{this.props.data.doc1[1]}</li>
              <li>{this.props.data.doc1[2]}</li>
              <li>{this.props.data.doc1[3]}</li> */}
            </ul>
          </div>
          <div className="overview-columns p-margin1 col-xl-3 col-lg-3 col-md-3">
            <ul>
              {table.length >= 2
                ? table[1].partner_details[0].documents.map((data) => (
                    <li>{data}</li>
                  ))
                : ""}
            </ul>
          </div>

          <div className="overview-columns p-margin col-xl-4 col-lg-4 col-md-4">
            <ul>
              {table.length >= 3
                ? table[2].partner_details[0].documents.map((data) => (
                    <li>{data}</li>
                  ))
                : ""}
            </ul>
          </div>
          {/* <!--colunms end--> */}
        </div>
     
        {/* <div className="row">
          <div className="overview-column1 col-xl-2 col-lg-2 col-md-2">
            <h5 className="actions-heading">NEARBY LOCATION</h5>
          </div>
                  {this.props.table && this.props.table.map((table,index)=>{
                        return(
                          <div className={`overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3`}>
                             <div className="map-btn-div" style={{paddingLeft: index === 0 ? 20 : index === 1 ? 40 : index === 2 ? 70 : 0, paddingTop:5}} >
                                <i
                                  className='fa fa-map-marked-alt'
                                  onClick={() =>
                                    this.toggleChooseLocation(
                                      table.location
                                    )
                                  }
                                ></i>
                            </div>
                        </div>
                        )
                  })}
        </div> */}
        <div className="row">
          <div className="overview-column1 col-xl-2 col-lg-2 col-md-2">
            <h5>NEARBY LOCATION</h5>
          </div>
          <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3">
            <p style={{cursor:"pointer"}} onClick={()=> this.props.delTableIndex(0)}> 
              {table.length >= 1
                ?  <iframe
                className="comparision-map-width"
                height='100'
                id='gmap_canvas'
                src={`https://maps.google.com/maps?q=${table[0].location[0].location.coordinates[1]},${table[0].location[0].location.coordinates[0]}&hl=en&z=14&output=embed`} 
                frameborder='0'
                scrolling='no'
                marginheight='0'
                marginwidth='0'
              ></iframe>
                : ""}
            </p>
          </div>
          <div className="overview-columns p-margin1 col-xl-3 col-lg-3 col-md-3">
            <p style={{cursor:"pointer"}} onClick={()=> this.props.delTableIndex(1)}>
              {table.length >= 2
                ?  <iframe
                className="comparision-map-width"
                height='100'
                id='gmap_canvas'
                src={`https://maps.google.com/maps?q=${table[1].location[0].location.coordinates[1]},${table[1].location[0].location.coordinates[0]}&hl=en&z=14&output=embed`} 
                frameborder='0'
                scrolling='no'
                marginheight='0'
                marginwidth='0'
              ></iframe>
                : ""}
            </p>
          </div>

          <div className="overview-columns p-margin col-xl-4 col-lg-4 col-md-4">
            <p style={{cursor:"pointer"}} onClick={()=> this.props.delTableIndex(2)}>
              {table.length >= 3
                ?  <iframe
                
                className="comparision-map-width"
                height='100'
                id='gmap_canvas'
                src={`https://maps.google.com/maps?q=${table[2].location[0].location.coordinates[1]},${table[2].location[0].location.coordinates[0]}&hl=en&z=14&output=embed`} 
                frameborder='0'
                scrolling='no'
                marginheight='0'
                marginwidth='0'
              ></iframe>
                : ""}
                
            </p>
          </div>
          {/* <!--colunms end--> */}
        </div>

        <div className="row">
          <div className="overview-column1 col-xl-2 col-lg-2 col-md-2">
            <h5>ACTION</h5>
          </div>
          <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3">
            <p style={{cursor:"pointer", color:"#f90d26", fontWeight:700}} onClick={()=> this.props.delTableIndex(0)}> 
              {table.length >= 1
                ? "Remove"
                : ""}
            </p>
          </div>
          <div className="overview-columns p-margin1 col-xl-3 col-lg-3 col-md-3">
            <p style={{cursor:"pointer", color:"#f90d26", fontWeight:700}} onClick={()=> this.props.delTableIndex(1)}>
              {table.length >= 2
                ? "Remove"
                : ""}
            </p>
          </div>

          <div className="overview-columns p-margin col-xl-4 col-lg-4 col-md-4">
            <p style={{cursor:"pointer", color:"#f90d26", fontWeight:700}} onClick={()=> this.props.delTableIndex(2)}>
              {table.length >= 3
                ? "Remove"
                : ""}
                
            </p>
          </div>
          {/* <!--colunms end--> */}
        </div>

        {/* <div className="row">
          <div className="overview-column1 col-xl-2 col-lg-2 col-md-2">
            <h5 className="actions-heading">ACTION</h5>
          </div>


                  {this.props.table && this.props.table.map((table,index)=>{
                        return(
                          <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3">
                          <div onClick={()=> this.props.delTableIndex(index)} className="delete-btn-div"  style={{paddingLeft: index === 0 ? 20 : index === 1 ? 40 : index === 2 ? 70 : 0}}>
                             <i className="fa fa-trash"></i>
                          </div>
                        </div>
                        )
                  })}
        </div> */}
    





     
     
      </div>
    );
  }
}
