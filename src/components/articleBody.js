import React from 'react';


export default class ArticleBody extends React.Component {
    render() {
        return (
            <div>
                <div className="row articleRow2">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h1 style={{    lineHeight: 1}}>{this.props.data.title}</h1>
                    </div>
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <p>{this.props.data.text}</p>
                        {/* <p>{this.props.data.paragraph2}</p> */}
                    </div>
                    {/* <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div className="picDiv1">
                            <div className="picDiva"></div>
                            <div className="picDivb">
                                <img src={this.props.data.img2} />
                            </div>
                        </div>
                    </div> */}
                </div>

                {/* <div className="row articleRow3">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <p>{this.props.data.paragraph3}</p>
                    <p>{this.props.data.paragraph4}</p>
                    </div>
                </div> */}

            </div>
        )
    }
}