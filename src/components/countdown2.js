import React from "react";
import "../css/bootstrap.css";
import "../css/countdown.css";
import "../css/style-contactUs+specialOffer+blogs.css";
import Countdown from "react-countdown-now";

export default class Countdown2 extends React.Component {
  render() {
    let { data } = this.props;
    // console.log("countdown", data);
    // Random component
    const Completionist = () => <span>You are good to go!</span>;

    // Renderer callback with condition
    const renderer = ({
      total,
      days,
      hours,
      minutes,
      seconds,
      milliseconds,
      completed,
    }) => {
      if (completed) {
        // Render a completed state
        return <Completionist />;
      } else {
        // Render a countdown
        return (
          <span>
            {days}:{hours}:{minutes}:{seconds}
          </span>
        );
      }
    };
    return (
      <div>
        <div className="specialOfferContent">
          <div className="special_sec2">
            <div className=" row special_sec2_row2 col-md-12">
              <div className="special_sec2_row2_col1 col-md-3">
                <div className="special_sec2_row2_col1_img">
                  <img src={data.img} alt="" />
                </div>
              </div>

              <div className="special_sec2_row2_col2 col-md-6">
                <div className="special_sec2_row2_col2_row1 Lato_Black">
                  <h4>{data.title}</h4>
                </div>

                <div className="special_sec2_row2_col2_row2">
                  <p>{data.text}</p>
                </div>
              </div>

              <div className="special_sec2_row2_col3 col-md-3">
                <div className="special_sec2_row2_col3_row1 Lato_Black">
                  <h6>Offer Expires in</h6>
                </div>

                <div className="special_sec2_row2_col3_row2">
                  <div className="countdown-timer-wrapper">
                    <div className="timer">
                      <Countdown date={data.expiryDate} renderer={renderer} />
                    </div>
                    <span className="text">days </span>
                    <span className="text">hrs </span>

                    <span className="text">mins </span>

                    <span className="text">secs</span>
                  </div>
                </div>

                <div className="special_sec2_row2_col3_row3">
                  <a href="#">
                    <button className="btnGet">GET THE OFFER</button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
// class Countdown extends React.Component {
//     state = {
//         days: undefined,
//         hours: undefined,
//         minutes: undefined,
//         seconds: undefined
//     };

//     componentDidMount() {
//         this.interval = setInterval(() => {
//             const { timeTillDate, timeFormat } = this.props;
//             const then = moment(timeTillDate, timeFormat);
//             const now = moment();
//             const countdown = moment(then - now);
//             const days = countdown.format('D');
//             const hours = countdown.format('HH');
//             const minutes = countdown.format('mm');
//             const seconds = countdown.format('ss');

//             this.setState({ days, hours, minutes, seconds });
//         }, 1000);
//     }

//     componentWillUnmount() {
//         if (this.interval) {
//             clearInterval(this.interval);
//         }
//     }

//     render() {
//         const { days, hours, minutes, seconds } = this.state;

//         return (
//             <div>
//                 <h1>Countdown</h1>
//                 <div className="countdown-wrapper">
//                     <div className="countdown-item">
//                         {days}
//                         <span>days</span>
//                     </div>
//                     <div className="countdown-item">
//                         {hours}
//                         <span>hours</span>
//                     </div>
//                     <div className="countdown-item">
//                         {minutes}
//                         <span>minutes</span>
//                     </div>
//                     <div className="countdown-item">
//                         {seconds}
//                         <span>seconds</span>
//                     </div>
//                 </div>
//             </div>
//         );
//     }
// }
