import React from 'react';
import {Link} from "react-router-dom"
  
export default class Articletab extends React.Component{
  componentDidMount(){
    window.scroll(0,0)
  }
  render() {
    return (
      <div className="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12" style={{marginBottom:20}}>
      <div className="card">
        <img src={this.props.data.img} height="200" className="card-img-top" alt="..."/>
        <div style={{height:265, overflow:"hidden"}} className="card-body">
            <h5 className="card-title">Article</h5>
            <p style={{display: "-webkit-box", WebkitLineClamp: 2, webkitBoxOrient: "vertical", overflow: "hidden", color:"#5f5f5f", fontWeight:700}}>{this.props.data.title}</p>
            <p className="card-text" style={{display: "-webkit-box", WebkitLineClamp: 5, webkitBoxOrient: "vertical", overflow: "hidden"}}>{this.props.data.text}</p>
            <Link
                    to={{
                      pathname: "/Articles",
                      state: {
                        bannerImg: this.props.data.img,
                        bannerTitle: this.props.data.bannerTitle,
                        title: this.props.data.title,
                        text: this.props.data.text,
                        articlesNumber: this.props.articleNumber + 1,
                        createdDate: this.props.data.createdAt
                      },
                    }}
                  >
                   <p className="card-text"><small className="" style={{ color: "#f90d26", fontSize:14, fontWeight:700}}>+Read More..</small></p>
                  </Link>
            {/* <a href="javascript:void(0)" onClick={() => this.props.history.push("/Articles")}>
                <p className="card-text"><small className="" style={{ color: "#f90d26", fontSize:14, fontWeight:700}}>+Read More..</small></p>
            </a> */}
        </div>  
      </div>
       </div>
      
    )
  }
}