import React from 'react';

export default class DashboardUserComponent extends React.Component {
    render() {
        return(
            <div className="container">
                <div className="row">
                    <div className="main_con col-xl-4 col-lg-4 col-md-6">
                        <div className="dashboard_tags"><p>Favourite</p></div>
                        <div className="admin_main_column">
                            <div className="container0">
                                <svg className="progress-circle" width="200px" height="200px"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <circle className="progress-circle-back" cx="80" cy="80" r="68"></circle>
                                    <circle className="progress-circle-prog" cx="80" cy="80" r="68"></circle>
                                </svg>
                                <div className="measurement">
                                    <img className="Inbound" src="/img/Admin/red_box.png" />
                                    <p>Inbound</p>
                                    <img className="Outbound" src="/img/Admin/gray_box.png" />
                                    <p>Outbound</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="main_con col-xl-4 col-lg-4 col-md-6">
                        <div className="dashboard_tags"><p>Tracking</p></div>
                        <div className="admin_main_column">
                            <div id="container2"></div>
                            <div className="measurement2">
                                <img className="red_triangle" src="/img/Admin/triangle_box.png" />
                                <h6>5.023%</h6>
                                <p>Increase in Cost</p>
                            </div>
                        </div>
                    </div>

                    <div className="main_con col-xl-4 col-lg-4 col-md-6">
                        <div className="dashboard_tags"><p>Auction</p></div>
                        <div className="admin_main_column">
                            <div className="history_label">
                                <p id="label_500">500</p>
                                <p>400</p>
                                <p>300</p>
                                <p>200</p>
                                <p>100</p>
                                <p>0</p>
                            </div>
                            <div id="container3"></div>
                            <div className="history_label0">
                                <p id="p1">June 27</p>
                                <p id="p2">June 26</p>
                                <p id="p3">June 25</p>
                                <p id="p4">June 24</p>
                                <p id="p5">June 23</p>
                                <p id="p6">June 22</p>
                            </div>
                        </div>
                    </div>

                    <div className="main_con col-xl-4 col-lg-4 col-md-6">
                        <div className="dashboard_tags"><p>Promotion</p></div>
                        <div className="admin_main_column">
                            <div id="container4">
                                <div className="progress" style={{height: "20px"}}>
                                    <text opacity="1">Jan</text>
                                    <div className="progress-bar" role="progressbar" style={{width: "70%"}}
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">1259809prn</div>
                                </div><br />
                                <div className="progress" style={{height: "20px"}}>
                                    <text opacity="1">Feb</text>
                                    <div className="progress-bar gray" role="progressbar" style={{width: "60%"}}
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">125978prn</div>
                                </div><br />
                                <div className="progress" style={{height: "20px"}}>
                                    <text opacity="1">Mar</text>
                                    <div className="progress-bar gray1" role="progressbar" style={{width: "50%"}}
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">12565prn</div>
                                </div><br />
                                <div className="progress" style={{height: "20px"}}>
                                    <text opacity="1">May</text>
                                    <div className="progress-bar gray2" role="progressbar" style={{width: "40%"}}
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">1244prn</div>
                                </div><br />
                                <div className="progress" style={{height: "20px"}}>
                                    <text opacity="1">June</text>
                                    <div className="progress-bar gray3" role="progressbar" style={{width: "30%"}}
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">123prn</div>
                                </div><br />
                                <div className="progress" style={{height: "20px"}}>
                                    <text opacity="1">July</text>
                                    <div className="progress-bar gray4" role="progressbar" style={{width: "20%"}}
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">1prn</div>
                                </div>
                            </div>
                            <div className="measurement3">
                                <p>According to</p>
                                <p>Active Users</p>
                            </div>
                        </div>
                    </div>

                    <div className="main_con col-xl-4 col-lg-4 col-md-6">
                        <div className="dashboard_tags"><p>Feedback</p></div>
                        <div className="admin_main_column">
                            <div className="container5">
                                <svg className="progress-circle" width="200px" height="200px"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <circle className="progress-circle-back" cx="80" cy="80" r="70"></circle>
                                    <circle className="progress-circle-prog" cx="80" cy="80" r="70"></circle>
                                </svg>
                                <div className="progress-text" data-progress="0">
                                    <h1>25%</h1>
                                    <p>As per Collected Data</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="main_con col-xl-4 col-lg-4 col-md-6">
                        <div className="dashboard_tags"><p>Market Analysis</p></div>
                        <div className="admin_main_column">
                            <div id="container6">
                                <p opacity="1" id="Targated">Targated</p>
                                <div className="progress" style={{height: "20px"}}>

                                    <div className="progress-bar" role="progressbar" style={{width: "75%"}}
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">75%</div>
                                </div><br />
                                <p opacity="1">Achieved</p>
                                <div className="progress" style={{height: "20px"}}>
                                    <div className="progress-bar gray2" role="progressbar" style={{width: "55%"}}
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">55%</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="main_con col-xl-4 col-lg-4 col-md-6">
                        <div className="dashboard_tags"><p>History</p></div>
                        <div className="admin_main_column">
                            <div className="history_labels">
                                <p id="label_100">100</p>
                                <p>80</p>
                                <p>60</p>
                                <p>40</p>
                                <p>20</p>
                                <p>0</p>
                            </div>
                            <div id="container7">
                            </div>
                            <div className="measurement4">
                                <img className="Inbound" src="/img/Admin/red_box.png" />
                                <p>Rapid Remit</p>
                                <img className="Outbound" src="/img/Admin/gray_box.png" />
                                <p>Others</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}