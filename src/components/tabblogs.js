import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { blogs } from "../Store/actions/userAuth";
import Moment from "react-moment";

class Blogtab extends React.Component {
  componentWillMount() {
    this.props.getData();
  }
  render() {
    let { blogs } = this.props;
    return (
      <div className="row">
        {blogs && blogs.length > 0
          ? blogs.map((blog, index) => (
            index % 2 === 0 ?
              <div style={{display:"flex", justifyContent:"center", backgroundColor:"#f2f2f2", marginBottom:50, padding:"20px 0"}}>
                <div className="john-detail col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                  <span className="spanHeading">Blogs</span>
                  <span className="spanDate">
                    <Moment format="Do MMM,YYYY">
                      {blog ? blog.createdAt : null}
                    </Moment>
                  </span>
                  <br />
                  <p style={{display: "-webkit-box", WebkitLineClamp: 2, webkitBoxOrient: "vertical", overflow: "hidden",}} className="cardNewsTitle">{blog ? blog.title : null}</p>
                  <p style={{display: "-webkit-box", WebkitLineClamp: 5, webkitBoxOrient: "vertical", overflow: "hidden",}} id="para2">{blog ? blog.text : null}</p>
                  <Link
                    to={{
                      pathname: "/blogsReadMore",
                      state: {
                        blogNumber: index,
                        date: blog.createdAt,
                        heading: blog.title,
                        subHeading: blog.subTitle,
                        paragraph: blog.text,
                      },
                    }}
                  >
                    <span className="spanHeading" style={{color:"#f90d26"}}>+Read More..</span>
                  </Link>
                </div>
                <div className="col-xl-4 col-lg-4 d-lg-block d-md-none d-sm-none col-sm-12 d-none col-12">
                  <div className="pic1">
                    <div className="pic-i"></div>
                    <div className="pic-j">
                      <img src={blog.img} />
                    </div>
                  </div>
                </div>
              </div>
              :
              <div style={{display:"flex", justifyContent:"center", backgroundColor:"#f2f2f2", marginBottom:50, padding:"20px 0"}}>
                 <div className="col-xl-4 col-lg-4 d-lg-block d-md-none d-sm-none col-sm-12 d-none col-12">
                  <div className="pic1">
                    <div className="pic-i-mirror"></div>
                    <div className="pic-j">
                      <img src={blog.img} />
                    </div>
                  </div>
                </div>
                <div style={{marginBottom:20}} className="john-detail col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                  <span className="spanHeading">Blogs</span>
                  <span className="spanDate">
                    <Moment format="Do MMM,YYYY">
                      {blog ? blog.createdAt : null}
                    </Moment>
                  </span>
                  <br />
                  <p style={{display: "-webkit-box", WebkitLineClamp: 2, webkitBoxOrient: "vertical", overflow: "hidden",}} className="cardNewsTitle">{blog ? blog.title : null}</p>
                  <p style={{display: "-webkit-box", WebkitLineClamp: 5, webkitBoxOrient: "vertical", overflow: "hidden",}} id="para2">{blog ? blog.text : null}</p>
                  <Link
                    to={{
                      pathname: "/blogsReadMore",
                      state: {
                        blogNumber: index,
                        date: blog.createdAt,
                        heading: blog.title,
                        subHeading: blog.subTitle,
                        paragraph: blog.text,
                      },
                    }}
                  >
                    <span className="spanHeading" style={{color:"#f90d26"}}>+Read More..</span>
                  </Link>
                </div>
               
              </div>
            ))
          : null}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    blogs: state.Blogs.blogData,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: bindActionCreators(blogs, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Blogtab);
