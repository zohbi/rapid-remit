import React from "react";
import "../css/style-dashboardCommonFile.css";
import DashboardUserComponent from "./DashboardUserComponent";
import DashboardReviewsComponent from "./DashboardReviewsComponent";
import DashboardFeedbackComponent from "./DashboardFeedbackComponent";
import DashboardMarketComponent from "./DashboardMarketComponent";
import DashboardTracking from "./dashboardTracking";
import DashboardHistoryBD from "./dashboardHistoryBD";
import Promotions from "../Containers/promotions";
import $ from "jquery";
import date from "date-and-time";
import chart_instance from "../instance/chart_instance";
import { connect } from "react-redux";
import { Liverate } from "../Store/actions/index";
import { Comparisons } from '../Store/actions';

// import { Promotions } from '../Store/actions';

class DashboardLayout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showNav: false,
      time: 6,
      partner: "moneygram",
      currency: "AUD"
    };
    chart_instance.header = this;
  }

  componentDidMount() {
    /*//////////////////////////////////////////////////////////////////////////*/
    /*/////////////////////////////////////dashboard1 STARTS/////////////////////////////////////*/
    /*//////////////////////////////////////////////////////////////////////////*/
    this.props.currency();
    this.props.partner();
    $("#days7").click(function () {
      // $("#buttonSlideRed").css({ "margin-left": "10px" });
      $("#days7").css({ width: "2400px" });
      $("#days7").style.width = "300px";
      // $("#days30").css({ color: "#555555" });
    });

    $("#buttonSlideRed").click(function () {
      $("#buttonSlideRed").style.display = "none";
    });

    // $(".dashboard1 .divB .divBrow1 .nav ul #li1").click(function () {
    //   $(".buttonSlide").css({ "margin-left": "1px" });
    //   $(".dashboard1 .divB .divBrow1 .nav ul #li1").css({ color: "#FFFFFF" });
    //   $(".dashboard1 .divB .divBrow1 .nav ul #li2").css({ color: "#555555" });
    //   $(".dashboard1 .divB .divBrow1 .nav ul #li3").css({ color: "#555555" });
    // });
    // $(".dashboard1 .divB .divBrow1 .nav ul #li2").click(function () {
    //   $(".buttonSlide").css({ "margin-left": "76px" });
    //   $(".dashboard1 .divB .divBrow1 .nav ul #li2").css({ color: "#FFFFFF" });
    //   $(".dashboard1 .divB .divBrow1 .nav ul #li1").css({ color: "#555555" });
    //   $(".dashboard1 .divB .divBrow1 .nav ul #li3").css({ color: "#555555" });
    // });
    // $(".dashboard1 .divB .divBrow1 .nav ul #li3").click(function () {
    //   $(".buttonSlide").css({ "margin-left": "149px" });
    //   $(".dashboard1 .divB .divBrow1 .nav ul #li3").css({ color: "#FFFFFF" });
    //   $(".dashboard1 .divB .divBrow1 .nav ul #li1").css({ color: "#555555" });
    //   $(".dashboard1 .divB .divBrow1 .nav ul #li2").css({ color: "#555555" });
    // });

    // menu bar for small screen STARTS
    $(".divBurgerMenu img").click(function () {
      $(".secondVerticalMenu").css({ display: "block" });
    });
    $(".divBurgerMenu #secondVerticalMenu").click(function () {
      $(".secondVerticalMenu").css({ display: "none" });
    });

    
    // menu bar for small screen ENDS
    /*//////////////////////////////////////////////////////////////////////////*/
    /*/////////////////////////////////////dashboard1 ENDS/////////////////////////////////////*/
    /*//////////////////////////////////////////////////////////////////////////*/
    /*//////////////////////////////////////////////////////////////////////////*/
    /*/////////////////////////////////////HISTORY STARTS/////////////////////////////////////*/
    /*//////////////////////////////////////////////////////////////////////////*/

    $("#question1 p").slideUp();
    $("#question2 p").slideUp();
    $("#question3 p").slideUp();
    $("#question4 p").slideUp();
    $("#question5 p").slideUp();

    $("#question1 .plus .img2UD").hide();
    $("#question2 .plus .img2UD").hide();
    $("#question3 .plus .img2UD").hide();
    $("#question4 .plus .img2UD").hide();
    $("#question5 .plus .img2UD").hide();

    $("#question1 .plus .img1UD").click(function () {
      $("#question1 .plus .img1UD").hide();
      $("#question1 .plus .img2UD").show();
    });
    $("#question1 .plus .img2UD").click(function () {
      $("#question1 .plus .img1UD").show();
      $("#question1 .plus .img2UD").hide();
    });

    $("#question2 .plus .img1UD").click(function () {
      $("#question2 .plus .img1UD").hide();
      $("#question2 .plus .img2UD").show();
    });
    $("#question2 .plus .img2UD").click(function () {
      $("#question2 .plus .img1UD").show();
      $("#question2 .plus .img2UD").hide();
    });

    $("#question3 .plus .img1UD").click(function () {
      $("#question3 .plus .img1UD").hide();
      $("#question3 .plus .img2UD").show();
    });
    $("#question3 .plus .img2UD").click(function () {
      $("#question3 .plus .img1UD").show();
      $("#question3 .plus .img2UD").hide();
    });

    $("#question4 .plus .img1UD").click(function () {
      $("#question4 .plus .img1UD").hide();
      $("#question4 .plus .img2UD").show();
    });
    $("#question4 .plus .img2UD").click(function () {
      $("#question4 .plus .img1UD").show();
      $("#question4 .plus .img2UD").hide();
    });

    $("#question5 .plus .img1UD").click(function () {
      $("#question5 .plus .img1UD").hide();
      $("#question5 .plus .img2UD").show();
    });
    $("#question5 .plus .img2UD").click(function () {
      $("#question5 .plus .img1UD").show();
      $("#question5 .plus .img2UD").hide();
    });
    $("#question1 .plus").click(function () {
      $("#question1 p").slideToggle();
    });
    $("#question2 .plus").click(function () {
      $("#question2 p").slideToggle();
    });
    $("#question3 .plus").click(function () {
      $("#question3 p").slideToggle();
    });
    $("#question4 .plus").click(function () {
      $("#question4 p").slideToggle();
    });
    $("#question5 .plus").click(function () {
      $("#question5 p").slideToggle();
    });
    $("#question6 .plus").click(function () {
      $("#question6 p").slideToggle();
    });
    $("#question7 .plus").click(function () {
      $("#question7 p").slideToggle();
    });
    /*//////////////////////////////////////////////////////////////////////////*/
    /*/////////////////////////////////////HISTORY ENDS/////////////////////////////////////*/
    /*//////////////////////////////////////////////////////////////////////////*/

    /*//////////////////////////////////////////////////////////////////////////*/
    /*/////////////////////////////////////dashboard PROMOTIONS (UD) STARTS/////////////////////////////////////*/
    /*//////////////////////////////////////////////////////////////////////////*/
    $("#bigCard1").hide();
    $("#bigCard2").hide();

    $("#smallCard1Button").click(function () {
      $("#smallCard1").hide();
      $("#smallCard2").hide();
      $("#bigCard1").show();
    });
    $("#bigCard1Cross").click(function () {
      $("#bigCard1").hide();
      $("#smallCard1").show();
      $("#smallCard2").show();
    });

    $("#smallCard2Button").click(function () {
      $("#smallCard1").hide();
      $("#smallCard2").hide();
      $("#bigCard2").show();
    });
    $("#bigCard2Cross").click(function () {
      $("#bigCard2").hide();
      $("#smallCard1").show();
      $("#smallCard2").show();
    });
    /*//////////////////////////////////////////////////////////////////////////*/
    /*/////////////////////////////////////dashboard PROMOTIONS (UD) ENDS/////////////////////////////////////*/
    /*//////////////////////////////////////////////////////////////////////////*/

    // Vertical Menu for Dashboard Starts
    //document.getElementById("DashboardReviewsComponent").style.display = "none";
    // document.getElementById("DashboardTracking").style.display = "none";
    // document.getElementById("DashboardHistoryBD").style.display = "none";
    document.getElementById("DashboardPromotionsUD").style.display = "none";
    document.getElementById("DashboardMarketComponent").style.display = "none";
    document.getElementById("DashboardFeedbackComponent").style.display =
      "none";
    document.getElementById("menu1").className = "borderLeft";
    // Vertical Menu for Dashboard Ends
  }
  onClickChange = (id) => {
    // console.log(id);
    if (id == "DashboardHistoryBD") {
      document.getElementById(id).style.display = "block";
    }
    // else if(id == "DashboardTracking") {
    //     document.getElementById(id).style.display="flex"
    // }
    else document.getElementById(id).style.display = "flex";
    if (id == "DashboardReviewsComponent") {
      // document.getElementById('DashboardReviewsComponent').style.display="none";
      // document.getElementById("DashboardTracking").style.display = "none";
      // document.getElementById("DashboardHistoryBD").style.display = "none";
      document.getElementById("DashboardPromotionsUD").style.display = "none";
      document.getElementById("DashboardMarketComponent").style.display =
        "none";
      document.getElementById("DashboardFeedbackComponent").style.display =
        "none";
      document.getElementById("menu1").className = "borderLeft";
      // document.getElementById("menu2").className = "";
      // document.getElementById("menu3").className = "";
      document.getElementById("menu4").className = "";
      document.getElementById("menu5").className = "";
      document.getElementById("menu6").className = "";
      this.setState({
        showNav: false,
      });
    }
    // else if (id == "DashboardHistoryBD") {
    //   document.getElementById("DashboardReviewsComponent").style.display =
    //     "none";
    //   document.getElementById("DashboardTracking").style.display = "none";
    //   document.getElementById("DashboardPromotionsUD").style.display = "none";
    //   document.getElementById("DashboardMarketComponent").style.display =
    //     "none";
    //   document.getElementById("DashboardFeedbackComponent").style.display =
    //     "none";
    //   document.getElementById("menu3").className = "borderLeft";
    //   document.getElementById("menu1").className = "";
    //   document.getElementById("menu2").className = "";
    //   document.getElementById("menu4").className = "";
    //   document.getElementById("menu5").className = "";
    //   document.getElementById("menu6").className = "";
    //   this.setState({
    //     showNav: false,
    //   });
    // } 
    //else if (id == "DashboardTracking") {
    //   document.getElementById("DashboardHistoryBD").style.display = "none";
    //   document.getElementById("DashboardPromotionsUD").style.display = "none";
    //   document.getElementById("DashboardMarketComponent").style.display =
    //     "none";
    //   document.getElementById("DashboardReviewsComponent").style.display =
    //     "none";
    //   document.getElementById("DashboardFeedbackComponent").style.display =
    //     "none";
    //   document.getElementById("menu2").className = "borderLeft";
    //   document.getElementById("menu3").className = "";
    //   document.getElementById("menu1").className = "";
    //   document.getElementById("menu4").className = "";
    //   document.getElementById("menu5").className = "";
    //   document.getElementById("menu6").className = "";
    //   this.setState({
    //     showNav: false,
    //   });
    // } 
    else if (id == "DashboardPromotionsUD") {
      // document.getElementById("DashboardHistoryBD").style.display = "none";
      document.getElementById("DashboardReviewsComponent").style.display =
        "none";
      // document.getElementById("DashboardTracking").style.display = "none";
      document.getElementById("DashboardMarketComponent").style.display =
        "none";
      document.getElementById("DashboardFeedbackComponent").style.display =
        "none";
      document.getElementById("menu4").className = "borderLeft";
      // document.getElementById("menu2").className = "";
      document.getElementById("menu1").className = "";
      // document.getElementById("menu3").className = "";
      document.getElementById("menu5").className = "";
      document.getElementById("menu6").className = "";
      this.setState({
        showNav: false,
      });
    } else if (id == "DashboardMarketComponent") {
      // document.getElementById("DashboardHistoryBD").style.display = "none";
      document.getElementById("DashboardReviewsComponent").style.display =
        "none";
      document.getElementById("DashboardPromotionsUD").style.display = "none";
      // document.getElementById("DashboardTracking").style.display = "none";
      document.getElementById("DashboardFeedbackComponent").style.display =
        "none";
      document.getElementById("menu5").className = "borderLeft";
      // document.getElementById("menu2").className = "";
      document.getElementById("menu1").className = "";
      document.getElementById("menu4").className = "";
      // document.getElementById("menu3").className = "";
      document.getElementById("menu6").className = "";
      this.setState({
        showNav: true,
      });
    } else if (id == "DashboardFeedbackComponent") {
      // document.getElementById("DashboardHistoryBD").style.display = "none";
      document.getElementById("DashboardReviewsComponent").style.display =
        "none";
      document.getElementById("DashboardPromotionsUD").style.display = "none";
      // document.getElementById("DashboardTracking").style.display = "none";
      document.getElementById("DashboardMarketComponent").style.display =
        "none";
      document.getElementById("menu6").className = "borderLeft";
      // document.getElementById("menu2").className = "";
      document.getElementById("menu1").className = "";
      document.getElementById("menu4").className = "";
      // document.getElementById("menu3").className = "";
      document.getElementById("menu5").className = "";
      this.setState({
        showNav: false,
      });
    }
  };

  sevenDays = () => {
    document.getElementById("buttonSlideRed").style.marginLeft = "0px";
    document.getElementById("days7").style.color = "#ffffff";

    document.getElementById("days15").style.color = "#000000";
    document.getElementById("days30").style.color = "#000000";
  };
  fifteenDays = () => {
    document.getElementById("buttonSlideRed").style.marginLeft = "48px";
    document.getElementById("days15").style.color = "#ffffff";

    document.getElementById("days7").style.color = "#000000";
    document.getElementById("days30").style.color = "#000000";
  };

  thirtyDays = () => {
    document.getElementById("buttonSlideRed").style.marginLeft = "95px";
    document.getElementById("days30").style.color = "#ffffff";

    document.getElementById("days15").style.color = "#000000";
    document.getElementById("days7").style.color = "#000000";
  };
  componentWillMount() {
    this.props.getData("USD", this.state.time, this.state.partner);
  }
  render() {
    // console.log("this.props", this.props.Liverates);
    const pattern = date.compile("MMM D, YYYY");
    let image;
    if (localStorage.getItem("token")) {
      image = JSON.parse(localStorage.getItem("UserData")).image;
    }
    // console.log("currencyData",this.props.currencyData)
    return (
      <div className="dashboard1">
        <div className="divA">
          <div className="divProfile">
            <i
              className="fas fa-pen white"
              onClick={() => (window.location = "/profile")}
            ></i>
            <div className="imgProfile sider-d">
              <img
                className="imgProfilePic"
                src={image ? image : require("../img/dashboard1/profile.png")}
                alt=""
              />
            </div>
            <p>
              {localStorage.getItem("token")
                ? JSON.parse(localStorage.getItem("UserData")).firstName +
                " " +
                JSON.parse(localStorage.getItem("UserData")).lastName
                : ""}
            </p>
          </div>
          <div className="divVerticalMenu">
            <ul>
              <a
                href="#"
                onClick={() => this.onClickChange("DashboardReviewsComponent")}
              >
                <li id="menu1">FAVORITES</li>
              </a>
              {/* <a
                href="#"
                onClick={() => this.onClickChange("DashboardTracking")}
              >
                <li id="menu2">TRACKING</li>
              </a>
              <a
                href="#"
                onClick={() => this.onClickChange("DashboardHistoryBD")}
              >
                <li id="menu3">HISTORY</li>
              </a> */}
              <a
                href="#"
                onClick={() => this.onClickChange("DashboardPromotionsUD")}
              >
                <li id="menu4">PROMOTIONS</li>
              </a>
              <a
                href="#"
                onClick={() => this.onClickChange("DashboardMarketComponent")}
              >
                <li id="menu5">MARKET ANALYSIS</li>
              </a>
              <a
                href="#"
                onClick={() => this.onClickChange("DashboardFeedbackComponent")}
              >
                <li id="menu6">FEEDBACK</li>
              </a>
            </ul>
          </div>
        </div>
        <div className="divB">
          <div className="row divBrow1">
            <div className="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-12">
              <div className="divBurgerMenu">
                <div className="secondVerticalMenu side-drawer-d">
                  <div className="divProfile">
                  <span style={{display:"flex", justifyContent:"space-between"}}>
                    <i
                      className="fas fa-pen white"
                      onClick={() => (window.location = "/profile")}
                    ></i>
                   
                      <img
                        className="img1"
                        id="secondVerticalMenu"
                        src={require("../img/dashboard1/cross.png")}
                        alt=""
                      />
                    </span>
                    <div className="imgProfile">
                      <img
                        className="imgProfilePic"
                        src={image ? image : require("../img/dashboard1/profile.png")}
                        alt=""
                      />
                    </div>
                    <p>
                      {localStorage.getItem("token")
                        ? JSON.parse(localStorage.getItem("UserData")).firstName +
                        " " +
                        JSON.parse(localStorage.getItem("UserData")).lastName
                        : ""}
                    </p>
                  </div>
                  <div className="divVerticalMenu">
                    <ul>
                      <a
                        href="#"
                        onClick={() => this.onClickChange("DashboardReviewsComponent")}
                      >
                        <li id="menu1">FAVORITES</li>
                      </a>
                      {/* <a
                        href="#"
                        onClick={() => this.onClickChange("DashboardTracking")}
                      >
                        <li id="menu2">TRACKING</li>
                      </a>
                      <a
                        href="#"
                        onClick={() => this.onClickChange("DashboardHistoryBD")}
                      >
                        <li id="menu3">HISTORY</li>
                      </a> */}
                      <a
                        href="#"
                        onClick={() => this.onClickChange("DashboardPromotionsUD")}
                      >
                        <li id="menu4">PROMOTIONS</li>
                      </a>
                      <a
                        href="#"
                        onClick={() => this.onClickChange("DashboardMarketComponent")}
                      >
                        <li id="menu5">MARKET ANALYSIS</li>
                      </a>
                      <a
                        href="#"
                        onClick={() => this.onClickChange("DashboardFeedbackComponent")}
                      >
                        <li id="menu6">FEEDBACK</li>
                      </a>
                    </ul>
                  </div>
                </div>
                <img
                  className="burgerButton"
                  src={require("../img/dashboard1/burgerMenu.png")}
                  alt=""
                />
              </div>
              <h3>USER DASHBOARD</h3>
              {this.state.showNav ? (
                <div className="nav">
                  <ul className="oho">
                    <div className="buttonSlide" id="buttonSlideRed"></div>

                    <div
                      id="7"
                      className="buttonSlide2"
                      onClick={() => {
                        this.sevenDays();
                        this.setState({ time: 7 });
                        this.props.getData(this.state.currency, 6, this.state.partner);
                      }}
                    >
                      <p id="days7"> 7 Days</p>
                    </div>
                    <div
                      id="15"
                      className="buttonSlide2"
                      style={{cursor:"pointer"}}
                      onClick={() => {
                        this.fifteenDays();
                        this.setState({ time: 15 });
                        this.props.getData(this.state.currency, 15, this.state.partner);
                      }}
                    >
                      <p id="days15"> 15 Days</p>
                    </div>

                    <div
                      id="30"
                      className="buttonSlide2"
                      onClick={() => {
                        this.thirtyDays();
                        this.setState({ time: 30 });
                        // console.log("pp", this.state.partner);
                        this.props.getData(this.state.currency, 30, this.state.partner);
                      }}
                    >
                      <p id="days30"> 30 Days</p>
                    </div>
                  </ul>
                  <select
                    name="partners"
                    style={{ width: 150 }}
                    onChange={(e) => {
                      // console.log("partner", this.state.partner);
                      this.setState({ partner: e.target.value });
                      this.props.getData(
                        this.state.currency,
                        this.state.time,
                        e.target.value
                      );
                    }}
                    value={this.state.partners}
                  // onChange={(e) => {
                  //   console.log("partners", this.state.partners);
                  //   this.setState({ partners: e.target.value });
                  //   this.props.getData(
                  //     "USD",
                  //     this.state.time,
                  //     this.state.partners
                  //   );
                  // }}
                  >
                    {this.props.part.map((val, ind) => <option key={ind} value={val.name.toUpperCase()}>{val.name.toUpperCase()}</option>)}
                    {/* <option value="moneygram">Money Gram</option>
                    <option value="xpressmoney">Xpress Money</option>
                    <option value="alansariexchange">Alansari Exchange</option> */}
                  </select>
                  <select
                    name="currency"
                    onChange={(e) => {
                      // console.log("partner", this.state.partner);
                      this.setState({ currency: e.target.value });

                      // this.props.getData(
                      //   e.target.value,
                      //   this.state.time,
                      //   this.state.partner
                      // );
                    }}
                    value={this.state.currency}
                  // onChange={(e) => {
                  //   console.log("partners", this.state.partners);
                  //   this.setState({ partners: e.target.value });
                  //   this.props.getData(
                  //     "USD",
                  //     this.state.time,
                  //     this.state.partners
                  //   );
                  // }}
                  >
                    {this.props.currencyData.map((val, ind) => <option key={ind} value={val}>{val}</option>)}
                    {/* <option value="moneygram">Money Gram</option>
                    <option value="xpressmoney">Xpress Money</option>
                    <option value="alansariexchange">Alansari Exchange</option> */}
                  </select>
                </div>
              ) : (
                  ""
                )}
            </div>
            <div className="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-12">
              {/* <div className="date">{date.format(new Date(), pattern)}</div> */}
            </div>
          </div>
          <div className="row divScroll" style={{ overflowY: "auto" }} >
            <DashboardReviewsComponent />
            {/* <DashboardTracking />
            <DashboardHistoryBD /> */}
            <Promotions />
            <DashboardMarketComponent currency={this.state.currency} />
            <DashboardFeedbackComponent />
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  currencyData: state.Comparisons.currency,
  // Liverates: state.Comparisons.Liverates,
  part: state.Comparisons.part,
})
const mapDispatchToProps = (dispatch) => ({
  getData: (obj, bb, cc) => dispatch(Liverate.rates(obj, bb, cc)),
  partner: () => dispatch(Comparisons.partner()),
  currency: () => dispatch(Comparisons.currency()),
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardLayout);
