import React from "react";
import "../css/bootstrap.css";
import "../css/style-our-network.css";
import "../css/style-footer.css";

export default class Footer2 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div>
        <div className="picture2">
          <div className="container1">
            <div className="row">
              <div className="col-x-2 col-lg-2 col-md-0 col-sm-0 col-0"></div>
              <div className="col-x-6 col-lg-6 col-md-12 col-sm-12 col-12 promotionbanner">
                <h1 className="Lato_Black">Download Now!</h1>
                <p>
                  Get the Best Rates,
                  <br />
                  Important Remittance Information and
                </p>
                <h4 className="Lato_Black">
                  Special Promotion Alerts on the go!
                </h4>
                <img src={require("../img/footer/google-play.png")} />
                <img src={require("../img/footer/app-store.png")} />
              </div>
            </div>
          </div>
        </div>
        <footer className="footer">
          <div className="container-fluid">
            <div className="container1">
              <div className="row">
                <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
                <div className="col-xl-4 col-lg-4 col-md-3 col-sm-12 col-12">
                  <div className="contact-links">
                    <h5 className="Lato_Black">Help & Support</h5>
                    <a href="" onClick={() => this.props.history.push("/FAQs")}>
                      FAQs
                    </a>
                    <br />
                    <a
                      href=""
                      onClick={() => this.props.history.push("/ContactUs")}
                    >
                      Contact Us
                    </a>
                    <br />
                    <a
                      href=""
                      onClick={() => this.props.history.push("/ournetwork")}
                    >
                      Partners
                    </a>
                  </div>
                </div>

                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                  <h5 className="followus Lato_Black">Follow Us</h5>
                  <ul className="footer-links">
                    <a href="" className="Lato_Regular">
                      <img src={require("../img/footer/fb.png")} />
                    </a>
                    <a href="" className="Lato_Regular">
                      <img src={require("../img/footer/insta.png")} />
                    </a>
                    <a href="" className="Lato_Regular">
                      <img src={require("../img/footer/linkin.png")} />
                    </a>
                    <a href="" className="Lato_Regular">
                      <img src={require("../img/footer/twitter.png")} />
                    </a>
                  </ul>
                </div>

                <div className="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12">
                  <h5 className="contact Lato_Black">Contact Information</h5>
                  <p className="Lato_Regular">
                    Dubai International Office
                    <br />
                    Office # 100, level 2, Precinct
                    <br />
                    Building 5<br />
                    The Gate District
                    <br />
                    P.O.Box 507048, Dubai, UAE
                  </p>
                </div>
              </div>
              <div className="row row2">
                <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-12"></div>
                <div
                  className="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12 Lato_Regular"
                  id="footer_links"
                >
                  <a
                    href=""
                    onClick={() => this.props.history.push("/PrivacyPolicy")}
                  >
                    Privacy Policy |
                  </a>
                  <a
                    href=""
                    onClick={() =>
                      this.props.history.push("/TermsAndConditions")
                    }
                  >
                    Terms & Conditions |
                  </a>
                  <a
                    href=""
                    onClick={() => this.props.history.push("/CookiesPolicy")}
                  >
                    Cookies Policy
                  </a>
                </div>
                <div className="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12 phone Lato_Regular">
                  Phone: 111 111 123
                  <br />
                  Email: contactus@rapidRemir.com
                </div>
              </div>
            </div>
          </div>
        </footer>
        <div className="copyrights">
          <div className="container1">
            <div className="row d-flex justify-content-center">
              <p className="Lato_Regular">
                Copyright &copy; 2019 RapidRemit.biz.All Rights Reserved
              </p>

              {/* <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
              <div className="col-xl-8 col-lg-8 col-md-6 col-sm-12 col-12">
                <p className="Lato_Regular">
                  Copyright &copy; 2019 RapidRemit.biz.All Rights Reserved
                </p>
              </div>
              <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <p className="Lato_Regular">Powered by Untitled10</p>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
