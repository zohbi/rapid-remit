import React from "react";
import "../css/bootstrap.css";
import Loader from "./loader";
import "../css/style-about.css";
import Fade from "react-reveal/Fade";
import Zoom from "react-reveal/Zoom";

export default class ContactUsform extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      contact: "",
      title: ""
    };
  }
  validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
  contactUsSubmit = (e) => {
    e.preventDefault()
    let { email, name, contact, title } = this.state;
    if (email == "" && name == "" && contact == "" && title == "") {
      this.setState({ error: "Please fill all value" });
    } else if(!this.validateEmail(email)){
      this.setState({ error: "Invalid email address" });
    } else if (email == "") {
      this.setState({ error: "Please add your email address" });
    } else if (name == "") {
      this.setState({ error: "Please add your name" });
    } else if (title == "") {
      this.setState({ error: "Please add your title" });
    } else if (contact == "") {
      this.setState({ error: "Please add your contact" });
    } else {
      let data = {
        name: name,
        email: email,
        contact: contact,
        title: title
      };
      document.getElementById("name").value=""
      document.getElementById("email2").value=""
      document.getElementById("password2").value=""
      document.getElementById("description1").value=""
      // console.log("no error", data);
      this.props.postData(data);
      this.setState({ error: undefined });
    }
  };

  onChangeValue = (name, value) => {
    this.setState({
      [name]: value
    });
  };
  componentDidUpdate(prevProps) {
    if (prevProps.data !== this.props.data) {
      let {companyName, androidLink, iosLink, officeAddress, poBox, city, country, facebookLink, instagramLink, linkedInLink, twitterLink, email, mapCoortinates} = this.props.data[0]
      this.setState({
        // androidLink,
        city,
        companyName,
        country,
        companyEmail:email,
        // facebookLink,
        // instagramLink,
        // iosLink,
        // linkedInLink,
        mapCoortinates,
        // name,
        officeAddress,
        poBox,
        // twitterLink,
      })
    }
  }
  render() {
    let { error, poBox,officeAddress, mapCoortinates, companyEmail, country, companyName, city } = this.state;
    
    return (
        <Zoom delay={500}>
      <div>
          <div className="Contact_Us_sec2_bg_image">
            <Fade right opposite cascade duration={1000} mirror>
              <div className="wrapper">
                <div className="row Contact_Us_sec2_row col-md-12">
                <div className="Contact_Us_sec2_col1 col-md-1 d-md-block d-sm-none"></div>
                  <div className="Contact_Us_sec2_col1 col-md-5 col-sm-6">
                    <form className="form">
                      <h2>CONTACT US</h2>




<div className="form-body">
  
<div className="form-group">
                        <label for="name">NAME</label>
                        <input
                          type="name"
                          className="form-control"
                          id="name"
                          placeholder=""
                          onChange={e =>
                            this.onChangeValue("name", e.target.value)
                          }
                        />
                      </div>
                      <div className="form-group">
                        <label for="email2">EMAIL</label>
                        <input
                          type="email"
                          className="form-control"
                          id="email2"
                          aria-describedby="emailHelp"
                          placeholder=""
                          onChange={e =>
                            this.onChangeValue("email", e.target.value)
                          }
                        />
                      </div>

                      <div className="form-group">
                        <label for="password2">CONTACT</label>
                        <input
                          type="tect"
                          className="form-control"
                          id="password2"
                          placeholder=""
                          onChange={e =>
                            this.onChangeValue("contact", e.target.value)
                          }
                        />
                      </div>

                      <div className="form-group">
                        <label for="description1">TITLE</label>
                        <textarea
                          className="form-control"
                          id="description1"
                          rows="3"
                          placeholder="Enter your question here"
                          onChange={e =>
                            this.onChangeValue("title", e.target.value)
                          }
                        ></textarea>
                      </div>

</div>






                      <button
                        type="submit"
                        className="Submit_btn "
                        onClick={(e) => this.contactUsSubmit(e)}
                      >
                        {!this.props.isLoading ? (
                          "Submit"
                        ) : (
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-around",
                              alignItems: "center"
                            }}
                          >
                            Submit <Loader contactUs={true} />
                          </div>
                        )}
                      </button>
                    </form>
                  </div>
                  {/* <!------------Contact_Us_sec2_col1----------> */}
                  <div className="Contact_Us_sec2_col2 col-md-5 col-sm-6">
                    <div className="Contact_Us_sec2_col2_text">
                      <p>
                        {companyName}
                        <br />
                        {officeAddress}
                        <br />
                        {/* Building 5<br />
                        The Gate DIstrict
                        <br /> */}
                        P.O. Box {poBox}, <br/>
                        {city}, {country}
                      </p>
                      <div className="horizontal-white-line"></div>
                      <p>
                        {/* Phone:111 111 123 */}
                        {/* <br /> */}
                        <a style={{textDecoration:"none", color:"inherit"}} href={`mailto:${companyEmail}`}>Email: {companyEmail}</a>
                      </p>
                    </div>
                    {/* <!--------Contact_Us_sec2_col2_text end-----------> */}
                  </div>
                  {/* <!------------Contact_Us_sec2_col2----------> */}
                <div className="Contact_Us_sec2_col1 col-md-1 col-sm-0"></div>
                </div>
                {/* <!---Contact_Us_sec2_row-----> */}
              </div>
            </Fade>
          </div>
          {error && error.length ? (
            <div
              style={{ width: "40%", marginLeft: "30px" }}
              className="alert alert-danger alert-dismissible fade show"
              id="dataErr"
            >
              {error}
            </div>
          ) : (
            " "
          )}
      </div>
        </Zoom>
    );
  }
}
