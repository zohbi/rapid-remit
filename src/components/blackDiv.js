import React from 'react';


export default class BlackDiv extends React.Component {
    render() {
        return (
            <div className="row companyOverviewContentRow5">
                <div className="container1">
                    <div className="row divBlack">
                    <div className="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
                        <p>Share your experience with us, Get register your self and let us know yourself</p>
                    </div>
                    <div className="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                        <h3>Register/Login</h3>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}