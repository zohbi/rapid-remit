import React from "react";
import DashboardUserComponent from "./DashboardUserComponent";
import DashboardReviewsComponent from "./DashboardReviewsComponent";
import DashboardFeedbackComponent from "./DashboardFeedbackComponent";
import DashboardMarketComponent from "./DashboardMarketComponent";
import DashboardTracking from "./dashboardTracking";
import DashboardHistoryUD from "./dashboardHistoryUD";
import AddPromotion from "./DashboardAddPromotion";
import DashboardDemographics from "./dashboardDemographics";
import $ from "jquery";
import DashboardAddDiscount from "./DashboardAddDiscount";
import "../css/style-dashboardCommonFile.css";

export default class DashboardLayout2 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showNav: true
    };
  }

  componentDidMount() {
    /*//////////////////////////////////////////////////////////////////////////*/
    /*/////////////////////////////////////dashboard1 STARTS/////////////////////////////////////*/
    /*//////////////////////////////////////////////////////////////////////////*/
    $(".dashboard1 .divB .divBrow1 .nav ul #li1").click(function() {
      $(".buttonSlide").css({ "margin-left": "1px" });
      $(".dashboard1 .divB .divBrow1 .nav ul #li1").css({ color: "#FFFFFF" });
      $(".dashboard1 .divB .divBrow1 .nav ul #li2").css({ color: "#555555" });
      $(".dashboard1 .divB .divBrow1 .nav ul #li3").css({ color: "#555555" });
    });
    $(".dashboard1 .divB .divBrow1 .nav ul #li2").click(function() {
      $(".buttonSlide").css({ "margin-left": "76px" });
      $(".dashboard1 .divB .divBrow1 .nav ul #li2").css({ color: "#FFFFFF" });
      $(".dashboard1 .divB .divBrow1 .nav ul #li1").css({ color: "#555555" });
      $(".dashboard1 .divB .divBrow1 .nav ul #li3").css({ color: "#555555" });
    });
    $(".dashboard1 .divB .divBrow1 .nav ul #li3").click(function() {
      $(".buttonSlide").css({ "margin-left": "149px" });
      $(".dashboard1 .divB .divBrow1 .nav ul #li3").css({ color: "#FFFFFF" });
      $(".dashboard1 .divB .divBrow1 .nav ul #li1").css({ color: "#555555" });
      $(".dashboard1 .divB .divBrow1 .nav ul #li2").css({ color: "#555555" });
    });
    // menu bar for small screen STARTS
    $(".divBurgerMenu img").click(function() {
      $(".secondVerticalMenu").css({ display: "block" });
    });
    $(".divBurgerMenu #secondVerticalMenu").click(function() {
      $(".secondVerticalMenu").css({ display: "none" });
    });
    // menu bar for small screen ENDS
    /*//////////////////////////////////////////////////////////////////////////*/
    /*/////////////////////////////////////dashboard1 ENDS/////////////////////////////////////*/
    /*//////////////////////////////////////////////////////////////////////////*/
    /*//////////////////////////////////////////////////////////////////////////*/
    /*/////////////////////////////////////HISTORY STARTS/////////////////////////////////////*/
    /*//////////////////////////////////////////////////////////////////////////*/
    $("#question1 p").slideUp();
    $("#question2 p").slideUp();
    $("#question3 p").slideUp();
    $("#question4 p").slideUp();
    $("#question5 p").slideUp();

    $("#question1 .plus .img2UD").hide();
    $("#question2 .plus .img2UD").hide();
    $("#question3 .plus .img2UD").hide();
    $("#question4 .plus .img2UD").hide();
    $("#question5 .plus .img2UD").hide();

    $("#question1 .plus .img1UD").click(function() {
      $("#question1 .plus .img1UD").hide();
      $("#question1 .plus .img2UD").show();
    });
    $("#question1 .plus .img2UD").click(function() {
      $("#question1 .plus .img1UD").show();
      $("#question1 .plus .img2UD").hide();
    });

    $("#question2 .plus .img1UD").click(function() {
      $("#question2 .plus .img1UD").hide();
      $("#question2 .plus .img2UD").show();
    });
    $("#question2 .plus .img2UD").click(function() {
      $("#question2 .plus .img1UD").show();
      $("#question2 .plus .img2UD").hide();
    });

    $("#question3 .plus .img1UD").click(function() {
      $("#question3 .plus .img1UD").hide();
      $("#question3 .plus .img2UD").show();
    });
    $("#question3 .plus .img2UD").click(function() {
      $("#question3 .plus .img1UD").show();
      $("#question3 .plus .img2UD").hide();
    });

    $("#question4 .plus .img1UD").click(function() {
      $("#question4 .plus .img1UD").hide();
      $("#question4 .plus .img2UD").show();
    });
    $("#question4 .plus .img2UD").click(function() {
      $("#question4 .plus .img1UD").show();
      $("#question4 .plus .img2UD").hide();
    });

    $("#question5 .plus .img1UD").click(function() {
      $("#question5 .plus .img1UD").hide();
      $("#question5 .plus .img2UD").show();
    });
    $("#question5 .plus .img2UD").click(function() {
      $("#question5 .plus .img1UD").show();
      $("#question5 .plus .img2UD").hide();
    });
    $("#question1 .plus").click(function() {
      $("#question1 p").slideToggle();
    });
    $("#question2 .plus").click(function() {
      $("#question2 p").slideToggle();
    });
    $("#question3 .plus").click(function() {
      $("#question3 p").slideToggle();
    });
    $("#question4 .plus").click(function() {
      $("#question4 p").slideToggle();
    });
    $("#question5 .plus").click(function() {
      $("#question5 p").slideToggle();
    });
    $("#question6 .plus").click(function() {
      $("#question6 p").slideToggle();
    });
    $("#question7 .plus").click(function() {
      $("#question7 p").slideToggle();
    });
    /*//////////////////////////////////////////////////////////////////////////*/
    /*/////////////////////////////////////HISTORY ENDS/////////////////////////////////////*/
    /*//////////////////////////////////////////////////////////////////////////*/
    // Vertical Menu for Dashboard Starts
    // document.getElementById('DashboardHistoryUD').style.display="none";
    document.getElementById("DashboardMarketComponent").style.display = "none";
    document.getElementById("DashboardDemographics").style.display = "none";
    document.getElementById("AddPromotion").style.display = "none";
    document.getElementById("DashboardAddDiscount").style.display = "none";
    document.getElementById("menu1").className = "borderLeft";
    // Vertical Menu for Dashboard Ends
  }

  onClickChange = id => {
    if (id == "DashboardMarketComponent") {
      document.getElementById(id).style.display = "flex";
    } else document.getElementById(id).style.display = "block";
    if (id == "DashboardHistoryUD") {
      document.getElementById("DashboardMarketComponent").style.display =
        "none";
      document.getElementById("DashboardDemographics").style.display = "none";
      document.getElementById("AddPromotion").style.display = "none";
      document.getElementById("DashboardAddDiscount").style.display = "none";
      document.getElementById("menu1").className = "borderLeft";
      document.getElementById("menu2").className = "";
      document.getElementById("menu3").className = "";
      document.getElementById("menu4").className = "";
      document.getElementById("menu5").className = "";
      this.setState({
        showNav: true
      });
    } else if (id == "DashboardMarketComponent") {
      document.getElementById("DashboardHistoryUD").style.display = "none";
      document.getElementById("DashboardDemographics").style.display = "none";
      document.getElementById("AddPromotion").style.display = "none";
      document.getElementById("DashboardAddDiscount").style.display = "none";
      document.getElementById("menu2").className = "borderLeft";
      document.getElementById("menu1").className = "";
      document.getElementById("menu3").className = "";
      document.getElementById("menu4").className = "";
      document.getElementById("menu5").className = "";
      this.setState({
        showNav: true
      });
    } else if (id == "DashboardDemographics") {
      document.getElementById("DashboardMarketComponent").style.display =
        "none";
      document.getElementById("DashboardHistoryUD").style.display = "none";
      document.getElementById("AddPromotion").style.display = "none";
      document.getElementById("DashboardAddDiscount").style.display = "none";
      document.getElementById("menu3").className = "borderLeft";
      document.getElementById("menu2").className = "";
      document.getElementById("menu1").className = "";
      document.getElementById("menu4").className = "";
      document.getElementById("menu5").className = "";
      this.setState({
        showNav: true
      });
    } else if (id == "AddPromotion") {
      document.getElementById("DashboardHistoryUD").style.display = "none";
      document.getElementById("DashboardMarketComponent").style.display =
        "none";
      document.getElementById("DashboardDemographics").style.display = "none";
      document.getElementById("DashboardAddDiscount").style.display = "none";
      document.getElementById("menu4").className = "borderLeft";
      document.getElementById("menu2").className = "";
      document.getElementById("menu1").className = "";
      document.getElementById("menu3").className = "";
      document.getElementById("menu5").className = "";
      this.setState({
        showNav: false
      });
    } else if (id == "DashboardAddDiscount") {
      document.getElementById("DashboardHistoryUD").style.display = "none";
      document.getElementById("DashboardMarketComponent").style.display =
        "none";
      document.getElementById("AddPromotion").style.display = "none";
      document.getElementById("DashboardDemographics").style.display = "none";
      document.getElementById("menu5").className = "borderLeft";
      document.getElementById("menu2").className = "";
      document.getElementById("menu1").className = "";
      document.getElementById("menu4").className = "";
      document.getElementById("menu3").className = "";
      this.setState({
        showNav: false
      });
    }
  };

  render() {
    return (
      <div className="dashboard1">
        <div className="divA">
          <div className="divProfile">
            <div className="imgProfile">
              <img src={require("../img/dashboard1/profile.png")} alt="" />
            </div>
            <p>YOUR NAME HERE</p>
          </div>
          <div className="divVerticalMenu">
            <ul>
              <a
                href="#"
                onClick={() => this.onClickChange("DashboardHistoryUD")}
              >
                <li id="menu1">HISTORY</li>
              </a>
              <a
                href="#"
                onClick={() => this.onClickChange("DashboardMarketComponent")}
              >
                <li id="menu2">MARKET TRENDS</li>
              </a>
              <a
                href="#"
                onClick={() => this.onClickChange("DashboardDemographics")}
              >
                <li id="menu3">DEMOGRAPHICS</li>
              </a>
              <a href="#" onClick={() => this.onClickChange("AddPromotion")}>
                <li id="menu4">ADD PROMOTIONS</li>
              </a>
              <a
                href="#"
                onClick={() => this.onClickChange("DashboardAddDiscount")}
              >
                <li id="menu5">ADD DISCOUNT</li>
              </a>
            </ul>
          </div>
        </div>
        <div className="divB">
          <div className="row divBrow1">
            <div className="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-12">
              <div className="divBurgerMenu">
                <div className="secondVerticalMenu">
                  <div className="divProfile">
                    <span>
                      <img
                        className="img1"
                        id="secondVerticalMenu"
                        src={require("../img/dashboard1/cross.png")}
                        alt=""
                      />
                    </span>
                    <div className="imgProfile">
                      <img
                        src={require("../img/dashboard1/profile.png")}
                        alt=""
                      />
                    </div>
                    <p>YOUR NAME HERE</p>
                  </div>
                  <div className="divVerticalMenu">
                    <ul>
                      <a
                        href="#"
                        onClick={() => this.onClickChange("DashboardHistoryUD")}
                        id="Menu1"
                      >
                        <li id="menu1">HISTORY</li>
                      </a>
                      <a
                        href="#"
                        onClick={() =>
                          this.onClickChange("DashboardMarketComponent")
                        }
                        id="Menu2"
                      >
                        <li id="menu2">MARKET TRENDS</li>
                      </a>
                      <a
                        href="#"
                        onClick={() =>
                          this.onClickChange("DashboardDemographics")
                        }
                        id="Menu3"
                      >
                        <li id="menu3">DEMOGRAPHIVS</li>
                      </a>
                      <a
                        href="#"
                        onClick={() => this.onClickChange("AddPromotion")}
                        id="Menu4"
                      >
                        <li id="menu4">ADD PROMOTIONS</li>
                      </a>
                      <a
                        href="#"
                        onClick={() =>
                          this.onClickChange("DashboardAddDiscount")
                        }
                        id="Menu5"
                      >
                        <li id="menu5">ADD DISCOUNT</li>
                      </a>
                    </ul>
                  </div>
                </div>
                <img
                  className="burgerButton"
                  src={require("../img/dashboard1/burgerMenu.png")}
                  alt=""
                />
              </div>
              <h3>USER DASHBOARD</h3>
              {this.state.showNav ? (
                <div className="nav">
                  <ul className="oho">
                    <div className="buttonSlide"></div>
                    <li id="li1">TODAY</li>
                    <li id="li2"> MONTH</li>
                    <li id="li3">YEAR</li>
                  </ul>
                </div>
              ) : (
                ""
              )}
            </div>
            <div className="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-12">
              <div className="date">JUNE 27,2019</div>
            </div>
          </div>
          <div className="row divScroll">
            <DashboardHistoryUD />
            <DashboardMarketComponent />
            <DashboardDemographics />
            <AddPromotion />
            <DashboardAddDiscount />
            {/* <DashboardUserComponent /> */}
          </div>
        </div>
      </div>
    );
  }
}
