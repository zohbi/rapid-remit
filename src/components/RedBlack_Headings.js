import React from 'react';
import '../css/bootstrap.css';
import '../css/style-cookies.css'

export default class RedBlack_Headings extends React.Component {
    render() {
        return (
            <div>
                <h2>{this.props.data.redHeading}</h2>
                <h4>{this.props.data.blackHeading}</h4>
            </div>
        )
    }
}