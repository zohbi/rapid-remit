import React from 'react';
import '../css/bootstrap.css';
import '../css/style-footer.css';
import { connect } from 'react-redux';
import '../css/style-our-network.css';
import { bindActionCreators } from 'redux';
import { footer, getSettings } from '../Store/actions/userAuth';

class Footer extends React.Component {
  constructor() {
    super();
    this.state = {
      // setting: {
      //   androidLink: "",
      //   city: "",
      //   companyName: "",
      //   country: "",
      //   email: "",
      //   facebookLink: "",
      //   instagramLink: "",
      //   iosLink: "",
      //   linkedInLink: "",
      //   mapCoortinates: [],
      //   name: "",
      //   officeAddress: "",
      //   poBox: "",
      //   twitterLink: "",
      // }
      setting:{

      }
    }
  }
  componentDidMount() {
    this.props.getData();
    this.props.getSettings()
  }
  componentDidUpdate(prevProps) {
    if (prevProps.setting !== this.props.setting) {
      // console.log("this.props.setting", this.props.setting)
      let {companyName, androidLink, iosLink, officeAddress, poBox, city, country, facebookLink, instagramLink, linkedInLink, twitterLink} = this.props.setting
      this.setState({
        androidLink,
        city,
        companyName,
        country,
        // email,
        facebookLink,
        instagramLink,
        iosLink,
        linkedInLink,
        // mapCoortinates,
        // name,
        officeAddress,
        poBox,
        twitterLink,
      })
    }
  }
  render() {
    let setting = this.state;
    let {companyName, androidLink, iosLink, officeAddress, poBox, city, country, facebookLink, twitterLink, linkedInLink, instagramLink} = this.state
    let { footer } = this.props;
    return (
      <div className='Footer'>
        <div className='picture'>
          <div className='container1'>
            <div className='row no-gutters'>
              <div className='col-x-2 col-lg-2 col-md-0 col-sm-0 col-0'></div>
              <div className='col-x-6 col-lg-6 col-md-12 col-sm-12 col-12 promotionbanner'>
                <h1 className='Lato_Black'>Download Now!</h1>
                <p>
                  Get the Best Rates,
                  <br />
                  Important Remittance Information and
                </p>
                <h4 className='Lato_Black'>
                  Special Promotion Alerts on the go!
                </h4>
                <div className='app-stores'>
                  <a href={androidLink} target="_blank" >
                    <img src={require('../img/footer/google-play.png')} />
                  </a>
                  <a href={iosLink} target="_blank">
                    <img src={require('../img/footer/app-store.png')} />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer className='footer'>
          <div className='container-fluid'>
            <div className='container1'>
              <div className='row'>
                <div className='col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0'></div>
                <div className='col-xl-4 col-lg-4 col-md-3 col-sm-12 col-12'>
                  <div className='contact-links'>
                    <h5 className='Lato_Black'>Help & Support</h5>
                    <a href='' onClick={() => this.props.history.push('/FAQs')}>
                      FAQs
                    </a>
                    <br />
                    <a
                      href=''
                      onClick={() => this.props.history.push('/ContactUs')}
                    >
                      Contact Us
                    </a>
                    <br />
                    <a
                      href=''
                      onClick={() => this.props.history.push('/ournetwork')}
                    >
                      Partners
                    </a>
                  </div>
                </div>

                <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12'>
                  <h5 className='followus Lato_Black'>Follow Us</h5>
                  <ul className='footer-links'>
                    <a href={facebookLink} target="_blank" className='Lato_Regular'>
                      <i className='fab fa-facebook-square bg-icon-img facebook'></i>
                    </a>
                    <a href={instagramLink} target="_blank" className='Lato_Regular'>
                      <i className='fab fa-instagram-square bg-icon-img instagram'></i>
                    </a>
                    <a href={linkedInLink} target="_blank" className='Lato_Regular'>
                      <i className='fab fa-linkedin bg-icon-img linkedin'></i>
                    </a>
                    <a href={twitterLink} target="_blank" className='Lato_Regular'>
                      <i className='fab fa-twitter-square bg-icon-img twitter'></i>
                    </a>
                  </ul>
                </div>

                <div className='col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12'>
                  <h5 className='contact Lato_Black'>Contact Information</h5>
                  <p className='Lato_Regular'>
                    {/* {footer && footer.length > 0 && footer[0].address ? (
                      <span>
                        {' '}
                        {setting.companyName}
                        <br />
                        Office # 100, level 2, Precinct
                        <br />
                        Building 5<br />
                        The Gate District
                        <br />
                        P.O.Box 507048, Dubai, UAE
                      </span>
                    ) : (
                        <span>
                          {' '}
                          {setting.companyName}
                          <br />
                        Office # 100, level 2, Precinct
                          <br />
                        Building 5<br />
                        The Gate District
                          <br />
                        P.O.Box 507048, Dubai, UAE
                        </span>
                      )} */}
                      <span>
                        {' '}
                        {companyName}
                        <br />
                        {officeAddress}
                        <br />
                        P.O.Box {poBox}, {city}, {country}
                      </span>
                  </p>
                </div>
              </div>
              <div className='row row2'>
                <div className='col-xl-1 col-lg-1 col-md-0 col-sm-0 col-12'></div>
                <div
                  className='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 Lato_Regular text-center'
                  id='footer_links'
                >
                  {/* <a
                    href=''
                    onClick={() => this.props.history.push('/PrivacyPolicy')}
                  >
                    Privacy Policy &nbsp; | &nbsp;
                  </a>
                  <a
                    href=''
                    onClick={() =>
                      this.props.history.push('/TermsAndConditions')
                    }
                  >
                    Terms & Conditions &nbsp; | &nbsp;
                  </a>
                  <a
                    href=''
                    onClick={() => this.props.history.push('/CookiesPolicy')}
                  >
                    Cookies Policy
                  </a> */}
                  <a
                    href=''
                    onClick={() => this.props.history.push('/PrivacyPolicy')}
                  >
                    Privacy Policy &nbsp; | &nbsp;
                  </a>
                  <a
                    href=''
                    onClick={() => this.props.history.push('/PrivacyPolicy')}
                  >
                    Terms & Conditions &nbsp; | &nbsp;
                  </a>
                  <a
                    href=''
                    onClick={() => this.props.history.push('/PrivacyPolicy')}
                  >
                    Cookies Policy
                  </a>
                </div>
                {/* <div className="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12 phone Lato_Regular">
                  Phone:{" "}
                  {footer && footer.length > 0 && footer[0].phone ? (
                    footer[0].phone
                  ) : (
                    <span>111 222 333</span>
                  )}
                  <br />
                  Email:{" "}
                  {footer && footer.length > 0 && footer[0].email ? (
                    footer[0].email
                  ) : (
                    <span>contactus@rapidRemir.com</span>
                  )}
                </div> */}
              </div>
            </div>
          </div>
        </footer>
        <div className='copyrights'>
          <div className='container1'>
            <div className='row no-gutters d-flex justify-content-center'>
              <p className='Lato_Regular'>
                Copyright &copy; {(new Date().getUTCFullYear())} <a style={{ textDecoration: "none", color: "inherit" }} href="https://www.rapidremit.biz">RapidRemit.biz.</a> All Rights Reserved
              </p>

              {/* <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
              <div className="col-xl-8 col-lg-8 col-md-6 col-sm-12 col-12">
                <p className="Lato_Regular">
                  Copyright &copy; 2019 RapidRemit.biz.All Rights Reserved
                </p>
              </div>
              <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                <p className="Lato_Regular">Powered by Untitled10</p>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    footer: state.ContactUs.footer,
    setting: state.FAQS.setting
  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: bindActionCreators(footer, dispatch),
  getSettings: bindActionCreators(getSettings, dispatch),

});

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
