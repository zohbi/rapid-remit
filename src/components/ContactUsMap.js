import React from "react";
import "../css/bootstrap.css";
import "../css/style-about.css";

export default class ContactUsMap extends React.Component {
  render() {
    if (this.props?.data && this.props.data[0]) {
      var locationLink = `https://maps.google.com/maps?q=${this.props.data[0].mapCoortinates[1]},${this.props.data[0].mapCoortinates[0]}&hl=en&z=14&output=embed`;
    }
    return (
      <div>
        <div className="Contact_Us_main_bg_image">
          <div className="wrapper">
            <div className="Contact_Us_sec1_image">
              <div className="mapouter">
                <div className="gmap_canvas">
                  <iframe
                    id="gmap_canvas"
                    src={locationLink || ""}
                    // src="https://maps.google.com/maps?q=burjul%20arab&t=&z=13&ie=UTF8&iwloc=&output=embed"
                    frameborder="0"
                    scrolling="no"
                    marginheight="0"
                    marginwidth="0"
                  ></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
