import React from 'react';
import '../css/bootstrap.css';
import '../css/style-our-network.css';
// import $ from 'jquery';


export default class Overviewsmalltable extends React.Component {

    ReturnCompareCardJSXForCarousel = (table, ind) => {
        return (

            <div key={ind} className={`carousel-item ${ind === 0 && "active"}`}>
                <div className="row">
                    <div className="overview-column col-xl-2 col-lg-2 col-md-2 col-sm-3 col-3"></div>
                    <div className="col-xl-3 col-lg-3 col-md-3 col-sm-9 col-9">
                        <div className="column1">
                            <div className="img-div">
                                <div className="logoDiv">
                                    <img
                                        src={table.partner_details[0].img || ""}
                                    />
                                </div>
                            </div>
                            <h2 style={{ textTransform: "capitalize", fontSize: 20, marginTop: 10 }}>
                                {table.partner_name || ""}
                            </h2>
                            {this.props.showRatings(table.partner_details[0].rating || 0)}
                            {this.props.showGray(table.partner_details[0].rating || 0)}
                            <br />
                            <a
                                href={
                                    table.partner_details[0].website_url
                                    || ""
                                }
                                target="_blank"
                            >
                                <button className="overview-form-btn" name="action">
                                    {<span>Go To</span> || ""}{" "}
                                    {table.partner_name || ""}
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                <div>
                    <div className="row">
                        <div className="overview-column1 col-xl-2 col-lg-2 col-md-2 col-sm-3 ">
                            <h5>RATE</h5>
                        </div>
                        <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3 col-sm-9">
                            <p>
                                {
                                    table.currency ? (
                                        <span>{table.currency}</span>
                                    ) : null
                                }
                                {table.rate.toFixed(3) || ""}{" "}
                            </p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="overview-column1 col-xl-2 col-lg-2 col-md-2 col-sm-3 ">
                            <h5>TRANSFER FEE</h5>
                        </div>
                        <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3 col-sm-9">
                            <p>{table && "Applicable "}</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="overview-column1 col-xl-2 col-lg-2 col-md-2 col-sm-3 ">
                            <h5>PAYMENT TYPE</h5>
                        </div>
                        <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3 col-sm-9">
                            <ul>
                                {
                                    table.partner_details[0].payment_methods.map((m) => (
                                        <li style={{ textTransform: 'capitalize' }}>{m}</li>
                                    ))
                                }
                            </ul>
                        </div>
                    </div>
                    <div className="row">
                        <div className="overview-column1 col-xl-2 col-lg-2 col-md-2 col-sm-3 ">
                            <h5>CONTACT</h5>
                        </div>
                        <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3 col-sm-9">
                            <p>{`+${table.partner_details[0].phone}` || ""}</p>
                        </div>
                        {/* <!--colunms end--> */}
                    </div>
                    <div className="row">
                        <div className="overview-column1 col-xl-2 col-lg-2 col-md-2 col-sm-3 ">
                            <h5>COMPANY TYPE</h5>
                        </div>
                        <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3 col-sm-9">
                            <p>
                                {
                                    table.partner_details[0].company_type
                                    || ""
                                }
                            </p>
                        </div>

                    </div>
                    {
                    }
                    <div className="row">
                        <div className="overview-column1 col-xl-2 col-lg-2 col-md-2 col-sm-3 ">
                            <h5>MOBILE APP</h5>
                        </div>
                        <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3 col-sm-9">
                            <p style={{ display: "flex" }} >
                                {table?.partner_details[0] && <>
                                    {((table.partner_details[0].ios_url !== "Not Available " || !table.partner_details[0].ios_url) || (table.partner_details[0].android_url !== "Not Available " || !table.partner_details[0].android_url)) ?
                                        <>
                                            {table.partner_details[0].android_url && table.partner_details[0].android_url !== "Not Available " && <a style={{ width: 40, height: 40 }} href={table.partner_details[0].android_url} target="_blank" ><img style={{ marginRight: 20, }} width="40" height="40" src={require('../img/play-playstore-icon.png')} /></a>}
                                            {table.partner_details[0].ios_url && table.partner_details[0].ios_url !== "Not Available " && <a style={{ width: 40, height: 40, marginLeft: 20 }} href={table.partner_details[0].ios_url} target="_blank"  ><img width="40" height="40" src={require('../img/iOS-Apple-icon.png')} /></a>}
                                        </>
                                        :
                                        "Not Available"
                                    }
                                </>
                                }
                            </p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="overview-column1 col-xl-2 col-lg-2 col-md-2 col-sm-3 ">
                            <h5>KEY FEATURES</h5>
                        </div>
                        <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3 col-sm-9">
                            <ul>
                                {
                                    table.partner_details[0].key_features.map((data) => (
                                        <li>{data}</li>
                                    ))
                                    || ""
                                }
                            </ul>
                        </div>
                    </div>
                    <div className="row">
                        <div className="overview-column1 col-xl-2 col-lg-2 col-md-2 col-sm-3 ">
                            <h5>DOCUMENT</h5>
                        </div>
                        <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3 col-sm-9">
                            <ul>
                                {
                                    table.partner_details[0].documents.map((data) => (
                                        <li>{data}</li>
                                    ))
                                    || ""
                                }
                            </ul>
                        </div>
                    </div>
                    <div className="row">
                        <div className="overview-column1 col-xl-2 col-lg-2 col-md-2 col-sm-3 ">
                            <h5>NEARBY LOCATION</h5>
                        </div>
                        <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3 col-sm-9">
                            <p style={{ cursor: "pointer" }} onClick={() => this.props.delTableIndex(ind)}>
                                {table
                                    ? <iframe
                                        width='250'
                                        height='100'
                                        id='gmap_canvas'
                                        src={`https://maps.google.com/maps?q=${table.location[0].location.coordinates[1]},${table.location[0].location.coordinates[0]}&hl=en&z=14&output=embed`}
                                        frameborder='0'
                                        scrolling='no'
                                        marginheight='0'
                                        marginwidth='0'
                                    ></iframe>
                                    : ""}
                            </p>
                        </div>
                    </div>

                    <div className="row">
                        <div className="overview-column1 col-xl-2 col-lg-2 col-md-2 col-sm-3 ">
                            <h5>ACTION</h5>
                        </div>
                        <div className="overview-columns p-margin2 col-xl-3 col-lg-3 col-md-3 col-sm-9">
                            <p style={{ cursor: "pointer", color: "#f90d26", fontWeight: 700 }} onClick={() => this.props.delTableIndex(ind)}>
                                {table
                                    ? "Remove"
                                    : ""}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    render() {
        // console.log(this.props)
        const { table } = this.props
        return (
            <div>
                <div className="overview-rate-boxes2">
                    <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                        <ol className="carousel-indicators">
                            {table && table.map((val, ind) => <li data-target="#carouselExampleIndicators" data-slide-to={ind} className={`${ind === 0 ? "active" : ""}`}></li>) }
                        </ol>
                        <div className="carousel-inner">
                            {table.map((val, ind) => {
                                return this.ReturnCompareCardJSXForCarousel(val, ind)
                            })}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

