import React, { Fragment } from "react";
import Moment from "react-moment";
import Footer from "./footer";

export default class BlogsReadMore extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      blogsData: [
        {
          blogNumber: "01",
          date: "1TH JUNE,2019",
          heading: "What is Lorem Ipsum?",
          subHeading:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
          paragraph:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever book. It has survived not only five centuries, but also the leap into electronic typesetting, since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
        },
        {
          blogNumber: "02",
          date: "12TH JUNE,2019",
          heading: "What is Lorem Ipsum?",
          subHeading:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
          paragraph:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever book. It has survived not only five centuries, but also the leap into electronic typesetting, since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
        },
        {
          blogNumber: "03",
          date: "13TH JUNE,2019",
          heading: "What is Lorem Ipsum?",
          subHeading:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
          paragraph:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever book. It has survived not only five centuries, but also the leap into electronic typesetting, since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
        },
      ],
    };
  }
componentDidMount(){
  window.scroll(0,0)
}
  render() {
    return (
      <Fragment>
        <div className="container1 blogs">
          <div className="wrapper">
            <div className="blog_readmore_2_sec1">
              <div className=" row blog_readmore_2_sec1_row1">
                <div className="blog_readmore_2_sec1_row1_col1 col-md-6">
                  <h1>Blogs</h1>
                  <div className="blogsDiv">
                    <div className="blogsDivSmall">
                      <span>
                        <p className="p1">BLOG</p>
                      </span>
                      <span>
                        <p className="p2">
                          {this.props?.location?.state?.blogNumber || 0 + 1}
                        </p>
                      </span>
                      <span>
                        <h6>
                          {" "}
                          <Moment format="Do MMM,YYYY">
                            {this.props?.location?.state?.date}
                          </Moment>{" "}
                        </h6>
                      </span>
                    </div>
                    <img
                      src={require("../img/blogs/blogsImgMain.png")}
                      alt=""
                    />
                  </div>
                </div>

                <div className="blog_readmore_2_sec1_row1_col2 col-md-6">
                  <div className="blog_readmore_2_sec1_row1_col2_row1">
                    <h3>{this.props?.location?.state?.heading}</h3>
                  </div>
                  <div className="blog_readmore_2_sec1_row1_col2_row2">
                    <h4>{this.props?.location?.state?.subHeading || "No Description"}</h4>
                  </div>

                  <div className="blog_readmore_2_sec1_row1_col2_row3">
                    <p>{this.props?.location?.state?.paragraph}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer history={this.props.history} />
      </Fragment>
    );
  }
}
