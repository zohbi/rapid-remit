import React, { Component } from "react";
import IndexNavigation from "./indexNavigation";
import Fade from "react-reveal/Fade";
import Flip from "react-reveal/Flip";

export default class AboutJumbo extends React.Component {
  render() {
    return (
      <div className="about-us-wrapper">
        <div className="container-fluid about-us">
          {/* <IndexNavigation aboutUs={true} history={this.props.history} /> */}
          <div className="row m-0">
            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <Flip right opposite cascade duration={3000} mirror> <h1 className="Lato_Black">ABOUT RAPID REMIT</h1></Flip>
            </div>
          </div>
        </div>
        <Fade right>
          <div className="container1">
            <div className="row about-second-container">
              <div className="col-xl-5 col-lg-5 col-md-10 col-sm-12 col-12">
                <h2 className="Lato_Black">{this.props.aboutdata?.abouttitle || "About US"}</h2>
                <h5 className="Lato_Regular">
                  {this.props.aboutdata?.aboutheading || "Regions first money transfer comparison service"}
                  {/* Regions first money transfer comparison service */}
                </h5>
                <div className="horizontal-red-line col-xl-5 col-lg-6 col-md-8 col-sm-11 col-11"></div>
              </div>

              <div className="about-us-imgs col-xl-6 col-lg-7 col-md-0 col-sm-0 col-0">
                <div className="img1">
                  <img src={require("../img/aboutUs/Group-24.png")} />
                </div>
                <div className="img2">
                  <img src={require("../img/aboutUs/Group-22.png")} />
                </div>
                <div className="img3">
                  <img src={require("../img/aboutUs/Group-23.png")} />
                </div>
              </div>
              <div className="about-para col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                <p className="Lato_Regular">
                  {this.props.aboutdata?.abouttext || "No Description"}
                  {/* RapidRemit.biz makes it easy to compare money transfer services, allowing you to assess companies side-by-side and select the best value for your money. Users can compare, choose, and review exchange rates, transfer fees, and other information from the most well known and popular money transfer service providers, all in one place. For people seeking the best provider to send money overseas, we present the information they need, fast and free. */}
                </p>
              </div>
              <div className="about-para2 col-xl-8 col-lg-11 col-md-12 col-sm-12 col-12">
                {/* <p className="Lato_Regular">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  <br /> sed do eiusmod tempor incididunt ut labore et dolore
                  magna aliqua.
                  <br /> Ut enim ad minim veniam, quis nostrud exercitation
                  ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                  aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint
                  occaecat cupidatat non proident, sunt in culpa qui officia
                  deserunt mollit anim id est laborum. Excepteur sint occaecat
                  cupidatat nonproident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.
                </p> */}
              </div>
              {/* <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h1 className="Lato_Black">About Us</h1>
              </div> */}
            </div>
          </div>
        </Fade>
      </div>
    );
  }
}
