import React from "react";
import "../css/bootstrap.css";
import "../css/style-cookies.css";
import HeadingImgLine from "./HeadingImgLine";
import RedBlack_Headings from "./RedBlack_Headings";
import HeadngPlusPara from "./headngPlusPara";
import Footer from "../components/footer";
import { connect } from "react-redux"
import { getPrivacyHeading } from "../Store/actions/userAuth";
import ReactHtmlParser from "react-html-parser"


class PrivacyPolicy extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      HeadingImgLine: {
        heading: "Privacy Policy",
        img: require("../img/privacy/privacyImg1.png"),
      },

      RedBlack_Headings: {
        redHeading: "Privacy Policy",
        blackHeading:
          "How do we keep your Personal Information safe and secure?",
      },

      arr: [
        {
          heading: "What is Lorem Ipsum?",
          paragraph:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!",
        },
        {
          heading: "Why Do we use It?",
          paragraph:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!",
        },
        {
          heading: "Where does It come from?",
          paragraph:
            "dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!",
          paragraph2:
            "Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere ut fugiat beatae ea eveniet sit quia quis, iste laborum harum, itaque aut odio pariatur. Quos obcaecati suscipit asperiores temporibus nam?",
        },
        {
          heading: "Where can I get some?",
          paragraph:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!. Lpsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lpsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti! Lpsum dolor sit amet, consectetur adipisicing elit. Expedita quo dolorem nesciunt velit consequuntur fugiat voluptates natus perferendis, consectetur hic, eos suscipit soluta tempore. Voluptates facere nulla magnam optio deleniti!",
        },
      ],
    };
  }

  componentDidUpdate(prvProps) {
    if (prvProps.privacyheading !== this.props.privacyheading) {
      this.setState({
        privacyheadingData: this.props.privacyheading?.text
      })
    }
  }
  componentDidMount() {
    this.props.getPrivacyHeading()
  }

  render() {
    const { privacyheadingData } = this.state
    return (
      <div>
        <div className="container-fluid cookies">
          <div className="container1">
            <HeadingImgLine
              data={this.state.HeadingImgLine}
              redlineclassName="privacyRedLine"
            />
            {ReactHtmlParser(privacyheadingData)}
            {/* <div className="cookiesContent">
              <RedBlack_Headings data={this.state.RedBlack_Headings} />
              <p>
                <span>RapidRemit</span> ("us", "we", or "our") operates
                the website (the "Service").
              </p>
              <p>
                This page informs you of our policies regarding the collection,
                use, and disclosure of personal data when you use our Service
                and the choices you have associated with that data.
              </p>
              <p>
                We use your data to provide and improve the Service. By using
                the Service, you agree to the collection and use of information
                in accordance with this policy. Unless otherwise defined in this
                Privacy Policy, terms used in this Privacy Policy have the same
                meanings as in our Terms and Conditions, accessible from
              </p>
              <h6>Information Collection and Use</h6>
              <p>
                We collect several different types of information for various
                purposes to provide and improve our Service to you.
              </p>
              <h6>Types of Data Collected</h6>
              <h6>Personal Data  </h6>
              <p>
                While using our Service, we may ask you to provide us with
                certain personally identifiable information that can be used to
                contact or identify you ("Personal Data"). Personally,
                identifiable information may include, but is not limited to:
              </p>
              <ul>
                <li>Email address</li>
                <li>First name and last name</li>
                <li>Phone number</li>
                <li>Cookies and Usage Data</li>
              </ul>
              <h6>Usage Data  </h6>
              <p>
                We may also collect information how the Service is accessed and
                used ("Usage Data"). This Usage Data may include information
                such as your computer's Internet Protocol address (e.g. IP
                address), browser type, browser version, the pages of our
                Service that you visit, the time and date of your visit, the
                time spent on those pages, unique device identifiers and other
                diagnostic data.
              </p>
              <h6>Tracking & Cookies Data  </h6>
              <p>
                We use cookies and similar tracking technologies to track the
                activity on our Service and hold certain information.
              </p>
              <p>
                Cookies are files with small amount of data which may include an
                anonymous unique identifier. Cookies are sent to your browser
                from a website and stored on your device. Tracking technologies
                also used are beacons, tags, and scripts to collect and track
                information and to improve and analyze our Service. You can
                instruct your browser to refuse all cookies or to indicate when
                a cookie is being sent. However, if you do not accept cookies,
                you may not be able to use some portions of our Service.
              </p>
              <p>Examples of Cookies we use:</p>
              <ul>
                <li>
                  <span>Session Cookies.</span> We use Session Cookies to
                  operate our Service.
                </li>
                <li>
                  <span>Preference Cookies.</span> We use Preference Cookies to
                  remember your preferences and various settings.
                </li>
                <li>
                  <span>Security Cookies.</span> We use Security Cookies for
                  security purposes.
                </li>
              </ul>
              <h6>Use of Data  </h6>
              <p>RapidRemit uses the collected data for various purposes:</p>
              <ul>
                <li>To provide and maintain the Service</li>
                <li>To notify you about changes to our Service</li>
                <li>
                  To allow you to participate in interactive features of our
                  Service when you choose to do so
                </li>
                <li>To provide customer care and support</li>
                <li>
                  To provide analysis or valuable information so that we can
                  improve the Service
                </li>
                <li>To monitor the usage of the Service</li>
                <li>To detect, prevent and address technical issues</li>
              </ul>
              <h6>Transfer of Data</h6>
              <p>
                Your information, including Personal Data, may be transferred to
                — and maintained on — computers located outside of your state,
                province, country or other governmental jurisdiction where the
                data protection laws may differ than those from your
                jurisdiction.
              </p>
              <p>
                Your consent to this Privacy Policy followed by your submission
                of such information represents your agreement to that transfer.
              </p>
              <p>
                RapidRemit will take all steps reasonably necessary to ensure
                that your data is treated securely and in accordance with this
                Privacy Policy and no transfer of your Personal Data will take
                place to an organization or a country unless there are adequate
                controls in place including the security of your data and other
                personal information.
              </p>
              <h6>Disclosure of Data</h6>
              <h6>Legal Requirements</h6>
              <p>
                RapidRemit may disclose your Personal Data in the good faith
                belief that such action is necessary to:
              </p>
              <ul>
                <li>To comply with a legal obligation</li>
                <li>
                  To protect and defend the rights or property of RapidRemit
                </li>
                <li>
                  To prevent or investigate possible wrongdoing in connection
                  with the Service
                </li>
                <li>
                  To protect the personal safety of users of the Service or the
                  public
                </li>
                <li>To protect against legal liability</li>
              </ul>
              <h6>Security Of Data</h6>
              <p>
                The security of your data is important to us but remember that
                no method of transmission over the Internet, or method of
                electronic storage is 100% secure. While we strive to use
                commercially acceptable means to protect your Personal Data, we
                cannot guarantee its absolute security.
              </p>
              <h6>Service Providers</h6>
              <p>
                We may employ third party companies and individuals to
                facilitate our Service ("Service Providers"), to provide the
                Service on our behalf, to perform Service-related services or to
                assist us in analyzing how our Service is used.
              </p>
              <p>
                These third parties have access to your Personal Data only to
                perform these tasks on our behalf and are obligated not to
                disclose or use it for any other purpose.
              </p>
              <h6>Analytics</h6>
              <p>
                We may use third-party Service Providers to monitor and analyze
                the use of our Service.
              </p>
              <h6>Google Analytics</h6>
              <p>
                Google Analytics is a web analytics service offered by Google
                that tracks and reports website traffic.
              </p>
              <p>
                Google uses the data collected to track and monitor the use of
                our Service. This data is shared with other
              </p>
              <p>
                Google services. Google may use the collected data to
                contextualize and personalize the ads of its own advertising
                network.
              </p>
              <p>
                You can opt-out of having made your activity on the Service
                available to Google Analytics by installing the Google Analytics
                opt-out browser add-on. The add-on prevents the Google Analytics
                JavaScript (ga.js, analytics.js, and dc.js) from sharing
                information with Google Analytics about visits activity. For
                more information on the privacy practices of Google, please
                visit the Google Privacy & Terms web page:
                <a
                  href="https://policies.google.com/privacy?hl=en"
                  target="_blank"
                >
                  https://policies.google.com/privacy?hl=en
                </a>
              </p>
              <h6>Links to Other Sites</h6>
              <p>
                Our Service may contain links to other sites that are not
                operated by us. If you click on a third-party link, you will be
                directed to that third party's site. We strongly advise you to
                review the Privacy Policy of every site you visit.
              </p>

              <p>
                We have no control over and assume no responsibility for the
                content, privacy policies or practices of any third-party sites
                or services.
              </p>
              <h6>Children's Privacy</h6>
              <p>
                Our Service does not address anyone under the age of 18
                ("Children").
              </p>
              <p>
                We do not knowingly collect personally identifiable information
                from anyone under the age of 18. If you are a parent or guardian
                and you are aware that your Children has provided us with
                Personal Data, please contact us. If we become aware that we
                have collected Personal Data from children without verification
                of parental consent, we take steps to remove that information
                from our servers.
              </p>
              <h6>Changes To This Privacy Policy</h6>
              <p>
                We may update our Privacy Policy from time to time. We will
                notify you of any changes by posting the new Privacy Policy on
                this page.
              </p>
              <p>
                We will let you know via email and/or a prominent notice on our
                Service, prior to the change becoming effective and update the
                "effective date" at the top of this Privacy Policy.
              </p>
              <p>
                You are advised to review this Privacy Policy periodically for
                any changes. Changes to this Privacy Policy are effective when
                they are posted on this page.
              </p>
              <h6>Contact Us</h6>
              <p>
                If you have any questions about this Privacy Policy, please
                contact us:
              </p>
              <ul>
                <li>By email: info@rapidremit.biz</li>
              </ul>
            </div> */}
          </div>
        </div>
        <Footer history={this.props.history} />
      </div>
    );
  }
}



const mapStateToProps = (state) => {
  return {
    privacyheading: state.FAQS.privacyheading,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getPrivacyHeading: () => dispatch(getPrivacyHeading())
});

export default connect(mapStateToProps, mapDispatchToProps)(PrivacyPolicy);
