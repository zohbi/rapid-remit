import React, { Fragment } from "react";
import "../css/bootstrap.css";
import "../css/style-news.css";
import "../css/font.css";
import Moment from "react-moment";
import Footer from "./footer";

export default class News extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      newsData: [
        {
          img: require("../img/news/newsImg.png"),
          newsNumber: "01",
          date: "1TH JUNE,2019",
          heading: "What is Lorem Ipsum?",
          subHeading:
            "",
          paragraph:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever book. It has survived not only five centuries, but also the leap into electronic typesetting, since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
        },
      ],
    };
  }
  componentDidMount(){
    window.scroll(0,0)
  }
  render() {
    // console.log("locat6ion", this.props.location.state);
    // this.setState({
    //   news: this.props.location.state
    // })
    return (
      <Fragment>
        <div className="container-fluid news">
          <div className="container1">
            <div className="row newsRow1">
              <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <h1 style={{ color: "#f90d26" }}>NEWS</h1>
                <img src={this.state.newsData[0].img} alt="" />
                <div className="newsRedLine"></div>
                <h2>{this.state.newsData[0].subHeading}</h2>
              </div>
              <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div className="News01">
                  <h4>NEWS</h4>
                  <h6 className="">{this.props.location.state.key1}</h6>
                  <h5>
                    <Moment format="ddd, YYYY/MM/DD">
                      {this.props.location.state.createdAt}
                    </Moment>
                  </h5>
                  <h3>{this.props.location.state.heading}</h3>
                  <p>{this.props.location.state.news}</p>
                  {/* <a href="#">
                  <span className="spanHeading">+Read More..</span>
                </a> */}
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer history={this.props.history} />
      </Fragment>
    );
  }
}
