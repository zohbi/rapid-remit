import React from 'react';
import '../css/style-dashboardAddPromotions.css'
  
export default class AddPromotion extends React.Component{

  render() {
    return (
        <div className="row divScroll2" id="AddPromotion">

            <div className="row">
            <div className="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-9">
                <a href="#">
                <button className="dashboardButton1">CURRENT & UPCOMING</button>
                </a>
            </div>
            <div className="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2"></div>
            <div className="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1"></div>
            </div>

            <div className="row">
            <div className="col-xl-9 col-lg-9 col-md-9 col-sm-9 col-12">
                <p>Create promotions and manage current and upcoming onces.</p>
            </div>
            <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                <button className="dashboardButton2">Create Promotion</button>
            </div>
            </div>

            <div className="row rowTable">
            <table>
                <thead>
                <tr>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Name</th>
                    <th>Available On</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1 August, 2019 - 9:00am</td>
                    <td>30 August, 2019 - 5:00am</td>
                    <td>Iphone XS</td>
                    <td>Creating Account</td>
                </tr>
                <tr>
                    <td>1 August, 2019 - 9:00am</td>
                    <td>30 August, 2019 - 5:00am</td>
                    <td>Iphone XS</td>
                    <td>Creating Account</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            
            
            </div>

        </div>
    )
  }
}