import React from "react";
import "../css/linechart.css";
// import { Bar, Line } from "react-chartjs-2";
import CanvasJSReact from "../canvasJS/canvasjs.react";
import { connect } from "react-redux";
import { Liverate } from "../Store/actions/index";
import { Spinner } from "react-activity";
import "react-activity/dist/react-activity.css";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const options3 = {
  backgroundColor: "transparent",
  animationEnabled: true,
  // dataPointWidth: 120,

  // backgroundColor:'blue',
  width: 700,
  height: 240,

  axisX: {
    lineColor: "transparent",
    tickColor: "transparent",
    // labelAngle: -90,
  },
  axisY: {
    gridColor: "transparent",
    lineColor: "transparent",
    tickColor: "transparent",
    interval: 100,
    maximum: 500,
  },
  data: [
    {
      type: "line", //change it to line, area, bar, pie, etc
      showInLegend: true,
      dataPoints: [
        { label: "Jun 27       Jun 26", y: 230, color: "#2DA8A8" },
        { label: "Jun 25       Jun 24", y: 390, color: "#FF851B" },
        { label: "Jun 23       Jun 22", y: 80, color: "#9C2BAD" },
        { label: "Jun 23       Jun 22", y: 260, color: "#FFCA1E" },
        { label: "Jun 23       Jun 22", y: 450, color: "#FF0000" },
      ],
    },
  ],
};
class DashboardMarketComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: 7,
      partners: "moneygram",
      // chartData: {
      //   labels: [
      //     0,
      //     1,
      //     2,
      //     3,
      //     4,
      //     5,
      //     6,
      //     7,
      //     8,
      //     9,
      //     10,
      //     11,
      //     12,
      //     13,
      //     14,
      //     15,
      //     16,
      //     17,
      //     18,
      //     19,
      //     20,
      //     21,
      //     22,
      //     23,
      //   ],
      //   datasets: [
      //     {
      //       data: [
      //         5,
      //         7,
      //         9,
      //         12,
      //         14,
      //         17,
      //         15,
      //         18,
      //         15,
      //         11,
      //         12,
      //         14,
      //         20,
      //         10,
      //         15,
      //         18,
      //         17,
      //         10,
      //         15,
      //         16,
      //         8,
      //         7,
      //         9,
      //         3,
      //       ],
      //       backgroundColor: "#F6F6F6",
      //     },
      //   ],
      // },
      // options: {
      //   animationEnabled: true,
      //   axisY: {
      //     includeZero: true,
      //     suffix: "K",
      //     maximum: 3,
      //     interval: 1,
      //   },
      //   toolTip: {
      //     shared: true,
      //   },
      //   data: [
      //     {
      //       type: "line",
      //       name: "2016",
      //       lineDashType: "dot",
      //       color: "#528FF5",
      //       markerType: "none",
      //       dataPoints: [
      //         { y: 2, label: "11" },
      //         { y: 1, label: "12" },
      //         { y: 3, label: "13" },
      //         { y: 2, label: "14" },
      //         { y: 1, label: "15" },
      //         { y: 0, label: "16" },
      //         { y: 0, label: "17" },
      //       ],
      //     },
      //   ],
      // },
      // lineData: {
      //   labels: [11, 12, 13, 14, 15, 16, 17],
      //   datasets: [
      //     {
      //       data: [0, 2, 5, 11, 14, 8, 6, 3],
      //       backgroundColor: "#528FF5",
      //     },
      //     {
      //       data: [1, 8, 6, 12, 14, 17, 10, 8],
      //       backgroundColor: "#F4191F",
      //     },
      //   ],
      //},
    };
  }
  // componentWillMount() {
  //   this.props.getData("USD", this.state.time, "uaeexchange");
  //   //this.props.getData("PKR", this.state.time, "uaeexchange");
  // }

  componentDidMount() {
    let options = {};
    this.setState({
      options: options,
    });
  }
  chartFilterData = (rates, currency) => {
      let filteredData;
      rates.map((val,index) => {
          if(val[currency]){
            filteredData = val[currency]
          }
      })

      return {
        type: "spline",
        name: currency,
        showInLegend: true,
        dataPoints:
        filteredData && filteredData.length > 0 ? filteredData.reverse() : [],
      }
  }
  render() {
    const { currency } = this.props
    const options = {
      animationEnabled: true,

      width: 640,
      // height: 400,

      title: {
        text: "Rate Chart",
      },
      axisY: {
        title: "Rates",
        includeZero: false,
      },
      // toolTip: {
      //   shared: true,
      // },
      data: [
        this.chartFilterData(this.props.rates, currency),
        // {
        //   type: "spline",
        //   name: "AUD",
        //   showInLegend: true,
        //   dataPoints:
        //     this.props.rates.length > 1 ? this.props.rates[0]["AUD"] : [],
        // },
        // {
        //   type: "spline",
        //   name: "EGP",
        //   showInLegend: true,
        //   dataPoints:
        //     this.props.rates.length > 1 ? this.props.rates[2]["EGP"] : [],
        // },

        // {
        //   type: "spline",
        //   name: "EUR",
        //   showInLegend: true,
        //   dataPoints:
        //     this.props.rates.length > 1 ? this.props.rates[3]["EUR"] : [],
        // },
        // {
        //   type: "spline",
        //   name: "GBP",
        //   showInLegend: true,
        //   dataPoints:
        //     this.props.rates.length > 1 ? this.props.rates[4]["GBP"] : [],
        // },
        // {
        //   type: "spline",
        //   name: "INR",
        //   showInLegend: true,
        //   dataPoints:
        //     this.props.rates.length > 1 ? this.props.rates[5]["INR"] : [],
        // },
        // {
        //   type: "spline",
        //   name: "JOD",
        //   showInLegend: true,
        //   dataPoints:
        //     this.props.rates.length > 1 ? this.props.rates[6]["JOD"] : [],
        // },
        // {
        //   type: "spline",
        //   name: "LKR",
        //   showInLegend: true,
        //   dataPoints:
        //     this.props.rates.length > 1 ? this.props.rates[7]["LKR"] : [],
        // },
        // {
        //   type: "spline",
        //   name: "NPR",
        //   showInLegend: true,
        //   dataPoints:
        //     this.props.rates.length > 1 ? this.props.rates[8]["NPR"] : [],
        // },
        // {
        //   type: "spline",
        //   name: "PHP",
        //   showInLegend: true,
        //   dataPoints:
        //     this.props.rates.length > 1 ? this.props.rates[9]["PHP"] : [],
        // },
        // {
        //   type: "spline",
        //   name: "PKR",
        //   showInLegend: true,
        //   dataPoints:
        //     this.props.rates.length > 1 ? this.props.rates[10]["PKR"] : [],
        // },
        // {
        //   type: "spline",
        //   name: "USD",
        //   showInLegend: true,
        //   dataPoints:
        //     this.props.rates.length > 1 ? this.props.rates[11]["USD"] : [],
        // },
        // {
        //   type: "spline",
        //   name: "USD",
        //   showInLegend: true,
        //   dataPoints:
        //     this.props.rates.length > 1 ? this.props.rates[0]["USD"] : [],
        // },
      ],
    };

    return (

      <div className="row divScroll" id="DashboardMarketComponent">
        <div className="line_chart col-x-12 col-lg-12 col-md-12 col-sm-12 col-12">
          {/* <div className="row">
            <div className="chart_details col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
              <h6>Users</h6>
              <h5 style={{ fontFamily: "Lato-Black" }}>14K</h5>
              <p id="red_text">11.7%</p>
              <p>vs last 7 days</p>
            </div>
            <div className="chart_details col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
              <p>Revenue</p>
              <h5>$92K</h5>
              <p id="red_text">29.8</p>
            </div>
            <div className="chart_details col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
              <p>Conversion Rate</p>
              <h5>2.88%</h5>
              <p id="green_text">27.2%</p>
            </div>
            <div className="chart_details col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
              <p>Sessions</p>
              <h5>17K</h5>
              <p id="red_text">11.4%</p>
            </div>
          </div> */}
          {/* LINE CHART */}
          {this.props.loading ? (
            <Spinner size="100px" />
          ) : (
            <>
              <div style={{ margin: "0 auto" }}>
                {/* <select
                name="time"
                value={this.state.time}
                onChange={(e) => {
                  this.setState({ time: e.target.value });
                  this.props.getData(
                    "USD",
                    e.target.value,
                    this.state.yarn
                  );
                }}
              >
                <option value="7">7 days</option>
                <option value="30">30 days</option>
              </select> */}
              </div>
              <div id="chartContainer" className="mx-auto">
                <CanvasJSChart options={options} />
              </div>
            </>
          )}
        </div>

        {/* GRAY DIVIDER */}

        {/* <div className="second_divider col-xl-4 col-lg-4 col-md-4 col-sm-5 col-12">
          <p id="right_now">User right now</p>
          <div id="ten">10</div>
          <p>Pages view per minute</p>
          <hr />

          <div className="chart">
            <Bar
              data={this.state.chartData}
              options={{
                scales: {
                  xAxes: [{ display: false }],
                  yAxes: [{ display: false }],
                },
              }}
              legend={{ display: false }}
              height={80}
            />
          </div>

          <p>Top Active Pages</p>
          <p className="right">Users</p>
          <p>/</p>
          <p className="right">4</p>
          <p>/basket.html</p>
          <p className="right">1</p>
          <p>/basket.html?e...ionstatus = true</p>
          <p className="right">1</p>
          <p>/google+redesign/apparel</p>
          <p className="right">1</p>
          <p>/google+redesign/bags</p>
          <p className="right">1</p>
          <p id="realtime">REALTIME REPORT</p>
          <i className="fas fa-angle-right lessthan"></i>
        </div> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    rates: state.Liverate.rate,
    loading: state.Liverate.isLoading,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: (obj, bb, cc) => dispatch(Liverate.rates(obj, bb, cc)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardMarketComponent);
