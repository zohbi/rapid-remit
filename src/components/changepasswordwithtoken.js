import React from "react";
import "../css/bootstrap.css";
import "../css/style-registration.css";
import "../css/font.css";
import "../css/loader.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { UpdateUser, UpdateUserPassword, Recover } from "../Store/actions";
import { bindActionCreators } from "redux";
import firebase from "firebase";
import { storage } from "../firebase";

// import login from "../Store/reducer/login";

// <-------------- Password Encode Decode --------------->

// import HTMLDecoderEncoder from "html-encoder-decoder";
import { Base64 } from "js-base64";
import Loader from "./loader";
import Toast from "./toast";
import validator from "validator";

var storageRef = firebase.storage().ref("/user");

// global variables
var error;
// ---

// -------------------------><-----------------------------

class Profile extends React.Component {
  constructor() {
    super();
    this.state = {
      password: "",
      confirmPassword: "",
    };
  }

  onChangeValue = (name, value) => {
    let stateName = name;
    this.setState({
      [stateName]: value,
    });
  };

  changePassword = (e) => {
    e.preventDefault()

    let data = {
      password: this.state.password,
      conformPassword: this.state.confirmPassword,
      email: this.state.email,
    };
    // console.log("data", data);
    if (data.password == "") {
      this.setState({ error1: "please fill your password" });
    }else if(data.password.length < 8){
      this.setState({ error1: "Invalid password lenght!! Password contain max 8 characters!" });
    }
    else if (data.conformPassword !== data.password) {
      this.setState({ error1: "password not match" });
    } else if (data.password == "") {
      this.setState({ error1: "please fill your password" });
    }
    else {
      // console.log("all done");
      let loginData = {
        email: this.state.email,
        newpassword: this.state.password,
      };
      // console.log("loginData", loginData);
      // let id = JSON.parse(localStorage.getItem("UserData"))._id;
      // console.log("-id", id);
      this.props.UpdateUserPassword(loginData);
      //localStorage.setItem("password", loginData.password);

    }
  };

  onChangeValue = (name, value) => {
    let stateName = name;
    this.setState({
      [stateName]: value,
    });
  };

  componentDidUpdate(prevprops, prevstate) {
    if (prevprops.Recoverdata !== this.props.Recoverdata) {
        this.setState({ email: this.props.Recoverdata?.email })
    }
  }
  componentDidMount(){
    this.props.Recover({token: this.props.match.params.token})
  }

  render() {
    let data = JSON.parse(localStorage.getItem("UserData"));
    const {email} = this.state
    // console.log("DATALOAGIN", this.state.Individual, this.state.Business);
    return (
      <div className="registration">
        <div className="container01">
          <div className="registrationDivMain">
            <div className="form">
              <div className="">
                <div
                  style={{ margin: 0 }}
                  className="back-arrow-button"
                  onClick={() => this.props.history.goBack()}
                >
                  <i
                    className="fa fa-arrow-left"

                  ></i>
                </div>
              </div>
              <form>
                <div className="row">
                  <h1>Reset Password</h1>
                </div>
                {/* <div className="form-group row">
                  <div className="col-sm-12">
                    <div className="fields">
                      <label for="password" className=" col-form-label">
                        Email
                      </label>
                      <input
                        disabled
                        type="email"
                        className="form-control"
                        id="email"
                        defaultValue={this.state.email}
                        placeholder="info@gmail.com"
                        onChange={e =>
                          this.onChangeValue("oldPassword", e.target.value)
                        }
                      />
                    </div>
                  </div>
                </div> */}
                <div className="form-group row">
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="password" className=" col-form-label">
                        Password
                      </label>
                      <input
                        disabled={(email) ? false : true}
                        type="password"
                        className="form-control"
                        id="password"
                        placeholder="XXX XXX XXX"
                        min={8}
                        onChange={e =>
                          this.onChangeValue("password", e.target.value)
                        }
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="confirmPassword" className=" col-form-label">
                        Confirm Password
                      </label>
                      <input
                        type="password"
                        disabled={(email) ? false : true}
                        className="form-control"
                        id="confirmPassword"
                        placeholder="XXX XXX XXX"
                        min={8}
                        onChange={e =>
                          this.onChangeValue("confirmPassword", e.target.value)
                        }
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-sm-12 d-flex justify-content-end">
                    <button
                     disabled={(email) ? false : true}
                      type="submit"
                      className="btn btn-primary"
                      onClick={(e) => this.changePassword(e)}
                      style={{
                        backgroundColor: this.state.isLoadingLogin
                          ? "#F64846"
                          : "",
                      }}
                    >
                      {!this.state.isLoadingRegister ? (
                        "Change Password"
                      ) : (
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-around",
                              alignItems: "center",
                            }}
                          >
                            Change Password <Loader />
                          </div>
                        )}
                    </button>
                  </div>
                  {this.state.error1 ? (
                    <div className="alert alert-danger alert-dismissible fade show">
                      1. {this.state.error1}
                    </div>
                  ) : (
                      " "
                    )}
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    Recoverdata: state.Recover.Recover,
  };
};

const mapDispatchToProps = (dispatch) => ({
  Recover:obj => dispatch(Recover.Recover(obj)),
  UpdateUserPassword : bindActionCreators(UpdateUserPassword, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
