import React from "react";
import ReactHtmlParser from "react-html-parser"

export default class RatingCard extends React.Component {
  render() {
    // console.log("default", this.props);
    return (
      <div>
        {!this.props.changePage ? (
          <div className="container-fluid Instructions">
            <div className="container1">
              <div className="row">
                <div className="instruct1 col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                  <div className="picDivi">
                    <div className="picDivx"></div>
                    <div className="picDivy">
                      <img src={this.props.data.img} />
                    </div>
                  </div>
                </div>
                <div className="instruct col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
                  <h1>{this.props.data.heading}</h1>

                  {ReactHtmlParser(this.props.HTMLContent)}
                  {/* {this.props.content &&
                    this.props.content[0] &&
                    this.props.content[0].comparison_one
                      ? this.props.content[0].comparison_one
                      : ""} */}

                  {/* <p>
                    {this.props.content &&
                    this.props.content[0] &&
                    this.props.content[0].comparison_two
                      ? this.props.content[0].comparison_two
                      : ""}
                  </p> */}
                </div>
              </div>
            </div>
          </div>
        ) : !this.props.face ? (
          <div className="row">
            <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
              <div className="picDiv1">
                <div className="picDiva  "></div>
                <div className="picDivb">
                  <img src={this.props.img} alt="" />
                </div>
              </div>
            </div>
            <div className="col-Text col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
              <h2>{this.props.heading}</h2>
              <p>{this.props.paragraph}</p>
              <p>{this.props.paragraph1}</p>
            </div>
          </div>
        ) : (
              <div className="row">
                <div className="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                  <h2>{this.props.heading}</h2>
                  <p>{this.props.paragraph}</p>
                  <p>{this.props.paragraph1}</p>
                </div>
                <div className="row2col2 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                  <div className="picDiv1">
                    <div className="picDiva  "></div>
                    <div className="picDivb">
                      <img src={this.props.img} alt="" />
                    </div>
                  </div>
                </div>
              </div>
            )}
      </div>
    );
  }
}
