import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Base64 } from 'js-base64';
import { connect } from 'react-redux';
import { Login, Logout, SetIsNavActive } from '../Store/actions';
import { createBrowserHistory } from "history";

const history = createBrowserHistory();
class Header extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Sign In / Register',
      navOptions: [
        {
          name: 'Comparison',
          navigateTo: '/comparison',
          borderBottom: false,
          dropdown: false,
        },
        {
          name: 'Our Network',
          navigateTo: '/ournetwork',
          borderBottom: false,
          dropdown: false,
        },
        {
          name: 'Special Offer',
          navigateTo: '/SpecialOffer',
          borderBottom: false,
          dropdown: false,
        },
        {
          name: 'About Us',
          navigateTo: '/AboutUs',
          borderBottom: false,
          dropdown: false,
        },
        {
          name: 'Learn',
          navigateTo: '/KnowledgeBase',
          subLinks: [
            {
              name: 'News',
              navigateTo: '/KnowledgeBase',
              hashLink:'#firstNews'
            },
            {
              name: 'Articles',
              navigateTo: '/KnowledgeBase',
              hashLink:'#firstArticle'
            },
            {
              name: 'Blogs',
              navigateTo: '/KnowledgeBase',
              hashLink: '#firstBlog'
            },
          ],
          borderBottom: false,
          dropdown: true,
        },
        // {
        //   name: 'More',
        //   navigateTo: '/AboutUs',
        //   subLinks: [
        //     {
        //       name: 'About Us',
        //       navigateTo: '/AboutUs',
        //     },
        //     {
        //       name: 'Contact Us',
        //       navigateTo: '/ContactUs',
        //     },
        //     {
        //       name: 'Forex Calculator',
        //       navigateTo: '/Converter',
        //     },
        //   ],
        //   borderBottom: false,
        //   dropdown: true,
        // },
      ],
      borderBottom: false,
    };
  }

  componentWillMount() {
    if (localStorage.getItem('UserData')) {
      let data = JSON.parse(localStorage.getItem('UserData'));
      let pass = localStorage.getItem('PassSec');
      let secPass = Base64.decode(pass);
      // console.log();
      if (localStorage.getItem('PassSec')) {
        this.setState({
          name: data.firstName,
        });
        // console.log('PAsssssssssssssswooooooord', this.state.name, 'Nammmmeee');
        let loginData = {
          email: data.email,
          password: secPass,
        };
        this.props.login(loginData);
      } else {
        this.setState({
          name: data.firstName,
        });
        let loginData = {
          email: data.email,
          password: localStorage.getItem('password'),
        };
        this.props.login(loginData);
      }
      // console.log(data);
    } else {
      // console.log('user already logout');
    }
  }

  logout = () => {
    this.props.logout();
    if (this.props.isLogout) {
      localStorage.clear();
      window.location.reload();
      // console.log("props", history,this.props.isLogout)
      history.push("/registration")
    }
  };

  componentDidMount() {
    if (
      window.location.pathname === '/'
    ) {
      document.getElementById('routerNav').style.display = 'none';
    } else {
      document.getElementById('routerNav').style.display = 'block';
    }

    var header = document.getElementById('navFirstMainDiv');
    var btns = header.getElementsByClassName('nav-link');
    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener('click', function () {
        var current = document.getElementsByClassName('active');
        if (current.length > 0) {
          current[0].className = current[0].className.replace(' active', '');
          this.className += ' active';
        }
      });
    }
  }

  render() {
    // console.log('componentDidMount', this.props);
    // console.log("isNavActive", this.props.isNavActive);

    return (
      <header className='navigationForRouter' id=''>
        <div id='routerNav'>
          <div className='navFirstMainDiv' id='navFirstMainDiv'>
            <div className='navFirst'>
              <div className='divLeft'>
                <div className='bs-example'>
                  {/* <div className="dropdown">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                      Eng
                    </a>
                    <div className="dropdown-menu">
                      <a href="#" className="dropdown-item">
                        Arabic
                      </a>
                      <a href="#" className="dropdown-item">
                        English
                      </a>
                    </div>
                  </div> */}
                </div>
                <a
                  href='#'
                  className='supportBtn'
                  onClick={() => (window.location = '/Converter')}
                >
                  <span className='span2' style={{color:"white"}}>Forex Calculator</span>
                </a>
              </div>
              <div className='divRight'>
                {this.state.name !== 'Sign In / Register' ? (
                  <a href='#' style={{ margin: '-8px 0px 0px 0px' }}>
                    <li
                      className='dropdown'
                      style={{
                        listStyleType: 'none',
                      }}
                    >
                      <Link
                        className='nav-link dropdown-toggle'
                        href='#'
                        id='navbarDropdownMenuLink'
                        role='button'
                        data-toggle='dropdown'
                        aria-haspopup='true'
                        aria-expanded='false'
                      >
                        <span
                          onClick={() =>
                            this.state.name == 'Sign In / Register'
                              ? (window.location = '/registration')
                              : ''
                          }
                        >
                          {this.state.name}
                          <img src={require('../img/home/profile2.png')} />
                        </span>
                      </Link>
                      <div
                        className='dropdown-menu'
                        aria-labelledby='navbarDropdownMenuLink'
                        id='navFirstMainDiv'
                        style={{
                          zIndex: '10',
                          transform: 'unset',
                        }}
                      >
                        <Link
                          id='dashboard-button'
                          to='/DashboardLayout'
                          className='dropdown-item'
                          style={{
                            color: '#000000',
                            padding: '0px 1.5rem',
                            zIndex: '10',
                          }}
                        >
                          Dashboard
                        </Link>
                        <Link
                          id='changepassword-button'
                          to='/changepassword'
                          className='dropdown-item'
                          style={{
                            color: '#000000',
                            padding: '0px 1.5rem',
                            zIndex: '10',
                          }}
                        >
                          Change Password
                        </Link>
                        <Link
                          to={
                            {
                              // pathname: "/Knowledgebase",
                            }
                          }
                          onClick={() => this.logout()}
                          className='dropdown-item'
                          style={{
                            color: '#000000',
                            padding: '0px 1.5rem',
                            zIndex: '10',
                          }}
                        >
                          Log Out
                        </Link>
                      </div>
                    </li>
                  </a>
                ) : (
                  <a href='#'>
                    <span
                      onClick={() =>
                        this.state.name == 'Sign In / Register'
                          ? (window.location = '/registration')
                          : ''
                      }
                    >
                     {this.state.name}
                      <img src={require('../img/home/profile2.png')} />
                    </span>
                  </a>
                )}
              </div>
            </div>
          </div>
          <div className='navSecond' id='navFirstMainDiv'>
            <nav
              className='navbar navbar-expand-lg navbar-light'
              id='navFirstMainDiv'
            >
              <Link 
              onClick={() =>
                {
                  this.props.SetIsNavActive("/")
                }}
               className='navbar-brand' to='/'
               >
                <img src={require('../img/logo.png')} />
              </Link>
              <button
                className='navbar-toggler'
                type='button'
                data-toggle='collapse'
                data-target='#navbarNavDropdown'
                aria-controls='navbarNavDropdown'
                aria-expanded='false'
                aria-label='Toggle navigation'
              >
                <span className='navbar-toggler-icon'></span>
              </button>
              <div
                className='collapse navbar-collapse justify-content-end'
                id='navbarNavDropdown'
              >
                <ul className='navbar-nav' id='navFirstMainDiv'>
                  {/* navFirstMainDiv navFirst navSecond divRed */}

                  {this.state.navOptions.map((value, index) => {
                    // console.log('this.state.navOptions', this.state.navOptions);
                    if (!value.dropdown) {
                      return (
                        <li>
                          <Link
                            to={value.navigateTo}
                            className={
                              this.state.borderBottom === value.navigateTo
                                ? 'nav-link red-border-bottom'
                                : 'nav-link'
                            }
                            onClick={() =>
                              {
                                this.props.SetIsNavActive(value.navigateTo)
                              // this.setState({
                              //   borderBottom: value.navigateTo,
                              // })
                            }
                            }
                          >
                            {value.name}
                          </Link>
                        </li>
                      );
                    } else {
                      return (
                        <li className='dropdown'>
                          <Link
                            className={
                              this.state.borderBottom == value.navigateTo
                                ? 'nav-link red-border-bottom'
                                : 'nav-link'
                            }
                            href='#'
                            id='navbarDropdownMenuLink'
                            role='button'
                            data-toggle='dropdown'
                            aria-haspopup='true'
                            aria-expanded='false'
                          >
                            {value.name}
                          </Link>
                          <div
                            className='dropdown-menu'
                            aria-labelledby='navbarDropdownMenuLink'
                            id='navFirstMainDiv'
                          >
                            {value.subLinks.map((val, ind) => {
                              // console.log('val-----', val.navigateTo)
                              return (
                                <Link
                                  to={{
                                    pathname: val.navigateTo,
                                    state: { selected: ind + 1 },
                                    hash:val.hashLink
                                  }}
                                  className='dropdown-item'
                                  onClick={() =>
                                    {
                                      this.props.SetIsNavActive(value.navigateTo)
                                    // this.setState({
                                    //   borderBottom: value.navigateTo,
                                    // })
                                  }
                                  }
                                >
                                  {val.name}
                                </Link>
                              );
                            })}
                          </div>
                        </li>
                      );
                    }
                  })}

                  {/* 
                  <li className="dropdown">
                    <Link
                      className="nav-link dropdown-toggle"
                      href="#"
                      id="navbarDropdownMenuLink"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      Learn
                    </Link>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="navbarDropdownMenuLink"
                      id="navFirstMainDiv"
                    >
                      <Link
                        to={{
                          pathname: "/Knowledgebase",
                          state: { selected: 1 },
                        }}
                        className="dropdown-item"
                      >
                        News
                      </Link>
                      <Link
                        to={{
                          pathname: "/Knowledgebase",
                          state: { selected: 2 },
                        }}
                        className="dropdown-item"
                      >
                        Articles
                      </Link>
                      <Link
                        to={{
                          pathname: "/Knowledgebase",
                          state: { selected: 3 },
                        }}
                        className="dropdown-item"
                      >
                        Blogs
                      </Link>
                    </div>
                  </li>




                  <li className="dropdown" id="navFirstMainDiv">
                    <Link
                      className="nav-link dropdown-toggle"
                      href="#"
                      id="navbarDropdownMenuLink"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      More
                    </Link>
                    <div
                      className="dropdown-menu"
                      aria-labelledby="navbarDropdownMenuLink"
                    >
                      <Link to={"/AboutUs"} className="dropdown-item">
                        About Us
                      </Link>
                      <Link to={"/ContactUs"} className="dropdown-item">
                        Contact Us
                      </Link>
                      <Link to={"/Converter"} className="dropdown-item">
                        Calculator
                      </Link>
                    </div>
                  </li> */}
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </header>
    );
  }
  componentDidUpdate(prevsProps, prevsState) {
    if (prevsProps.isNavActive !== this.props.isNavActive) {
        this.setState({
          borderBottom: this.props.isNavActive
        })
    }
  }
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.Logout.isLoading,
    isLogout: state.Logout.Logout,
    isNavActive: state.SelectedNav.isNavActive
  };
};

const mapDispatchToProps = (dispatch) => ({
  login: (obj) => dispatch(Login.Login(obj)),
  logout: () => dispatch(Logout.Logout()),
  SetIsNavActive: (data) => dispatch(SetIsNavActive(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
