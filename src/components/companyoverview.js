import React from "react";
import "../css/bootstrap.css";
import "../css/style-our-network.css";
import RedForm from "./RedForm";
import Footer2 from "./footer";
import OverviewTable from "./OverviewTable";
import Overviewsmalltable from "./Overviewsmalltable";
import { connect } from "react-redux";
import { Comparisons } from "../Store/actions/index";
import { bindActionCreators } from "redux";

class CompanyOverview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      rate: {
        rate1: "R.s 150",
        rate2: "R.s 150",
        rate3: "R.s 150",
      },
      transferfee: {
        transferfee1: "AED 5",
        transferfee2: "AED 5",
        transferfee3: "AED 5",
      },
      transfertype: {
        transfertype1: "International Bank Transfer",
        transfertype2: "International Bank Transfer",
        transfertype3: "International Bank Transfer",
      },
      contact: {
        contact1: "+111 111 111",
        contact2: "+111 111 111",
        contact3: "+111 111 111",
      },
      companytype: {
        companytype1: "Remmitance Provider",
        companytype2: "Remmitance Provider",
        companytype3: "Remmitance Provider",
      },
      mobileapp: {
        mobileapp1: "Mobile app",
        mobileapp2: "Mobile app",
        mobileapp3: "Mobile app",
      },
      keyfeature: {
        keyfeature1: "Mobile app",
        keyfeature2: "Mobile app",
        keyfeature3: "Mobile app",
      },

      doc1: ["-Original Passport", "-NIC", "-Photographs", "-Legal Documents"],
      doc2: ["-Original Passport", "-NIC", "-Photographs", "-Legal Documents"],
      doc3: ["-Original Passport", "-NIC", "-Photographs", "-Legal Documents"],
    };
  }
  componentWillUnmount() {
    this.props.delTable();
  }
  componentDidMount() {}

  showRatings(data) {
    let array = [];
    for (let i = 1; i <= data; i++) {
      // console.log("datarating", i);

      array.push(<i className="fas fa-star red"></i>);
    }
    return array;
  }
  showGray(data) {
    let no = 5 - data;
    let gray = [];
    for (let i = 1; i <= no; i++) {
      // console.log("datarating", i);

      gray.push(<i className="fas fa-star gray"></i>);
    }
    return gray;
  }

  render() {
    // console.log("data kahan hai", this.props.table);
    const { table } = this.props;
    let t1 =
      table[0] && table[0].partner_details[0].rating
        ? table[0].partner_details[0].rating
        : 0;
    let t2 =
      table[1] && table[1].partner_details[0].rating
        ? table[1].partner_details[0].rating
        : 0;
    let t3 =
      table[2] && table[2].partner_details[0].rating
        ? table[2].partner_details[0].rating
        : 0;
    // console.log("table", table);

    // for (let i =1 ; i<=t1.length ; i++){
    //   <i className="fas fa-star red"></i>
    //   // <i className="fas fa-star red"></i>
    //   // <i className="fas fa-star red"></i>
    //   // <i className="fas fa-star gray"></i>
    //   // <i className="fas fa-star gray"></i>
    // }


    if(table.length === 0){
        window.location = '/comparison'
    }



    return (
      <div>
        <div className="background">
          <div className="container1">
            <div 
              className="back-arrow-button"
              onClick={() => this.props.history.push("/comparison")}
            >
              <i
                className="fa fa-arrow-left"
               
              ></i>
            </div>
          </div>

          <div className="over-view-container over-view">
            {/* <div className="container1">
              <RedForm />
            </div> */}

            <div className="container1 overview-rate-boxes">
              <div className="row">
                <div className="overview-column col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2"></div>

                {table.length >= 1 ? (
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                    <div className="column1">
                      <div className="img-div">
                        <div className="logoDiv">
                          <img
                            src={
                              table.length >= 1
                                ? table[0].partner_details[0].img
                                : ""
                            }
                          />
                        </div>

                        {/* <div className="review-num">
                          <div className="review2">REVIEWS</div>
                          <div className="three-twenty">
                            {table.length > 0 ? table[0].reviews.length : 0}
                          </div>
                        </div> */}
                      </div>
                      <h2 style={{ textTransform: "capitalize", fontSize:20, marginTop: 10 }}>
                        {table.length > 0 ? table[0].partner_name : ""}
                      </h2>
                      {/* {table.length > 0 ? table[0].partner_details[0].rating : ""} */}
                      {this.showRatings(t1)}
                      {this.showGray(t1)}
                      {/* <i className="fas fa-star red"></i>
                      <i className="fas fa-star red"></i>
                      <i className="fas fa-star red"></i>
                      <i className="fas fa-star gray"></i>
                      <i className="fas fa-star gray"></i> */}
                      <br />
                      <a
                        href={
                          table.length >= 1
                            ? table[0].partner_details[0].website_url
                            : ""
                        }
                        target="_blank"
                      >
                        <button className="overview-form-btn" name="action">
                          {table.length >= 1 ? <span>Go To</span> : ""}{" "}
                          {table.length >= 1 ? table[0].partner_name : ""}
                        </button>
                      </a>
                    </div>
                  </div>
                ) : (
                  ""
                )}

                {table.length >= 2 ? (
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                    <div className="column1  col-margin">
                      <div className="img-div">
                        <div className="logoDiv">
                          <img
                            src={
                              table.length >= 2
                                ? table[1].partner_details[0].img
                                : ""
                            }
                          />
                        </div>
                        {/* <div className="review-num">
                          <div className="review2">REVIEWS</div>
                          <div className="three-twenty">
                            {" "}
                            {table.length > 1 ? table[1].reviews.length : 0}
                          </div>
                        </div> */}
                      </div>
                      <h2 style={{ textTransform: "capitalize", fontSize:20, marginTop: 10 }}>
                        {table.length >= 2 ? table[1].partner_name : ""}
                      </h2>
                      {this.showRatings(t2)}
                      {this.showGray(t2)}
                      <br />
                      <a
                        href={
                          table.length >= 2
                            ? table[1].partner_details[0].website_url
                            : ""
                        }
                        target="_blank"
                      >
                        <button className="overview-form-btn" name="action">
                          {table.length >= 2 ? <span>Go To</span> : ""}{" "}
                          {table.length >= 2 ? table[1].partner_name : ""}
                        </button>
                      </a>
                    </div>
                  </div>
                ) : (
                  ""
                )}

                {table.length >= 3 ? (
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                    <div className="column1 col-margin2">
                      <div className="img-div">
                        <div className="logoDiv">
                          <img
                            src={
                              table.length >= 3
                                ? table[2].partner_details[0].img
                                : ""
                            }
                          />
                        </div>
                        {/* <div className="review-num">
                          <div className="review2">REVIEWS</div>
                          <div className="three-twenty">
                            {" "}
                            {table.length > 2 ? table[2].reviews.length : 0}
                          </div>
                        </div> */}
                      </div>
                      <div>
                        <h2 style={{ textTransform: "capitalize", fontSize:20, marginTop: 10 }}>
                          {table.length >= 3 ? table[2].partner_name : ""}
                        </h2>

                        {table.length >= 3 ? table[2].rating : ""}
                        {this.showRatings(t3)}
                        {this.showGray(t3)}
                        <br />
                        <a
                          href={
                            table.length >= 3
                              ? table[2].partner_details[0].website_url
                              : ""
                          }
                          target="_blank"
                        >
                          <button className="overview-form-btn" name="action">
                            {table.length >= 3 ? <span>Go To</span> : ""}{" "}
                            {table.length >= 3 ? table[2].partner_name : ""}
                          </button>
                        </a>
                      </div>
                    </div>
                  </div>
                ) : (
                  ""
                )}

                {/* <!--colunms end--> */}
              </div>

              <OverviewTable table={table} data={this.state} delTableIndex={this.props.delTableIndex} />
            </div>
            <Overviewsmalltable table={table} data={this.state} showRatings={this.showRatings} showGray={this.showGray} delTableIndex={this.props.delTableIndex}  />
          </div>


          <Footer2 history={this.props.history} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    table: state.Comparisons.table,
    comparse: state.Comparisons.comparisons,
  };
};

const mapDispatchToProps = (dispatch) => ({
  delTable: bindActionCreators(Comparisons.tableDel, dispatch),
  delTableIndex:bindActionCreators(Comparisons.delTableIndex, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(CompanyOverview);
