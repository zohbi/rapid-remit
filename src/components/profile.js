import React from "react";
import "../css/bootstrap.css";
import "../css/style-registration.css";
import "../css/font.css";
import "../css/loader.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { UpdateUser } from "../Store/actions";
import { bindActionCreators } from "redux";
import firebase from "firebase";
import { storage } from "../firebase";

// import login from "../Store/reducer/login";

// <-------------- Password Encode Decode --------------->

// import HTMLDecoderEncoder from "html-encoder-decoder";
import { Base64 } from "js-base64";
import Loader from "./loader";
import Toast from "./toast";
import validator from "validator";

var storageRef = firebase.storage().ref("/user");

// global variables
var error;
// ---

// -------------------------><-----------------------------

class Profile extends React.Component {
  constructor() {
    super();
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      streetAddress: "",
      address: "",
      city: "",
      country: "",
      password: "",
      confirmPassword: "",
      loginPassword: "",
      nameOrEmail: "",
      role: [],
      Business: false,
      Individual: false,
      img: "",
      img_url: "",
      // isLoadingLogin: falsse
    };
  }

  onChangeValue = (name, value) => {
    let stateName = name;
    this.setState({
      [stateName]: value,
    });
  };

  register = (e) => {
    e.preventDefault()
    let role = [];
    if (this.state.Individual) {
      role = [...role, "Individual"];
    }
    if (this.state.Business) {
      role = [...role, "Business"];
    }
    let data = {
      email: this.state.email,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      password: this.state.password,
      city: this.state.city,
      phone: parseInt(this.state.phone),
      country: this.state.country,
      streetAddress: this.state.streetAddress,
      address: this.state.address,
      role: role,
      image: this.state.img_url,
      // loading: this.state.signing,
    };
    // console.log("data", data);
    if (data.email == "") {
      this.setState({ error1: `${data.email} is not  valid email` });
    } else if (data.firstName == "") {
      this.setState({ error1: "please fill your First Name" });
    } else if (data.lastName == "") {
      this.setState({ error1: "please fill your Last Name" });
    } else if (data.city == "") {
      this.setState({ error1: "please add your city Name" });
    } else if (data.phone == "") {
      this.setState({ error1: "please add your Phone Number" });
    } else if (data.country == "") {
      this.setState({ error1: "please add your Country Name" });
    } else if (data.streetAddress == "") {
      this.setState({ error1: "please Add your State" });
    } else if (data.firstName == "") {
      this.setState({ error1: "please Add your Postal Code" });
    } else if (data.role.length == 0) {
      this.setState({ error1: "Role is missing" });
    } else {
      // console.log("all done");
      let loginData = {
        email: this.state.email,
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        city: this.state.city,
        phone: parseInt(this.state.phone),
        country: this.state.country,
        streetAddress: this.state.streetAddress,
        address: this.state.address,
        image: this.state.img_url,
      };
      // console.log("loginData", loginData);
      let id = JSON.parse(localStorage.getItem("UserData"))._id;
      // console.log("-id", id);
      this.props.update(id, loginData);
      //localStorage.setItem("password", loginData.password);
      this.props.history.push('/DashboardLayout')
    }
    //window.location = '/DashboardLayout'
  };

  onChangeValue = (name, value) => {
    let stateName = name;
    this.setState({
      [stateName]: value,
    });
  };

  //   componentWillReceiveProps(nextProps) {
  //     let { isLoadingLogin, isLoadingRegister, loginFailed } = nextProps;
  //     this.setState({
  //       isLoadingLogin: isLoadingLogin,
  //       isLoadingRegister: isLoadingRegister,
  //       loginFailed: loginFailed,
  //     });

  // if (this.props.loginFailed) {
  //   console.log("====================", "this.setState is working bro!");
  // }

  componentWillReceiveProps(nextProps) {
    // console.log("nextProps", nextProps.Login);
    let data = nextProps.Login;
    // console.log("data", data);
    let Individual = false;
    let Business = false;
    if (nextProps.Login.role.includes("Individual")) {
      // console.log("Individual start");
      Individual = true;
    }
    if (nextProps.Login.role.includes("Business")) {
      // console.log("Business start");
      Business = true;
    }
    // console.log("check", Individual, Business);
    this.setState({
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      phone: data.phone,
      streetAddress: data.streetAddress,
      address: data.address,
      city: data.city,
      country: data.country,
      img_url: data.image,
      Individual,
      Business,
    });
  }

  uploadImage(e) {
    //let imageFormObj = new FormData();
    // console.log("_FILES", e.target.files[0].name);
    let file = e.target.files[0];
    // console.log("target file", file);
    this.setState({ img: file.name });
    const uploadTask = storageRef.child(`${file.name}`).put(file);
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        // console.log("snapshot", snapshot);
        // Observe state change events such as progress, pause, and resume
      },
      (error) => {
        // Handle unsuccessful uploads
        // console.log("aaaaaa", error);
      },
      () => {
        // Do something once upload is complete
        // console.log("success");
        storageRef
          .child(this.state.img)
          .getDownloadURL()
          .then((fireBaseUrl) => {
            // console.log("fireBaseUrl", fireBaseUrl);
            //image_url = fireBaseUrl;
            this.setState({ img_url: fireBaseUrl });
          });
      }
    );
  }
  render() {
    let data = JSON.parse(localStorage.getItem("UserData"));
    // console.log("DATALOAGIN", this.state.Individual, this.state.Business);
    return (
      <div className="registration">
        <div className="container01">
          <div className="registrationDivMain">
            <div className="form">
              <div className="">
                <div
                  style={{ margin: 0 }}
                  className="back-arrow-button"
                  onClick={() => this.props.history.goBack()}
                >
                  <i
                    className="fa fa-arrow-left"

                  ></i>
                </div>
              </div>
              <form>
                <div className="row">
                  <h1>Profile</h1>
                </div>
                <input
                  type="file"
                  name="pic"
                  accept="image/*"
                  multiple={true}
                  onChange={(e) => {
                    this.uploadImage(e);
                  }}
                />

                <div>
                  {this.state.img_url ? <img src={this.state.img_url} /> : null}
                </div>
                <div className="form-group row">
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="firstName" className="col-form-label">
                        First Name
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="firstName"
                        value={this.state.firstName}
                        placeholder="First Name"
                        onChange={(e) =>
                          this.onChangeValue("firstName", e.target.value)
                        }
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="lastName" className="col-form-label">
                        Last Name
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="lastName"
                        placeholder="Last Name"
                        value={this.state.lastName}
                        onChange={(e) =>
                          this.onChangeValue("lastName", e.target.value)
                        }
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="email" className=" col-form-label">
                        Email
                      </label>
                      <input
                        type="email"
                        className="form-control"
                        id="email"
                        placeholder="info@gmail.com"
                        value={this.state.email}
                      // onChange={(e) =>
                      //   this.onChangeValue("email", e.target.value)
                      // }
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="phone" className=" col-form-label">
                        Phone No.
                      </label>
                      <input
                        type="number"
                        className="form-control"
                        id="phone"
                        value={this.state.phone}
                        placeholder="Phone No."
                        onChange={(e) =>
                          this.onChangeValue("phone", e.target.value)
                        }
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="country" className=" col-form-label">
                        Country
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="country"
                        value={this.state.country}
                        placeholder="UAE"
                        onChange={(e) =>
                          this.onChangeValue("country", e.target.value)
                        }
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="city" className=" col-form-label">
                        City
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="city"
                        value={this.state.city}
                        placeholder="Dubai"
                        onChange={(e) =>
                          this.onChangeValue("city", e.target.value)
                        }
                      />
                    </div>
                  </div>

                </div>
                <div className="form-group row">
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="streetAddress" className=" col-form-label">
                        State
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="streetAddress"
                        value={this.state.streetAddress}
                        placeholder="State"
                        onChange={(e) =>
                          this.onChangeValue("streetAddress", e.target.value)
                        }
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="address" className=" col-form-label">
                        Postal Code
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="address"
                        value={this.state.address}
                        placeholder="Postal Code"
                        onChange={(e) =>
                          this.onChangeValue("address", e.target.value)
                        }
                      />
                    </div>
                  </div>
                </div>

                {/* <div className="form-group row">
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="password" className=" col-form-label">
                        Password
                      </label>
                      <input
                        type="password"
                        className="form-control"
                        id="password"
                        placeholder="xxx xxx xxx"
                        onChange={(e) =>
                          this.onChangeValue("password", e.target.value)
                        }
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="confirmPassword" className=" col-form-label">
                        Confirm Password
                      </label>
                      <input
                        type="password"
                        className="form-control"
                        id="confirmPassword"
                        placeholder="xxx xxx xxx"
                        onChange={(e) =>
                          this.onChangeValue("confirmPassword", e.target.value)
                        }
                      />
                    </div>
                  </div>
                </div> */}
                <div className="form-group row">
                  <div className="col-sm-6 col-form-label">
                    <h4>User Roles</h4>
                    <div className="row">
                      <div className="form-check col-sm-4">
                        <input
                          // className="form-check-input"
                          style={{width:"auto"}}
                          type="checkbox"
                          id="gridCheck2"
                          checked={this.state.Individual}
                          //defaultChecked={this.state.Individual}
                          value={this.state.Individual}
                          onChange={() => {
                            this.setState({
                              Individual: !this.state.Individual,
                            });
                          }}
                        />
                        <label className="form-check-label" for="gridCheck2">
                          Individual{" "}
                        </label>
                      </div>
                      <div className="form-check col-sm-4">
                        <input
                          // className="form-check-input"
                          style={{width:"auto"}}
                          type="checkbox"
                          id="gridCheck1"
                          defaultChecked={this.state.Business}
                          //value={this.state.Business}
                          onChange={() => {
                            this.setState({ Business: !this.state.Business });
                          }}
                        />
                        <label className="form-check-label" for="gridCheck1">
                          Business{" "}
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6 d-flex justify-content-end">
                    <button
                      type="submit"
                      className="btn btn-primary"
                      onClick={(e) => this.register(e)}
                      style={{
                        backgroundColor: this.state.isLoadingLogin
                          ? "#F64846"
                          : "",
                      }}
                    >
                      {!this.state.isLoadingRegister ? (
                        "UPDATE"
                      ) : (
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "space-around",
                              alignItems: "center",
                            }}
                          >
                            UPDATE <Loader />
                          </div>
                        )}
                    </button>
                  </div>
                  {this.state.error1 ? (
                    <div className="alert alert-danger alert-dismissible fade show">
                      1. {this.state.error1}
                    </div>
                  ) : (
                      " "
                    )}
                </div>
                {/*<div className="form-group row">
                  <div className="col-sm-6 col-form-label">
                    <div className="row">
                      <div className="form-check col-sm-12">
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id="gridCheck4"
                        />
                        <label className="form-check-label" for="gridCheck4">
                          Remain logged in to website
                        </label>
                      </div>
                      <div className="col-sm-12 forget">
                        <Link to={"/Forget"} href="">
                          Forgot Password?
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <button
                      type="button"
                      className="btn btn-primary"
                      onClick={() => this.login()}
                      style={{
                        backgroundColor: this.state.isLoadingLogin
                          ? "#F64846"
                          : "",
                      }}
                    >
                      {!this.state.isLoadingLogin ? (
                        "Login"
                      ) : (
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-around",
                            alignItems: "center",
                          }}
                        >
                          Login <Loader />{" "}
                        </div>
                      )}
                    </button>
                  </div>
                  {this.state.error ? (
                    <div
                      className="alert alert-danger alert-dismissible fade show"
                      id="localErr"
                    >
                      1. {this.state.error}
                    </div>
                  ) : (
                    " "
                  )}
                  {error && error.length ? (
                    <div
                      className="alert alert-danger alert-dismissible fade show"
                      id="this.stateErr"
                    >
                      {error}
                    </div>
                  ) : (
                    " "
                  )}
                      </div> */}
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    // isLoading: state.Comparisons.isLoading,
    // part: state.Comparisons.part,
    Login: state.Login.Login,
  };
};

const mapDispatchToProps = (dispatch) => ({
  update: bindActionCreators(UpdateUser, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
