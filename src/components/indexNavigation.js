import React from 'react';
import bg from '../img/aboutUs/group.png';
import Aboutrow_1 from './Aboutrow_1';
import Zoom from 'react-reveal/Zoom';
import { Link } from 'react-router-dom';
import { Logout } from '../Store/actions';
import { connect } from 'react-redux';
import { Comparisons, SetIsNavActive, getHomebanner } from '../Store/actions';
import swal from 'sweetalert';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel } from 'react-responsive-carousel';
import Geocode from 'react-geocode';
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
} from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { Divider } from 'antd';

class IndexNavigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      countries: [
        { country: 'Pakistan', currency_code: 'PKR' },
        { country: 'India', currency_code: 'INR' },
        { country: 'China', currency_code: 'CNY' },
        { country: 'United Kingdom', currency_code: 'GBP' },
        { country: 'United States', currency_code: 'USD' },
        { country: 'Australia', currency_code: 'AUD' },
        { country: 'Philippines', currency_code: 'PHP' },
        { country: 'Egypt', currency_code: 'EGP' },
        { country: 'Srilanka', currency_code: 'LKR' },
        { country: 'Jordan', currency_code: 'JOD' },
        { country: 'Nepal', currency_code: 'NPR' },
      ],
      name: 'Sign In / Register',
      row_1: {
        heading: 'ABOUT RAPID REMIT',
      },
      row_2: {
        img1: require('../img/aboutUs/Group-24.png'),
        img2: require('../img/aboutUs/Group-22.png'),
        img3: require('../img/aboutUs/Group-23.png'),
        heading: 'About Us',
        heading2:
          'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.',
      },
      from: 'AED',
      to: '',
      amount: null,
      from_country: 'UAE',
      country_name: '',
      location: 'House 123',
      latitude: '',
      longitute: '',
      rates: [],
      currency: [
        'AUD',
        // 'CAD',
        'EGP',
        // 'EUR',
        'GBP',
        'INR',
        'JOD',
        'LKR',
        'NPR',
        'PHP',
        'PKR',
        'USD',
        'CNY',
      ],
      links: [
        {
          title: 'Comparison',
          link: '/comparison',
        },
        {
          title: 'Our Network',
          link: '/ournetwork',
        },
        {
          title: 'Special Offer',
          link: '/SpecialOffer',
        },
        {
          title: 'About Us',
          link: '/AboutUs',
        },
        {
          title: 'Learn',
          link: '#',
          dropdown: true,
          id: 'navbarDropdownMenuLink',
          links: [
            {
              title: 'News',
              link: '/knowledgeBase',
              params: { selected: 1 },
              hashLink:'#firstNews'
            },
            {
              title: 'Articles',
              link: '/knowledgeBase',
              params: { selected: 2 },
              hashLink:'#firstArticle'

            },
            {
              title: 'Blogs',
              link: '/knowledgeBase',
              params: { selected: 3 },
              hashLink:'#firstBlog'

            },
          ],
        },
        // {
        //   title: 'More',
        //   link: '#',
        //   dropdown: true,
        //   id: 'navbarDropdownMenuLink',
        //   links: [
        //     {
        //       title: 'About Us',
        //       link: '/AboutUs',
        //       params: { selected: 1 },
        //     },
        //     {
        //       title: 'Contact Us',
        //       link: '/contactus',
        //       params: { selected: 2 },
        //     },
        //     {
        //       title: 'Calculator',
        //       link: '/converter',
        //       params: { selected: 3 },
        //     },
        //   ],
        // },
      ],
      borderBottom: props.isNavActive
    };
  }

  logout = () => {
    this.props.logout();
    if (this.props.isLogout) {
      localStorage.clear();
      window.location.reload();
    }
  };
  componentWillMount() {
    this.props.currency();
  }

  // getAddress = () => {
  //   Geocode.setApiKey('AIzaSyDJybEMQu5_e8t8oc6WLmR1kUKiOaknMuo');
  //   Geocode.setLanguage('en');

  //   if ('geolocation' in navigator) {
  //     console.log('geolocation Available');
  //     navigator.geolocation.getCurrentPosition(function (position) {
  //       this.setState({
  //         latitude: position.coords.latitude,
  //         longitude: position.coords.longitude,
  //       });
  //       console.log('geolocation Latitude is :', position.coords.latitude);
  //       console.log('geolocation Longitude is :', position.coords.longitude);
  //       console.log('geolocation Longitude is :', position);
  //       // Geocode.fromLatLng(
  //       //   position.coords.latitude,
  //       //   position.coords.longitude
  //       // ).then(
  //       //   (response) => {
  //       //     const address = response.results[0].formatted_address;
  //       //     console.log('Address We found', address);
  //       //   },
  //       //   (error) => {
  //       //     console.error('Address We found ', error);
  //       //   }
  //       // );
  //     });
  //   } else {
  //     console.log('Not Available');
  //   }
  // };

  componentDidMount() {
    var latitude;
    var longitude;
    this.props.getRates();
    this.props.getHomebanner();
    if (localStorage.getItem('UserData')) {
      let data = JSON.parse(localStorage.getItem('UserData'));
      this.setState({
        name: data.firstName,
      });
    }
    if (!this.props.data) {
      // document.getElementById("data").style.display = "none";
    }
    // document.getElementById("bgNone").style.display = "none";

    if (this.props.aboutUs) {
      document.getElementById('bg').style.backgroundImage = 'none';
    }

    // if (window.location.pathname == '/AboutUs') {
    //   document.getElementById(
    //     'bg'
    //   ).style.backgroundImage = `${require('./up.png')} !important`;
    // }

    // console.log('CALLING ADDRESS');
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition((position) => {
        // console.log('Latitude is :', position.coords.latitude);
        // console.log('Longitude is :', position.coords.longitude);

        latitude = position.coords.latitude;
        longitude = position.coords.longitude;

        this.setState({
          latitude: latitude,
          longitude: longitude,
        });
      });
    }
  }

  handleChange = (e) => {
    if (e.target.name === 'country_name') {
      this.setState({
        to: e.target.value,
      });
    }

    if (e.target.name == 'amount') {
      if (parseFloat(e.target.value) > 0) {
        this.setState({ [e.target.name]: e.target.value });
      } else {
        this.setState({ [e.target.name]: '' });
      }
    } else {
      // console.log('target', e.target.name, e.target.value);
      this.setState({
        [e.target.name]: e.target.value,
      });
    }
  };

  send = (e, history) => {
    let data = {
      from: this.state.from,
      to: this.state.to,
      amount: this.state.amount,
      country_name: this.state.country_name,
      from_country: this.state.from_country,
      location: this.state.location,
    };
    // console.log('data', data);

    if (data.from == '') {
      swal({
        title: 'From currency is empty',
        icon: 'error',
        button: 'Ok',
      });
      // alert("from currency is empty");
    } else if (data.to == '') {
      swal({
        title: 'To currency is empty',
        icon: 'error',
        button: 'Ok',
      });
    } else if (data.amount == '') {
      swal({
        title: 'Amount is empty',
        icon: 'error',
        button: 'Ok',
      });
    } else if (data.country_name == '') {
      swal({
        title: 'To country name  is empty',
        icon: 'error',
        button: 'Ok',
      });
    } else if (data.from_country == '') {
      swal({
        title: 'From country name is empty',
        icon: 'error',
        button: 'Ok',
      });
    } else if (data.location == '') {
      swal({
        title: 'Location is empty',
        icon: 'error',
        button: 'Ok',
      });
    } else if (0 >= data.amount) {
      swal({
        title: 'Amount must be greater than zero',
        icon: 'error',
        button: 'Ok',
      });
    } else {
      // console.log('all done');
      let comparisonData = {
        from: this.state.from,
        to: this.state.to,
        amount: parseInt(this.state.amount),
        latitude: this.state.latitude,
        longitude: this.state.longitude,
      };
      // console.log('check', history);
      this.props.getData(comparisonData, history, 'inform');
    }
    e.preventDefault();
  };
  componentWillMount() {
    this.props.getRates();
  }
  // UNSAFE_componentWillReceiveProps(next) {
  //   console.log("componentWillReceiveProps", next.rates);
  //   if (next.rates) {
  //     console.log("rates", next.rates.shift());
  //     this.setState({ rates: next.rates.splice(0, 1) });
  //   }
  // }

  render() {
    // /console.log("render", this.props.rates[0], this.state.rates);
    let cData = this.props.currencyData;
    let {homebannerData} = this.state

    this.state.countries.sort(function(a, b){
      if(a.country < b.country) { return -1; }
      if(a.country > b.country) { return 1; }
      return 0;
  })
    // console.log("isNavActive", this.props.isNavActive);
    const rates = this.props.rates;
    // console.log('borderBottom', this.state.borderBottom);
    return (
      <div className='container-fluid div1Index' id='bg'>
        <header
          id='navigationIndex'
          className={
            window.location.pathname == '/AboutUs' ? 'about-us-header' : ''
          }
        >
          <div className='navFirst'>
            <div className='divLeft'>
              <div className='bs-example'>
                {/* <div className="dropdown">
                  <a
                    href="#"
                    className="dropdown-toggle"
                    data-toggle="dropdown"
                  >
                    Eng
                  </a>
                  <div className="dropdown-menu">
                    <a href="#" className="dropdown-item">
                      Arabic
                    </a>
                    <a href="#" className="dropdown-item">
                      English
                    </a>
                  </div>
                </div> */}
              </div>
              <a
                href='#'
                className='supportBtn'
                onClick={() => (window.location = '/Converter')}
              >
                <span className='span2' style={{color:"white"}}>Forex Calculator</span>
              </a>
            </div>
            <div className='divRight'>
              {this.state.name !== 'Sign In / Register' ? (
                <a href='#' style={{ margin: '-8px 0px 0px 0px' }}>
                  <li
                    className='dropdown'
                    style={{
                      listStyleType: 'none',
                    }}
                  >
                    <Link
                      className='nav-link dropdown-toggle'
                      href='#'
                      id='navbarDropdownMenuLink'
                      role='button'
                      data-toggle='dropdown'
                      aria-haspopup='true'
                      aria-expanded='false'
                    >
                      <span
                        onClick={() =>
                          this.state.name == 'Sign In / Register'
                            ? (window.location = '/registration')
                            : ''
                        }
                      >
                        {this.state.name}
                        <img src={require('../img/home/profile2.png')} />
                      </span>
                    </Link>
                    <div
                      className='dropdown-menu'
                      aria-labelledby='navbarDropdownMenuLink'
                      id='navFirstMainDiv'
                      style={{
                        margin: '40px 0px 0px 0px',
                        zIndex: '10',
                      }}
                    >
                      <Link
                        onClick={() => {
                          window.location = '/DashboardLayout';
                        }}
                        //to="/DashboardLayout"
                        id='dashboard-button'
                        className='dropdown-item'
                        style={{
                          color: '#000000',
                          padding: '0px 1.5rem',
                          zIndex: '10',
                        }}
                      >
                        Dashboard
                      </Link>
                      <Link
                          id='changepassword-button'
                          to='/changepassword'
                          className='dropdown-item'
                          style={{
                            color: '#000000',
                            padding: '0px 1.5rem',
                            zIndex: '10',
                          }}
                        >
                          Change Password
                        </Link>
                      <Link
                        to={
                          {
                            // pathname: "/Knowledgebase",
                          }
                        }
                        onClick={() => this.logout()}
                        className='dropdown-item'
                        style={{
                          color: '#000000',
                          padding: '0px 1.5rem',
                          zIndex: '10',
                        }}
                      >
                        Log Out
                      </Link>
                    </div>
                  </li>
                </a>
              ) : (
                <a href='#'>
                  <span
                    onClick={() =>
                      this.state.name == 'Sign In / Register'
                        ? (window.location = '/registration')
                        : ''
                    }
                  >
                   {this.state.name}
                    <img src={require('../img/home/profile2.png')} />
                  </span>
                </a>
              )}
            </div>
          </div>
          <div className='navSecond'>
            <nav className='navbar navbar-expand-lg navbar-light '>
              <Link
               onClick={() =>
                {
                  this.props.SetIsNavActive("/")
                }}
                to={"/"}  
                className='navbar-brand'
                // href=''
                // onClick={() => this.props.history.push('/')}
              >
                <img src={require('../img/home/logo.png')} />
              </Link>
              <button
                className='navbar-toggler'
                type='button'
                data-toggle='collapse'
                data-target='#navbarNavDropdown'
                aria-controls='navbarNavDropdown'
                aria-expanded='false'
                aria-label='Toggle navigation'
              >
                <span className='navbar-toggler-icon'></span>
              </button>
              <div
                className='collapse navbar-collapse justify-content-end'
                id='navbarNavDropdown'
              >
                <ul className='navbar-nav'>
                  {this.state.links.map((link, index) => {
                    if (!link.dropdown) {
                      return (
                        <li className='nav-item active'>
                          <Link
                            to={link.link}
                            className={
                              this.state.borderBottom === link.link
                                ? 'nav-link red-border-bottom'
                                : 'nav-link'
                              
                            }
                            onClick={() =>{
                              this.props.SetIsNavActive(link.link)
                              // this.setState({
                              //   borderBottom: link.link,
                              // })
                            }
                            }
                          >
                            {link.title}
                          </Link>
                          {/* <a
                            className='nav-link'
                            href=''
                            onClick={() => this.props.history.push(link.link)}
                          >
                            {link.title}
                          </a> */}
                        </li>
                      );
                    } else {
                      return (
                        <li className='nav-item dropdown'>
                          <a
                            className='nav-link dropdown-toggle'
                            href='#'
                            id='navbarDropdownMenuLink'
                            role='button'
                            data-toggle='dropdown'
                            aria-haspopup='true'
                            aria-expanded='false'
                          >
                            {link.title}
                          </a>
                          <div
                            className='dropdown-menu'
                            aria-labelledby='navbarDropdownMenuLink'
                          >
                            {link.links.map((child, index) => {
                              return (
                                <Link
                                  to={{
                                    pathname: child.link,
                                    state: child.params,
                                    hash:child.hashLink
                                  }}
                                  className='dropdown-item'
                                >
                                  {child.title}
                                </Link>
                              );
                            })}
                          </div>
                        </li>
                      );
                    }
                  })}
                </ul>
              </div>
            </nav>
          </div>
        </header>
        {!this.props.aboutUs ? (
          <div className='container2' id='data'>
            <div className='row headerHome'>
              <div className='col-xl-6 col-lg-6 col-md-6 col-sm-0 col-12'>
                <div className='divRed'>
                  <div className='divRedWhiteLine'></div>
                  <div className='divHeading'></div>
                  <Zoom Top>
                    {' '}
                    <h1 className='Lato-Black'>
                      {/* RAPID REMIT YOUR PERSONAL GUIDE TO REMITTANCE PUTTING YOU
                      IN CONTROL OF YOUR TRANSFER EXPERIENCE */}
                      {homebannerData && homebannerData.heading}
                      {/* Rapid Remit your personal guide to remittance putting you
                      in control of your transfer experience */}
                    </h1>
                  </Zoom>
                </div>
              </div>
              <div className='col-xl-4 col-lg-6 col-md-6 col-sm-8 col-12'>
                <div className='divWhite'>
                  <form action=''>
                    <div className='input-group'>
                      <div className='input-group-prepend'>
                        <span className='input-group-text'>From</span>
                      </div>
                      <input
                        className='custom-input'
                        id='inputGroupSelect01'
                        name='from_country'
                        value={this.state.from_country}
                        // style={{borderRight:"1px solid #727272"}}
                        disabled
                        onChange={(e) => this.handleChange(e)}
                      >
                        {/* <option value='UAE'>UAE</option> */}
                      </input>
                      <div style={{padding:"5px 0"}}>
                        <Divider style={{width:1,  backgroundColor:"#727272"}} type="vertical" />
                      </div>
                      <input
                        className='custom-input'
                        id='inputGroupSelect01'
                        name='from'
                        value={this.state.from}
                        onChange={(e) => this.handleChange(e)}
                        disabled
                      >
                        {/* <option value='AED'>AED</option> */}
                      </input>
                    </div>
                    <div className='input-group'>
                      <div className='input-group-prepend'>
                        <span className='input-group-text'>
                          To <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </span>
                      </div>
                      <select
                        className='custom-select'
                        id='inputGroupSelect01'
                        name='country_name'
                        value={this.state.country_name}
                        onChange={(e) => this.handleChange(e)}
                      >
                        <option>Select</option>
                        {this.state.countries.map((a, index) => (
                          <option value={a.currency_code}>{a.country}</option>
                        ))}
                      </select>
                      {/* <input
                        className='custom-input'
                        id='inputGroupSelect01'
                        name='to'
                        value={this.state.to}
                        onChange={(e) => this.handleChange(e)}
                        disabled
                      >
                      </input> */}
                      <select
                        className='custom-select'
                        id='inputGroupSelect01'
                        name='to'
                        value={this.state.to}
                        onChange={(e) => this.handleChange(e)}
                      >
                        <option selected value=''>
                          Select
                        </option>

                        {this.state.currency
                          ? this.state.currency.sort().map((data) => {
                              return (
                                <option
                                  selected={data + '' === this.state.to + ''}
                                  value={data}
                                >
                                  {data}
                                </option>
                              );
                            })
                          : null}
                      </select>
                    </div>
                    <div className='input-group'>
                      <div className='input-group-prepend'>
                        <span className='input-group-text' id='basic-addon1'>
                          Amount
                        </span>
                      </div>
                      <input
                        type='number'
                        className='form-control'
                        placeholder='1'
                        aria-label='Username'
                        aria-describedby='basic-addon1'
                        name='amount'
                        value={this.state.amount}
                        onChange={(e) => this.handleChange(e)}
                        min={0}
                      />
                    </div>
                    <button
                      style={
                        this.state.from !== '' &&
                        this.state.to !== '' &&
                        this.state.from_country.length > 0 &&
                        this.state.amount > 0 &&
                        this.state.country_name.length > 0 &&
                        this.state.location.length > 0
                          ? { backgroundColor: '#DB1027', color: '#ffffff' }
                          : { backgroundColor: 'gray' }
                      }
                      onClick={(e) => this.send(e, this.props.history)}
                    >
                      Find Best Rates
                    </button>
                  </form>
                </div>
              </div>
              <div className='col-xl-2 d-xl-block d-lg-none d-md-none col-sm-4 col-12'>
                {/* <div className="whiteRedA"> */}
                <div className='whiteRed vertical-carousel'>
                  {rates.length > 0 ? (
                    <>
                      <Carousel
                        showIndicators={false}
                        showStatus={false}
                        showArrows={false}
                        axis='vertical'
                        autoPlay='true'
                        interval={3000}
                        infiniteLoop={true}
                      >
                        {rates.map((rate, index) => {
                          // console.log('country', rates[1]);

                          return (
                            <div>
                              <div className='whiteRedA'>
                                <h3>AED : {rate.country_name}</h3>
                                <h4>
                                  {rate && rate.rate ? (
                                    rate.rate.toFixed(2)
                                  ) : (
                                    <span> Rs.41.92 </span>
                                  )}
                                </h4>
                                {/* <p className="pSmall1">First Time Users Only</p> */}
                                <div className='lineRed'></div>
                              </div>

                              <div className='whiteRedB'>
                                <h3>{rates[index].country_name} : AED </h3>
                                <h4>
                                  {rate && rate.rate ? (
                                    (1 / rate.rate).toFixed(2)
                                  ) : (
                                    <span> Rs.41.92 </span>
                                  )}
                                </h4>
                                {/* <p className="pSmall2">First Time Users Only</p> */}
                                <div className='lineWhite'></div>
                              </div>
                            </div>
                          );
                        })}
                      </Carousel>

                      {/* <Carousel
                    showIndicators={false}
                    showStatus={false}
                    axis="verticall"
                    autoPlay="true"
                  > */}
                      {/* {this.props.rates.map((rate) => {
                      return (
                        <div
                          className="whiteRedB"
                          style={{ padding: "0px" }}
                          style={{ border: "2px solid blue" }}
                        >
                          <h3>AED TO {rate.country_name}</h3>
                          <h4>
                            {PKR && PKR.rate ? <span>RS.</span> : ""}{" "}
                            {rate && rate.rate ? (
                              rate.rate.toFixed(2)
                            ) : (
                              <span> Rs.41.92 </span>
                            )}
                          </h4>
                          <p className="pSmall1">First Time Users Only</p>
                          <div className="lineRed"></div>
                        </div>
                      );
                    })}
                  </Carousel> */}

                      {/* <h3>AED TO PKR</h3>
                    <h4>
                      {PKR && PKR.rate ? <span>RS.</span> : ""}{" "}
                      {PKR && PKR.rate ? (
                        PKR.rate.toFixed(2)
                      ) : (
                        <span> Rs.41.92 </span>
                      )}
                    </h4>
                    <p className="pSmall1">First Time Users Only</p>
                    <div className="lineRed"></div> */}
                      {/* </div> */}
                      {/* <div className="whiteRedB">
                    <h3>AED TO USD</h3>
                    <h4>
                      {USD && USD.rate ? <span>RS.</span> : ""}{" "}
                      {USD && USD.rate ? (
                        USD.rate.toFixed(2)
                      ) : (
                        <span> Rs. 0.27 </span>
                      )}
                    </h4>
                    <p className="pSmall2">First Time Users Only</p>
                    <div className="lineWhite"></div>
                  </div> */}
                      {/* </div> */}
                    </>
                  ) : (
                    <div className='default-ticker-view'>
                      <p>
                        No <br /> Connection
                      </p>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className='container-fluid remittance'>
              <div className='container1'>
                <div className='row'>
                  <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                    <div className='dropdown1'>
                      <a href='#whyCHooseUs'>
                        <div className='img'>
                          <img src={require('../img/home/dropdown3.png')} />
                        </div>
                        <h6>Scroll Down</h6>
                      </a>
                      <div className='img'>
                        <img src={require('../img/home/verticalLine2.png')} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          // <Aboutrow_1 data={this.state.row_1} />
          ''
        )}
      </div>
    );
  }
  componentDidUpdate(prevsProps, prevsState) {
    // console.log("prevsProps", prevsProps.isNavActive, "this.props", this.props.isNavActive )
    if (prevsProps.isNavActive !== this.props.isNavActive) {
        this.setState({
          borderBottom: this.props.isNavActive
        })
    }
    if (prevsProps.homebanner !== this.props.homebanner) {
      this.setState({
        homebannerData: this.props.homebanner
      })
    }
  }
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.Logout.isLoading,
    isLogout: state.Logout.Logout,
    rates: state.Comparisons.rates,
    isLoading: state.Comparisons.isLoading,
    currencyData: state.Comparisons.currency,
    isNavActive: state.SelectedNav.isNavActive,
    homebanner: state.FAQS.homebanner
    
  };
};

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(Logout.Logout()),
  getData: (obj, history, inform) =>
    dispatch(Comparisons.Comparisons(obj, history, inform)),
  getRates: () => dispatch(Comparisons.rate()),
  currency: () => dispatch(Comparisons.currency()),
  SetIsNavActive: (data) => dispatch(SetIsNavActive(data)),
  getHomebanner: () => dispatch(getHomebanner()),
});

export default connect(mapStateToProps, mapDispatchToProps)(IndexNavigation);
