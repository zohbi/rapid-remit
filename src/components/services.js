import React from 'react';
import '../css/bootstrap.css';
import '../css/style-services.css'
import HeadingImgLine from './HeadingImgLine'
import Instructions from './Instructions'

export default class Service extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      
        HeadingImgLine : {
          heading : "Service",
          img: require('../img/services/servicesImg1.png')
        },
        allData: [{
          heading:"Instructions",
          img:require('../img/services/servicesImg2.png'),
          paragraph: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat, consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse.",
          paragraph1:"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat."
        },
        {
          heading:"Business Services",
          face: true,
          img:require('../img/services/servicesImg3.png'),
          paragraph: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat, consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse.",
          paragraph1:"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat."
        },{
          heading:"Partner Services",
          face: false,
          img:require('../img/services/servicesImg4.png'),
          paragraph: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat, consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse.",
          paragraph1:"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat."
        }
      ],
  
        Instructions:{
          heading:"Instructions",
          face: true,
          img:require('../img/Instruction.png'),
          paragraph: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat, consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse.",
          paragraph1:"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat."
      }
      }
  }


    render() {
        let {allData} = this.state;
        return(
            <div className="container-fluid service">
                <div className="container1">

                <HeadingImgLine data={this.state.HeadingImgLine} redlineclassName="serviceRedLine" />


                <div className="serviceContent">
                {
                    allData ? allData.map(value => {
                        return(
                            value.face !== true ? 
                            <Instructions changePage={true} img={value.img} heading={value.heading} paragraph={value.paragraph} paragraph1={value.paragraph1}  />
                            :
                            <Instructions changePage={true} face={true} img={value.img} heading={value.heading} paragraph={value.paragraph} paragraph1={value.paragraph1}  />
                        )
                        // :

                    }) : ''
                }
              {/* <Instructions changePage={true} data = {this.state.Instructions}  /> */}


                    {/* <div className="row">
                        <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                            <div className="picDiv1">
                                <div className="picDiva"></div>
                                <div className="picDivb">
                                    <img src={require("../img/service/serviceImg2.png")} alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="col-Text col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                            <h2>User Service</h2>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Mollitia, quo esse. Magnam nesciunt ab dolor
                            neque cum, corrupti minus rerum nihil voluptas odit perferendis vel. Modi facilis vero cum minima. Lorem
                            ipsum dolor sit, amet consectetur adipisicing elit. Repellendus dicta ipsum veniam repellat sint optio sit
                            atque alias impedit placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione.</p>
                            <p>Ipsum dolor sit, amet elit. Repellendus dicta ipsum veniam repellat sint optio sit atque alias impedit
                            placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione. Lorem ipsum dolor sit, amet
                            consectetur adipisicing elit. Repellendus dicta ipsum veniam repellat sint optio sit atque alias impedit
                            placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione. Lorem ipsum dolor sit, amet
                            consectetur adipisicing elit. Repellendus dicta ipsum veniam repellat sint optio sit atque alias impedit
                            placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione.</p>
                        </div>
                    </div>



                    <div className="row">
                        
                        <div className="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                            <h2>Business Service</h2>
                            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Mollitia, quo esse. Magnam nesciunt ab dolor
                            neque cum, corrupti minus rerum nihil voluptas odit perferendis vel. Modi facilis vero cum minima. Lorem
                            ipsum dolor sit, amet consectetur adipisicing elit. Repellendus dicta ipsum veniam repellat sint optio sit
                            atque alias impedit placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione.</p>
                            <p>Ipsum dolor sit, amet elit. Repellendus dicta ipsum veniam repellat sint optio sit atque alias impedit
                            placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione. Lorem ipsum dolor sit, amet
                            consectetur adipisicing elit. Repellendus dicta ipsum veniam repellat sint optio sit atque alias impedit
                            placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione. Lorem ipsum dolor sit, amet
                            consectetur adipisicing elit. Repellendus dicta ipsum veniam repellat sint optio sit atque alias impedit
                            placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione.</p>
                        </div>
                            <div className="row2col2 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                <div className="picDiv1">
                                    <div className="picDiva  "></div>
                                    <div className="picDivb">
                                        <img src={require("../img/service/serviceImg3.png")} alt="" />
                                    </div>
                                </div>
                            </div>
                    </div>




                    <div className="row">
                    <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                        <div className="picDiv1">
                        <div className="picDiva  "></div>
                        <div className="picDivb">
                            <img src={require("../img/service/serviceImg4.png")} alt="" />
                        </div>
                        </div>
                    </div>
                    <div className="col-Text col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                        <h2>Partner Service</h2>
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Mollitia, quo esse. Magnam nesciunt ab dolor
                        neque cum, corrupti minus rerum nihil voluptas odit perferendis vel. Modi facilis vero cum minima. Lorem
                        ipsum dolor sit, amet consectetur adipisicing elit. Repellendus dicta ipsum veniam repellat sint optio sit
                        atque alias impedit placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione.</p>
                        <p>Ipsum dolor sit, amet elit. Repellendus dicta ipsum veniam repellat sint optio sit atque alias impedit
                        placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione. Lorem ipsum dolor sit, amet
                        consectetur adipisicing elit. Repellendus dicta ipsum veniam repellat sint optio sit atque alias impedit
                        placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione. Lorem ipsum dolor sit, amet
                        consectetur adipisicing elit. Repellendus dicta ipsum veniam repellat sint optio sit atque alias impedit
                        placeat eum magnam qui officiis excepturi omnis, minus sed, aut ratione.</p>
                    </div>
                    </div> */}
                </div>
                </div>
                </div>
        
        )
    }
}