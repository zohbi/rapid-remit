import React from "react";

export default class CompanyOverviewText extends React.Component {
  render() {
    // console.log("data", this.props.data);
    return (
      <div className="row companyOverviewContentRow1">
        <div className="col-xl-3 col-lg-4 col-md-12 col-sm-12 d-sm-block d-none">
          <div className="picDivb">
            <img
              src={require("../img/CompanyOverview/CompanyOverviewImg1.png")}
              alt=""
            />
            <div className="CompanyOverviewRedLine"></div>
          </div>
        </div>

        <div className="col-Text col-xl-9 col-lg-8 col-md-12 col-sm-12 col-12">
          <h2>{this.props.data.partner_name}</h2>
          <p>{this.props.data.partner_details[0].about_exchange}</p>
        </div>
      </div>
    );
  }
}
