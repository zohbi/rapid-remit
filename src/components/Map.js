import React from "react";
import "../css/style-comprision.css";
import GoogleMapReact from "google-map-react";

const AnyReactComponent = ({ text }) => <div>{text}</div>;
export default class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      center: {
        lat: 59.955413,
        lng: 0.337844,
      },
      zoom: 11,
    };
  }

  render() {
    return (
      <div style={{ height: "100vh", width: "100%",marginTop:50 }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyDJybEMQu5_e8t8oc6WLmR1kUKiOaknMuo" }}
          defaultCenter={this.state.center}
          defaultZoom={this.state.zoom}
          
        >
          <AnyReactComponent lat={59.955413} lng={30.337844} text="My Marker" />
        </GoogleMapReact>
      </div>
      //  <div className="container1">
      //   <div className="row">
      //     <div className="locate col-xl-1 col-lg-1 col-md-12 col-sm-12 col-12">
      //       <h1>{this.props.data.heading}</h1>
      //     </div>
      //     <div className="col-xl-11 col-lg-11 col-md-12 col-sm-12 col-12">
      //       <div className="mapouter">
      //         <div className="gmap_canvas">
      //           <iframe
      //             width="600"
      //             height="500"
      //             id="gmap_canvas"
      //             src="https://maps.google.com/maps?q=burjul%20arab&t=&z=13&ie=UTF8&iwloc=&output=embed"
      //             frameborder="0"
      //             scrolling="no"
      //             marginheight="0"
      //             marginwidth="0"
      //           ></iframe>
      //           <a href="https://www.embedgooglemap.net/blog/divi-discount-code-elegant-themes-coupon/">
      //             embedgooglemap.net
      //           </a>
      //           {/* <img src={this.props.data.img} className="location" />
      //           <img src={this.props.data.img} className="location2" /> */}
      //         </div>
      //       </div>
      //     </div>
      //   </div>
      // </div>
    );
  }
}

// <div className="container-fluid Instructions">
//         <div
//           className="mapouter"
//           style={{
//             position: "relative",
//             textAlign: "right",
//             height: "500px",
//             width: "600px",
//           }}
//         >
//           <div
//             className="gmap_canvas"
//             style={{
//               overflow: "hidden",
//               background: "none!important",
//               height: "500px",
//               width: "600px",
//             }}
//           >
//             <iframe
//               width="600"
//               height="500"
//               id="gmap_canvas"
//               src="https://maps.google.com/maps?q=burjul%20arab&t=&z=13&ie=UTF8&iwloc=&output=embed"
//               frameborder="0"
//               scrolling="no"
//               marginheight="0"
//               marginwidth="0"
//             ></iframe>
//             <a href="https://www.embedgooglemap.net/blog/divi-discount-code-elegant-themes-coupon/">
//               embedgooglemap.net
//             </a>
//           </div>
