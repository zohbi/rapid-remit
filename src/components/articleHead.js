import React from 'react';
import moment from "moment"

export default class ArticleHead extends React.Component {
    
    render() {
        return (
            
            <div className="row articleRow1">
                    <div className="col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12">
                    <h1></h1>
                    <img src={this.props.data.bannerImg} alt="" />
                    <div className="articleRedLine"></div>
                    <h2>{this.props.data.bannerTitle}</h2>
                </div>
                <div className="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
                    <div className="News01">
                        <h4>ARTICLE</h4>
                        <h6>{this.props.data.articlesNumber}</h6>
                        <h5>{moment(this.props.data.createdDate).format("MMM D YYYY")}</h5>
                        {/* <p>{this.props.data.smallPara}</p> */}
                    </div>
                </div>
            </div>

        )
    }
}