import React from "react";
import "../css/bootstrap.css";
import "../css/style-registration.css";
import "../css/font.css";
import "../css/loader.css";
import { Link } from "react-router-dom";
// import login from "../Store/reducer/login";

// <-------------- Password Encode Decode --------------->

// import HTMLDecoderEncoder from "html-encoder-decoder";
import { Base64 } from "js-base64";
import Loader from "./loader";
import Toast from "./toast";
import validator from "validator";

// global variables
var error;
// ---

// -------------------------><-----------------------------

export default class Registration extends React.Component {
  constructor() {
    super();
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      streetAddress: "",
      address: "",
      city: "",
      country: "",
      password: "",
      confirmPassword: "",
      loginPassword: "",
      nameOrEmail: "",
      role: [],
      Business: false,
      Individual: false,
      // isLoadingLogin: falsse
      isCreateAccountActive:false
    };
  }

  onChangeValue = (name, value) => {
    let stateName = name;
    this.setState({
      [stateName]: value
    });
  };

  register = (e) => {
    e.preventDefault()
    let role = [];
    if (this.state.Individual) {
      role = [...role, "Individual"];
    }
    if (this.state.Business) {
      role = [...role, "Business"];
    }
    let data = {
      email: this.state.email,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      password: this.state.password,
      city: this.state.city,
      phone: parseInt(this.state.phone),
      country: this.state.country,
      conformPassword: this.state.confirmPassword,
      streetAddress: this.state.streetAddress,
      address: this.state.address,
      role: role
      // loading: this.state.signing,
    };
    if (!validator.isEmail(data.email)) {
      this.setState({ error1: `${data.email} is not a valid email` });
    } else if (data.firstName == "") {
      this.setState({ error1: "please fill your First Name" });
    } else if (data.lastName == "") {
      this.setState({ error1: "please fill your Last Name" });
    } else if (data.password == "") {
      this.setState({ error1: "please fill your password" });
    } else if (data.city == "") {
      this.setState({ error1: "please add your city Name" });
    } else if (data.phone == "") {
      this.setState({ error1: "please add your Phone Number" });
    } else if (data.conformPassword !== data.password) {
      this.setState({ error1: "password not match" });
    } else if (data.country == "") {
      this.setState({ error1: "please add your Country Name" });
    } else if (data.password.length < 8) {
      this.setState({ error1: "your password must be 8 character long" });
    } else if (data.streetAddress == "") {
      this.setState({ error1: "please Add your State" });
    } else if (data.firstName == "") {
      this.setState({ error1: "please Add your Postal Code" });
    } else if (data.password !== data.conformPassword) {
      this.setState({ error1: "Your password in not match" });
    } else if (data.role.length == 0) {
      this.setState({ error1: "Role is missing" });
    } else {
      // console.log("all done");
      let loginData = {
        email: this.state.email,
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        password: this.state.password,
        city: this.state.city,
        phone: parseInt(this.state.phone),
        country: this.state.country,
        streetAddress: this.state.streetAddress,
        address: this.state.address
      };
      localStorage.setItem("UserData", JSON.stringify(loginData));
      this.props.postData(data);
      localStorage.setItem("password", loginData.password);
    }
  };

  onChangeValue = (name, value) => {
    let stateName = name;
    this.setState({
      [stateName]: value
    });
  };

  login = (e) => {
    e.preventDefault()
    const rules = {
      email: "required!email",
      password: "required|string|min:8|confirmed"
    };
    let loginData = {
      email: this.state.nameOrEmail,
      password: this.state.loginPassword
    };

    if (loginData.email == "" && loginData.password == "") {
      this.setState({
        error: "Please fill your inputs"
      });
    } else if (!validator.isEmail(loginData.email)) {
      this.setState({ error: `${loginData.email} is not a valid email` });
      if (document.getElementById("dataErr")) {
        document.getElementById("dataErr").style.display = "none";
      }
    } else if (loginData.password == "") {
      this.setState({ error: "please add your password" });

      if (document.getElementById("dataErr")) {
        document.getElementById("dataErr").style.display = "none";
      }
    } else if (loginData.password.length < 8) {
      this.setState({ error: "your password must be 8 character long" });
      if (document.getElementById("dataErr")) {
        document.getElementById("dataErr").style.display = "none";
      }
    } else {
      this.props.login(loginData);
      if (document.getElementById("dataErr")) {
        document.getElementById("dataErr").style.display = "block";
      }
      let encoded = Base64.encode(loginData.password);
      localStorage.setItem("PassSec", encoded);

      this.setState({
        error: undefined,
        success: "mil gyi akhir"
      });
    }
  };

  componentWillReceiveProps(nextProps) {
    let { isLoadingLogin, isLoadingRegister, loginFailed } = nextProps;
    this.setState({
      isLoadingLogin: isLoadingLogin,
      isLoadingRegister: isLoadingRegister,
      loginFailed: loginFailed
    });

    // if (this.props.loginFailed) {
    //   console.log("====================", "this.setState is working bro!");
    // }
    // console.log(
    //   isLoadingLogin,
    //   isLoadingRegister,
    //   loginFailed,
    //   "here is login failed"
    // );
  }

  componentDidMount() {
    // console.log(this.props, "ye state hy bhai check kar");

    if (localStorage.getItem("UserData")) {
      this.props.history.push("/");
    }
  }

  componentWillUpdate(props) {
    // console.log(props, "state check it componentwillupdate");
    if (props.loginFailed) { 
      error = props.loginFailed || "Something went wrong!";
    }
  }

  onToggleCreateAccount = () => {
    this.setState({
      isCreateAccountActive: !this.state.isCreateAccountActive
    })
  }
  render() {
    // console.log(this.props);
    const {isCreateAccountActive} =this.state
    return (
      <div className="registration">
        <div className="container01">
          <div className="registrationDivMain">
            <div className="form">
              <form>
              {isCreateAccountActive ? <React.Fragment> 
                <div className="">
                  <div 
                    style={{margin:0}}
                    className="back-arrow-button"
                    onClick={() => this.onToggleCreateAccount()}
                  >
                    <i
                      className="fa fa-arrow-left"
                    
                    ></i>
                  </div>
                </div>
                <div className="row">
                  <h1>REGISTRATION</h1>
                </div>
                <div className="form-group row">
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="firstName" className="col-form-label">
                        First Name
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="firstName"
                        placeholder="First Name"
                        onChange={e =>
                          this.onChangeValue("firstName", e.target.value)
                        }
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="lastName" className="col-form-label">
                        Last Name
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="lastName"
                        placeholder="Last Name"
                        onChange={e =>
                          this.onChangeValue("lastName", e.target.value)
                        }
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="email" className=" col-form-label">
                        Email
                      </label>
                      <input
                        type="email"
                        className="form-control"
                        id="email"
                        placeholder="info@gmail.com"
                        onChange={e =>
                          this.onChangeValue("email", e.target.value)
                        }
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="phone" className=" col-form-label">
                        Phone No.
                      </label>
                      <input
                        type="number"
                        className="form-control"
                        id="phone"
                        placeholder="Phone No."
                        onChange={e =>
                          this.onChangeValue("phone", e.target.value)
                        }
                      />
                    </div>
                  </div>
                </div>
                
                <div className="form-group row">
                <div className="col-sm-6">
                    <div className="fields">
                      <label for="country" className=" col-form-label">
                        Country
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="country"
                        placeholder="UAE"
                        onChange={e =>
                          this.onChangeValue("country", e.target.value)
                        }
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="city" className=" col-form-label">
                        City
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="city"
                        placeholder="Dubai"
                        onChange={e =>
                          this.onChangeValue("city", e.target.value)
                        }
                      />
                    </div>
                  </div>
                  
                </div>
                <div className="form-group row">
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="streetAddress" className=" col-form-label">
                        State
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="streetAddress"
                        placeholder="State"
                        onChange={e =>
                          this.onChangeValue("streetAddress", e.target.value)
                        }
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="address" className=" col-form-label">
                        Postal Code
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="address"
                        placeholder="Postal Code"
                        onChange={e =>
                          this.onChangeValue("address", e.target.value)
                        }
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="password" className=" col-form-label">
                        Password
                      </label>
                      <input
                        type="password"
                        className="form-control"
                        id="password"
                        placeholder="XXX XXX XXX"
                        onChange={e =>
                          this.onChangeValue("password", e.target.value)
                        }
                      />
                    </div>
                  </div>
                  <div className="col-sm-6">
                    <div className="fields">
                      <label for="confirmPassword" className=" col-form-label">
                        Confirm Password
                      </label>
                      <input
                        type="password"
                        className="form-control"
                        id="confirmPassword"
                        placeholder="XXX XXX XXX"
                        onChange={e =>
                          this.onChangeValue("confirmPassword", e.target.value)
                        }
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                  <div className="col-sm-6 col-form-label">
                    <h4>Select Your Role</h4>
                    <div className="row">
                      <div className="form-check col-sm-12 col-md-6 col-lg-4">
                        <input
                          // className="form-check-input"
                          type="checkbox"
                          style={{width:"auto"}}
                          id="gridCheck2"
                          value={this.state.Individual}
                          onChange={() => {
                            this.setState({
                              Individual: !this.state.Individual
                            });
                          }}
                        />
                        <label className="form-check-label" for="gridCheck2">
                          Individual{" "}
                        </label>
                      </div>
                      <div className="form-check col-sm-12 col-md-6 col-lg-4 ">
                        <input
                          // className="form-check-input"
                          type="checkbox"
                          style={{width:"auto"}}
                          id="gridCheck1"
                          value={this.state.Business}
                          onChange={() => {
                            this.setState({ Business: !this.state.Business });
                          }}
                        />
                        <label className="form-check-label" for="gridCheck1">
                          Business{" "}
                        </label>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-6 d-flex justify-content-end">
                    <button
                      type="submit"
                      className="btn btn-primary"
                      onClick={(e) => this.register(e)}
                      style={{
                        backgroundColor: this.state.isLoadingLogin
                          ? "#F64846"
                          : ""
                      }}
                    >
                      {!this.state.isLoadingRegister ? (
                        "REGISTER"
                      ) : (
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-around",
                            alignItems: "center"
                            
                          }}
                        >
                          REGISTER <Loader />
                        </div>
                      )}
                    </button>
                  </div>
                  {this.state.error1 ? (
                    <div className="alert alert-danger alert-dismissible fade show">
                      1. {this.state.error1}
                    </div>
                  ) : (
                    " "
                  )}
                </div> </React.Fragment>
              :  
               <React.Fragment> <div className="row">
                  <h1 className="h1Account">ACCOUNT</h1>
                </div>
                <div className="form-group row">
                  <div className="col-sm-12">
                    <div className="fields">
                      <label for="firstName" className="col-form-label">
                        Email Address
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="email"
                        onChange={e =>
                          this.onChangeValue("nameOrEmail", e.target.value)
                        }
                        placeholder="info@gmail.com"
                      />
                    </div>
                  </div>
                  <div className="col-sm-12">
                    <div className="fields">
                      <label for="password" className=" col-form-label">
                        Password
                      </label>
                      <input
                        type="password"
                        onChange={e =>
                          this.onChangeValue("loginPassword", e.target.value)
                        }
                        className="form-control"
                        id="password"
                        placeholder="XXX XXX XXX"
                      />
                    </div>
                  </div>
                </div>
                <div className="form-group row">
                <div className="col-sm-12 col-md-12 col-form-label">
                    <div className="row">
                      <div className="form-check col-sm-12 col-md-7">
                      <div className="row no-gutters align-items-center">
                        <input
                          className=""
                          style={{width:"auto"}}
                          type="checkbox"
                          id="gridCheck4"
                        />
                        <label  style={{width:"auto", marginLeft:10, marginBottom:0, fontSize:12}} className="" for="gridCheck4">
                          Remain logged in to website
                        </label>
                        </div>
                      </div>
                      <div className="col-sm-12 col-md-5">
                      <div className="row no-gutters justify-content-end align-items-center">
                        <Link to={"/Forget"} href="">
                          <span style={{color:"#e8051c", fontWeight:700, textDecoration:"underline", textDecorationColor:'#e8051c', cursor:"pointer", fontSize:12}}>Forgot Password?</span>
                        </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                <div className="form-group row">
                  
                  <div className="col-sm-12 d-flex justify-content-end">
                    <button
                      type="submit"
                      className="btn btn-primary"
                      onClick={(e) => this.login(e)}
                      style={{
                        backgroundColor: this.state.isLoadingLogin
                          ? "#F64846"
                          : ""
                      }}
                    >
                      {!this.state.isLoadingLogin ? (
                        "LOGIN"
                      ) : (
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-around",
                            alignItems: "center"
                          }}
                        >
                          LOGIN <Loader />{" "}
                        </div>
                      )}
                    </button>
                  </div>
                  {this.state.error ? (
                    <div
                      className="alert alert-danger alert-dismissible fade show"
                      id="localErr"
                    >
                      1. {this.state.error}
                    </div>
                  ) : (
                    " "
                  )}
                  {error && error.length ? (
                    <div
                      className="alert alert-danger alert-dismissible fade show"
                      id="dataErr"
                    >
                      {error}
                    </div>
                  ) : (
                    " "
                  )}
                </div>
                <div className="row" style={{justifyContent:"flex-end", marginTop: 50, padding:"0px 16px"}}>
                  <h6 ><span style={{color:"#929292", fontWeight:700}}>Don't have an account?</span> <span onClick={() => this.onToggleCreateAccount()} style={{color:"#e8051c", fontWeight:700, textDecoration:"underline", textDecorationColor:'#e8051c', cursor:"pointer"}}>Create Account.</span></h6>
                </div>
                </React.Fragment>
                }
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
