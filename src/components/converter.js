import React, { Fragment } from "react";
import "../css/bootstrap.css";
import "../css/cal.css";
import CountryCard from "./countryCard";
import Footer from "../components/footer";
// import { BulletList } from "react-content-loader";
import ContentLoader from "react-content-loader";
import Loader from "./loader";
import Loader1 from "./loader1";
import date from "date-and-time";
import calc_instnace from "../instance/calc_instance";
// const MyLoader = () => <ContentLoader />;
import swal from "sweetalert";


export default class Converter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      arr: [
        {
          img: require("../img/calculator/british.png"),
          heading3: "GBP",
          p1: "British Pound",
          heading4: "£ 799.0711",
          p2: "1GBP = 1.2515 USD",
        },
        {
          img: require("../img/calculator/AUD.png"),
          heading3: "AUS",
          p1: "Australian Dollar",
          heading4: "$ 799.0711",
          p2: "1GBP = 1.2515 USD",
        },
        {
          img: require("../img/calculator/singapore.png"),
          heading3: "SGD",
          p1: "Singapore Dollar",
          heading4: "$ 888.0711",
          p2: "1SGD = 1.1260 USD",
        },
        {
          img: require("../img/calculator/south-african.png"),
          heading3: "ZAR",
          p1: "South African Rand",
          heading4: "$ 888.0711",
          p2: "1SGD = 1.1260 USD",
        },
        {
          img: require("../img/calculator/ind.png"),
          heading3: "INR",
          p1: "Indian Rupee",
          heading4: "Rs 159,538.071",
          p2: "1GBP = 0.0063 USD",
        },

        {
          img: require("../img/calculator/russian.png"),
          heading3: "RUB",
          p1: "Russian Rouble",
          heading4: "P 159,538.071",
          p2: "1GBP = 0.0063 USD",
        },
        {
          img: require("../img/calculator/malasia.png"),
          heading3: "MYR",
          p1: "Malaysia Ringgit",
          heading4: "3672.0711ع .د ",
          p2: "1GBP = 1.0.2723 USD",
        },
        {
          img: require("../img/calculator/kuwaiti.png"),
          heading3: "KWD",
          p1: "Kuwaiti Dinar",
          heading4: "3672.0711ع .د ",
          p2: "1GBP = 1.0.2723 USD",
        },
      ],
      from: "USD",
      to: "",
      amount: "",
    };

    calc_instnace.calculator_instance = this;
  }

  converted = () => {
    let to = document.getElementById("to").value;
    let from = document.getElementById("from").value;
    let { amount } = this.state;

    if (amount === "" || parseFloat(amount) < 0) {
      swal({
        title: "Amount should be greater than 0",
        icon: "error",
        button: "Ok",
      });
      return;
    }

    this.setState({
      from,
      to,
      searched: true,
    });
    if (this.state.amount == "") {
      // console.log("please fill your amount");
    } else {
      this.props.convert({ from, to, amount: this.state.amount });
    }

    // console.log(to, from, amount, "check it");
  };

  // componentWillReceiveProps(nextProps) {

  // }

  componentDidMount() {
    this.props.getData(this.state.from);
    let to = document.getElementById("to").value;
    this.setState({
      to: to,
    });
    if (
      window.location.pathname == "/" ||
      window.location.pathname == "/AboutUs"
    ) {
      document.getElementById("routerNav").style.display = "none";
    } else {
      document.getElementById("routerNav").style.display = "block";
    }

    this.fixFlag();
  }
  fixFlag = () => {
    let from_flag, to_flag;
    if (this.state.data && this.state.data.length > 0) {
      from_flag = this.state.data.filter((f) => f.currency_code == "USD")[0];

      to_flag = this.state.data.filter((f) => f.currency_code == "CAD")[0];
      document.getElementById(
        "to"
      ).style.backgroundImage = `url(${from_flag.country_flag})`;

      document.getElementById(
        "from"
      ).style.backgroundImage = `url(${to_flag.country_flag})`;
    }
  };
  shiftToInputText = () => {
    document.getElementById("ammount").focus();
  };
  swapOptions = () => {
    let to = document.getElementById("to").value;
    let from = document.getElementById("from").value;
    let bgTo = document.getElementById("to").style.backgroundImage;
    let bgFrom = document.getElementById("from").style.backgroundImage;
    document.getElementById("to").style.backgroundImage = bgFrom;
    document.getElementById("from").style.backgroundImage = bgTo;
    document.getElementById("to").value = from;
    document.getElementById("from").value = to;
  };

  changeImageAndState = (which, what, flag) => {
    alert("deon");
    // console.log("console");
    // console.log("which", which, what, flag);
    this.setState({ [which]: what });
    document.getElementById(which).style.backgroundImage = `url(${flag})`;
  };

  showConversion() {
    let { data, from, to } = this.state;
    let from_currency, to_currency;
    // console.log("abc", data, from, to);
    if (data && data.length > 0 && from && to) {
      from_currency = data.filter((f) => f.currency_code == from)[0]
        .currency_name;
      to_currency = data.filter((f) => f.currency_code == to)[0].currency_name;
      // console.log(
      //   "currencies",
      //   from_currency.currency_name,
      //   to_currency.currency_name
      // );
    }
    return `${from_currency} to ${to_currency} Conversion`;
  }
  componentWillMount() {
    // this.props.convert({
    //   form: this.state.from,
    //   to: this.state.to,
    //   amount: this.state.amount,
    // });
  }

  send = (e, history) => {
    let to = document.getElementById("to").value;
    let from = document.getElementById("from").value;
    let { amount } = this.state;
    let data = {
      from: from,
      to: to,
      amount: amount,
      country_name: this.state.country_name,
      from_country: this.state.from_country,
      location: this.state.location,
    };
    // console.log("data", data);

    if (data.from == "") {
      swal({
        title: "From currency is empty",
        icon: "error",
        button: "Ok",
      });
      // alert("from currency is empty");
    } else if (data.to == "") {
      swal({
        title: "To currency is empty",
        icon: "error",
        button: "Ok",
      });
    } else if (data.amount == "") {
      swal({
        title: "Amount is empty",
        icon: "error",
        button: "Ok",
      });
    } else if (data.country_name == "") {
      swal({
        title: "To country name  is empty",
        icon: "error",
        button: "Ok",
      });
    } else if (data.from_country == "") {
      swal({
        title: "From country name is empty",
        icon: "error",
        button: "Ok",
      });
    } else if (0 >= data.amount) {
      swal({
        title: "Amount must be greater than zero",
        icon: "error",
        button: "Ok",
      });
    } else {
      // console.log("all done");
      let comparisonData = {
        from: "AED",
        to: to,
        amount: parseInt(amount),
      };
      // console.log("check", history);
      this.props.compare(comparisonData, history, "inform");
    }
    e.preventDefault();
  };

  // shouldComponentUpdate(nextProps){
  // console.lof

  // this.forceUpdate()
  // console.log(this.state.convertedRate);
  // return true;
  // }

  componentWillReceiveProps(nextProps) {
    let { dataLiveRate, isLoading, convertedRate } = nextProps;
    // console.log("dataLiveRate",dataLiveRate)
    this.setState({
      data: dataLiveRate,
      isLoading: isLoading,
      convertedRate: convertedRate,
    });
  }

  render() {
    // let { dataLiveRate, isLoading, convertedRate } = this.props;
    // console.log("FLAGS UPDATE STATE", this.props);
    return (
      <div className="coooom">
        <div className="Converter">
          <div className="container-fluid cal-container">
            <div className="row">
              <div className="cal-first-heading col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h1>
                  {/* {this.state.amount ? this.state.amount : ""}{" "}
                  {this.state.from ? this.state.from + " to" : " "}{" "}
                  {this.state.to ? this.state.to + " = " : ""} {""}
                  {this.state.convertedRate
                    ? this.state.convertedRate.value
                      ? Number(this.state.convertedRate.value).toFixed(2)
                      : null
                    : "enter your amount"}
                  {this.state.to ? ` ${this.state.to}` : ""} */}
                </h1>
              </div>
            </div>
            <div className="container1">
              <div className="cal-container2">
                <div className="row cal-row">
                  <form>
                    <div className="form-group col-xl-4 col-lg-4 col-md-10 col-sm-10 col-11">
                      <label className="center-label">Amount</label>
                      <input
                        className="center-label bg-white"
                        placeholder=""
                        id="ammount"
                        type="number"
                        min="0"
                        onChange={(e) =>
                          this.setState({ amount: e.target.value })
                        }
                      />
                    </div>
                    <div className="form-group col-xl-3 col-lg-3 col-md-5 col-sm-10 col-11">
                      <label className="center-label1">From</label>
                      <select
                        name="countries"
                        className="cal-dropdown1 center-label1 bg-white"
                        id="from"
                        onChange={(e) => {
                          // console.log("occhnage", e.target.value);
                          let change = this.state.data.filter(
                            (f) => f.currency_code == e.target.value
                          )[0];
                          // console.log(
                          //   "occhnage",
                          //   e.target.value,
                          //   change.country_flag
                          // );
                          //this.setState({ [which]: what });
                          document.getElementById(
                            "from"
                          ).style.backgroundImage = `url(${change.country_flag})`;
                        }}
                      >
                        {this.state.data ? (
                          this.state.data.map((value, key) => {
                            // console.log("flag", value);
                            return (
                              <option
                                className="cal-opt"
                                key={key}
                                id="cal-opt7"
                                value={value.currency_code}
                                selected={
                                  value.currency_code == "USD" ? "selected" : ""
                                }
                              >
                                {value.currency_code} {value.currency_name}
                              </option>
                            );
                          })
                        ) : (
                          <option
                            className="cal-opt"
                            // key={key}
                            id="cal-opt7"
                            value={<Loader />}

                            //   onClick={() => this.setState({to: value.currency_code})}
                          >
                            <Loader />
                          </option>
                        )}
                      </select>
                    </div>
                    <div className="form-group col-xl-1 col-lg-1 col-md-1 col-sm-0 col-0">
                      <img
                        onClick={() => this.swapOptions()}
                        src={require("../img/calculator/arrows.png")}
                        className="arrows"
                        style={{ cursor: "pointer" }}
                      />
                    </div>
                    <div className="form-group col-xl-3 col-lg-3 col-md-5 col-sm-10 col-11">
                      <label className="center-label2">To</label>
                      <select
                        name="countries"
                        className="cal-dropdown2 center-label2 bg-white"
                        id="to"
                        onChange={(e) => {
                          // console.log("occhnage", e.target.value);
                          let change = this.state.data.filter(
                            (f) => f.currency_code == e.target.value
                          )[0];
                          // console.log(
                          //   "occhnage",
                          //   e.target.value,
                          //   change.country_flag
                          // );
                          //this.setState({ [which]: what });
                          document.getElementById(
                            "to"
                          ).style.backgroundImage = `url(${change.country_flag})`;
                        }}
                      >
                        {/* <option className="cal-opt" id="cal-opt2" onClick={() => this.setState({to: "PKR"})} value="PKR">PKR Rupee</option>
                            <option className="cal-opt" id="cal-opt1" onClick={() => this.setState({to: "AED"})} value="AED">AED Emirati Dirham</option>
                            <option className="cal-opt" id="cal-opt3" onClick={() => this.setState({to: "GBP"})} value="GBP">GBP British Pound</option>
                            <option className="cal-opt" id="cal-opt4" onClick={() => this.setState({to: "AUS"})} value="AUS">AUS Australian Dollar</option>
                            <option className="cal-opt" id="cal-opt5" onClick={() => this.setState({to: "IND"})} value="IND">IND Rupee</option>
                            <option className="cal-opt" id="cal-opt6" onClick={() => this.setState({to: "SGD"})} value="SGD">SGD Singapore Dollar</option>
                            <option className="cal-opt" id="cal-opt7" onClick={() => this.setState({to: "ZAR"})} value="ZAR">ZAR South African Rand</option> */}
                        {this.state.data
                          ? this.state.data.map((value, key) => {
                            // console.log("valueFlag",value)
                              return (
                                <option
                                  className="cal-opt"
                                  key={key}
                                  id="cal-opt7"
                                  value={value.currency_code}
                                >
                                  {value.currency_code} {value.currency_name}
                                </option>
                              );
                            })
                          : ""}
                      </select>
                    </div>
                    <div className="form-group col-xl-1 col-lg-1 col-md-1 col-sm-12 col-11">
                      {/* <a> */}
                      <button type="button" onClick={() => this.converted()}>
                        <img src={require("../img/calculator/less-than.png")} />
                      </button>
                      {/* </a> */}
                    </div>
                  </form>
                </div>

                {this.props.convertIsLoading ? (
                  <ContentLoader
                    className="loader"
                    height={160}
                    width={600}
                    speed={2}
                    primaryColor="#fb374b"
                    secondaryColor="#fdb4bb"
                  >
                    <rect
                      x="232"
                      y="14"
                      rx="5"
                      ry="5"
                      width="122"
                      height="15"
                    />
                    <rect
                      x="142"
                      y="41"
                      rx="5"
                      ry="5"
                      width="311"
                      height="22"
                    />
                    <rect
                      x="232"
                      y="79"
                      rx="5"
                      ry="5"
                      width="122"
                      height="15"
                    />
                    <rect
                      x="232"
                      y="104"
                      rx="5"
                      ry="5"
                      width="122"
                      height="15"
                    />
                    <rect
                      x="438"
                      y="93"
                      rx="5"
                      ry="5"
                      width="153"
                      height="12"
                    />
                    <rect x="4" y="93" rx="5" ry="5" width="153" height="12" />
                    <rect
                      x="56"
                      y="138"
                      rx="5"
                      ry="5"
                      width="493"
                      height="12"
                    />
                  </ContentLoader>
                ) : (
                  // </div>
                  <div className="row">
                    <div className="center-para col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                      {/* <span>
                        {this.state.amount ? this.state.amount : "000"}{" "}
                        {this.state.from ? this.state.from : "USD"}
                      </span> */}
                      <h1 onClick={() => this.shiftToInputText()}>
                        {this.state.convertedRate
                          ? this.state.convertedRate.value
                            ? Number(this.state.convertedRate.value).toFixed(2)
                            : "Enter your amount"
                          : "enter the number"}
                        {/* { "PKR"} */}{" "}
                        {/* {this.state.convertedRate ? this.state.to : ""} */}
                      </h1>
                    </div>
                    <div className="sides-para col-xl-3 col-lg-4 col-md-4 col-sm-0 col-0">
                      {/* <p>
                        {this.state.from && this.state.to
                          ? this.showConversion()
                          : null}
                      </p> */}
                    </div>

                    <div className="center-para col-xl-6 col-lg-5 col-md-4 col-sm-12 col-12">
                      <h4>
                        {this.state.to ? (
                          <Fragment>
                            {this.state.from ? `1 ${this.state.from} = ` : ""}{" "}
                            {this.state.convertedRate
                              ? this.state.convertedRate.rate
                                ? Number(this.state.convertedRate.rate).toFixed(
                                    2
                                  )
                                : null
                              : "enter your amount"}{" "}
                            {this.state.to ? this.state.to : ""}
                            <br />
                            {this.state.to ? `1 ${this.state.to} = ` : ""}{" "}
                            {this.state.convertedRate
                              ? this.state.convertedRate.value
                                ? Number(
                                    1 / this.state.convertedRate.rate
                                  ).toFixed(4)
                                : null
                              : "enter your amount"}{" "}
                            {this.state.from ? this.state.from : ""}
                          </Fragment>
                        ) : (
                          ""
                        )}
                      </h4>
                    </div>
                    <div className="sides-para col-xl-3 col-lg-3 col-md-4 col-sm-0 col-0">
                      {/* <p>
                        Last updated:{" "}
                        {date.format(new Date(), "YYYY/MM/DD HH:mm:ss")}
                      </p> */}
                    </div>

                    {/* Goto Comparison Page Button */}
                    {/* {this.state.searched && (
                      <div className="center-para col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="row d-flex justify-content-center pb-4">
                          <button
                            className="compareBtn"
                            type="button"
                            onClick={(e) => this.send(e, this.props.history)}
                          >
                            Go to Comparison
                             // {this.state.to}
                          </button>
                        </div>
                      </div>
                    )} */}
                    {/* Goto Comparison Page Button */}

                    <div className="center-para col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                      <p>
                        All figures are live mid-market rates, which are not
                        available to consumers and are for informational
                        purposes only.
                      </p>
                    </div>
                  </div>
                )}
              </div>
              <div className="cal-container3">
                {/* {      this.state.data ? (
                     this.state.data.map((value, index) => {
                      return <CountryCard key={index} data={value} />;
                     })
                     ) : (
                   )
                   } */}

                {/* new */}
                <div className="country-card-wrapper df f-wrap jcc">
                  {this.state.data
                    ? this.state.data.map((value, index) => {
                        return (
                          <CountryCard
                            currency_code={this.state.from}
                            key={index}
                            data={value}
                            amount={this.state.amount}
                          />
                        );
                      })
                    : ""}
                  {this.props.isLoading ? (
                    <div
                      style={{
                        height: 400,
                        width: "100%",
                        // marginLeft: 35
                      }}
                      className="column col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    >
                      <ContentLoader
                        height={250}
                        width={600}
                        speed={2}
                        primaryColor="lightgray"
                        secondaryColor="gray"
                      >
                        <rect
                          x="5"
                          y="19"
                          rx="0"
                          ry="0"
                          width="285"
                          height="50"
                        />
                        <rect
                          x="294"
                          y="19"
                          rx="0"
                          ry="0"
                          width="285"
                          height="50"
                        />
                        <rect
                          x="5"
                          y="90"
                          rx="0"
                          ry="0"
                          width="285"
                          height="50"
                        />
                        <rect
                          x="294"
                          y="90"
                          rx="0"
                          ry="0"
                          width="285"
                          height="50"
                        />
                        <rect
                          x="294"
                          y="160"
                          rx="0"
                          ry="0"
                          width="285"
                          height="50"
                        />
                        <rect
                          x="5"
                          y="160"
                          rx="0"
                          ry="0"
                          width="285"
                          height="50"
                        />
                      </ContentLoader>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
                {/* new */}
                {/* old */}
                {/* <div className="row">
                  {this.state.data
                    ? this.state.data.map((value, index) => {
                        return (
                          <CountryCard
                            currency_code={this.state.from}
                            key={index}
                            data={value}
                            amount={this.state.amount}
                          />
                        );
                      })
                    : ""}
                  {this.props.isLoading ? (
                    <div
                      style={{ height: 400, width: "100%", marginLeft: 35 }}
                      className="column col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                    >
                      <ContentLoader
                        height={250}
                        width={600}
                        speed={2}
                        primaryColor="lightgray"
                        secondaryColor="gray"
                      >
                        <rect
                          x="5"
                          y="19"
                          rx="0"
                          ry="0"
                          width="285"
                          height="50"
                        />
                        <rect
                          x="294"
                          y="19"
                          rx="0"
                          ry="0"
                          width="285"
                          height="50"
                        />
                        <rect
                          x="5"
                          y="90"
                          rx="0"
                          ry="0"
                          width="285"
                          height="50"
                        />
                        <rect
                          x="294"
                          y="90"
                          rx="0"
                          ry="0"
                          width="285"
                          height="50"
                        />
                        <rect
                          x="294"
                          y="160"
                          rx="0"
                          ry="0"
                          width="285"
                          height="50"
                        />
                        <rect
                          x="5"
                          y="160"
                          rx="0"
                          ry="0"
                          width="285"
                          height="50"
                        />
                      </ContentLoader>
                    </div>
                  ) : (
                    ""
                  )}
                </div> */}
                {/* old */}
              </div>
            </div>
          </div>
        </div>
        <div className="container1">
          <div className="row cal-note">
            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <h2>Note</h2>
              {/* <span>
                {date.format(new Date(), "YYYY/MM/DD HH:mm:ss")}
                {" UTC"}
              </span> */}
              <p>
                All figures re live mid-market rates, which are not available to
                consumers and are for informational purposes only. To see the
                rates we quote for money transfer, please select Live Money
                Transfer Rates.
              </p>
            </div>
          </div>
        </div>

        <Footer history={this.props.history} />
      </div>
    );
  }
}
