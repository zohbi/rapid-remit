import React from "react";
import "../css/bootstrap.css";
import "../css/cal.css";

export default class CountryCard extends React.Component {
  render() {
    if (this.props.currency_code === this.props.data.currency_code) {
      return null;
    }

    let calculation = parseFloat(this.props.data.currency_values);
    let amoiuntt = this.props.amount ? this.props.amount : 1;

    let calculationVal = calculation * amoiuntt;
    return (
      <>
        {/* new */}

        <div className="contact-card df jcb aic">
          <div className="df aic">
            <div className="divFlag df aic">
              <img src={this.props.data.country_flag} className="flag" />
            </div>

            <div className="currency-name-div">
              <h3>{this.props.data.currency_code}</h3>
              <p>{this.props.data.currency_name}</p>
            </div>
          </div>

          <div className="">
            <h4>{calculationVal.toFixed(4)}</h4>
            <p>
              1 <span>{this.props.data.base_currency}</span> ={" "}
              {this.props.data.currency_values}{" "}
              <span>{this.props.data.currency_code}</span>
            </p>
          </div>
        </div>

        {/* new */}

        {/* old */}
        {/* <div className="column col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
            <div className="row">
              <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                <div className="divFlag">
                  <img src={this.props.data.country_flag} className="flag" />
                </div>
              </div>
              <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <h3>{this.props.data.currency_code}</h3>
                <p>{this.props.data.currency_name}</p>
              </div>
              <div className="converter col-xl-5 col-lg-5 col-md-5 col-sm-5 col-5">
                <h4>{calculationVal.toFixed(4)}</h4>
                <p>
                  1 <span>{this.props.data.base_currency}</span> ={" "}
                  {this.props.data.currency_values}{" "}
                  <span>{this.props.data.currency_code}</span>
                </p>
              </div>
            </div>
          </div> */}
        {/* old */}
      </>
    );
  }
}
