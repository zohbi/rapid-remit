import React from "react";
import "../css/bootstrap.css";
import "../css/style-about.css";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { counts } from "../Store/actions/userAuth";

class Aboutrow_5 extends React.Component {
  componentWillMount() {
    this.props.getData();
  }
  render() {
    // console.log("props", this.props.counts);
    let { counts } = this.props;
    return (
      <div>
        <div className="container-fluid">
          <div className="row red-row">
            <div className="col-xl-2 col-lg-1 col-md-1 col-sm-1 col-0"></div>
            <div className="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-6">
              <h1 className="Lato_Black">
                {counts && counts.countries_count
                  ? counts.countries_count + "+"
                  : 0}
              </h1>
              <h5 className="Lato_Black">Countries</h5>
            </div>
            <div className="col-xl-2 col-lg-3 col-md-3 col-sm-3 col-6">
              <h1 className="Lato_Black">
                {counts && counts.user_count ? counts.user_count + "+" : 0}
              </h1>
              <h5 className="Lato_Black">Trusted Clients</h5>
            </div>
            <div className="col-xl-2 col-lg-3 col-md-3 col-sm-3 col-6">
              <h1 className="Lato_Black">
                {counts && counts.transaction_count
                  ? counts.transaction_count + "+"
                  : 0}
              </h1>
              <h5 className="Lato_Black">Daily Transactions</h5>
            </div>
            <div className="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-6">
              <h1 className="Lato_Black">
                {counts && counts.partners_count
                  ? counts.partners_count + "+"
                  : 0}
              </h1>
              <h5 className="Lato_Black">Business Partners</h5>
            </div>

            <div className="col-xl-2 col-lg-1 col-md-0 col-sm-0 col-0"></div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    counts: state.Offers.counts,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: bindActionCreators(counts, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Aboutrow_5);
