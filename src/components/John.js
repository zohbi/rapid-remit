import React from "react";
import "../css/bootstrap.css";
import "../css/style-about.css";

export default class John extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    // console.log();
    return (
      <div className="container1">
        {/* <div className="container1">
            <div className="row mr-john">
            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <img src={this.props.data.img} className="inverted-coma"/>
                <div className="pic1">
                <div className="pic-i"></div>
                <div className="pic-j">
                    <img src={this.props.data.img2} className="john"/>
                </div>
                </div>
            </div>
            <div className="john-detail col-xl-7 col-lg-7 col-md-7 col-sm-8 col-8">
                <h5 className="Lato_Black">{this.props.data.heading}</h5>
                <p id="para2" className="Lato_Regular">{this.props.data.paragraph}</p>
            </div>
            <div className="col-xl-1 col-lg-1 col-md-1 col-sm-0 col-0">
                <img src={this.props.data.img3} className="inverted-coma2"/>
            </div>
            </div>
            </div> */}
        <div className="row john-row">
          <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
          <div className="mr-john col-xl-3 col-lg-3 col-md-2 col-sm-2 col-2">
            <img src={this.props.data.img} className="inverted-coma" />
            <div className="pic1">
              <div className="pic-i"></div>
              <div className="pic-j">
                <img
                  src={
                    this.props.review && this.props.review.img
                      ? this.props.review.img
                      : ""
                  }
                  className="john-img"
                />
              </div>
            </div>
          </div>
          <div className="mr-john john-detail col-xl-6 col-lg-6 col-md-9 col-sm-9 col-8">
            <h5>
              {this.props.review && this.props.review.name
                ? this.props.review.name
                : ""}
            </h5>
            <p id="para2" className="Lato_Regular">
              {this.props.review && this.props.review.text
                ? this.props.review.text
                : ""}
            </p>
          </div>
          <div className="mr-john col-xl-1 col-lg-1 col-md-1 col-sm-1 col-2">
            <img src={this.props.data.img3} className="inverted-coma2" />
          </div>
          <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
        </div>
      </div>
    );
  }
}
