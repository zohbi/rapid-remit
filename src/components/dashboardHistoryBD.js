import React from "react";
import { connect } from "react-redux";
import { Comparisons } from "../Store/actions/index";
import { bindActionCreators } from "redux";
import { Dots } from "react-activity";
import "react-activity/dist/react-activity.css";

class DashboardHistoryBD extends React.Component {
  componentWillMount() {
    // console.log("dddddd");
    if (localStorage.getItem("token")) {
      let user_id = JSON.parse(localStorage.getItem("UserData"))._id;
      // console.log("user_id", user_id);

      this.props.remmitanceHistory(user_id);
    } else {
      window.location = "/registration";
    }
  }
  render() {
    // console.log("this.props.isloading");
    // console.log("this.props.isloading", this.props.data, this.props.isloading);
    return (
      <div className="questions" id="DashboardHistoryBD">
        {this.props.data &&
        this.props.data.length > 0 &&
        !this.props.isloading ? (
          this.props.data.map((history) => {
            return (
              <div className="question" id="question1">
                <h6>TRANSACTION-AED +{history.amount} </h6>
                <h6 className="quesionH6Second">STATUS-+{history.status} </h6>
                <h5 className="plus">
                  <img
                    className="img1UD"
                    src={require("../img/dashboard1/dropdown1.png")}
                    alt=""
                  />
                  <img
                    className="img2UD"
                    src={require("../img/dashboard1/dropdown2.png")}
                    alt=""
                  />
                </h5>
                <p>
                  it amet consectetur adipisicing elit. Modi nobis
                  exercitationem quo labore, in voluptas necessitatibus nihil.
                  Voluptas cumque iure mollitia placeat molestias a beatae,
                  consequuntur nam provident obcaecati? Dolor? Lorem ipsum
                  dolor, sit amet consectetur adipisicing elit.
                </p>
              </div>
            );
          })
        ) : this.props.isloading ? (
          <center>
            <Dots size="100px" />
          </center>
        ) : (
          <div
            style={{
              display: 'flex',
              width: "100%",
              height: '100%',
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <h1>No History Found</h1>
          </div>
        )}
        {/* {this.props.data.length == 0 && !this.props.isLoading ? (
        
            <h1>No History Found</h1>
          
        ) : null} */}
        {/* {/* <div className="question" id="question2">
                        <h6>TRANSACTION-AED 45,000</h6>
                        <h6 className="quesionH6Second">STATUS-CLOSED</h6>
                        <h5 className="plus">
                            <img className="img1UD" src={require("../img/dashboard1/dropdown1.png")} alt="" />
                            <img className="img2UD" src={require("../img/dashboard1/dropdown2.png")} alt="" />
                        </h5>
                        <p>it amet consectetur adipisicing elit. Modi nobis exercitationem quo labore, in voluptas necessitatibus
                        nihil. Voluptas cumque iure mollitia placeat molestias a beatae, consequuntur nam provident obcaecati?
                        Dolor? Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi nobis exercitationem quo labore,
                        elit. Modi nobis exercitationem quo labore, in voluptas necessitatibus nihil. Voluptas cumque iure
                        mollitia placeat molestias a beatae.</p>
                    </div>
                    <div className="question" id="question3">
                        <h6>TRANSACTION-AED 32,000</h6>
                        <h6 className="quesionH6Second">STATUS-CLOSED</h6>
                        <h5 className="plus">
                            <img className="img1UD" src={require("../img/dashboard1/dropdown1.png")} alt="" />
                            <img className="img2UD" src={require("../img/dashboard1/dropdown2.png")} alt="" />
                        </h5>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi nobis exercitationem quo labore, in
                        voluptas necessitatibus nihil. Voluptas cumque iure mollitia placeat molestias a beatae, consequuntur nam
                        provident obcaecati? Dolor? Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi nobis
                        exercitationem quo labore, in voluptas necessitatibus nihil. Voluptas cumque iure mollitia placeat
                        molestias a beatae, consequuntur nam provident obcaecati? Dolor?</p>
                    </div>
                    <div className="question" id="question4">
                        <h6>TRANSACTION-AED 20,000</h6>
                        <h6 className="quesionH6Second">STATUS-CLOSED</h6>
                        <h5 className="plus">
                            <img className="img1UD" src={require("../img/dashboard1/dropdown1.png")} alt="" />
                            <img className="img2UD" src={require("../img/dashboard1/dropdown2.png")} alt="" />
                        </h5>
                        <p>dolor, sit amet elit. Modi nobis exercitationem quo labore, in voluptas necessitatibus nihil. Voluptas
                        cumque iure mollitia placeat molestias a beatae, consequuntur nam provident obcaecati? Dolor? Lorem ipsum
                        dolor, sit amet consectetur cumque iure mollitia placeat consectetur adipisicing elit. Modi nobis
                        exercitationem quo labore, in voluptas necessitatibus nihil. Voluptas cumque iure mollitia placeat
                        molestias a beatae, consequuntur nam provident obcaecati? Dolor? Lorem ipsum dolor, sit amet consectetur
                        adipisicing elit. Modi nobis exercitationem quo labore.</p>
                    </div>
                    <div className="question" id="question5">
                        <h6>TRANSACTION-AED 70,000</h6>
                        <h6 className="quesionH6Second">STATUS-CLOSED</h6>
                        <h5 className="plus">
                            <img className="img1UD" src={require("../img/dashboard1/dropdown1.png")} alt="" />
                            <img className="img2UD" src={require("../img/dashboard1/dropdown2.png")} alt="" />
                        </h5>
                        <p>Dolor, sit amet consectetur adipisicing elit. Modi nobis exercitationem quo labore, in voluptas
                        necessitatibus nihil. Voluptas cumque iure mollitia placeat molestias a beatae, consequuntur nam provident
                        obcaecati? Dolor? Lorem ipsum dolor, sit amet consectetur adipisicing elit. Modi nobis exercitationem quo
                        labore, in voluptas necessitatibus nihil. Voluptas cumque iure mollitia placeat molestias a beatae,
                        consequuntur nam provident obcaecati? Dolor?</p>
                    </div> */}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.History.allData,
    isloading: state.History.isLoading,
  };
};

const mapDispatchToProps = (dispatch) => ({
  remmitanceHistory: bindActionCreators(
    Comparisons.remmitanceHistory,
    dispatch
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(DashboardHistoryBD);
