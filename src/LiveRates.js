import React, { Component } from "react";
import "./css/bootstrap.css";
import "./css/style-footer.css";
import "./css/style-our-network.css";
import John from "./components/John";
import Flip from "react-reveal/Flip";
import Fade from "react-reveal/Fade";
import { connect } from "react-redux";
import Footer from "./components/footer";
import Loader2 from "./components/loader2";
import { bindActionCreators } from "redux";
import RatingCard from "./components/RatingCard";
import { Dots } from 'react-activity';
import { Carousel } from "react-responsive-carousel";
import { Comparisons, addReviews, userReviews, SetIsNavActive, getOurNetworkHeading } from "./Store/actions/userAuth";

class LiveRates extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      arr: [
        {
          heading: "City Express",
          img: require("./img/our-network/city-express.png"),
          reviews: 320,
          rating: 4,
        },
        {
          heading: "Transfer Wise",
          img: require("./img/our-network/transfer.png"),
          reviews: 320,
          rating: 5,
        },
        {
          heading: "World Remit",
          img: require("./img/our-network/world-remit.png"),
          reviews: 320,
          rating: 3,
        },
        {
          heading: "City Express",
          img: require("./img/our-network/city-express.png"),
          reviews: 320,
          rating: 4,
        },
        {
          heading: "Transfer Wise",
          img: require("./img/our-network/transfer.png"),
          reviews: 320,
          rating: 5,
        },
        {
          heading: "World Remit",
          img: require("./img/our-network/world-remit.png"),
          reviews: 320,
          rating: 3,
        },
        {
          heading: "City Express",
          img: require("./img/our-network/city-express.png"),
          reviews: 320,
          rating: 4,
        },
        {
          heading: "Transfer Wise",
          img: require("./img/our-network/transfer.png"),
          reviews: 320,
          rating: 5,
        },
        {
          heading: "World Remit",
          img: require("./img/our-network/world-remit.png"),
          reviews: 320,
          rating: 3,
        },
      ],

      John: {
        img: require("./img/our-network/invertedcoma.png"),
        img2: require("./img/our-network/john.png"),
        img3: require("./img/our-network/invertedcoma2.png"),
        heading: "Mr.John Musk",
        paragraph:
          "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
      },

      perPageCount: 6,
      startedPageCount: 0,
    };
  }
  onLoadMoreHandler = (startPoint, event, index) => {
    try {
      const { prevParaIndex } = this.state
      let currentPara = event.target;

      if (prevParaIndex !== undefined && prevParaIndex !== "" && prevParaIndex !== null) {
        let prevPara = document.querySelector(`#pagination-li-${prevParaIndex} p`)
        if (prevPara?.classList?.contains("page-active")) {
          prevPara.classList.remove("page-active");
        }
      }

      currentPara.className += " page-active"
      this.setState({
        startedPageCount: startPoint,
        prevParaIndex: index
      })
    } catch (e) {
      // console.log(e)
    }
  }
  componentDidMount() {
    this.props.SetIsNavActive("/ournetwork")
    this.props.getOurNetworkHeading()
    this.props.getData();
    this.props.userReview();
    this.props.getComp({
      from: "AED",
      to: "USD",
      amount: 1000,
      data: "live",
    });

    if (window.location.pathname == "/") {
      //  console.log("ASsads")
      document.getElementById("routerNav").style.display = "none";
      //  document.getElementById('navigationIndex').style.display="block"
    } else {
      //  console.log("nhi hy")
      document.getElementById("routerNav").style.display = "block";
      //  document.getElementById('navigationIndex').style.display="none"
    }
  }
  componentDidUpdate(preProps) {
    if (preProps.ournetworkheading !== this.props.ournetworkheading) {
      // console.log(this.props.ournetworkheading)
      this.setState({
        heading: this.props.ournetworkheading?.heading,
        text_1:this.props.ournetworkheading?.text_1,
        text_2:this.props.ournetworkheading?.text_2,
      })
    }
  }
  pagination = (data) => {
    try {
      const { perPageCount } = this.state
      let paginationCount = Math.ceil(data.length / perPageCount);
      var rows = [];

      for (let i = 0; i < paginationCount; i++) {
        rows.push(<a href="#ournetwork-companies" className="anchor-decotation"><li id={`pagination-li-${i}`} onClick={(e) => this.onLoadMoreHandler(i * perPageCount, e, i)} style={{ cursor: "pointer" }} className="page-item"><p className="page-link">{i + 1}</p></li></a>);
      }
      return <ul id="paginationUl" className="pagination justify-content-end" >
        {rows}
      </ul>
    } catch (e) {
      // console.log(e)
    }
  }

  onHandleSelect = (e) => {
    try {
      const { prevParaIndex } = this.state
      if (prevParaIndex !== undefined && prevParaIndex !== "" && prevParaIndex !== null) {
        let prevPara = document.querySelector(`#pagination-li-${prevParaIndex} p`)
        if (prevPara?.classList?.contains("page-active")) {
          prevPara.classList.remove("page-active");
        }
      }
      this.setState({
        perPageCount: e.target.value,
        startedPageCount: 0,
      })
    } catch (e) {
      // console.log(e)
    }
  }

  returnRatingCardWithPaginationAndPageSize = (value, ind) => {
    try {
      const { startedPageCount, perPageCount } = this.state
      let endedPoiint = +startedPageCount + +perPageCount;
      if (ind >= startedPageCount && ind < endedPoiint) {
        return (
          <div style={{ margin: "5px" }}>
            <RatingCard
              key={Math.random}
              data={value}
              history={this.props.history}
              getFavouriteList={() => this.getFavouriteList()}
            />
          </div>
        );
      }
    } catch (e) {
      // console.log(e)
    }
  }
  //componentWillMount() {}
  // componentWillReceiveProps(nextProps) {
  //   let { liverate } = nextProps;
  //   this.setState({ data: liverate });
  //   console.log(this.state.data);
  // }
  getFavouriteList=() =>{
    if (localStorage.getItem("token")) {
      // let user_id = JSON.parse(localStorage.getItem("UserData"))._id;
      // console.log("user_id", user_id);
      // let token = JSON.parse(localStorage.getItem("token"));
      // console.log("8user", token);
      this.props.getData();
    } else {
      window.location = "/registration";
    }
  }
  render() {
    // console.log("data hi hai", this.props.part);
    const { startedPageCount, endedPageCount, perPageCount,ournetworkData, heading, text_1, text_2 } = this.state
    console.log("ournetworkData",ournetworkData);
    return (
      <div className="LiveRate">
        <div className="our-network">
          <div className="container1">
            <div className="row">
              <div className="col-x-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h1 className="first-heading Lato_Black">
                  {heading}
                </h1>
              </div>
              <div className="col-x-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <img
                  src={require("./img/our-network/dum.png")}
                  className="buildings"
                />
              </div>

              <div className="divRedWhiteLine"></div>
            </div>
          </div>

          <div id="ournetwork-companies" className="container1 rate-boxes">
            <div className="row">
              <div className="col-x-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <Flip right opposite cascade mirror>
                  <br />
                  <h1 className="second-heading Lato_Black">
                    {heading}
                  </h1>
                </Flip>
                <Flip left opposite cascade duration={3000} mirror>
                  <p id="para1">
                    {text_1}.
                  </p>
                  <center>
                    <p>
                      {text_2}
                      {/* Best Money Transfer Services Compared & Reviewed in Transfer
                      Fees, Exchange Rate, Speed and More. */}
                    </p>
                  </center>
                </Flip>
              </div>

              {this.props.isLoading ? <div style={{ width: "100%" }}><center>
                <Dots color='red' size='100px' />
              </center>
              </div> : null}
              {this.props.part && !this.props.isLoading &&
                this.props.part.length > 0 && <div style={{ display: "flex", justifyContent: "flex-end", width: "100%" }}>
                  <div id="comparision-list" className='container3' style={{ width: "95%" }}>
                    <div style={{ background: "white", padding:"0px" }} className='row no-gutters comparision-form-row2'>
                      <div className='col-xl-2 col-lg-3 col-md-4 col-sm-4 col-5 ml-auto'>
                        <form>
                          <select
                            name='result-dropdown'
                            placeholder="Result per Page"
                            className='dropdownsC cal-dropdown1 center-label1 bg-white dropdownInput'
                            defaultValue={perPageCount}
                            onChange={(e) => this.onHandleSelect(e)}
                          >
                            <option className='opt' value={6}>
                              6
                          </option>

                            <option className='opt' value={9}>
                              9
                          </option>
                            <option className='opt' value={12}>
                              12
                          </option>
                            <option className='opt' value={15}>
                              15
                          </option>
                          </select>
                        </form>
                      </div>
                    </div>
                  </div>
                  {/* <div id="pagination-select" >
                    <select onChange={(e) => this.onHandleSelect(e)} defaultValue={perPageCount} className="form-control" id="sel1">
                      <option value={6}>6</option>
                      <option value={9}>9</option>
                      <option value={12}>12</option>
                      <option value={15}>15</option>
                    </select>
                  </div> */}
                </div>}
              <div className="our-network-rating-cards-main-div">
                {this.props.part &&
                  this.props.part.length > 0 &&
                  !this.props.isLoading
                  ? this.props.part.map((value, ind) => {
                    return this.returnRatingCardWithPaginationAndPageSize(value, ind)
                  })
                  : null}
              </div>
              <div className='container3' style={{ width: "97%" }}>
                <div style={{ width: "100%" }}>
                  {this.props.part && !this.props.isLoading &&
                    this.props.part.length > 0 && this.pagination(this.props.part)}
                </div>
              </div>
            </div>
          </div>
          {/* <Fade left>
            <div className="container-fluid divBottom bg-white">
              <div className="row companyOverviewContentRow4 ml-0 mr-0">
                <div className="container1">
                  <div id="carousel-for-reviews">
                    <Carousel
                      showIndicators={false}
                      showStatus={false}
                      // showArrows={false}
                      axis="horizontal"
                      autoPlay={true}
                    >
                      {this.props.data && this.props.data.length > 0
                        ? this.props.data.map((d) => (
                            <div className="row john-row">
                              <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
                              <div className="mr-john col-xl-4 col-lg-4 col-md-2 col-sm-2 col-2">
                                <img
                                  src={require("./img/aboutUs/invertedcoma.png")}
                                  className="inverted-coma"
                                />
                                <div className="pic1">
                                  <div className="pic-i"></div>
                                  <div className="pic-j">
                                    <img src={d.img} className="john-img" />
                                  </div>
                                </div>
                              </div>
                              <div className="mr-john john-detail col-xl-5 col-lg-5 col-md-9 col-sm-9 col-8 ">
                                <h5>{d.name}</h5>
                                <p id="para2">{d.text}</p>
                              </div>
                              <div className="mr-john col-xl-1 col-lg-1 col-md-1 col-sm-1 col-2">
                                <img
                                  src={require("./img/aboutUs/invertedcoma2.png")}
                                  className="inverted-coma2"
                                />
                              </div>
                              <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
                            </div>
                          ))
                        : ""}
                    </Carousel>
                  </div>
                </div>
              </div>
            </div>
          </Fade> */}
        </div>
        <Footer history={this.props.history} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.Comparisons.isLoading,
    part: state.Comparisons.part,
    reviewsData: state.Offers.reviews,
    data: state.Offers.user_review,
    ournetworkheading: state.FAQS.ournetworkheading

  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: () => dispatch(Comparisons.partner()),
  getFavourite: bindActionCreators(Comparisons.getFavourite, dispatch),
  getComp: (obj) => dispatch(Comparisons.Comparisons(obj)),
  userReview: bindActionCreators(userReviews, dispatch),
  SetIsNavActive: (data) => dispatch(SetIsNavActive(data)),
  getOurNetworkHeading: () => dispatch(getOurNetworkHeading()),
});

export default connect(mapStateToProps, mapDispatchToProps)(LiveRates);
