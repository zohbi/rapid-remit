import React, { Component } from "react";
import Routes from "./router";

// --------------------- React Redux ---------------------
import { Provider } from "react-redux";
import { store, persistor } from "./Store/store";
import { PersistGate } from "redux-persist/integration/react";
import { Helmet } from "react-helmet";

const TITLE = "Rapid Remit";
{
  /* <script src="https://www.gstatic.com/firebasejs/7.5.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.5.0/firebase-analytics.js"></script> */
}

{
  /* <script src="/__/firebase/7.14.2/firebase-app.js"></script>

<script src="/__/firebase/7.14.2/firebase-analytics.js"></script>

<script src="/__/firebase/init.js"></script> */
}

class App extends Component {
  componentDidMount() {
    // if(window.location="/"){
    //   //  console.log("ASsads")
    //       document.getElementById('routerNav').style.display="none"
    //   //  document.getElementById('navigationIndex').style.display="block"
    //  }else{
    //   //  console.log("nhi hy")
    //    document.getElementById('routerNav').style.display="block"
    //   //  document.getElementById('navigationIndex').style.display="none"
    //  }
  }
  componentDidMount() {
    // console.log("HAAAAAAA HY");
  }
  render() {
    //    if(window.location="/"){
    //   //  console.log("ASsads")
    //       document.getElementById('routerNav').style.display="none"
    //  }else{
    //   //  console.log("nhi hy")
    //    document.getElementById('routerNav').style.display="block"
    //   //  document.getElementById('navigationIndex').style.display="none"
    //  }
    return (
      <Provider store={store}>
        <Helmet>
          <title>{TITLE}</title>
        </Helmet>
        <PersistGate loading={null} persistor={persistor}>
          <Routes />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
