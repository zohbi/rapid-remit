import React, { Component } from "react";
import "./css/bootstrap.css";
import "./css/style-footer.css";
import "./css/style-comprision.css";
import Footer from "./components/footer";
import RedForm from "./components/RedForm";
import Comparision_table from "./components/comparision_table";
import Map from "./components/Map";
import Zoom from "react-reveal/Zoom";
import Instructions from "./components/Instructions";
import config from "react-reveal/globals";
import { connect } from "react-redux";
import { Comparisons, content } from "./Store/actions";
import Loader from "react-loader-spinner";
import comparison_instance from "./instance/comparison_instance";
import { Dots } from "react-activity";
import "react-activity/dist/react-activity.css";

config({ ssrFadeout: true });

class Comparision extends Component {
  constructor(props) {
    super(props);

    this.state = {
      arr: [
        {
          img: require("./img/money.png"),
          img1: require("./img/heart_filled.png"),
          img2: require("./img/checkbox filled.png"),
          paragraph: "320 REVIEWS",
          heading1: "RS.140",
          heading2: "RS.14,000,000",
          heading3: "ADE_0.00",
          heading4: "1 DAY",
        },
        {
          img: require("./img/money.png"),
          img1: require("./img/heart_filled.png"),
          img2: require("./img/checkbox filled.png"),
          paragraph: "320 REVIEWS",
          heading1: "RS.140",
          heading2: "RS.14,000,000",
          heading3: "ADE_0.00",
          heading4: "1 DAY",
        },
        {
          img: require("./img/money.png"),
          img1: require("./img/heart_filled.png"),
          img2: require("./img/checkbox filled.png"),
          paragraph: "320 REVIEWS",
          heading1: "RS.140",
          heading2: "RS.14,000,000",
          heading3: "ADE_0.00",
          heading4: "1 DAY",
        },
        {
          img: require("./img/money.png"),
          img1: require("./img/heart_filled.png"),
          img2: require("./img/checkbox filled.png"),
          paragraph: "320 REVIEWS",
          heading1: "RS.140",
          heading2: "RS.14,000,000",
          heading3: "ADE_0.00",
          heading4: "1 DAY",
        },
        {
          img: require("./img/money.png"),
          img1: require("./img/heart_filled.png"),
          img2: require("./img/checkbox filled.png"),
          paragraph: "320 REVIEWS",
          heading1: "RS.140",
          heading2: "RS.14,000,000",
          heading3: "ADE_0.00",
          heading4: "1 DAY",
        },
      ],
      Map: {
        heading: "Locate Near Exchange",
        img: require("./img/location-icon.png"),
      },
      Instructions: {
        heading: "Instructions",
        img: require("./img/Instruction.png"),
        paragraph:
          "Sending money abroad could cost you a lot, especially if you aren’t aware of the hidden fees. Money transfer companies and banks earn money by not only charging you a transfer fee, but also usually by offering you an exchange rate with a hidden markup. You could save a lot of money by comparing exchange rates and transfer fees in real-time. With our money transfer comparison tool, you’ll find the best way to send money internationally in just a few clicks.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat, consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse.",
        paragraph1:
          "Simply complete how much you want to send and currency to be received. We compare and review over all money transfer companies in your region and provide you with comapriosn of the least cost, fastest and most convinientDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat.",
      },
      limit: 10,
      sort: 1,
      name: "",
    };
    comparison_instance.header = this;
  }
  handleDropdown = (name, e) => {
    this.setState({ [name]: e.target.value });
  };

  componentWillMount() {
    this.props.currency();
    this.props.content();
  }

  componentDidMount() {
    if (window.location.pathname == "/") {
      //  console.log("ASsads")
      document.getElementById("routerNav").style.display = "none";
      //  document.getElementById('navigationIndex').style.display="block"
    } else {
      //  console.log("nhi hy")
      document.getElementById("routerNav").style.display = "block";
      //  document.getElementById('navigationIndex').style.display="none"
    }
  }
  render() {
    // console.log("coooom", this.props.contentData);
    let cData = this.props.currencyData;
    return (
      <div className="coooom">
        <Zoom ssrFadeout Left>
          <div className="comparision">
            <div className="main-container">
              <div className="container2">
                <RedForm
                  isLoading={
                    this.props.isLoading ? this.props.isLoading : false
                  }
                  getData={(abc) => this.props.getData(abc)}
                />
              </div>
              <div className="container2">
                <div className=" row comparision-form-row2">
                  <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <input
                      type="text"
                      className="input-search"
                      placeholder="Search..."
                      value={this.state.name}
                      onChange={(e) => {
                        this.setState({ name: e.target.value });
                      }}
                    />
                  </div>

                  {/* <div className="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <form>
                      <select
                        name="sort"
                        className="dropdownsC"
                        onChange={e => {
                          this.handleDropdown("sort", e);
                        }}
                      >
                        <option id="opt" value="Sort by Title">
                          Sort by Title
                        </option>
                        <option id="opt" value="title">
                          title
                        </option>
                      </select>
                    </form>
                  </div> */}

                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <form>
                      <select
                        name="result-dropdown"
                        className="dropdownsC"
                        value={this.state.limit}
                        onChange={(e) => {
                          this.handleDropdown("limit", e);
                        }}
                      >
                        <option className="opt" value="Result per Page">
                          Result per Page
                        </option>
                        <option className="opt" value="10">
                          10
                        </option>
                        <option className="opt" value="30">
                          30
                        </option>
                        <option className="opt" value="50">
                          50
                        </option>
                        <option className="opt" value="70">
                          70
                        </option>
                        <option className="opt" value="100">
                          100
                        </option>
                        <option className="opt" value="1">
                          1
                        </option>
                      </select>
                    </form>
                  </div>
                </div>

                {/* 
                <div className=" row comparision-form-row2">
                  <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <input
                      type="text"
                      className="input-search"
                      placeholder="Search..."
                    />
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <form>
                      <select name="sort" className="dropdowns">
                        <option id="opt" value="Sort by Title">
                          Sort by Title
                        </option>
                        <option id="opt" value="title">
                          title
                        </option>
                      </select>
                    </form>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                    <form>
                      <select name="result-dropdown" className="dropdowns">
                        <option className="opt" value="Result per Page">
                          Result per Page
                        </option>
                        <option className="opt" value="10">
                          10
                        </option>
                        <option className="opt" value="30">
                          30
                        </option>
                        <option className="opt" value="50">
                          50
                        </option>
                        <option className="opt" value="70">
                          70
                        </option>
                        <option className="opt" value="100">
                          100
                        </option>
                      </select>
                    </form>
                  </div>
                </div> */}
              </div>
              <div className="container2">
                <div className="row comparision-form-row3">
                  <div className="row col-md-12 col-lg-12 col-xl-12">
                    <div className="comparision-form-row3_col col-md-2 col-lg-2 col-xl-2">
                      <h6>PROVIDER</h6>
                    </div>
                    {/* <!-----com_sec1_row1_col2 end------> */}
                    <div className="comparision-form-row3_col col-md-1 col-lg-1 col-xl-1">
                      <h6>RATE</h6>
                    </div>
                    {/* <!-----com_sec1_row1_col2 end------> */}
                    <div className="comparision-form-row3_col col-md-2 col-lg-2 col-xl-2">
                      <h6>AMOUNT RECIEVABLE</h6>
                    </div>
                    {/* <!-----com_sec1_row1_col3 end------> */}
                    <div className="comparision-form-row3_col col-md-1 col-lg-1 col-xl-1">
                      <h6>FEE</h6>
                    </div>
                    {/* <!-----com_sec1_row1_col4 end------> */}
                    <div className="comparision-form-row3_col col-md-2 col-lg-2 col-xl-2">
                      <h6>TRANSFER TIME</h6>
                    </div>
                    <div className="comparision-form-row3_col col-md-2 col-lg-2 col-xl-2">
                      <h6>Tax</h6>
                    </div>
                    {/* <!-----com_sec1_row1_col5 end------> */}
                    <div className="comparision-form-row3_col col-md-2 col-lg-2 col-xl-2">
                      <h6>WEBSITE</h6>
                    </div>
                    {/* <!-----com_sec1_row1_col6 end------> */}
                    <div className="comparision-form-row3_col col-md-2 col-lg-2 col-xl-2">
                      <h6>COMPARE TABLE</h6>
                    </div>
                    {/* <!-----com_sec1_row1_col7 end------> */}
                  </div>
                </div>
              </div>
              {/* <!---- close container-----> */}
              {/* {this.state.arr.map((value, table) => {
                return <Comparision_table key={table} data={value} />;
              })} */}
              {this.props.isLoading ? (
                <center>
                  <Dots color="red" size="100px" />
                </center>
              ) : //  <Loader
              //     type="Oval"
              //     color="#00BFFF"
              //     height={150}
              //     width={1500}
              //     timeout={3000} //3 secs
              //   />
              // <center>
              //   <b>Loading ...</b>
              // </center>
              null}
              {this.props.data &&
              !this.props.isLoading &&
              this.props.data != "No Record Found" ? (
                this.props.data.map((value, table) => {
                  return (
                    <Comparision_table
                      key={table}
                      data={value}
                      history={this.props.history}
                    />
                  );
                })
              ) : (
                <center>
                  {!this.props.isLoading ? <b>No Record Found</b> : null}
                </center>
              )}
            </div>
          </div>

          <Instructions
            data={this.state.Instructions}
            content={this.props.contentData}
          />

          <Map data={this.state.Map} />
        </Zoom>
        <Footer history={this.props.history} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.Comparisons.isLoading,
    data: state.Comparisons.comparisons,
    currencyData: state.Comparisons.currency,
    contentData: state.Comparisons.content,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: (obj) => dispatch(Comparisons.Comparisons(obj)),
  currency: () => dispatch(Comparisons.currency()),
  content: () => dispatch(content()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Comparision);
