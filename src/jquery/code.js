$(document).ready(function(){
	/*//////////////////////////////////////////////////////////////////////////*/
	/*/////////////////////////////////////News + Articles + Blogs STARTS STARTS/////////////////////////////////////*/
	/*//////////////////////////////////////////////////////////////////////////*/
	$('#newsb').hide();
	$('#articlesa').hide();
	$('#blogsa').hide();

	$('#articlesTab').hide();
	$('#blogsTab').hide();

	$('#newsb').click(function(){
		$('#newsb').hide();
		$('#newsa').show();
		$('#articlesa').hide();
		$('#articlesb').show();
		$('#blogsa').hide();
		$('#blogsb').show();

		$('#articlesTab').hide();
		$('#blogsTab').hide();
		$('#newsTab').show();
	});

	$('#articlesb').click(function(){
		$('#newsa').hide();
		$('#newsb').show();
		$('#articlesb').hide();
		$('#articlesa').show();
		$('#blogsa').hide();
		$('#blogsb').show();

		$('#newsTab').hide();
		$('#blogsTab').hide();
		$('#articlesTab').show();
	});

	$('#blogsb').click(function(){
		$('#newsa').hide();
		$('#newsb').show();
		$('#articlesa').hide();
		$('#articlesb').show();
		$('#blogsb').hide();
		$('#blogsa').show();

		$('#newsTab').hide();
		$('#articlesTab').hide();
		$('#blogsTab').show();
	});
	/*//////////////////////////////////////////////////////////////////////////*/
	/*/////////////////////////////////////News + Articles + Blogs ENDS/////////////////////////////////////*/
	/*//////////////////////////////////////////////////////////////////////////*/

	/*//////////////////////////////////////////////////////////////////////////*/
	/*/////////////////////////////////////FAQs STARTS/////////////////////////////////////*/
	/*//////////////////////////////////////////////////////////////////////////*/
	$('#question1 p').slideUp();
	$('#question2 p').slideUp();
	$('#question3 p').slideUp();
	$('#question4 p').slideUp();
	$('#question5 p').slideUp();
	$('#question6 p').slideUp();
	$('#question7 p').slideUp();
	$("#question1 .plus").click(function(){
		$('#question1 p').slideToggle();
	})
	$("#question2 .plus").click(function(){
		$('#question2 p').slideToggle();
	})
	$("#question3 .plus").click(function(){
		$('#question3 p').slideToggle();
	})
	$("#question4 .plus").click(function(){
		$('#question4 p').slideToggle();
	})
	$("#question5 .plus").click(function(){
		$('#question5 p').slideToggle();
	})
	$("#question6 .plus").click(function(){
		$('#question6 p').slideToggle();
	})
	$("#question7 .plus").click(function(){
		$('#question7 p').slideToggle();
	})
	/*//////////////////////////////////////////////////////////////////////////*/
	/*/////////////////////////////////////FAQs ENDS/////////////////////////////////////*/
	/*//////////////////////////////////////////////////////////////////////////*/

	/*//////////////////////////////////////////////////////////////////////////*/
	/*/////////////////////////////////////HISTORY STARTS/////////////////////////////////////*/
	/*//////////////////////////////////////////////////////////////////////////*/
	$('#question1 .plus .img2').hide();
	$('#question2 .plus .img2').hide();
	$('#question3 .plus .img2').hide();
	$('#question4 .plus .img2').hide();
	$('#question5 .plus .img2').hide();

	$("#question1 .plus .img1").click(function(){
		$('#question1 .plus .img1').hide();
		$('#question1 .plus .img2').show();
	})
	$("#question1 .plus .img2").click(function(){
		$('#question1 .plus .img1').show();
		$('#question1 .plus .img2').hide();
	})

	$("#question2 .plus .img1").click(function(){
		$('#question2 .plus .img1').hide();
		$('#question2 .plus .img2').show();
	})
	$("#question2 .plus .img2").click(function(){
		$('#question2 .plus .img1').show();
		$('#question2 .plus .img2').hide();
	})

	$("#question3 .plus .img1").click(function(){
		$('#question3 .plus .img1').hide();
		$('#question3 .plus .img2').show();
	})
	$("#question3 .plus .img2").click(function(){
		$('#question3 .plus .img1').show();
		$('#question3 .plus .img2').hide();
	})

	$("#question4 .plus .img1").click(function(){
		$('#question4 .plus .img1').hide();
		$('#question4 .plus .img2').show();
	})
	$("#question4 .plus .img2").click(function(){
		$('#question4 .plus .img1').show();
		$('#question4 .plus .img2').hide();
	})

	$("#question5 .plus .img1").click(function(){
		$('#question5 .plus .img1').hide();
		$('#question5 .plus .img2').show();
	})
	$("#question5 .plus .img2").click(function(){
		$('#question5 .plus .img1').show();
		$('#question5 .plus .img2').hide();
	})
	/*//////////////////////////////////////////////////////////////////////////*/
	/*/////////////////////////////////////HISTORY ENDS/////////////////////////////////////*/
	/*//////////////////////////////////////////////////////////////////////////*/

	/*//////////////////////////////////////////////////////////////////////////*/
	/*/////////////////////////////////////dashboard1 STARTS/////////////////////////////////////*/
	/*//////////////////////////////////////////////////////////////////////////*/
	$(".dashboard1 .divB .divBrow1 .nav ul #li1").click(function(){
		$('.buttonSlide').css({"margin-left":"1px"});
		$('.dashboard1 .divB .divBrow1 .nav ul #li1').css({"color":"#FFFFFF"});
		$('.dashboard1 .divB .divBrow1 .nav ul #li2').css({"color":"#555555"});
		$('.dashboard1 .divB .divBrow1 .nav ul #li3').css({"color":"#555555"});
	})
	$(".dashboard1 .divB .divBrow1 .nav ul #li2").click(function(){
		$('.buttonSlide').css({"margin-left":"76px"});
		$('.dashboard1 .divB .divBrow1 .nav ul #li2').css({"color":"#FFFFFF"});
		$('.dashboard1 .divB .divBrow1 .nav ul #li1').css({"color":"#555555"});
		$('.dashboard1 .divB .divBrow1 .nav ul #li3').css({"color":"#555555"});
	})
	$(".dashboard1 .divB .divBrow1 .nav ul #li3").click(function(){
		$('.buttonSlide').css({"margin-left":"149px"});
		$('.dashboard1 .divB .divBrow1 .nav ul #li3').css({"color":"#FFFFFF"});
		$('.dashboard1 .divB .divBrow1 .nav ul #li1').css({"color":"#555555"});
		$('.dashboard1 .divB .divBrow1 .nav ul #li2').css({"color":"#555555"});
	})
		// menu bar for small screen STARTS
	$(".divBurgerMenu img").click(function(){
		$('.secondVerticalMenu').css({"display":"block"});
	})
	$(".divBurgerMenu #secondVerticalMenu").click(function(){
		$('.secondVerticalMenu').css({"display":"none"});
	})
		// menu bar for small screen ENDS
	/*//////////////////////////////////////////////////////////////////////////*/
	/*/////////////////////////////////////dashboard1 ENDS/////////////////////////////////////*/
	/*//////////////////////////////////////////////////////////////////////////*/

	/*//////////////////////////////////////////////////////////////////////////*/
	/*/////////////////////////////////////dashboard PROMOTIONS (UD) STARTS/////////////////////////////////////*/
	/*//////////////////////////////////////////////////////////////////////////*/
	$("#bigCard1").hide();
	$("#bigCard2").hide();

	$("#smallCard1Button").click(function(){
		$("#smallCard1").hide();
		$("#smallCard2").hide();
		$("#bigCard1").show();
	})
	$("#bigCard1Cross").click(function(){
		$("#bigCard1").hide();
		$("#smallCard1").show();
		$("#smallCard2").show();
	})

	$("#smallCard2Button").click(function(){
		$("#smallCard1").hide();
		$("#smallCard2").hide();
		$("#bigCard2").show();
	})
	$("#bigCard2Cross").click(function(){
		$("#bigCard2").hide();
		$("#smallCard1").show();
		$("#smallCard2").show();
	})
	/*//////////////////////////////////////////////////////////////////////////*/
	/*/////////////////////////////////////dashboard PROMOTIONS (UD) ENDS/////////////////////////////////////*/
	/*//////////////////////////////////////////////////////////////////////////*/



});








// $(document).ready(function(){
// 	$(".divNews").mouseover(function(){
// 	  this.hide();
// 	});
	
//   });

// $(document).ready(function(){
// 	$("button").mouseover(function(){
// 	  $("#div1").fadeIn();
// 	  $("#div2").fadeIn("slow");
// 	  $("#div3").fadeIn(3000);
// 	});
// 	$(".divNews").mouseover(function(){
// 		$(".divNews").fadeOut();
// 	  });
//   });