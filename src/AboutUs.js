import React, { Component } from "react";
import "./css/bootstrap.css";
import "./css/style-about.css";
import Fade from "react-reveal/Fade";
import Zoom from "react-reveal/Zoom";
import { connect } from "react-redux";
import Footer from "./components/footer";
import bg from "./img/aboutUs/group.png";
import { bindActionCreators } from "redux";
import Aboutrow_3 from "./components/Aboutrow_3";
import Aboutrow_2 from "./components/Aboutrow_2";
import Aboutrow_4 from "./components/Aboutrow_4";
import Aboutrow_5 from "./components/Aboutrow_5";
import Aboutrow_6 from "./components/Aboutrow_6";
import AboutJumbo from "./components/AboutJumbo";
import { Carousel } from "react-responsive-carousel";
import { userReviews, SetIsNavActive, getAboutHeading, getWhatwedoHeading, getMissionVisionHeading } from "./Store/actions/userAuth";
import IndexNavigation from "./components/indexNavigation";

class AboutUs extends Component {
  constructor(props) {
    super(props);

    this.state = {
      row_1: {
        heading: "ABOUT RAPID REMIT",
      },
      row_3: {
        img: require("./img/aboutUs/group-28.png"),
        heading: "Our Mission",
        paragraph:
          "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
        heading2: "Our Vision",
        paragraph2:
          "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
        img2: require("./img/aboutUs/group-30.png"),
      },
      row_2: {
        img1: require("./img/aboutUs/Group-24.png"),
        img2: require("./img/aboutUs/Group-22.png"),
        img3: require("./img/aboutUs/Group-23.png"),
        heading: "About Us ",
        heading2: "Regions first money transfer comparison service.",
      },
      row_4: {
        heading: "WHAT WE DO",
        heading2: "Putting you in control of your money transfer",
        paragraph:
          "At RapidRemit.com we set about tackling this problem with a core set of values and principles underpinning our approach " +
          <br /> +
          "Only obtain information in a live or real time manner" +
          <br /> +
          "Collect all costs and data which can affect a decision" +
          <br /> +
          "Present the data in a manner which is clear and transparent to users and allows for a fair comparison of providers" +
          <br /> +
          "The result of this project is what you can see in our website today - but it won’t stop here! The concept will grow, with us adding new features and ways of making even better decisions at a time that suits you.",
      },
    };
  }
  componentWillMount() {
    this.props.userReview();
  }
  componentDidUpdate(preProps) {
    if (preProps.aboutusheading !== this.props.aboutusheading) {
      // console.log(this.props.ournetworkheading)
      this.setState({
        aboutheading: this.props.aboutusheading?.sub_heading,
        abouttitle: this.props.aboutusheading?.heading,
        abouttext: this.props.aboutusheading?.text,
      })
    }
    if (preProps.whatwedoheading !== this.props.whatwedoheading) {
      // console.log(this.props.ournetworkheading)
      this.setState({
        whatheading: this.props.whatwedoheading?.sub_heading,
        whattitle: this.props.whatwedoheading?.heading,
        whattext: this.props.whatwedoheading?.text,
      })
    }
    if (preProps.missionvisionheading !== this.props.missionvisionheading) {
      // console.log(this.props.ournetworkheading)
      this.setState({
        ourVisionHTML: this.props.missionvisionheading?.our_vision,
        ourMissionHTML: this.props.missionvisionheading?.our_mission,
      })
    }
  }
  componentDidMount() {
    this.props.SetIsNavActive("/AboutUs")
    this.props.getMissionVisionHeading()
    this.props.getAboutHeading()
    this.props.getWhatwedoHeading()
    if (
      window.location.pathname == "/"
      // window.location.pathname == "/AboutUs"
    ) {
      //  console.log("ASsads")
      document.getElementById("routerNav").style.display = "none";
      //  document.getElementById('navigationIndex').style.display="block"
    } else {
      //  console.log("nhi hy")
      document.getElementById("routerNav").style.display = "block";
      //  document.getElementById('navigationIndex').style.display="none"
    }
  }

  render() {
    const { aboutheading, abouttitle, abouttext, whattext, whattitle, whatheading, ourMissionHTML, ourVisionHTML } = this.state
    return (
      <div style={{ backgroundColor: "white" }}>
        {/* <Aboutrow_2 data={this.state.row_2} /> */}

        <AboutJumbo history={this.props.history} aboutdata={{ aboutheading, abouttitle, abouttext }} />
        <Fade left>
          <Aboutrow_4 data={this.state.row_4} whatdata={{ whattext, whattitle, whatheading }}/>
        </Fade>
        <Fade right>
          <Aboutrow_3 data={this.state.row_3} ourVisionHTML={ourVisionHTML} ourMissionHTML={ourMissionHTML}  />
        </Fade>
        {/* <Zoom>
          <Aboutrow_5 />
        </Zoom> */}
        {/* <Zoom>
          <Aboutrow_6 />
        </Zoom> */}

        {/* <div className="container-fluid divBottom"> */}
        {" "}
        {/* copied from 'LiveRates' file, that's why name is not relevent */}
        {/* <div className="row companyOverviewContentRow4 ml-0 mr-0"> */}
        {/* copied from 'LiveRates' file, that's why name is not relevent */}
        {/* <div className="container1"> */}
        {/* <Carousel
                showIndicators={false}
                showStatus={false}
                axis="horizontal"
                autoPlay={true}
              >
                {this.props.data && this.props.data.length > 0
                  ? this.props.data.map((d) => (
                      <div className="row john-row">
                        <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
                        <div className="mr-john col-xl-4 col-lg-4 col-md-2 col-sm-2 col-2">
                          <img
                            src={require("./img/aboutUs/invertedcoma.png")}
                            className="inverted-coma"
                          />
                          <div className="pic1">
                            <div className="pic-i"></div>
                            <div className="pic-j">
                              <img src={d.img} className="john-img" />
                            </div>
                          </div>
                        </div>
                        <div className="mr-john john-detail col-xl-5 col-lg-5 col-md-9 col-sm-9 col-8 ">
                          <h5>{d.name}</h5>
                          <p id="para2">{d.text}</p>
                        </div>
                        <div className="mr-john col-xl-1 col-lg-1 col-md-1 col-sm-1 col-2">
                          <img
                            src={require("./img/aboutUs/invertedcoma2.png")}
                            className="inverted-coma2"
                          />
                        </div>
                        <div className="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
                      </div>
                    ))
                  : ""}
              </Carousel>
            </div>
          </div>
        </div> */}
        <Footer history={this.props.history} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    reviewsData: state.Offers.reviews,
    data: state.Offers.user_review,
    aboutusheading: state.FAQS.aboutusheading,
    whatwedoheading: state.FAQS.whatwedoheading,
    missionvisionheading: state.FAQS.missionvisionheading
  };
};

const mapDispatchToProps = (dispatch) => ({
  userReview: bindActionCreators(userReviews, dispatch),
  SetIsNavActive: (data) => dispatch(SetIsNavActive(data)),
  getAboutHeading: () => dispatch(getAboutHeading()),
  getWhatwedoHeading: () => dispatch(getWhatwedoHeading()),
  getMissionVisionHeading: () => dispatch(getMissionVisionHeading())


});

export default connect(mapStateToProps, mapDispatchToProps)(AboutUs);
