import firebase from 'firebase/app';
import 'firebase/storage';

const firebaseConfig = {
  // apiKey: "AIzaSyBAj0N336MQfcYKpqQKCeenHzgoOt99ZMQ",
  // authDomain: "rapidremit-41516.firebaseapp.com",
  // databaseURL: "https://rapidremit-41516.firebaseio.com",
  // projectId: "rapidremit-41516",
  // storageBucket: "rapidremit-41516.appspot.com",
  // messagingSenderId: "786924713940",
  // appId: "1:786924713940:web:22a26f9604f1c1a52e8841",
  // measurementId: "G-J40FP4XXT3",
  apiKey: 'AIzaSyBclYUnG9IJOSaNrN_2Xx_h4BXsYZh_K44',
  authDomain: 'rapidremit-e8df5.firebaseapp.com',
  databaseURL: 'https://rapidremit-e8df5.firebaseio.com',
  projectId: 'rapidremit-e8df5',
  storageBucket: 'rapidremit-e8df5.appspot.com',
  messagingSenderId: '133015792295',
  appId: '1:133015792295:web:4f304f52d20eb4776c0982',
  measurementId: 'G-0DHB7DWR4S',
};

firebase.initializeApp(firebaseConfig);
//firebase.analytics();
const storage = firebase.storage();

export { storage, firebase as default };
