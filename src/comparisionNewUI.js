import React, { Component } from 'react';
import './css/bootstrap.css';
import './css/style-footer.css';
import './css/style-comprision.css';
import Footer from './components/footer';
import RedForm from './components/RedForm';
import ComparisionTableForm from './components/ComparisionTableForm';
import Comparision_table from './components/comparision_table';
import Map from './components/Map';
import Zoom from 'react-reveal/Zoom';
import Fade from 'react-reveal/Fade';
import Instructions from './components/Instructions';
import config from 'react-reveal/globals';
import { connect } from 'react-redux';
import { Comparisons, content, empty, SetIsNavActive, getComparisonHeading } from './Store/actions';
import Loader from 'react-loader-spinner';
import comparison_instance from './instance/comparison_instance';
import ContactUsMap from './components/ContactUsMap';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { Link, withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import ComparisionTableCard from "./components/ComparisionTableCard"

var geocoder = require('geocoder');

config({ ssrFadeout: true });

class Comparision extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locationLink: '',
      isOldestFirst: true,
      Map: {
        heading: 'Locate Near Exchange',
        img: require('./img/location-icon.png'),
      },
      Instructions: {
        heading: 'Instructions',
        paragraph: 'Sending money abroad could cost you a lot, especially if you aren’t aware of the hidden fees. Money transfer companies and banks earn money by not only charging you a transfer fee, but also usually by offering you an exchange rate with a hidden markup. You could save a lot of money by comparing exchange rates and transfer fees in real-time. With our money transfer comparison tool, you’ll find the best way to send money internationally in just a few clicks.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat, consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse.',
        paragraph1: 'Simply complete how much you want to send and currency to be received. We compare and review over all money transfer companies in your region and provide you with comapriosn of the least cost, fastest and most convinientDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat.',
        img: require('./img/Instruction.png'),
      },

      limit: 5,
      sort: 1,
      name: '',
      empty: true,
      chooseLocation: false,
      startedPageCount: 0
    };
    comparison_instance.header = this;
  }
  handleDropdown = (name, e) => {
    try {
      const { prevParaIndex } = this.state
      if (prevParaIndex !== undefined && prevParaIndex !== "" && prevParaIndex !== null) {
        let prevPara = document.querySelector(`#pagination-li-${prevParaIndex} p`)
        if (prevPara?.classList?.contains("page-active")) {
          prevPara.classList.remove("page-active");
        }
      }
      this.setState({
        [name]: e.target.value,
        startedPageCount: 0,
      })
    } catch (e) {
      // console.log(e)
    }
  };
  btnClick = (data) => {
    // console.log('button', data, this.props);
    this.props.addToTable(data, this.props.history);
  }

  addFavourite = (id) => {
    if (localStorage.getItem('token')) {
      let user_id = JSON.parse(localStorage.getItem('UserData'))._id;
      // console.log(
      //   'user_id',
      //   user_id,
      //   JSON.parse(localStorage.getItem('token'))
      // );

      this.props.addFavourite(
        user_id,
        id,
        JSON.parse(localStorage.getItem('token'))
      );
    } else {
      window.location = '/registration';
    }
  }
  removeFavourite = (id) => {
    if (localStorage.getItem('token')) {
      let user_id = JSON.parse(localStorage.getItem('UserData'))._id;
      this.props.removeFavourite(
        user_id,
        id,
        JSON.parse(localStorage.getItem('token'))
      );
    } else {
      window.location = '/registration';
    }
  }

  componentWillMount() {
    if (localStorage.getItem('UserData')) {
      let id = JSON.parse(localStorage.getItem('UserData'))._id;
      // console.log('iiiid', id);
      this.props.getFavourite(id);
    }

    this.props.currency();
    this.props.content();
  }

  componentWillUnmount() {
    // console.log('componentWillUnmount', this.props.history);
    if (this.props.history.location.pathname != '/companyoverview') {
      // console.log('Unmount', this.state.empty);
      this.props.empty();
      this.props.ComparisionFormData(undefined)
    }
  }

  componentDidUpdate(preProps) {
    if (preProps.comparisonheading !== this.props.comparisonheading) {
      console.log(this.props.comparisonheading)
      this.setState({
        InstructionsContent: this.props.comparisonheading?.text
      })
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    this.props.getComparisonHeading()
    this.props.SetIsNavActive("/comparison")
    if (window.location.pathname == '/') {
      //  console.log("ASsads")
      document.getElementById('routerNav').style.display = 'none';
      //  document.getElementById('navigationIndex').style.display="block"
    } else {
      //  console.log("nhi hy")
      document.getElementById('routerNav').style.display = 'block';
      //  document.getElementById('navigationIndex').style.display="none"
    }
  }

  sort = (key) => {
    try {
      let list = this.state.data;
      this.state.isOldestFirst = !this.state.isOldestFirst;
      if (list) {
        switch (key) {
          case 'RATE': {
            let records = this.state.isOldestFirst
              ? list.sort((a, b) => a.rate - b.rate)
              : list.sort((a, b) => b.rate - a.rate);
            this.setState({
              data: records,
              sortOrder: key,
            });
            // console.log('RATES DATA componentWillReceiveProps SORT RATE', records);
          }
          case 'AMOUNT RECEIVABLE': {
            let records = this.state.isOldestFirst
              ? list.sort((a, b) => a.amount_recievable - b.amount_recievable)
              : list.sort((a, b) => b.amount_recievable - a.amount_recievable);
            this.setState({
              data: records,
              sortOrder: key,
            });
            // console.log('RATES DATA componentWillReceiveProps SORT RATE', records);
          }
          case 'FEE': {
            let records = this.state.isOldestFirst
              ? list.sort((a, b) => a.payment_fees - b.payment_fees)
              : list.sort((a, b) => b.payment_fees - a.payment_fees);
            this.setState({
              data: records,
              sortOrder: key,
            });
            // console.log('RATES DATA componentWillReceiveProps SORT RATE', records);
          }
          case 'TAX': {
            let records = this.state.isOldestFirst
              ? list.sort((a, b) => a.tax_deducted - b.tax_deducted)
              : list.sort((a, b) => b.tax_deducted - a.tax_deducted);
            this.setState({
              data: records,
              sortOrder: key,
            });
            // console.log('RATES DATA componentWillReceiveProps SORT RATE', records);
          }
        }
      }
    }
    catch (e) {
      // console.log(e)
    }
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      data: nextProps.data,
    });

    // console.log('RATES DATA componentWillReceiveProps', this.props.data);
  }

  toggleChooseLocation = (location) => {
    // console.log('LOCATION WE PICKED2', location[0].location.coordinates[1]);
    // const locationLink = `https://maps.google.com/maps?api=1&q=${location.location.coordinates[0].$numberDecimal},${location.location.coordinates[1].$numberDecimal}&hl=es&z=14&amp;output=embed`;
    const locationLink = `https://maps.google.com/maps?q=${location[0].location.coordinates[1]},${location[0].location.coordinates[0]}&hl=en&z=14&output=embed`;
    // console.log('LOCATION WE PICKED3', locationLink);

    this.setState({
      chooseLocation: !this.state.chooseLocation,
      location: location,
      locationLink: locationLink,
    });
  };
  onLoadMoreHandler = (startPoint, event, index) => {
    // console.log("startPoint, event, index", startPoint, event, index)
    try {
      const { prevParaIndex } = this.state
      let currentPara = event.target;

      if (prevParaIndex !== undefined && prevParaIndex !== "" && prevParaIndex !== null) {
        let prevPara = document.querySelector(`#pagination-li-${prevParaIndex} p`)
        if (prevPara?.classList?.contains("page-active")) {
          prevPara.classList.remove("page-active");
        }
      }

      currentPara.className += " page-active"
      this.setState({
        startedPageCount: startPoint,
        prevParaIndex: index
      })
    } catch (e) {
      // console.log(e)
    }
    // setTimeout(() => {
    //   this.setState({
    //     loadCount: this.state.loadCount + 6,
    //     isLoadMoreLoading: false
    //   })
    // }, 2000);

  }
  pagination = (data) => {
    // console.log("DataPagination", data)
    try {
      const { limit } = this.state
      let paginationCount = Math.ceil(data.length / limit);
      var rows = [];

      for (let i = 0; i < paginationCount; i++) {
        rows.push(<a href="#comparision-list" className="anchor-decotation"><li id={`pagination-li-${i}`} onClick={(e) => this.onLoadMoreHandler(i * limit, e, i)} style={{ cursor: "pointer" }} className="page-item"><p className="page-link">{i + 1}</p></li></a>);
      }
      return <ul id="paginationUl" className="pagination justify-content-end" >
        {rows}
      </ul>
    } catch (e) {
      // console.log(e)
    }
  }
  render() {
    // console.log('thisISER', this.props.data, this.props.tableData, this.props);
    console.log("", this.state.InstructionsContent)
    let fav_mark = [];
    this.props.favData.map((f) => {
      fav_mark = [...fav_mark, f.fav_id];
    });

    let table_mark = [];
    this.props.tableData.map((f) => {
      table_mark = [...table_mark, f._id];
    });
    function cs(data) {
      let currency_sign = '';
      if (data == 'USD' || data == 'AUD' || data == 'CAD') {
        currency_sign = '$';
      } else if (data == 'EGP') {
        currency_sign = 'E';
      } else if (data == 'EUR') {
        currency_sign = '€';
      } else if (data == 'GBP') {
        currency_sign = '£';
      } else if (data == 'INR') {
        currency_sign = '₹';
      } else if (data == 'JOD') {
        currency_sign = 'د.ا';
      } else if (data == 'JOD') {
        currency_sign = 'د.ا';
      } else if (data == 'LKR') {
        currency_sign = 'රු';
      } else if (data == 'NPR') {
        currency_sign = 'रू';
      } else if (data == 'PHP') {
        currency_sign = '₱';
      } else if (data == 'PKR') {
        currency_sign = '₨';
      }
      return currency_sign;
    }

    // console.log('coooom', this.state.data);
    // console.log('this.props?.location?.comparisionData', this.props?.location?.comparisionData);
    let cData = this.props.currencyData;
    const chooseLocation = this.state.chooseLocation;
    return (
      <div className='coooom'>
        {/* {this.props.tableData &&  this.props.tableData.length > 0 &&    */}
        <Fade
          bottom
          when={this.props.tableData && this.props.tableData.length > 0}
        >
          <div className='bottom-div'>
            <div className='d-flex justify-content-start'>
              {this.props.tableData &&
                this.props.tableData.map((partner, index) => {
                  return (
                    <div key={index} className='tab'>
                      <p className='d-inline-block'>
                        {partner && partner.partner_name.toUpperCase()}
                      </p>
                      <a>
                        <i
                          style={{ cursor: 'pointer' }}
                          onClick={() => this.props.delTableIndex(index)}
                          className='fa fa-times ml-3'
                        ></i>
                      </a>
                    </div>
                  );
                })}
              {/* */}
              {/* <div className="tab">
                      <p className="d-inline-block">Lorem isum dolor sit</p>
                      <i className="fa fa-times ml-3"></i>
                    </div>
                    <div className="tab">
                      <p className="d-inline-block">Lorem isum dolor sit</p>
                      <i className="fa fa-times ml-3"></i>
                    </div> */}
            </div>
            <div>
              <a
                className="comparision-compare-text-show"
                onClick={() => this.props.history.push('/companyoverview')}
                href='#'
                type='button'
              >
                Go to Comparision
              </a>
              <a
                className="comparision-compare-icon-show"
                onClick={() => this.props.history.push('/companyoverview')}
                href='#'
                type='button'
              >
                Compare
                {/* <i className="fas fa-star"></i> */}
              </a>
            </div>
          </div>
        </Fade>
        {/* } */}
        {chooseLocation ? (
          <div className='location-viewer-wrapper'>
            <div className='location-viewer'>
              <div className='d-flex justify-content-between'>
                <h5 className='instruct'>
                  {this.state.location && this.state.location.address}
                </h5>
                <i
                  className='fa fa-times'
                  onClick={() => this.toggleChooseLocation(this.state.location)}
                ></i>
              </div>
              <div class='mapouter'>
                <div class='gmap_canvas'>
                  {this.state.locationLink !== '' && (
                    <iframe
                      width='760'
                      height='480'
                      id='gmap_canvas'
                      src={this.state.locationLink} //"https://maps.google.com/maps?q=jedda&t=&z=13&ie=UTF8&iwloc=&output=embed"
                      frameborder='0'
                      scrolling='no'
                      marginheight='0'
                      marginwidth='0'
                    ></iframe>
                  )}
                </div>
              </div>
            </div>
          </div>
        ) : (
            ''
          )}
        <Zoom ssrFadeout Left>
          <div className='comparision-main-div'>
            <div className='comparision'>
              <div className='main-container'>
                <div className='container3'>
                  {/* <RedForm
                    isLoading={
                      this.props.isLoading ? this.props.isLoading : false
                    }
                    getData={(abc) => this.props.getData(abc)}
                  />{' '} */}


                </div>
                <div id="comparision-list" className='container3'>
                  <ComparisionTableForm
                    isLoading={
                      this.props.isLoading ? this.props.isLoading : false
                    }
                    home={this.props?.comparisonsFormData}
                    getData={(abc) => this.props.getData(abc)}
                  />{' '}
                  <div className=' row comparision-form-row2'>

                    {/* <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12'>
                      <div className='input-search-div'>
                        <input
                          type='text'
                          className='input-search'
                          placeholder='Search...'
                          value={this.state.name}
                          onChange={(e) => {
                            this.setState({ name: e.target.value });
                          }}
                        />
                        <div className='search-icon-div'></div>
                      </div>
                    </div> */}
                    {/* <div className="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                                    <form>
                                      <select
                                        name="sort"
                                        className="dropdownsC"
                                        onChange={e => {
                                          this.handleDropdown("sort", e);
                                        }}
                                      >
                                        <option id="opt" value="Sort by Title">
                                          Sort by Title
                                        </option>
                                        <option id="opt" value="title">
                                          title
                                        </option>
                                      </select>
                                    </form>
                                  </div> */}
                    <div className='col-xl-2 col-lg-3 col-md-4 col-sm-5 col-5 ml-auto'>
                      <form>
                        <select
                          name='result-dropdown'
                          placeholder="Result per Page"
                          className='dropdownsC cal-dropdown1 center-label1 bg-white dropdownInput'
                          value={this.state.limit}
                          // defaultValue={perPageCount}

                          onChange={(e) => {
                            this.handleDropdown('limit', e);
                          }}
                        >
                          {/* <option value={5}>Result per Page</option> */}
                          {/* <option className='opt' value={2}>
                            2
                          </option> */}
                          <option className='opt' value={5}>
                            5
                          </option>

                          <option className='opt' value={10}>
                            10
                          </option>
                          <option className='opt' value={15}>
                            15
                          </option>
                          {/* <option className='opt' value='70'>
                            70
                          </option>
                          <option className='opt' value='100'>
                            100
                          </option> */}
                        </select>
                      </form>
                    </div>
                  </div>
                </div>
                <div
                  className='container3'
                  style={{ backgroundColor: 'transparent', marginTop: 30 }}
                >

                  <div className='row'>
                    {this.state.data && Array.isArray(this.state.data) && this.state.data.length > 0 &&
                      !this.state.isLoading &&
                      this.state.data != 'No Record Found' ? (
                        this.state.data && Array.isArray(this.state.data) && this.state.data.length > 0 && this.state?.data?.map((value, ind) => {
                          const { startedPageCount, limit } = this.state
                          let endedPoiint = +startedPageCount + +limit;
                          if (ind >= startedPageCount && ind < endedPoiint) {
                            // console.log('nice', value);
                            return (
                              <ComparisionTableCard
                                toggleChooseLocation={this.toggleChooseLocation}
                                data={value}
                                ind={ind}
                                history={this.props.history}
                                fav_mark={fav_mark}
                                table_mark={table_mark}
                                btnClick={this.btnClick}
                                addFavourite={this.addFavourite}
                                removeFavourite={this.removeFavourite}

                              />)
                          }
                        }
                        ))
                      :
                      (
                        <>
                          <div className='row no-gutters' style={{ width: "100%", display: "flex", justifyContent: "center", marginBottom: 30, boxShadow: "3px 5px 11px 3px #e2e2e2e3" }}>
                            <h4 className='p-4'> No Amount Selected</h4>
                          </div>

                          {/*                               
                      <center>
                        {" "}
                        {!this.props.isLoading
                          ? // <b> No Record Foundasdasd </b>
                            null
                          : null}{" "}
                      </center> */}
                        </>
                      )
                    }

                    {/* <div className="card" style={{ width: "100%" }}>

                      <div className="row no-gutters justify-content-center" style={{ paddingTop: 20 }}>
                        <div className="col-sm-11">
                          <div className="row no-gutters justify-content-end ">
                            <img className="card-img" src={require("./img/comparision/com-heart-icon.png")} style={{ width: '20px' }} alt="" />
                          </div>
                          <div className="row no-gutters d-flex justify-content-center align-items-center">
                            <div className="col-sm-3">
                              <div className="row no-gutters d-flex justify-content-center" style={{ height: 70 }}>
                                <img className="card-img" src={require("./img/our-network/MoneyGram g.png")} alt="" />
                              </div>
                              <div className="row no-gutters d-flex justify-content-center" style={{ marginTop: 20 }}>
                                <p style={{ color: "#a3a3a3", textDecoration: "underline" }}>visit website</p>
                              </div>
                            </div>
                            <div className="col-sm-3">
                              <div className="row no-gutters d-flex justify-content-center" style={{ height: 70 }}>
                                <div className="col-sm-12">
                                  <div className="row no-gutters">
                                    <p style={{ color: "#a3a3a3", marginBottom: 0 }}>Rate USD 50</p>
                                  </div>
                                  <div className="row no-gutters" >
                                    <p style={{ color: "black", fontSize: 24, fontWeight: 700, marginBottom: 0 }}>200,000/-</p>
                                  </div>
                                  <div className="row no-gutters">
                                    <p style={{ color: "#a3a3a3", marginBottom: 0 }}>Amount Receivable</p>
                                  </div>
                                </div>
                              </div>
                              <div className="row no-gutters d-flex" style={{ marginTop: 20 }}>
                                <p style={{ color: "#a3a3a3" }}>Service Fee: AED 57.75</p>
                              </div>
                            </div>
                            <div className="col-sm-5">
                              <div className="row no-gutters d-flex justify-content-center" style={{ height: 70 }}>
                                <div className="col-sm-3">
                                  <div className="row no-gutters d-flex justify-content-center flex-column" >
                                    <div className="col-sm-12">
                                      <div className="row no-gutters d-flex justify-content-center" >
                                        <div className="col-sm-12">
                                          <div className="row no-gutters d-flex justify-content-center" >
                                            <img className="card-img" src={require("./img/comparision/transfer-time-icon.png")} style={{ width: '40px' }} alt="" />
                                          </div>
                                        </div>
                                        <div className="col-sm-12">
                                          <div className="row no-gutters d-flex justify-content-center" >
                                            <p style={{ color: "black", marginBottom: 0, fontSize: 12, fontWeight: 600, content:"justify" }}> 3-5</p>
                                          </div>
                                        </div>
                                        <div className="col-sm-12">
                                          <div className="row no-gutters d-flex justify-content-center" >
                                            <p style={{ color: "black", marginBottom: 0, fontSize: 12, fontWeight: 600 }}>DAYS</p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-sm-3">
                                  <div className="row no-gutters d-flex justify-content-center flex-column" >
                                    <div className="col-sm-12">
                                      <div className="row no-gutters d-flex justify-content-center" >
                                        <img className="card-img" src={require("./img/comparision/view-locations-icon.png")} style={{ width: '40px', height: "35px" }} alt="" />
                                      </div>
                                    </div>
                                    <div className="col-sm-12">
                                      <div className="row no-gutters d-flex justify-content-center" >
                                        <p style={{ color: "black", marginBottom: 0, fontSize: 10, fontWeight: 600 }}>View Locations</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-sm-5">
                                  <div className="row no-gutters d-flex justify-content-center flex-column" >
                                    <div className="col-sm-12">
                                      <div className="row no-gutters d-flex justify-content-center" >
                                        <img className="card-img" src={require("./img/comparision/check-outlined-icon.png")} style={{ width: '20px' }} alt="" />
                                      </div>
                                    </div>
                                    <div className="col-sm-12">
                                      <div className="row no-gutters d-flex justify-content-center" >
                                        <p style={{ color: "#a3a3a3", marginBottom: 0 }}>Add to Comparison</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="row no-gutters d-flex justify-content-space-between" style={{ marginTop: 20 }}>
                                <div className="col-sm-6">
                                  <p style={{ color: "#a3a3a3" }}>Receiving Fee: AED 157.75</p>
                                </div>
                                <div className="col-sm-6">
                                  <div className="row no-gutters d-flex justify-content-end" >
                                    <p style={{ color: "black", textDecoration: "underline", cursor: "pointer" }}>View Detail</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div> */}
                    <div style={{ width: "100%" }}>
                      {this.state.data &&
                        !this.state.isLoading &&
                        this.state.data != 'No Record Found' &&
                        this.props.data.length > 0 && this.pagination(this.props.data)}
                    </div>
                  </div>{' '}
                </div>
                {/* {this.state.arr.map((value, table) => {
                                return <Comparision_table key={table} data={value} />;
                              })} */}{' '}
                {this.props.isLoading ? (
                  <center>
                    <Dots color='red' size='100px' />
                  </center>
                ) : //  <Loader
                  //     type="Oval"
                  //     color="#00BFFF"
                  //     height={150}
                  //     width={1500}
                  //     timeout={3000} //3 secs
                  //   />
                  // <center>
                  //   <b>Loading ...</b>
                  // </center>
                  null}{' '}
              </div>{' '}
            </div>
            <Instructions
              data={this.state.Instructions}
              content={this.props.contentData}
              HTMLContent={this.state.InstructionsContent}
            />
            {/* <ContactUsMap /> */}

            <div className='row Map ml-0 mr-0'>
              {/* <div className="locate col-xl-1 col-lg-1 col-md-12 col-sm-12 col-12">
                    <h1></h1>
                </div> */}
              {/* <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                <div className='mapouter'>
                  <div className='gmap_canvas'>
                    <iframe
                      id='gmap_canvas'
                      src='https://maps.google.com/maps?q=university%20of%20Karachi&t=&z=13&ie=UTF8&iwloc=&output=embed'
                      frameborder='0'
                      scrolling='no'
                      marginheight='0'
                      marginwidth='0'
                    ></iframe>
                  </div>
                  <img
                    src={require('./img/locate-icon.png')}
                    className='location'
                  />
                  <img
                    src={require('./img/locate-icon.png')}
                    className='location2'
                  />
                </div>
              </div> */}
            </div>

            {/* <Map  data={this.state.Map} />{" "} */}
          </div>
        </Zoom > { ' '}
        < Footer history={this.props.history} />
      </div >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.Comparisons.isLoading,
    data: state.Comparisons.comparisons,
    currencyData: state.Comparisons.currency,
    contentData: state.Comparisons.content,
    favData: state.Favourite.allData,
    tableData: state.Comparisons.table,
    comparisonsFormData: state.Comparisons.comparisonsFormData,
    comparisonheading:state.FAQS.comparisonheading
  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: (obj) => dispatch(Comparisons.Comparisons(obj)),
  currency: () => dispatch(Comparisons.currency()),
  content: () => dispatch(content()),
  addToTable: bindActionCreators(Comparisons.table, dispatch),
  addFavourite: bindActionCreators(Comparisons.addFavourite, dispatch),
  removeFavourite: bindActionCreators(Comparisons.removeFavourite, dispatch),
  getFavourite: bindActionCreators(Comparisons.getFavourite, dispatch),
  addToTable: bindActionCreators(Comparisons.table, dispatch),
  empty: bindActionCreators(empty, dispatch),
  delTableIndex: bindActionCreators(Comparisons.delTableIndex, dispatch),
  ComparisionFormData: (obj) => dispatch(Comparisons.ComparisionFormData(obj)),
  SetIsNavActive: (data) => dispatch(SetIsNavActive(data)),
  getComparisonHeading: () => dispatch(getComparisonHeading()),

});

export default connect(mapStateToProps, mapDispatchToProps)(Comparision);
