import axios from 'axios';
import { handleApiErrors } from './api-errors';
//const API_URL = " https://rapid-remit.herokuapp.com/";
// const API_URL = 'https://rapidremit1.herokuapp.com/';

const API_URL = "http://localhost:4000/";
function processRequest(request) {
  return request
    .then((json) => {
      if (json.ok == true) {
      } else {
        return json;
      }
    })
    .catch((error) => {
      throw error;
    });
}

export function Get(path, obj) {
  const API_REQ_URL = API_URL + path;
  // console.log('GET CALL', API_REQ_URL);
  const request = axios.get(API_REQ_URL);
  return processRequest(request);
}

export function Post(path, obj, token) {
  const API_REQ_URL = API_URL + path;
  // console.log('------------');
  // console.log('POST CALL', API_REQ_URL);
  const request = axios.post(API_REQ_URL, obj, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: token,
    },
  });
  return processRequest(request);
}

export function Delete(path, obj, token) {
  const API_REQ_URL = API_URL + path;
  // console.log('------------');
  // console.log({ path, obj, token });
  // console.log('POST CALL', API_REQ_URL);
  const request = axios.delete(API_REQ_URL,  {
    headers: {
      'Content-Type': 'application/json',
      "Authorization": token,
    },
    data: obj
  });
  // console.log(request.json())
  return processRequest(request);
}
