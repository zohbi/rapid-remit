import React, { Component } from 'react';
import './css/bootstrap.css';
import './css/style-footer.css';
import './css/style-comprision.css';
import Footer from './components/footer';
import RedForm from './components/RedForm';
import ComparisionTableForm from './components/ComparisionTableForm';
import Comparision_table from './components/comparision_table';
import Map from './components/Map';
import Zoom from 'react-reveal/Zoom';
import Fade from 'react-reveal/Fade';
import Instructions from './components/Instructions';
import config from 'react-reveal/globals';
import { connect } from 'react-redux';
import { Comparisons, content, empty, SetIsNavActive } from './Store/actions';
import Loader from 'react-loader-spinner';
import comparison_instance from './instance/comparison_instance';
import ContactUsMap from './components/ContactUsMap';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import { Link, withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
var geocoder = require('geocoder');

config({ ssrFadeout: true });

class Comparision extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locationLink: '',
      isOldestFirst: true,
      Map: {
        heading: 'Locate Near Exchange',
        img: require('./img/location-icon.png'),
      },
      Instructions: {
        heading: 'Instructions',
        img: require('./img/Instruction.png'),
        paragraph:
          'Sending money abroad could cost you a lot, especially if you aren’t aware of the hidden fees. Money transfer companies and banks earn money by not only charging you a transfer fee, but also usually by offering you an exchange rate with a hidden markup. You could save a lot of money by comparing exchange rates and transfer fees in real-time. With our money transfer comparison tool, you’ll find the best way to send money internationally in just a few clicks.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat, consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse.',
        paragraph1:
          'Simply complete how much you want to send and currency to be received. We compare and review over all money transfer companies in your region and provide you with comapriosn of the least cost, fastest and most convinientDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat. consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat consequat.',
      },
      limit: 5,
      sort: 1,
      name: '',
      empty: true,
      chooseLocation: false,
      startedPageCount:0
    };
    comparison_instance.header = this;
  }
  handleDropdown = (name, e) => {
    try {
      const { prevParaIndex } = this.state
      if (prevParaIndex !== undefined && prevParaIndex !== "" && prevParaIndex !== null) {
        let prevPara = document.querySelector(`#pagination-li-${prevParaIndex} p`)
        if (prevPara?.classList?.contains("page-active")) {
          prevPara.classList.remove("page-active");
        }
      }
      this.setState({
        [name]: e.target.value,
        startedPageCount: 0,
      })
    } catch (e) {
      // console.log(e)
    }
  };
  btnClick(data) {
    // console.log('button', data);
    this.props.addToTable(data, this.props.history);
  }

  addFavourite(id) {
    if (localStorage.getItem('token')) {
      let user_id = JSON.parse(localStorage.getItem('UserData'))._id;
      // console.log(
      //   'user_id',
      //   user_id,
      //   JSON.parse(localStorage.getItem('token'))
      // );

      this.props.addFavourite(
        user_id,
        id,
        JSON.parse(localStorage.getItem('token'))
      );
    } else {
      window.location = '/registration';
    }
  }

  componentWillMount() {
    if (localStorage.getItem('UserData')) {
      let id = JSON.parse(localStorage.getItem('UserData'))._id;
      // console.log('iiiid', id);
      this.props.getFavourite(id);
    }

    this.props.currency();
    this.props.content();
  }

  componentWillUnmount() {
    // console.log('componentWillUnmount', this.props.history);
    if (this.props.history.location.pathname != '/companyoverview') {
      // console.log('Unmount', this.state.empty);
      this.props.empty();
    }
  }

  componentDidMount() {
    if (window.location.pathname == '/') {
      //  console.log("ASsads")
      document.getElementById('routerNav').style.display = 'none';
      //  document.getElementById('navigationIndex').style.display="block"
    } else {
      //  console.log("nhi hy")
      document.getElementById('routerNav').style.display = 'block';
      //  document.getElementById('navigationIndex').style.display="none"
    }
  }

  sort = (key) => {
    try {
    let list = this.state.data;
    this.state.isOldestFirst = !this.state.isOldestFirst;
    if(list){
        switch (key) {
          case 'RATE': {
            let records = this.state.isOldestFirst
              ? list.sort((a, b) => a.rate - b.rate)
              : list.sort((a, b) => b.rate - a.rate);
            this.setState({
              data: records,
              sortOrder: key,
            });
            // console.log('RATES DATA componentWillReceiveProps SORT RATE', records);
          }
          case 'AMOUNT RECEIVABLE': {
            let records = this.state.isOldestFirst
              ? list.sort((a, b) => a.amount_recievable - b.amount_recievable)
              : list.sort((a, b) => b.amount_recievable - a.amount_recievable);
            this.setState({
              data: records,
              sortOrder: key,
            });
            // console.log('RATES DATA componentWillReceiveProps SORT RATE', records);
          }
          case 'FEE': {
            let records = this.state.isOldestFirst
              ? list.sort((a, b) => a.payment_fees - b.payment_fees)
              : list.sort((a, b) => b.payment_fees - a.payment_fees);
            this.setState({
              data: records,
              sortOrder: key,
            });
            // console.log('RATES DATA componentWillReceiveProps SORT RATE', records);
          }
          case 'TAX': {
            let records = this.state.isOldestFirst
              ? list.sort((a, b) => a.tax_deducted - b.tax_deducted)
              : list.sort((a, b) => b.tax_deducted - a.tax_deducted);
            this.setState({
              data: records,
              sortOrder: key,
            });
            // console.log('RATES DATA componentWillReceiveProps SORT RATE', records);
          }
    }
  }
  }
  catch (e){
    // console.log(e)
  }
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      data: nextProps.data,
    });

    // console.log('RATES DATA componentWillReceiveProps', this.props.data);
  }

  toggleChooseLocation = (location) => {
    // console.log('LOCATION WE PICKED2', location[0].location.coordinates[1]);
    // const locationLink = `https://maps.google.com/maps?api=1&q=${location.location.coordinates[0].$numberDecimal},${location.location.coordinates[1].$numberDecimal}&hl=es&z=14&amp;output=embed`;
    const locationLink = `https://maps.google.com/maps?q=${location[0].location.coordinates[1]},${location[0].location.coordinates[0]}&hl=en&z=14&output=embed`;
    // console.log('LOCATION WE PICKED3', locationLink);

    this.setState({
      chooseLocation: !this.state.chooseLocation,
      location: location,
      locationLink: locationLink,
    });
  };
onLoadMoreHandler = (startPoint, event, index) => {
  // console.log("startPoint, event, index", startPoint, event, index)
    try {
      const { prevParaIndex } = this.state
      let currentPara = event.target;

      if (prevParaIndex !== undefined && prevParaIndex !== "" && prevParaIndex !== null) {
        let prevPara = document.querySelector(`#pagination-li-${prevParaIndex} p`)
        if (prevPara?.classList?.contains("page-active")) {
          prevPara.classList.remove("page-active");
        }
      }

      currentPara.className += " page-active"
      this.setState({
        startedPageCount: startPoint,
        prevParaIndex: index
      })
    } catch (e) {
      // console.log(e)
    }
    // setTimeout(() => {
    //   this.setState({
    //     loadCount: this.state.loadCount + 6,
    //     isLoadMoreLoading: false
    //   })
    // }, 2000);

  }
  pagination = (data) => {
    // console.log("DataPagination", data)
    try {
      const { limit } = this.state
      let paginationCount = Math.ceil(data.length / limit);
      var rows = [];

      for (let i = 0; i < paginationCount; i++) {
        rows.push(<a href="#comparision-list" className="anchor-decotation"><li id={`pagination-li-${i}`} onClick={(e) => this.onLoadMoreHandler(i * limit, e, i)} style={{ cursor: "pointer" }} className="page-item"><p className="page-link">{i + 1}</p></li></a>);
      }
      return <ul id="paginationUl" className="pagination justify-content-end" >
        {rows}
      </ul>
    } catch (e) {
      // console.log(e)
    }
  }
  render() {
    // console.log('thisISER', this.props.data, this.props.tableData);
    let fav_mark = [];
    this.props.favData.map((f) => {
      fav_mark = [...fav_mark, f.fav_id];
    });

    let table_mark = [];
    this.props.tableData.map((f) => {
      table_mark = [...table_mark, f._id];
    });
    function cs(data) {
      let currency_sign = '';
      if (data == 'USD' || data == 'AUD' || data == 'CAD') {
        currency_sign = '$';
      } else if (data == 'EGP') {
        currency_sign = 'E';
      } else if (data == 'EUR') {
        currency_sign = '€';
      } else if (data == 'GBP') {
        currency_sign = '£';
      } else if (data == 'INR') {
        currency_sign = '₹';
      } else if (data == 'JOD') {
        currency_sign = 'د.ا';
      } else if (data == 'JOD') {
        currency_sign = 'د.ا';
      } else if (data == 'LKR') {
        currency_sign = 'රු';
      } else if (data == 'NPR') {
        currency_sign = 'रू';
      } else if (data == 'PHP') {
        currency_sign = '₱';
      } else if (data == 'PKR') {
        currency_sign = '₨';
      }
      return currency_sign;
    }

    // console.log('coooom', this.props);
    let cData = this.props.currencyData;
    const chooseLocation = this.state.chooseLocation;
    return (
      <div className='coooom'>
        {/* {this.props.tableData &&  this.props.tableData.length > 0 &&    */}
        <Fade
          bottom
          when={this.props.tableData && this.props.tableData.length > 0}
        >
          <div className='bottom-div'>
            <div className='d-flex justify-content-start'>
              {this.props.tableData &&
                this.props.tableData.map((partner, index) => {
                  return (
                    <div key={index} className='tab'>
                      <p className='d-inline-block'>
                        {partner && partner.partner_name.toUpperCase()}
                      </p>
                      <a>
                        <i
                          style={{ cursor: 'pointer' }}
                          onClick={() => this.props.delTableIndex(index)}
                          className='fa fa-times ml-3'
                        ></i>
                      </a>
                    </div>
                  );
                })}
              {/* */}
              {/* <div className="tab">
                      <p className="d-inline-block">Lorem isum dolor sit</p>
                      <i className="fa fa-times ml-3"></i>
                    </div>
                    <div className="tab">
                      <p className="d-inline-block">Lorem isum dolor sit</p>
                      <i className="fa fa-times ml-3"></i>
                    </div> */}
            </div>
            <div>
              <a
                onClick={() => this.props.history.push('/companyoverview')}
                href='#'
                type='button'
              >
                Go to Comparision
              </a>
            </div>
          </div>
        </Fade>
        {/* } */}
        {chooseLocation ? (
          <div className='location-viewer-wrapper'>
            <div className='location-viewer'>
              <div className='d-flex justify-content-between'>
                <h5 className='instruct'>
                  {this.state.location && this.state.location.address}
                </h5>
                <i
                  className='fa fa-times'
                  onClick={() => this.toggleChooseLocation(this.state.location)}
                ></i>
              </div>
              <div class='mapouter'>
                <div class='gmap_canvas'>
                  {this.state.locationLink !== '' && (
                    <iframe
                      width='760'
                      height='480'
                      id='gmap_canvas'
                      src={this.state.locationLink} //"https://maps.google.com/maps?q=jedda&t=&z=13&ie=UTF8&iwloc=&output=embed"
                      frameborder='0'
                      scrolling='no'
                      marginheight='0'
                      marginwidth='0'
                    ></iframe>
                  )}
                </div>
              </div>
            </div>
          </div>
        ) : (
          ''
        )}
        <Zoom ssrFadeout Left>
          <div className='comparision-main-div'>
            <div className='comparision'>
              <div className='main-container'>
                <div className='container3'>
                  {/* <RedForm
                    isLoading={
                      this.props.isLoading ? this.props.isLoading : false
                    }
                    getData={(abc) => this.props.getData(abc)}
                  />{' '} */}

                  <ComparisionTableForm
                    isLoading={
                      this.props.isLoading ? this.props.isLoading : false
                    }
                    getData={(abc) => this.props.getData(abc)}
                  />{' '}
                </div>
                <div id="comparision-list" className='container3'>
                  <div className=' row comparision-form-row2'>
                    
                    {/* <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12'>
                      <div className='input-search-div'>
                        <input
                          type='text'
                          className='input-search'
                          placeholder='Search...'
                          value={this.state.name}
                          onChange={(e) => {
                            this.setState({ name: e.target.value });
                          }}
                        />
                        <div className='search-icon-div'></div>
                      </div>
                    </div> */}
                    {/* <div className="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                                    <form>
                                      <select
                                        name="sort"
                                        className="dropdownsC"
                                        onChange={e => {
                                          this.handleDropdown("sort", e);
                                        }}
                                      >
                                        <option id="opt" value="Sort by Title">
                                          Sort by Title
                                        </option>
                                        <option id="opt" value="title">
                                          title
                                        </option>
                                      </select>
                                    </form>
                                  </div> */}
                    <div className='col-xl-2 col-lg-3 col-md-4 col-sm-5 col-5 ml-auto'>
                      <form>
                        <select
                          name='result-dropdown'
                          placeholder="Result per Page"
                          className='dropdownsC cal-dropdown1 center-label1 bg-white dropdownInput'
                          value={this.state.limit}
                          // defaultValue={perPageCount}
                          onChange={(e) => {
                            this.handleDropdown('limit', e);
                          }}
                        >
                          {/* <option value={5}>Result per Page</option> */}
                          <option className='opt' value={5}>
                            5
                          </option>

                          <option className='opt' value={10}>
                            10
                          </option>
                          <option className='opt' value={15}>
                            15
                          </option>
                          {/* <option className='opt' value='70'>
                            70
                          </option>
                          <option className='opt' value='100'>
                            100
                          </option> */}
                        </select>
                      </form>
                    </div>
                  </div>
                </div>
                <div
                  className='container3 table-container'
                  style={{ backgroundColor: '#F0F0F0' }}
                >
                  <div className='row comparision-form-row3 comparision-table'>
                    <table>
                      <thead>
                        <tr>
                          <th style={{color:"#212529"}}> PROVIDER</th>
                          <th>
                            <div
                              onClick={() => this.sort('RATE')}
                              className='d-flex'
                            >
                              <span
                                style={{
                                  color:
                                    this.state.sortOrder === 'RATE'
                                      ? '#f40019'
                                      : '#212529',
                                }}
                              >
                                RATE
                              </span>
                              <span className='sort-icons'>
                                {!this.state.isOldestFirst ? (
                                  <i className='fa fa-sort-up '></i>
                                ) : (
                                  <i className='fa fa-sort-down '></i>
                                )}
                              </span>
                            </div>
                          </th>
                          <th>
                            <div
                              onClick={() => this.sort('AMOUNT RECEIVABLE')}
                              className='d-flex'
                            >
                              <span
                                style={{
                                  color:
                                    this.state.sortOrder === 'AMOUNT RECEIVABLE'
                                      ? '#f40019'
                                      : '#212529',
                                }}
                              >
                                {' '}
                                AMOUNT RECEIVABLE{' '}
                              </span>
                              <span className='sort-icons'>
                                {!this.state.isOldestFirst ? (
                                  <i className='fa fa-sort-up '></i>
                                ) : (
                                  <i className='fa fa-sort-down '></i>
                                )}
                              </span>
                            </div>
                          </th>
                          <th>
                            <div
                              onClick={() => this.sort('FEE')}
                              className='d-flex'
                            >
                              <span
                                style={{
                                  color:
                                    this.state.sortOrder === 'FEE'
                                      ? '#f40019'
                                      : '#212529',
                                }}
                              >
                                {' '}
                                SERVICE FEE{' '}
                              </span>
                              <span className='sort-icons'>
                                {!this.state.isOldestFirst ? (
                                  <i className='fa fa-sort-up '></i>
                                ) : (
                                  <i className='fa fa-sort-down '></i>
                                )}
                              </span>
                            </div>
                          </th>
                          <th style={{color:"#212529"}}>TRANSFER TIME </th>
                          <th>
                            <div
                              onClick={() => this.sort('TAX')}
                              className='d-flex'
                            >
                              <span
                                style={{
                                  color:
                                    this.state.sortOrder === 'TAX'
                                      ? '#f40019'
                                      : '#212529',
                                }}
                              >
                                {' '}
                                RECEIVING FEE{' '}
                              </span>
                              <span className='sort-icons'>
                                {!this.state.isOldestFirst ? (
                                  <i className='fa fa-sort-up '></i>
                                ) : (
                                  <i className='fa fa-sort-down '></i>
                                )}
                              </span>
                            </div>
                          </th>
                          {/* <th> WEBSITE </th> */}
                          <th style={{color:"#212529"}}> COMPARE TABLE </th>
                          <th style={{color:"#212529"}}> LOCATION </th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.data &&
                        !this.state.isLoading &&
                        this.state.data != 'No Record Found' ? (
                          this.state.data.map((value, ind) => {
                            const { startedPageCount, limit } = this.state
                            let endedPoiint = +startedPageCount + +limit;
                            if (ind >= startedPageCount && ind < endedPoiint) {
                            // console.log('nice', value);
                            return (
                              <tr>
                                <td style={{padding: "0 20px", textTransform: "capitalize"}} >
                                  <img
                                    width="110"
                                    height='50'
                                    src={
                                      value &&
                                      value.partner_details &&
                                      value.partner_details[0].img
                                        ? value.partner_details[0].img
                                        : 'no'
                                    }
                                  />{' '}
                                  <br/>
                                   
                                    {value &&
                                    value.partner_name 
                                        ? `${value.partner_name} `
                                      : null}
                                      <br />
                                      <p
                                      className='Lato_Regular comparision-website-text'
                                      onClick={() => {
                                        window.open(
                                          value.partner_details[0].website_url
                                        );
                                      }}
                                  >
                                    Visit Website{' '}
                                  </p>{' '}
                                  
                                  {/* <p className='logo-in-small-size'>
                                    {value &&
                                    value.reviews &&
                                    value.reviews.length
                                      ? value.reviews.length == 1
                                        ? `${value.reviews.length} REVIEW `
                                        : `${value.reviews.length} REVIEWS `
                                      : null}
                                  </p> */}
                                  {/* review{" "} */}
                                </td>
                                <td>
                                  {' '}
                                  {cs(value.country_name)}{' '}
                                  {value.rate ? value.rate.toFixed(2) : null}{' '}
                                </td>{' '}
                                <td>
                                  {' '}
                                  {value
                                    ? `${cs(value.country_name)} ` +
                                      value.amount_recievable.toFixed(2) +
                                      ' /-'
                                    : null}{' '}
                                </td>{' '}
                                <td> AED {value.payment_fees.toFixed(2)} </td>{' '}
                                <td > {value.transfer_time && value.transfer_time.length > 22 ? `${value.transfer_time.slice(0, 21)}...` : value.transfer_time }</td>{' '}
                                <td>
                                  {' '}
                                  {`AED ` + value.tax_deducted.toFixed(2)}{' '}
                                </td>{' '}
                                {/* <td>
                                  <button
                                    className='Lato_Regular'
                                    onClick={() => {
                                      window.open(
                                        value.partner_details[0].website_url
                                      );
                                    }}
                                  >
                                    VISIT{' '}
                                  </button>{' '}
                                </td>{' '} */}
                                <td>
                                  <div>
                                    {fav_mark.includes(
                                      value.partner_details[0]._id
                                    ) ? (
                                      <img
                                        className='icons'
                                        src={require('./img/heart_filled.png')}
                                      />
                                    ) : (
                                      <img
                                        className='icons'
                                        src={require('./img/Heart outline.png')}
                                      />
                                    )}
                                    {/* <img
                                      className="icons"
                                      src={require("./img/heart_filled.png")}
                                    /> */}
                                    <small
                                      onClick={() => {
                                        this.addFavourite(value.partner_id);
                                      }}
                                    >
                                      ADD TO FAVOURITE{' '}
                                    </small>{' '}
                                  </div>{' '}
                                  <div
                                    className='add-to-table-div'
                                    onClick={() => this.btnClick(value)}
                                  >
                                    {table_mark.includes(value._id) ? (
                                      <img
                                        className='icons'
                                        src={require('./img/checkbox filled.png')}
                                      />
                                    ) : (
                                      <img
                                        className='icons'
                                        src={require('./img/checkbox_empty.png')}
                                      />
                                    )}
                                     <small>
                                    ADD TO TABLE{' '}
                                    </small>{' '}
                                  </div>{' '}
                                  <div className='small2'>
                                    <p
                                      style={{color:"black"}}
                                      onClick={() => {
                                        this.props.history.push(
                                          `/partner/${value.partner_details[0]._id}`
                                        );
                                      }}
                                    >
                                      VIEW FULL DETAILS{' '}
                                    </p>{' '}
                                  </div>{' '}
                                </td>{' '}
                                <td>
                                  {value && value.location && (
                                    <i
                                      className='fa fa-map-marked-alt'
                                      onClick={() =>
                                        this.toggleChooseLocation(
                                          value.location
                                        )
                                      }
                                    ></i>
                                  )}
                                </td>
                              </tr>
                            );
                          }
                          })
                        ) : (
                          <>
                            <tr>
                              <td colSpan='8' className='pt-5'>
                                <h4 className='p-4'> No Amount Selected</h4>
                              </td>
                            </tr>

                            {/*                               
                          <center>
                            {" "}
                            {!this.props.isLoading
                              ? // <b> No Record Foundasdasd </b>
                                null
                              : null}{" "}
                          </center> */}
                          </>
                        )}
                        {/* {this.props.data && this.props.data.length == 0
                        ? "No Record Found"
                        : ""} */}
                        {/* <tr>
                                        <td className="first-td">
                                          <img src={require("../src/img/money.png")} />
                                          320 rev
                                        </td>
                                        <td>RS. 140</td>
                                        <td>Rs.14,000,000</td>
                                        <td>AED 0.00</td>
                                        <td>1 DAY</td>
                                        <td>AED 5</td>
                                        <td>
                                          <button className="Lato_Regular">VISIT</button>
                                        </td>
                                        <td>
                                          <div>
                                            <small>ADD TO FAVOURITE</small>
                                          </div>
                                          <div>
                                            <img />
                                            ADD TO TABLE
                                          </div>
                                          <div className="small2">
                                            <small>VIEW FULL DETAILSS</small>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td className="first-td">
                                          <img src={require("../src/img/money.png")} />
                                          320 rev
                                        </td>
                                        <td>RS. 140</td>
                                        <td>Rs.14,000,000</td>
                                        <td>AED 0.00</td>
                                        <td>1 DAY</td>
                                        <td>AED 5</td>
                                        <td>
                                          <button className="Lato_Regular">VISIT</button>
                                        </td>
                                        <td>
                                          <div>
                                            <small>ADD TO FAVOURITE</small>
                                          </div>
                                          <div>
                                            <img />
                                            ADD TO TABLE
                                          </div>
                                          <div className="small2">
                                            <small>VIEW FULL DETAILSS</small>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td className="first-td">
                                          <img src={require("../src/img/money.png")} />
                                          320 rev
                                        </td>
                                        <td>RS. 140</td>
                                        <td>Rs.14,000,000</td>
                                        <td>AED 0.00</td>
                                        <td>1 DAY</td>
                                        <td>AED 5</td>
                                        <td>
                                          <button className="Lato_Regular">VISIT</button>
                                        </td>
                                        <td>
                                          <div>
                                            <small>ADD TO FAVOURITE</small>
                                          </div>
                                          <div>
                                            <img />
                                            ADD TO TABLE
                                          </div>
                                          <div className="small2">
                                            <small>VIEW FULL DETAILSS</small>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td className="first-td">
                                          <img src={require("../src/img/money.png")} />
                                          320 rev
                                        </td>
                                        <td>RS. 140</td>
                                        <td>Rs.14,000,000</td>
                                        <td>AED 0.00</td>
                                        <td>1 DAY</td>
                                        <td>AED 5</td>
                                        <td>
                                          <button className="Lato_Regular">VISIT</button>
                                        </td>
                                        <td>
                                          <div>
                                            <small>ADD TO FAVOURITE</small>
                                          </div>
                                          <div>
                                            <img />
                                            ADD TO TABLE
                                          </div>
                                          <div className="small2">
                                            <small>VIEW FULL DETAILSS</small>
                                          </div>
                                        </td>
                                      </tr>*/}{' '}
                      </tbody>{' '}
                    </table>{' '}
                      <div style={{ width: "100%" }}>
                      {this.state.data &&
                        !this.state.isLoading &&
                        this.state.data != 'No Record Found' && 
                          this.props.data.length > 0 && this.pagination(this.props.data)}
                      </div>
                  </div>{' '}
                </div>
                {/* {this.state.arr.map((value, table) => {
                                return <Comparision_table key={table} data={value} />;
                              })} */}{' '}
                {this.props.isLoading ? (
                  <center>
                    <Dots color='red' size='100px' />
                  </center>
                ) : //  <Loader
                //     type="Oval"
                //     color="#00BFFF"
                //     height={150}
                //     width={1500}
                //     timeout={3000} //3 secs
                //   />
                // <center>
                //   <b>Loading ...</b>
                // </center>
                null}{' '}
              </div>{' '}
            </div>
            <Instructions
              data={this.state.Instructions}
              content={this.props.contentData}
            />
            {/* <ContactUsMap /> */}

            <div className='row Map ml-0 mr-0'>
              {/* <div className="locate col-xl-1 col-lg-1 col-md-12 col-sm-12 col-12">
                    <h1></h1>
                </div> */}
              {/* <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12'>
                <div className='mapouter'>
                  <div className='gmap_canvas'>
                    <iframe
                      id='gmap_canvas'
                      src='https://maps.google.com/maps?q=university%20of%20Karachi&t=&z=13&ie=UTF8&iwloc=&output=embed'
                      frameborder='0'
                      scrolling='no'
                      marginheight='0'
                      marginwidth='0'
                    ></iframe>
                  </div>
                  <img
                    src={require('./img/locate-icon.png')}
                    className='location'
                  />
                  <img
                    src={require('./img/locate-icon.png')}
                    className='location2'
                  />
                </div>
              </div> */}
            </div>

            {/* <Map  data={this.state.Map} />{" "} */}
          </div>
        </Zoom>{' '}
        <Footer history={this.props.history} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.Comparisons.isLoading,
    data: state.Comparisons.comparisons,
    currencyData: state.Comparisons.currency,
    contentData: state.Comparisons.content,
    favData: state.Favourite.allData,
    tableData: state.Comparisons.table,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: (obj) => dispatch(Comparisons.Comparisons(obj)),
  currency: () => dispatch(Comparisons.currency()),
  content: () => dispatch(content()),
  addToTable: bindActionCreators(Comparisons.table, dispatch),
  addFavourite: bindActionCreators(Comparisons.addFavourite, dispatch),
  getFavourite: bindActionCreators(Comparisons.getFavourite, dispatch),
  addToTable: bindActionCreators(Comparisons.table, dispatch),
  empty: bindActionCreators(empty, dispatch),
  delTableIndex: bindActionCreators(Comparisons.delTableIndex, dispatch),


});

export default connect(mapStateToProps, mapDispatchToProps)(Comparision);
