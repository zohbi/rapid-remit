import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from 'react-router-dom';
import './css/bootstrap.css';
import './css/style-header.css';
import './css/style-headerForRouter.css';
import Index from './components/index';
import Comparision from './comparision';
import ComparisionNewUI from './comparisionNewUI';
import LiveRates from './LiveRates';
import SpecialOffer from './Containers/offers';
import KnowledgeBase from './Containers/news';
import AboutUs from './AboutUs';
import ContactUs from './Containers/contactUs';
import Converter from './Containers/converter';
import CompanyOverview from './components/companyoverview';
import CountryCard from './components/countryCard';
import PrivacyPolicy from './components/privacyPolicy';
import BlogsReadMore from './components/blogsReadMore';
import Services from './components/services';
import Registration from './Containers/registration';
import DashboardUser from './pages/dashboardUser';
import DashboardLayout from './components/dashboardLayout';
import transferWise from './components/transferWise';
// <--------------- Redux ----------------->
import { connect } from 'react-redux';
import { Login } from './Store/actions';
// ------------------><----------------------
// <--------------- Encode Decode ----------------->
import CookiesPolicy from './components/cookiesPolicy';
import FAQs from './components/FAQs';
import TermsAndConditions from './components/termsAndConditions';
import Support from './pages/support';
import News from './components/news';
import Articles from './pages/articles';
import matchPath from './matchPath';
import DashboardLayout2 from './components/dashboardLayout2';
import Header from './components/header';
import Forget from './components/Forget';
import Profile from './components/profile';
import ChangePassword from './components/changepassword';
import ChangePasswordWithToken from './components/changepasswordwithtoken';

// ----------------------><--------------------------
// var url = require("url");
// function activeRoute(): string {
//     var routerUrl = window.location.hash.substr(1);
//     return url.parse(routerUrl).pathname;
// }
class Routes extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Guest',
    };
  }

  componentDidMount() {
    if (
      // window.location.pathname == '/AboutUs' ||
      window.location.pathname == '/'
    ) {
      document.getElementById('routerNav').style.display = 'none';
    } else {
      document.getElementById('routerNav').style.display = 'block';
    }
  }
  render() {
    // const isMovieWatchPathActive = !!matchPath(
    //   window.location.pathname,
    //   '/'
    // console.log("render", this.props);
    // );

    // console.log("isMovieWatchPathActive" , activeRoute());
    return (
      <Router>
        <div className='div1' id='navigationGray'>
          <Header />
          <Switch>
            <Route exact path='/' component={Index} />
            <Route path='/comparison' render={(props) => <ComparisionNewUI {...props} />} />
            <Route path='/ournetwork' component={LiveRates} />
            <Route path='/SpecialOffer' component={SpecialOffer} />
            <Route path='/companyoverview' component={CompanyOverview} />
            <Route path='/DashboardLayout' component={DashboardLayout} />
            <Route path='/DashboardLayout2' component={DashboardLayout2} />
            <Route exact path='/blogsReadMore' component={BlogsReadMore} />
            <Route path='/Services' component={Services} />
            <Route path='/registration' component={Registration} />
            <Route path='/changepassword' component={ChangePassword} />
            <Route path='/forgetpassword/:token' component={ChangePasswordWithToken} />
            <Route path='/Forget' component={Forget} />
            <Route path='/partner/:id' component={transferWise} />
            <Route path='/profile' component={Profile} />

            <Route exact path='/KnowledgeBase' component={KnowledgeBase} />
            <Route exact path='/AboutUs' component={AboutUs} />
            <Route exact path='/ContactUs' component={ContactUs} />
            <Route exact path='/DashboardUser' component={DashboardUser} />
            <Route exact path='/Converter' component={Converter} />

            <Route exact path='/PrivacyPolicy' component={PrivacyPolicy} />
            <Route exact path='/CookiesPolicy' component={CookiesPolicy} />
            <Route exact path='/FAQs' component={FAQs} />
            <Route
              exact
              path='/TermsAndConditions'
              component={TermsAndConditions}
            />
            <Route exact path='/Support' component={Support} />
            <Route exact path='/News' component={News} />
            <Route exact path='/Articles' component={Articles} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default Routes;
