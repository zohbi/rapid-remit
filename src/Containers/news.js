import { connect } from "react-redux";
import { News, Article, Blogs, SetIsNavActive } from "../Store/actions/index";
import KnowledgeBase from "../components/KnowledgeBase";

const mapStateToProps = state => {
  return {
    News: state.News.News,
    Article: state.Article.Article,
    Blog: state.Blogs.Blogs,
    isLoadingArticle: state.Article.isLoading,
    isLoadingBlogs: state.Blogs.isLoading,
    isLoading: state.News.isLoading
  };
};

const mapDispatchToProps = dispatch => ({
  getNews: obj => dispatch(News.News(obj)),
  getArticles: obj => dispatch(Article.Article(obj)),
  getBlogs: obj => dispatch(Blogs.Blogs(obj)),
  SetIsNavActive: (data) => dispatch(SetIsNavActive(data)),

//   convert: obj => dispatch(Convert.Convert(obj)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(KnowledgeBase);
