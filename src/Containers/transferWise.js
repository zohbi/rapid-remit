import React from "react";
import { connect } from "react-redux";
import {} from "../";
import { transferWise } from "../Store/actions";
class TransferWise extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    //this.props.getData(this.props.match.params.id);
    // console.log("trnasfer", this.props);

    // console.log("abcdefgh", this.props.match.params.id);
    this.props.transferWise(this.props.match.params.id);
    //console.log("abc", this.props.match.params.id);
  }

  render() {
    // console.log("trnasfer", this.props.data);

    return (
      <div className="row transfer_row">
        <div className="col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12">
          <h1 className="Lato_Black">Transfer Wise</h1>
          <i className="fas fa-star red"></i>
          <i className="fas fa-star red"></i>
          <i className="fas fa-star red"></i>
          <i className="fas fa-star gray"></i>
          <i className="fas fa-star gray"></i>
          <div className="review">
            <span>REVIEWS</span>
            <br />
            <span className="three-twenty">{}</span>
          </div>
          <div className="write_review">
            <a href="#">WRITE A REVIEW</a>
          </div>
        </div>
        <div className="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
          <div className="div10">
            <div className="divA">
              <label for="">FROM:&nbsp;&nbsp;</label>
              <input type="text" placeholder="UAE" />
              <label for="">&nbsp;TO:&nbsp;</label>
              <input type="text" placeholder="PKR" />
            </div>
            <div className="divB">
              <a href="#">
                <button className="buttonGoto">GOTO TRANSFER WISE</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ Comparisons: { data } }) => ({
  data
});

export default connect(mapStateToProps, { transferWise })(TransferWise);
