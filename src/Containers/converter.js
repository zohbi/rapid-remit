import { connect } from "react-redux";
import Converter from "../components/converter";
import { Convert, Liverate } from "../Store/actions/userAuth";
// import ComparisonsCom from "../comparision";
import { Comparisons } from "../Store/actions";
const mapStateToProps = (state) => {
  return {
    dataLiveRate: state.Liverate.Liverates,
    isLoading: state.Liverate.isLoading,
    convertIsLoading: state.Convert.isLoading,
    convertedRate: state.Convert.Convert,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: (obj) => dispatch(Liverate.Liverates(obj)),
  convert: (obj) => dispatch(Convert.Convert(obj)),
  compare: (obj, history, inform) =>
    dispatch(Comparisons.Comparisons(obj, history, inform)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Converter);
