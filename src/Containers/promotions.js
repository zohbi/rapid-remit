import { connect } from "react-redux";
import PromotionsUD from "../components/dashboardPromotionsUD";

import { Offers } from "../Store/actions/userAuth";

const mapStateToProps = (state) => {
  return {
    Offers: state.Offers.Offers,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getData: (obj) => dispatch(Offers.Offers(obj)),
});

// import { Promotions } from "../Store/actions/index";

// const mapStateToProps = state => {
//   return {
//     promotions: state.Promotions
//   };
// };

// const mapDispatchToProps = dispatch => ({
//   getData: obj => dispatch(Promotions.Promotions(obj)),
// //   convert: obj => dispatch(Convert.Convert(obj)),
// });

export default connect(mapStateToProps, mapDispatchToProps)(PromotionsUD);
