import { connect } from "react-redux";
import SpecialOffers from "../SpecialOffer";
import { Offers, SetIsNavActive } from "../Store/actions/index";

const mapStateToProps = state => {
  return {
    Offers: state.Offers.Offers,
    isLoading: state.Offers.isLoading,

  };
};

const mapDispatchToProps = dispatch => ({
  getData: obj => dispatch(Offers.Offers(obj)),
  SetIsNavActive: (data) => dispatch(SetIsNavActive(data)),


});

export default connect(mapStateToProps, mapDispatchToProps)(SpecialOffers);
