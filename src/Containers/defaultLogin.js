import { connect } from "react-redux";
import { Login } from "../Store/actions";
import Routes from "../router";


const mapStateToProps = state => {
  return {
    // allInventory: state.AllInventory.AllInventory
  };
};

const mapDispatchToProps = dispatch => ({
  login: obj => dispatch(Login.Login(obj)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Routes);
