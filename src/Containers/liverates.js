import { connect } from "react-redux";
import LiveRates from "../LiveRates";
import { Liverate } from "../Store/actions/index";

const mapStateToProps = state => {
  return {
    liverate: state.liverate
  };
};

const mapDispatchToProps = dispatch => ({
  getData: obj => dispatch(Liverate.Liverates(obj)),
//   convert: obj => dispatch(Convert.Convert(obj)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LiveRates);
