import { connect } from "react-redux";
import { Signup, Login } from "../Store/actions";
import Registration from "../components/registration";

const mapStateToProps = state => {
  return {
    // allInventory: state.AllInventory.AllInventory
    isLoadingLogin: state.Login.isLoading,
    isloadingRegister: state.Signup.isLoading,
    loginFailed: state.Login.error  
  };
};

const mapDispatchToProps = dispatch => ({
  postData: obj => dispatch(Signup.Signup(obj)),
  login: obj => dispatch(Login.Login(obj))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Registration);
