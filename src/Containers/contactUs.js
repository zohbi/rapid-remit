import { connect } from "react-redux";
import { ContactUs } from "../Store/actions";
import { getSettings } from "../Store/actions/userAuth";
import ContactUsCom from "../ContactUs";


const mapStateToProps = state => {
  return {
    isLoading: state.ContactUs.isLoading,
    setting: state.FAQS.setting
  };
};

const mapDispatchToProps = dispatch => ({
  postData: obj => dispatch(ContactUs.ContactUs(obj)),
  getSettings:obj => dispatch(getSettings(obj)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactUsCom);