import { connect } from "react-redux";
import { Comparisons } from "../Store/actions";
import ComparisonsCom from "../comparision";

const mapStateToProps = state => {
  return {
    isLoading: state.Comparisons.isLoading
  };
};

const mapDispatchToProps = dispatch => ({
  getData: obj => dispatch(Comparisons.Comparisons(obj))
});

export default connect(mapStateToProps, mapDispatchToProps)(ComparisonsCom);
