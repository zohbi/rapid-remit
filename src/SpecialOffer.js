import React, { Component } from "react";
import "./css/bootstrap.css";
import "./css/style-footer.css";
import "./css/style-contactUs+specialOffer+blogs.css";
import "./css/countdown.css";
import Footer from "./components/footer";
import Countdown from "./components/countdown2";
import Flip from "react-reveal/Flip";
import Fade from "react-reveal/Fade";
import { connect } from "react-redux";
import { Offers } from "./Store/actions/index";
import { bindActionCreators } from "redux";
import { Dots } from 'react-activity';

// import '../css/font.css';

export default class SpecialOffer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      arr: [
        {
          img: require("./img/specialOffer/ria.png"),
          title: "Refer and Earn",
          text:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
        },
        {
          img: require("./img/specialOffer/paypal.png"),
          title: "Get 15% discount on registration",
          text:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
        },
        {
          img: require("./img/specialOffer/Xe.png"),
          title: "Send AED Gift Cards Free",
          text:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
        },
        {
          img: require("./img/specialOffer/remitly.png"),
          title: "Get upto 500 AED Gift Vouchers!",
          text:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
        },
      ],
    };
  }

  componentWillReceiveProps(nextProps) {
    // console.log("nextProps", nextProps);
    let { Offers } = nextProps;
    this.setState({
      offerData: Offers,
    });
  }

  componentDidMount() {
    this.props.SetIsNavActive("/SpecialOffer")
    if (
      window.location.pathname == "/"
      // window.location.pathname == "/AboutUs"
    ) {
      //  console.log("ASsads")
      document.getElementById("routerNav").style.display = "none";
      //  document.getElementById('navigationIndex').style.display="block"
    } else {
      //  console.log("nhi hy")
      document.getElementById("routerNav").style.display = "block";
      //  document.getElementById('navigationIndex').style.display="none"
    }
    this.props.getData();
  }
  // componentWillMount() {
  //   this.props.offers();
  // }

  render() {
    // console.log("history", this.props.history);

    // console.log("this.props.getData()", this.state.offerData);
    let { offerData } = this.state;
    return (
      <div className="bg-white">
        <div className="container-fluid specialOffer">
          <div className="container1">
            <h1>Special Offer</h1>
            <img
              src={require("./img/specialOffer/specialOfferImg1.png")}
              alt=""
            />
            <div className="specialOfferRedLine"></div>
            <Flip right opposite cascade duration={3000} mirror>
              <h3>Special Offer</h3>
            </Flip>
            {/* <Fade right>
              {this.state.arr.map((value, countdown) => {
                return <Countdown key={countdown} data={value} />;
              })}
            </Fade> */}
            {this.props.isLoading ? <div style={{ width: "100%" }}><center>
              <Dots color='red' size='100px' />
            </center></div> :
              <Fade right>
                {this.state.offerData && Array.isArray(this.state.offerData) && this.state.offerData.length > 0 ? (
                  <React.Fragment>
                    {this.state.offerData && !this.props.isLoading && this.state.offerData.length > 0
                      ? this.state.offerData.map((value, countdown) => {
                        return <Countdown key={countdown} data={value} />;
                      })
                      : ""}
                  </React.Fragment>
                ) : (
                    <div className="p-5" style={{ backgroundColor: "#eee" }}>
                      <h1
                        className="first-heading Lato_Black text-center p-5 "
                        style={{ color: "#ccc" }}
                      >
                        No Offers Found!
                  </h1>
                    </div>
                  )}
              </Fade>}
          </div>
        </div>
        {/* <Fade left>
          <div className="container1">
            <div className=" row special_sec3_row1 col-md-12">
              <div className="special_sec3_row1_col1 col-md-6 paddingOn768">
                <div className="special_sec3_row1_col1_img">
                  <img
                    src={require("./img/specialOffer/special-offer.png")}
                    alt=""
                  />
                </div>
              </div>

              <div className="special_sec3_row1_col2 col-md-6">
                <div className="special_sec3_row1_col2_row1 Lato_Black">
                  <h4>Terms & Conditions</h4>
                </div>
                <div className="special_sec3_row1_col2_row2">
                  <ol>
                    <li style={{ marginTop: "25px" }}>
                      As place holder put in standard text from any remittance
                      comparison remittance company e.g.
                      https://www.compareremit.com/terms-and-conditions/
                    </li>
                    <li style={{ marginTop: "20px" }}>
                      As place holder put in standard text from any remittance
                      comparison remittance company e.g.
                      https://www.compareremit.com/terms-and-conditions/
                    </li>
                    <li style={{ marginTop: "20px" }}>
                      As place holder put in standard text from any remittance
                      comparison remittance company e.g.
                      https://www.compareremit.com/terms-and-conditions/
                    </li>
                    <li style={{ marginTop: "20px" }}>
                      As place holder put in standard text from any remittance
                      comparison remittance company e.g.
                      https://www.compareremit.com/terms-and-conditions/
                    </li>
                    <li style={{ marginTop: "20px" }}>
                      As place holder put in standard text from any remittance
                      comparison remittance company e.g.
                      https://www.compareremit.com/terms-and-conditions/
                    </li>
                  </ol>

                  
                </div>
              
              </div>
            </div>
          </div>
        </Fade> */}
        <Footer history={this.props.history} />
      </div>
    );
  }
}

// const mapStateToProps = state => {
//   return {
//     rates: state.Comparisons.rates
//   };
// };

// const mapDispatchToProps = dispatch => ({
//   offers: () => bindActionCreators(Offers.offers(), dispatch)
// });

// export default connect(mapStateToProps, mapDispatchToProps)(SpecialOffer);
